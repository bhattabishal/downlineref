-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2015 at 05:23 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `downlineref`
--

-- --------------------------------------------------------

--
-- Table structure for table `adv_click_info`
--
-- in use(#29 - File '.\downlineref\adv_click_info.MYD' not found (Errcode: 2 - No such file or directory))
-- Error reading data: (#29 - File '.\downlineref\adv_click_info.MYD' not found (Errcode: 2 - No such file or directory))

-- --------------------------------------------------------

--
-- Table structure for table `capcha_check`
--

CREATE TABLE IF NOT EXISTS `capcha_check` (
  `id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `code_value`
--

CREATE TABLE IF NOT EXISTS `code_value` (
  `code_value` varchar(200) NOT NULL,
  `code_label` varchar(200) NOT NULL,
  `code_type` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `code_value`
--

INSERT INTO `code_value` (`code_value`, `code_label`, `code_type`) VALUES
('1', 'program signup', 'earned'),
('2', 'clicking ads', 'earned'),
('3', 'referring new members', 'earned'),
('4', 'purchaged credits', 'earned'),
('1', 'referral requests', 'spend'),
('2', 'advertisment packages', 'spend'),
('3', 'jackpot game tickets', 'spend'),
('test@lionite.com', 'paypal_email', 'paypal_email'),
('bishalbhatta123@gmail.com', 'alertpay_email', 'alertpay_email'),
('6', 'premium_sixmonth', 'premium_sixmonth'),
('8', 'premium_oneyear', 'premium_oneyear'),
('10', 'premium_referral_request_discount', 'premium_referral_request_discount'),
('15', 'premium_credit_purchase_bonus', 'premium_credit_purchase_bonus'),
('5', 'referral_earning_level', 'referral_earning_level'),
('30', 'referral_request_minimum_credits', 'referral_request_minimum_credits'),
('http://localhost/downlineref/', 'website_url', 'website_url'),
('xMHOm6GJCq4BpWOI', 'alertpay_ipn_code', 'alertpay_ipn_code'),
('10', 'referral_earning_percentage', 'referral_earning_percentage'),
('4', 'advertise_first_spot_duration', 'advertise_first_spot_duration'),
('12', 'advertise_first_spot_price', 'advertise_first_spot_price'),
('4', 'advertise_second_spot_duration', 'advertise_second_spot_duration'),
('12', 'advertise_second_spot_price', 'advertise_second_spot_price'),
('4', 'advertise_third_spot_duration', 'advertise_third_spot_duration'),
('12', 'advertise_third_spot_price', 'advertise_third_spot_price'),
('3.95', 'premium_sixmonth_discount', 'premium_sixmonth_discount'),
('4.95', 'premium_oneyear_discount', 'premium_oneyear_discount'),
('5', 'standard_user_promotable_programs', 'standard_user_promotable_programs'),
('0.7', 'min_credits_per_adv_click', 'min_credits_per_adv_click'),
('100', 'min_adv_package_size', 'min_adv_package_size');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `code`) VALUES
(1, 'Afghanistan', 'af'),
(2, 'Albania', 'al'),
(3, 'Algeria', ''),
(4, 'American Samoa', 'as'),
(5, 'Andorra', 'ad'),
(6, 'Angola', 'ao'),
(7, 'Anguilla', 'ai'),
(8, 'Antarctica', 'aq'),
(9, 'Antigua And Barbuda', 'ag'),
(10, 'Argentina', 'ar'),
(11, 'Armenia', 'am'),
(12, 'Aruba', 'aw'),
(13, 'Australia', 'au'),
(14, 'Austria', 'at'),
(15, 'Azerbaijan', 'az'),
(16, 'Bahamas', 'bs'),
(17, 'Bahrain', 'bh'),
(18, 'Bangladesh', 'bd'),
(19, 'Barbados', 'bb'),
(20, 'Belarus', 'by'),
(21, 'Belgium', 'be'),
(22, 'Belize', 'bz'),
(23, 'Benin', 'bj'),
(24, 'Bermuda', 'bm'),
(25, 'Bhutan', 'bt'),
(26, 'Bolivia', 'bo'),
(27, 'Bosnia and Herzegovina', 'ba'),
(28, 'Botswana', 'bw'),
(29, 'Bouvet Island', 'bv'),
(30, 'Brazil', 'br'),
(31, 'British Indian Ocean Territory', ''),
(32, 'Brunei Darussalam', 'bn'),
(33, 'Bulgaria', 'bg'),
(34, 'Burkina Faso', ''),
(35, 'Burundi', ''),
(36, 'Cambodia', ''),
(37, 'Cameroon', ''),
(38, 'Canada', ''),
(39, 'Cape Verde', ''),
(40, 'Cayman Islands', ''),
(41, 'Central African Republic', ''),
(42, 'Chad', ''),
(43, 'Chile', ''),
(44, 'China', ''),
(45, 'Christmas Island', ''),
(46, 'Cocos (Keeling) Islands', ''),
(47, 'Colombia', ''),
(48, 'Comoros', ''),
(49, 'Congo', ''),
(50, 'Congo, Democractic Republic of the', ''),
(51, 'Cook Islands', ''),
(52, 'Costa Rica', ''),
(53, 'Cote D''Ivoire (Ivory Coast)', ''),
(54, 'Croatia (Hrvatska)', ''),
(55, 'Cuba', ''),
(56, 'Cyprus', ''),
(57, 'Czech Republic', ''),
(58, 'Denmark', ''),
(59, 'Djibouti', ''),
(60, 'Dominica', ''),
(61, 'Dominican Republic', ''),
(62, 'East Timor', ''),
(63, 'Ecuador', ''),
(64, 'Egypt', ''),
(65, 'El Salvador', ''),
(66, 'Equatorial Guinea', ''),
(67, 'Eritrea', ''),
(68, 'Estonia', ''),
(69, 'Ethiopia', ''),
(70, 'Falkland Islands (Islas Malvinas)', ''),
(71, 'Faroe Islands', ''),
(72, 'Fiji Islands', ''),
(73, 'Finland', ''),
(74, 'France', ''),
(75, 'French Guiana', ''),
(76, 'French Polynesia', ''),
(77, 'French Southern Territories', ''),
(78, 'Gabon', ''),
(79, 'Gambia, The', ''),
(80, 'Georgia', ''),
(81, 'Germany', ''),
(82, 'Ghana', ''),
(83, 'Gibraltar', ''),
(84, 'Greece', ''),
(85, 'Greenland', ''),
(86, 'Grenada', ''),
(87, 'Guadeloupe', ''),
(88, 'Guam', ''),
(89, 'Guatemala', ''),
(90, 'Guinea', ''),
(91, 'Guinea-Bissau', ''),
(92, 'Guyana', ''),
(93, 'Haiti', ''),
(94, 'Heard and McDonald Islands', ''),
(95, 'Honduras', ''),
(96, 'Hong Kong S.A.R.', ''),
(97, 'Hungary', ''),
(98, 'Iceland', ''),
(99, 'India', ''),
(100, 'Indonesia', ''),
(101, 'Iran', ''),
(102, 'Iraq', ''),
(103, 'Ireland', ''),
(104, 'Israel', ''),
(105, 'Italy', ''),
(106, 'Jamaica', ''),
(107, 'Japan', ''),
(108, 'Jordan', ''),
(109, 'Kazakhstan', ''),
(110, 'Kenya', ''),
(111, 'Kiribati', ''),
(112, 'Korea', ''),
(113, 'Korea, North', ''),
(114, 'Kosovo', ''),
(115, 'Kuwait', ''),
(116, 'Kyrgyzstan', ''),
(117, 'Laos', ''),
(118, 'Latvia', ''),
(119, 'Lebanon', ''),
(120, 'Lesotho', ''),
(121, 'Liberia', ''),
(122, 'Libya', ''),
(123, 'Liechtenstein', ''),
(124, 'Lithuania', ''),
(125, 'Luxembourg', ''),
(126, 'Macau S.A.R.', ''),
(127, 'Macedonia, Former Yugoslav Republic of', ''),
(128, 'Madagascar', ''),
(129, 'Malawi', ''),
(130, 'Malaysia', ''),
(131, 'Maldives', ''),
(132, 'Mali', ''),
(133, 'Malta', ''),
(134, 'Marshall Islands', ''),
(135, 'Martinique', ''),
(136, 'Mauritania', ''),
(137, 'Mauritius', ''),
(138, 'Mayotte', ''),
(139, 'Mexico', ''),
(140, 'Micronesia', ''),
(141, 'Moldova', ''),
(142, 'Monaco', ''),
(143, 'Mongolia', ''),
(144, 'Montenegro', ''),
(145, 'Montserrat', ''),
(146, 'Morocco', ''),
(147, 'Mozambique', ''),
(148, 'Myanmar', ''),
(149, 'Namibia', ''),
(150, 'Nauru', ''),
(151, 'Nepal', ''),
(152, 'Netherlands Antilles', ''),
(153, 'Netherlands, The', ''),
(154, 'New Caledonia', ''),
(155, 'New Zealand', ''),
(156, 'Nicaragua', ''),
(157, 'Niger', ''),
(158, 'Nigeria', ''),
(159, 'Niue', ''),
(160, 'Norfolk Island', ''),
(161, 'Northern Mariana Islands', ''),
(162, 'Norway', ''),
(163, 'Oman', ''),
(164, 'Pakistan', ''),
(165, 'Palau', ''),
(166, 'Palestine', ''),
(167, 'Panama', ''),
(168, 'Papua new Guinea', ''),
(169, 'Paraguay', ''),
(170, 'Peru', ''),
(171, 'Philippines', ''),
(172, 'Pitcairn Island', ''),
(173, 'Poland', ''),
(174, 'Portugal', ''),
(175, 'Puerto Rico', ''),
(176, 'Qatar', ''),
(177, 'Reunion', ''),
(178, 'Romania', ''),
(179, 'Russia', ''),
(180, 'Rwanda', ''),
(181, 'Saint Helena', ''),
(182, 'Saint Kitts And Nevis', ''),
(183, 'Saint Lucia', ''),
(184, 'Saint Pierre and Miquelon', ''),
(185, 'Saint Vincent And The Grenadines(', ''),
(186, 'Samoa', ''),
(187, 'San Marino', ''),
(188, 'Sao Tome and Principe', ''),
(189, 'Saudi Arabia', ''),
(190, 'Senegal', ''),
(191, 'Serbia', ''),
(192, 'Seychelles', ''),
(193, 'Sierra Leone', ''),
(194, 'Singapore', ''),
(195, 'Slovakia', ''),
(196, 'Slovenia', ''),
(197, 'Solomon Islands', ''),
(198, 'Somalia', ''),
(199, 'South Africa', ''),
(200, 'South Georgia And The South Sandwich Islands', ''),
(201, 'Spain', ''),
(202, 'Sri Lanka', ''),
(203, 'Sudan', ''),
(204, 'Suriname', ''),
(205, 'Svalbard And Jan Mayen Islands', ''),
(206, 'Swaziland', ''),
(207, 'Sweden', ''),
(208, 'Switzerland', ''),
(209, 'Syria', ''),
(210, 'Taiwan', ''),
(211, 'Tajikistan', ''),
(212, 'Tanzania', ''),
(213, 'Thailand', ''),
(214, 'Togo', ''),
(215, 'Tokelau', ''),
(216, 'Tonga', ''),
(217, 'Trinidad And Tobago', ''),
(218, 'Tunisia', ''),
(219, 'Turkey', ''),
(220, 'Turkmenistan', ''),
(221, 'Turks And Caicos Islands', ''),
(222, 'Tuvalu', ''),
(223, 'Uganda', ''),
(224, 'Ukraine', ''),
(225, 'United Arab Emirates', ''),
(226, 'United Kingdom', ''),
(227, 'United States', ''),
(228, 'United States Minor Outlying Islands', ''),
(229, 'Uruguay', ''),
(230, 'Uzbekistan', ''),
(231, 'Vanuatu', ''),
(232, 'Vatican City State (Holy See)', ''),
(233, 'Venezuela', ''),
(234, 'Vietnam', ''),
(235, 'Virgin Islands (British)', ''),
(236, 'Virgin Islands (US)', ''),
(237, 'Wallis And Futuna Islands', ''),
(238, 'Yemen', ''),
(239, 'Zambia', ''),
(240, 'Zimbabwe', '');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `message_id` bigint(15) NOT NULL,
  `message_from` int(11) NOT NULL,
  `message_to` int(11) NOT NULL,
  `message_subject` varchar(500) NOT NULL,
  `message_details` text NOT NULL,
  `created_date` datetime NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`message_id`, `message_from`, `message_to`, `message_subject`, `message_details`, `created_date`, `status`) VALUES
(24, 0, 17, 'Program Signup', 'Hello google,<br />A Member Has Signup to your program <b>neobux.com</b> on 2012-01-02 22:18:18 .Click on <a href="pendingRequest.php">pending/validate</a> link from Dashboard page and follow the instruction to validate the signup.You need to validate this sign-up within 5 days', '2012-01-02 22:18:18', '1'),
(23, 0, 19, 'Request Accepted', 'Hello ,<br />Your Request has been accepted for program <b>neobux.com</b> on 2012-01-02 22:14:21 .you will receive an equal share of credits for the duraction of the contract.Click on <a href="myProgramSignUp.php">my program signup</a> link from dashboard to view your credit transferred details', '2012-01-02 22:14:21', '1'),
(22, 0, 17, 'Program Signup', 'Hello google,<br />A Member Has Signup to your program <b>neobux.com</b> on 2012-01-02 22:05:34 .Click on <a href="pendingRequest.php">pending/validate</a> link from Dashboard page and follow the instruction to validate the signup.You need to validate this sign-up within 5 days', '2012-01-02 22:05:34', '1');

-- --------------------------------------------------------

--
-- Table structure for table `msg_sentbox`
--

CREATE TABLE IF NOT EXISTS `msg_sentbox` (
  `id` bigint(11) NOT NULL,
  `msg_id` int(11) NOT NULL,
  `sent_by` int(11) NOT NULL,
  `sent_to` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(6) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `buyer_firstname` varchar(200) NOT NULL,
  `buyer_lastname` varchar(200) NOT NULL,
  `buyer_email` varchar(200) NOT NULL,
  `payment_amount` decimal(7,2) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program_signup_child`
--

CREATE TABLE IF NOT EXISTS `program_signup_child` (
  `id` bigint(11) NOT NULL,
  `parent_user_id` int(11) NOT NULL,
  `child_user_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `program_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `update_count` int(11) NOT NULL,
  `status` enum('0','1','2','3','4') NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_signup_child`
--

INSERT INTO `program_signup_child` (`id`, `parent_user_id`, `child_user_id`, `username`, `email`, `program_id`, `created_by`, `created_date`, `updated_by`, `updated_date`, `update_count`, `status`) VALUES
(25, 17, 19, 'bishal@yahoo.com', 'bishal@yahoo.com', 55, 19, '2012-01-02 21:18:18', 0, '0000-00-00 00:00:00', 0, '0'),
(24, 17, 19, 'bishal@yahoo.com', 'bishal@yahoo.com', 54, 19, '2012-01-02 21:05:34', 0, '0000-00-00 00:00:00', 0, '4');

-- --------------------------------------------------------

--
-- Table structure for table `program_signup_validity`
--

CREATE TABLE IF NOT EXISTS `program_signup_validity` (
  `id` bigint(15) NOT NULL,
  `signed_user_id` int(11) NOT NULL,
  `program_signup_id` int(11) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_signup_validity`
--

INSERT INTO `program_signup_validity` (`id`, `signed_user_id`, `program_signup_id`, `valid_from`, `valid_to`) VALUES
(13, 19, 24, '2012-01-02 21:14:21', '2012-01-12 21:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advertisment`
--

CREATE TABLE IF NOT EXISTS `tbl_advertisment` (
  `id` bigint(15) NOT NULL,
  `title` varchar(500) NOT NULL,
  `short_desc` text NOT NULL,
  `credits` double NOT NULL,
  `website_url` varchar(350) NOT NULL,
  `package_size` varchar(50) NOT NULL,
  `sub_total` double NOT NULL,
  `span_fees` double NOT NULL,
  `total` double NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `update_count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_advertisment`
--

INSERT INTO `tbl_advertisment` (`id`, `title`, `short_desc`, `credits`, `website_url`, `package_size`, `sub_total`, `span_fees`, `total`, `status`, `created_by`, `created_date`, `updated_by`, `updated_date`, `update_count`) VALUES
(123, 'test', 'test', 500, 'http://www.google.com', '10', 0, 0, 0, '1', 0, '2011-07-16 00:00:00', 0, '2011-07-16 00:00:00', 1),
(124, 'Best PTC Is Growing UP', 'Just Sign UP And Try.', 50, 'http://www.onlinekhabar.com/', '20', 0, 0, 0, '1', 0, '2011-07-26 00:00:00', 0, '2011-07-26 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blogs`
--

CREATE TABLE IF NOT EXISTS `tbl_blogs` (
  `id` int(11) NOT NULL,
  `user_id` int(8) NOT NULL,
  `title_en` text COLLATE latin1_general_ci NOT NULL,
  `short_desc_en` longtext COLLATE latin1_general_ci NOT NULL,
  `content_en` longtext COLLATE latin1_general_ci NOT NULL,
  `title_np` text COLLATE latin1_general_ci NOT NULL,
  `short_desc_np` longtext COLLATE latin1_general_ci NOT NULL,
  `content_np` longtext COLLATE latin1_general_ci NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `video` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `publish` enum('yes','no') COLLATE latin1_general_ci NOT NULL,
  `entered_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `entered_date` date NOT NULL,
  `updated_by` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `updated_date` date NOT NULL,
  `update_count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_content`
--

CREATE TABLE IF NOT EXISTS `tbl_content` (
  `content_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `heading_np` varchar(500) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_metatag` varchar(255) DEFAULT NULL,
  `page_keywords` varchar(255) DEFAULT NULL,
  `page_description` varchar(255) DEFAULT NULL,
  `detail_desc_en` text NOT NULL,
  `brief_desc_en` text NOT NULL,
  `detail_desc_np` text NOT NULL,
  `brief_desc_np` text NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `publish` enum('yes','no') NOT NULL,
  `entered_by` varchar(50) NOT NULL,
  `entered_date` date NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_date` date NOT NULL,
  `update_count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_content`
--

INSERT INTO `tbl_content` (`content_id`, `heading`, `heading_np`, `page_title`, `page_metatag`, `page_keywords`, `page_description`, `detail_desc_en`, `brief_desc_en`, `detail_desc_np`, `brief_desc_np`, `menu_id`, `sub_menu_id`, `publish`, `entered_by`, `entered_date`, `updated_by`, `updated_date`, `update_count`) VALUES
(1, 'Home', '', '', '', '', '', '', '', '', '', 14, 0, 'yes', '', '0000-00-00', '', '2011-02-02', 2),
(2, 'Services', '', '', '', '', '', '', '', '', '', 16, 0, 'yes', '', '0000-00-00', '', '2011-02-02', 2),
(3, 'ABOUT US', '', '', '', '', '', '', '', '', '', 15, 0, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(4, 'Contact Us', '', '', '', '', '', '', '', '', '', 20, 0, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(5, 'Broadband', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 6, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(6, 'Banking', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 3, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(7, 'Business', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 5, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(8, 'Enterprise', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 7, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(9, 'System ', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 8, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(10, 'Policy', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 9, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(11, 'Project', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 10, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(12, 'Traning', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam  nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat  volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.  Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse  molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  eros et accumsan et iusto odio dignissim qui blandit praesent luptatum  zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber  tempor cum soluta nobis eleifend option congue nihil imperdiet doming id  quod mazim placerat facer possim assum. Typi non habent claritatem  insitam; est usus legentis in iis qui facit eorum claritatem.  Investigationes demonstraverunt lectores legere me lius quod ii legunt  saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc  putamus parum claram, anteposuerit litterarum formas humanitatis per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 11, 'yes', '', '2011-02-02', '', '2011-02-02', 1),
(13, 'CLIENT', '', '', '', '', '', '<p>In the short time that Magnus has been operational we have had the privilege to work with many clients. Below is a small list of our valued clients.</p>\r\n<h5><img width="64" height="69" align="left" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/logo_rmdc.gif" alt="RMDC" />Rural Microfinance Development Centre(RMDC)</h5>\r\n<p>Magnus is developing a MIS for RMDC with modules including Accounts, Loan, Training, Payroll and Reporting.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<h5><img width="221" height="44" align="right" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/ctzn.gif" alt="Citizens Bank" /><span style="display: none;">&nbsp;</span>Citizens Bank International</h5>\r\n<p>Magnus provided consulting services to Citizens Bank International for selection of their Financial System, including preparing of RFP, selection of bids, verification of functionality and final recommendations as per the banks needs.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<h5><img align="left" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/supreme.gif" alt="Supreme Court" /> Supreme Court of Nepal</h5>\r\n<p>Under the USAID funded project, Magnus did a System Assessment of Computer System in the Judiciary. This included reviewing the master plan, proposing changes and studying current infrastructure and software systems and provides recommendations to fully computerize the judiciary.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<h5>Office of the Attorney General of Nepal</h5>\r\n<p>Magnus prepared a master plan for the OAG for computerization and office automation.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<h5><img width="78" height="70" align="right" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/SFCLs.jpg" alt="Small Farmers Co-operatives" />Small Farmers Co-operatives</h5>\r\n<p>Magnus developed a MIS, Saral Bitta, for the use in Small Farmers Cooperatives. The system handles all of the financial process of SFCL and the user interface is in Nepali. 35 SFCLs all over Nepal are currently using this system, and is being deployed to many more.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<h5><img align="left" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/nta.jpg" alt="Nepal Telecommunication Authority" />Nepal Telecommunication Authority</h5>\r\n<p>Magnus provided consulting services for categorization power usage&nbsp;for public and private use of low power devices and&nbsp;development of Type Approval methodology.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<p><img width="150" height="45" align="right" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/image/Small.jpg" alt="USAID" /></p>\r\n<h5>United States Agency for International Development (USAID)</h5>\r\n<p>Under USAID ICT-GDA program, Magnus provided consulting services for setting up rural telecenters and training telecenter operators/managers at Syaphru Bensi in Rasuwa district and Phaplu and Sallery in&nbsp;Solukhumbu district.&nbsp;Multiple&nbsp;3-day trainings were conducted at Lalitpur for rests of the rural telecetner operators and managers. Scope of the project also included wireless survey, setting up Wi-Fi devices and test, developing&nbsp;training manual.</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<h5><img align="left" style="margin-right: 10px;" src="http://www.magnus.com.np/userfiles/image/ADB.jpg" alt="UNESCAP/ADB" />UNESCAP/ADB</h5>\r\n<p>Magnus is providing&nbsp;consulting services to United Nations Economic and Social Commission for Asia and the Pacific (UNESCAP) and Asian Development Bank (ADB) in establishing six Community e-Centers in Nepal. In parallel, such centers are being established in all South Asian Sub-regional Economic Cooperation (SASEC) nations (Bangladesh, Bhutan and&nbsp;India). Project scope includes conducting needs assessment workshops and&nbsp;feasibility study,&nbsp;development and prototyping of business model, website and training manual development, representing Nepal in SASEC CeC regional workshops and providing training.&nbsp;The company executes project action items&nbsp;through strategic partnership with Project Implementation Unit (PIU) at Ministry of Information an dCommunication (MoIC). MoIC is the executive government agency for the project.</p>', '', '', '', 21, 0, 'yes', '', '2011-02-03', '', '2011-02-03', 1),
(14, 'Careers', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 18, 0, 'yes', '', '2011-02-03', '', '2011-02-03', 1),
(15, 'Rular', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 10, 4, 'yes', '', '2011-02-03', '', '2011-02-03', 1),
(16, 'Refreshment', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 19, 0, 'yes', '', '2011-02-06', '', '2011-02-06', 1),
(17, 'Home', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 23, 0, 'yes', '', '2011-04-04', '', '2011-04-04', 1),
(18, 'Programs', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 24, 0, 'yes', '', '2011-04-04', '', '2011-04-04', 1),
(19, 'Terms of Service', '', '', '', '', '', '<div id="tos"><b>1. Definitions</b>\r\n<ol style="margin-top:0px;">\r\n    <li>By joining DownlineRefs.com you automatically agree to the terms  and conditions in this agreement. In these Terms of Service (TOS) the  user will be referred to as ''you'', whereas DownlineRefs.com will be  referred to as ''we''. A user that does not accept this Terms of Service  should stop using DownlineRefs.com and delete his/her account.</li>\r\n</ol>\r\n<b>2. Limitation of liabilities</b>\r\n<ol style="margin-top:0px;">\r\n    <li>We are not responsible for any losses and/or damages arising out  of the relationship between you and DownlineRefs.com. We are not  responsible for a user''s signup and/or activity in your program or a  promotor stopping the daily transfer of your credit payment.  Additionally, we cannot guarantee getting referrals for your programs.  Although new programs will be checked for illegal and/or offensive  content, we take not responsibility for the content displayed in any  third party page or banner listed on our website.</li>\r\n</ol>\r\n<b>3. Membership</b>\r\n<ol style="margin-top:0px;">\r\n    <li>You can create only one account per IP-address/household and you  must provide a valid email address. Your membership will automatically  terminate if you do not log in for 90 days or more. Upon termination,  your balance will be voided. Termination is irreversible and refunding  of your balance or non-expired memberships cannot be claimed.</li>\r\n</ol>\r\n<b>4. Purchases, Balances</b><br />\r\n<ol style="margin-top:0px;">\r\n    <li>Purchases, among which are Credit Packages and Premium  Memberships, are not refundable. Charge backs or reversed transactions  are not tolerated and will lead to termination of your account.</li>\r\n</ol>\r\n<b>5. Downline Builder</b><br />\r\n<ol style="margin-top:0px;">\r\n    <li>Requests pending for 16 weeks or longer will be automatically canceled and fully refunded.</li>\r\n    <li>When a user signs up to your program, you have to either accept or decline the signup within 5 days (=120 hours)     after the user has made a request for validation. Sign-ups are automatically accepted if you exceed this time frame.</li>\r\n    <li>Premium Members will receive 40% downline earnings over completed  signups made by their referred members to DownlineRefs.com.     Standard Members will receive 20% downline earnings over completed  signups made by their referred members to DownlineRefs.com.</li>\r\n    <li>Referral link exposures on the Click Advertisement page should be  free of framebreakers and inappropriate/abusive content. If your  advertisement does contain either of these, it will be removed without  notice and without refund.</li>\r\n</ol>\r\n<b>6. Violation of these Terms of Service</b><br />\r\n<ol style="margin-top:0px;">\r\n    <li>Although we do not intent to, DownlineRefs.com reserves the right  to block or terminate a user''s account. Valid reasons are, but not  limited to, violation of these Terms and Conditions, misuse of the  system, hacking, racism, spamming or behaviour that is commonly regarded  as unacceptable.</li>\r\n</ol>\r\n</div>', '', '', '', 25, 0, 'yes', '', '0000-00-00', '', '2011-04-18', 3),
(20, 'Privacy Policy', '', '', '', '', '', '<ul>\r\n    <li><b>Cookies</b><br />\r\n    We use cookies to collect some limited non-personal data and this  information on your own computer to be able to operate our service.  Information that will be stored in a cookie is: your username, your  password and your IP-address. This data is needed in order to facilitate  the automatic login feature. Most browsers are set up to automatically   accept cookies. If you wish, you can set up your browser to refuse  cookies or notify you when a cookie is being sent. However, the  automatic login feature will not work without cookies.<br />\r\n    </li>\r\n    <li><b>Database collection</b><br />\r\n    We routinely store a timestamp of your last login in our database, your IP-address and any incorrectly typed password.<br />\r\n    </li>\r\n    <li><b>Sharing</b><br />\r\n    Most effort has been done to secure our database from unauthorized  users. We will never intentionally share your personal detailes with  third parties. Your username, country of residence and date of  registration however will be accessible to both users and visitors of  DownlineRefs.com.</li>\r\n</ul>', '', '', '', 26, 0, 'yes', '', '0000-00-00', '', '2011-04-18', 3),
(21, 'Contact', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam    nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat    volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation    ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse    molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero    eros et accumsan et iusto odio dignissim qui blandit praesent luptatum    zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber    tempor cum soluta nobis eleifend option congue nihil imperdiet doming  id   quod mazim placerat facer possim assum. Typi non habent claritatem    insitam; est usus legentis in iis qui facit eorum claritatem.    Investigationes demonstraverunt lectores legere me lius quod ii legunt    saepius. Claritas est etiam processus dynamicus, qui sequitur  mutationem   consuetudium lectorum. Mirum est notare quam littera  gothica, quam  nunc  putamus parum claram, anteposuerit litterarum  formas humanitatis  per  seacula quarta decima et quinta decima. Eodem  modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in  futurum.</p>', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', 30, 0, 'yes', '', '0000-00-00', '', '2011-04-04', 3),
(22, 'Forum', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 29, 0, 'yes', '', '2011-04-04', '', '2011-04-04', 1),
(23, 'Advertisment', '', '', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam   nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat   volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation   ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse   molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero   eros et accumsan et iusto odio dignissim qui blandit praesent luptatum   zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber   tempor cum soluta nobis eleifend option congue nihil imperdiet doming id   quod mazim placerat facer possim assum. Typi non habent claritatem   insitam; est usus legentis in iis qui facit eorum claritatem.   Investigationes demonstraverunt lectores legere me lius quod ii legunt   saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem   consuetudium lectorum. Mirum est notare quam littera gothica, quam  nunc  putamus parum claram, anteposuerit litterarum formas humanitatis  per  seacula quarta decima et quinta decima. Eodem modo typi, qui nunc  nobis  videntur parum clari, fiant sollemnes in futurum.</p>', '', '', '', 28, 0, 'yes', '', '2011-04-04', '', '2011-04-04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_front_banner`
--

CREATE TABLE IF NOT EXISTS `tbl_front_banner` (
  `ban_id` bigint(15) NOT NULL,
  `purchased_by` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `txn_id` varchar(250) NOT NULL,
  `target_url` varchar(500) NOT NULL,
  `banner_url` varchar(500) NOT NULL,
  `purchaser_email` varchar(200) NOT NULL,
  `ban_spot` int(11) NOT NULL,
  `ban_weeks` int(11) NOT NULL,
  `ban_status` enum('0','1') NOT NULL COMMENT '0-inactive:1-active',
  `created_date` datetime NOT NULL,
  `valid_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_front_banner`
--

INSERT INTO `tbl_front_banner` (`ban_id`, `purchased_by`, `payment_type`, `txn_id`, `target_url`, `banner_url`, `purchaser_email`, `ban_spot`, `ban_weeks`, `ban_status`, `created_date`, `valid_date`) VALUES
(4, 17, 'alertpay', 'TEST TRANSACTION', 'http://www.clixsense.com', 'http://static.clixsense.com/banners/clixsense468x60b.png', 'rock@yahoo.com', 2, 4, '0', '2011-07-30 05:08:18', '2011-08-27 05:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_links`
--

CREATE TABLE IF NOT EXISTS `tbl_links` (
  `link_id` int(11) NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `link_url` text NOT NULL,
  `publish` enum('yes','no') NOT NULL,
  `entered_by` varchar(50) NOT NULL,
  `entered_date` date NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_date` date NOT NULL,
  `update_count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `name_np` varchar(255) NOT NULL,
  `menu_index` varchar(25) NOT NULL,
  `menu_type` enum('Home','Program','Adv','Help') DEFAULT NULL,
  `has_content` enum('yes','no') NOT NULL DEFAULT 'no',
  `publish` enum('yes','no') NOT NULL,
  `entered_by` varchar(50) NOT NULL,
  `entered_date` date NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_date` date NOT NULL,
  `update_count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `name_en`, `name_np`, `menu_index`, `menu_type`, `has_content`, `publish`, `entered_by`, `entered_date`, `updated_by`, `updated_date`, `update_count`) VALUES
(28, 'Advertising', '', '7', 'Adv', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-07-10', 7),
(27, 'Help', '', '6', 'Help', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-07-10', 5),
(26, 'Privacy Policy', '', '5', '', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-06-15', 4),
(23, 'Home', '', '1', 'Home', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-06-01', 2),
(24, 'Programs', '', '2', 'Program', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-06-15', 5),
(25, 'Terms of Service', '', '3', '', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-06-15', 4),
(30, 'Contact', '', '8', '', 'yes', 'yes', 'admin', '2011-04-04', 'admin', '2011-06-15', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_premium_members`
--

CREATE TABLE IF NOT EXISTS `tbl_premium_members` (
  `id` bigint(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mem_start_dt` datetime NOT NULL,
  `mem_end_dt` datetime NOT NULL,
  `mem_month` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_program_signup`
--

CREATE TABLE IF NOT EXISTS `tbl_program_signup` (
  `program_id` bigint(15) NOT NULL,
  `program_name` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referral_url` varchar(250) NOT NULL,
  `banner_url` varchar(500) NOT NULL,
  `program_type` varchar(50) NOT NULL,
  `earning_per_clk` double NOT NULL,
  `earning_per_ref_clk` double NOT NULL,
  `min_payout` double NOT NULL,
  `instant_payout` enum('Yes','No') NOT NULL,
  `payment_processor` varchar(50) NOT NULL,
  `downline_level` int(10) NOT NULL,
  `program_language` varchar(50) NOT NULL,
  `contract_duration` int(10) NOT NULL,
  `spend_credits` double NOT NULL,
  `discount` double NOT NULL,
  `total` double NOT NULL,
  `task_desc` text NOT NULL,
  `allowed_mem_rat` varchar(20) NOT NULL,
  `not_allowed_mem` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `update_count` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_program_signup`
--

INSERT INTO `tbl_program_signup` (`program_id`, `program_name`, `user_id`, `referral_url`, `banner_url`, `program_type`, `earning_per_clk`, `earning_per_ref_clk`, `min_payout`, `instant_payout`, `payment_processor`, `downline_level`, `program_language`, `contract_duration`, `spend_credits`, `discount`, `total`, `task_desc`, `allowed_mem_rat`, `not_allowed_mem`, `created_by`, `created_date`, `updated_by`, `updated_date`, `update_count`, `status`) VALUES
(54, 'neobux.com', 17, 'http://www.neobux.com', 'http://images.neobux.com/imagens/banner9.gif', 'ptc', 11, 11, 11, 'Yes', 'alertpay', 5, 'english', 10, 3000, 0, 300, 'be active', '', '', 17, '2011-07-28 08:40:23', 0, '0000-00-00 00:00:00', 0, '0'),
(55, 'neobux.com', 17, 'http://www.neobux.com', 'http://images.neobux.com/imagens/banner9.gif', 'ptc', 11, 11, 11, 'Yes', 'alertpay', 5, 'english', 5, 100, 0, 100, 'be active', '', '', 17, '2011-07-28 08:41:12', 0, '0000-00-00 00:00:00', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_program_voting`
--

CREATE TABLE IF NOT EXISTS `tbl_program_voting` (
  `id` bigint(15) NOT NULL,
  `vote_by` int(11) NOT NULL,
  `vote_value` enum('scam','trustful') NOT NULL,
  `vote_for` varchar(150) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_referral_favorites`
--

CREATE TABLE IF NOT EXISTS `tbl_referral_favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_referred_members`
--

CREATE TABLE IF NOT EXISTS `tbl_referred_members` (
  `refer_id` bigint(15) NOT NULL,
  `refer_by` int(15) NOT NULL,
  `refer_to` int(15) NOT NULL,
  `refer_level` varchar(50) NOT NULL,
  `refer_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sell_credits`
--

CREATE TABLE IF NOT EXISTS `tbl_sell_credits` (
  `credit_id` bigint(15) NOT NULL,
  `credit_title` varchar(500) NOT NULL,
  `total_credits` int(15) NOT NULL,
  `total_price` double NOT NULL,
  `remarks` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `update_count` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 -disable,1-enable'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sell_credits`
--

INSERT INTO `tbl_sell_credits` (`credit_id`, `credit_title`, `total_credits`, `total_price`, `remarks`, `created_by`, `created_date`, `updated_by`, `updated_date`, `update_count`, `status`) VALUES
(2, 'Credit Package A', 100, 4.5, 'limited offer', 0, '2011-04-10 00:00:00', 0, '2011-04-10 00:00:00', 1, '1'),
(3, 'Credit Package B', 20, 6.7, 'Limited Offer', 0, '2011-04-18 00:00:00', 0, '2011-04-18 00:00:00', 1, '1'),
(4, 'Credit Package C', 17, 9, 'Limited Offer', 0, '2011-04-18 00:00:00', 0, '2011-04-18 00:00:00', 1, '1'),
(5, 'Credit Package D', 56, 5.6, 'Limited Offer', 0, '2011-04-18 00:00:00', 0, '2011-04-18 00:00:00', 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_siteusers`
--

CREATE TABLE IF NOT EXISTS `tbl_siteusers` (
  `user_id` bigint(20) NOT NULL,
  `tree_id` varchar(50) NOT NULL,
  `user_parent_id` bigint(20) NOT NULL,
  `mem_type` enum('0','1') NOT NULL COMMENT '0-standard;1-premium',
  `random_distributed` varchar(50) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `computer_ip` varchar(50) NOT NULL,
  `country` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `referral_url` varchar(500) NOT NULL,
  `joined_date` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `gender` varchar(15) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_siteusers`
--

INSERT INTO `tbl_siteusers` (`user_id`, `tree_id`, `user_parent_id`, `mem_type`, `random_distributed`, `first_name`, `last_name`, `username`, `password`, `computer_ip`, `country`, `email`, `referral_url`, `joined_date`, `status`, `gender`) VALUES
(19, '0', 18, '0', 'No', 'gmail', 'gmail', 'client', '62608e08adc29a8d6dbc9754e659f125', '::1', 17, 'gmail@gmail.com', 'http://localhost/downlineref/?gmail', '2011-07-16 17:58:16', '1', 'not required'),
(17, '0', 0, '0', 'No', 'google', 'google', 'google', 'c822c1b63853ed273b89687ac505f9fa', '::1', 1, 'google@google.com', 'http://localhost/downlineref/?google', '2011-07-16 17:17:51', '1', 'not required'),
(18, '0', 17, '0', 'No', 'yahoo', 'yahoo', 'yahoo', '241fe8af1e038118cd817048a65f803e', '::1', 9, 'yahoo@yahoo.com', 'http://localhost/downlineref/?yahoo', '2011-07-16 17:42:25', '1', 'not required');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_menu` (
  `sub_menu_id` int(11) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `sub_menu_index` varchar(25) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `publish` enum('yes','no') NOT NULL,
  `entered_by` varchar(50) NOT NULL,
  `entered_date` date NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `updated_date` date NOT NULL,
  `update_count` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `designation` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(70) NOT NULL DEFAULT '',
  `joined_date` date DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT '1',
  `remarks` varchar(255) DEFAULT NULL,
  `entered_by` varchar(50) DEFAULT NULL,
  `entered_date` date DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `update_count` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `full_name`, `username`, `password`, `designation`, `email`, `joined_date`, `status`, `remarks`, `entered_by`, `entered_date`, `updated_by`, `updated_date`, `update_count`) VALUES
(3, 'bishal', 'bishal', '1adb27fabdfee91e29a94e3fb02e08bc', '', '', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_account`
--

CREATE TABLE IF NOT EXISTS `tbl_user_account` (
  `account_id` bigint(15) NOT NULL,
  `user_id` int(11) NOT NULL,
  `txn_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `credit_type` varchar(200) NOT NULL,
  `credits` double NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=308 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_account`
--

INSERT INTO `tbl_user_account` (`account_id`, `user_id`, `txn_id`, `created_date`, `credit_type`, `credits`) VALUES
(307, 17, 24, '2012-01-04 23:24:58', 'program signup', 168),
(306, 19, 24, '2012-01-04 21:14:21', 'program signup', 18),
(305, 19, 24, '2012-01-03 21:14:21', 'program signup', 18),
(302, 19, 123, '2012-01-02 14:25:12', 'clicking ads', 500),
(303, 18, 19, '2012-01-02 14:25:12', 'referring new members click', 50),
(304, 17, 19, '2012-01-02 14:25:12', 'referring new members click', 50),
(301, 17, 123, '2011-07-30 14:35:50', 'clicking ads', 500);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voting`
--

CREATE TABLE IF NOT EXISTS `tbl_voting` (
  `id` bigint(15) NOT NULL,
  `vote_by` int(11) NOT NULL,
  `vote_to` int(11) NOT NULL,
  `vote_type` enum('referral','promotor') NOT NULL,
  `vote_value` enum('accept','decline','terminate') NOT NULL,
  `vote_for` int(11) NOT NULL,
  `vote` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_voting`
--

INSERT INTO `tbl_voting` (`id`, `vote_by`, `vote_to`, `vote_type`, `vote_value`, `vote_for`, `vote`, `created_date`) VALUES
(53, 19, 17, 'promotor', 'terminate', 24, 2, '2012-01-04 23:24:58'),
(52, 17, 19, 'referral', 'accept', 24, 1, '2012-01-02 21:14:21'),
(51, 19, 17, 'promotor', 'accept', 24, 1, '2012-01-02 21:14:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `capcha_check`
--
ALTER TABLE `capcha_check`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `msg_sentbox`
--
ALTER TABLE `msg_sentbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_signup_child`
--
ALTER TABLE `program_signup_child`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_signup_validity`
--
ALTER TABLE `program_signup_validity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_advertisment`
--
ALTER TABLE `tbl_advertisment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blogs`
--
ALTER TABLE `tbl_blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_content`
--
ALTER TABLE `tbl_content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `tbl_front_banner`
--
ALTER TABLE `tbl_front_banner`
  ADD PRIMARY KEY (`ban_id`);

--
-- Indexes for table `tbl_links`
--
ALTER TABLE `tbl_links`
  ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tbl_premium_members`
--
ALTER TABLE `tbl_premium_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_program_signup`
--
ALTER TABLE `tbl_program_signup`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `tbl_program_voting`
--
ALTER TABLE `tbl_program_voting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_referral_favorites`
--
ALTER TABLE `tbl_referral_favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_referred_members`
--
ALTER TABLE `tbl_referred_members`
  ADD PRIMARY KEY (`refer_id`);

--
-- Indexes for table `tbl_sell_credits`
--
ALTER TABLE `tbl_sell_credits`
  ADD PRIMARY KEY (`credit_id`);

--
-- Indexes for table `tbl_siteusers`
--
ALTER TABLE `tbl_siteusers`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_sub_menu`
--
ALTER TABLE `tbl_sub_menu`
  ADD PRIMARY KEY (`sub_menu_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_account`
--
ALTER TABLE `tbl_user_account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `tbl_voting`
--
ALTER TABLE `tbl_voting`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `capcha_check`
--
ALTER TABLE `capcha_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `msg_sentbox`
--
ALTER TABLE `msg_sentbox`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `program_signup_child`
--
ALTER TABLE `program_signup_child`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `program_signup_validity`
--
ALTER TABLE `program_signup_validity`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_advertisment`
--
ALTER TABLE `tbl_advertisment`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `tbl_blogs`
--
ALTER TABLE `tbl_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_content`
--
ALTER TABLE `tbl_content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_front_banner`
--
ALTER TABLE `tbl_front_banner`
  MODIFY `ban_id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_links`
--
ALTER TABLE `tbl_links`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_premium_members`
--
ALTER TABLE `tbl_premium_members`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_program_signup`
--
ALTER TABLE `tbl_program_signup`
  MODIFY `program_id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tbl_program_voting`
--
ALTER TABLE `tbl_program_voting`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tbl_referral_favorites`
--
ALTER TABLE `tbl_referral_favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_referred_members`
--
ALTER TABLE `tbl_referred_members`
  MODIFY `refer_id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_sell_credits`
--
ALTER TABLE `tbl_sell_credits`
  MODIFY `credit_id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_siteusers`
--
ALTER TABLE `tbl_siteusers`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_sub_menu`
--
ALTER TABLE `tbl_sub_menu`
  MODIFY `sub_menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user_account`
--
ALTER TABLE `tbl_user_account`
  MODIFY `account_id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=308;
--
-- AUTO_INCREMENT for table `tbl_voting`
--
ALTER TABLE `tbl_voting`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
