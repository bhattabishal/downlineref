<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
//This code is for Re-captcha
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];


//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();

$user_details=$sDAO->fetchDetails($user_id);
$action="form";


if($_POST['cmdSubmit']==1)
{
	$errors=array();
	
	
		
	if($_POST['password']=="")
		$errors['password']="Password Is Required To Delete Your Account";
	
	if($_POST['password']!="")
	{
		
		if($user_details->password!=$_POST['password'])
		$errors['password']="Your Password Do Not Match";
	}
	
		
		
	if(count($errors)<=0)
	{
		
		
		
		if($sDAO->remove($user_id))
		{
			echo '<script language="javascript">alert("Your Account Has Been Successfully Deleted");</script>';
			
			Redirect("logout.php");
		}
		else
		{
			$action="error";
			$data=$_POST;
			
		}
	}
	else
	{
		$data=$_POST;
	}
	
		
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Account Statistics</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Account Statistics And Overview
        	</h1>
        </div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
         	<div class="message-block">
            	
                <?php if($action=="error"){ ?>
                <font color="#FF0000">Some Error Occured.Please Try Again</font>
                <?php } ?>
                
            </div>
        	<div class="container-table">
                    <div class="wrap-table"> 
                    	<form name="" method="post" action="">
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="r_name no_line">Delete Account</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Account Deletion is irreversible</td>
                                </tr>
                                 <tr>
                                   <td>Enter Your Password</td>
                                   <td><input type="password" name="password" /><span class="errormsg"><?php echo $errors['password']; ?></span></td>
                                </tr>
                                 
                                <tr>
                                	<td class="r_name">&nbsp;</td>
                                	<td  class="r_name">
                                    	
                                        
                                         <div class="buttons">
                                    	<button  id="submitbutton" class="positive" name="Delete Account" value="Delete Account" onclick="doSubmit(this.form);"><img src="images/cross.png" alt=""/>Delete Account</button>
                                        <button type="button" class="regular" name="Back" value="Back" onclick="location.href='accountdetails.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
                                        </div>
                                    </td>
                                </tr>
                                
                               
                            </tbody>
                        </table>
                          <input name="cmdSubmit" type="hidden" id="cmdSubmit" value="1" />
                        </form>
                        </div>
                </div>
           <!--table end-->
        
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
