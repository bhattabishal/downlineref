<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
$user_id=$_SESSION['SES_ID'];

$listUsers=array();
$action="form";

$pvo=new program_signup_childVO();
$pdao=new program_signup_childDAO();

$uvo=new siteusersVO();
$udao=new siteusersDAO();

$parentReferralDetails=$udao->fetchParent($user_id);
$listUsers[0]=$parentReferralDetails->user_parent_id;




$childReferralDetails=$udao->fetchChild($user_id);
$i=1;
foreach($childReferralDetails as $value)
{
	$listUsers[$i]=$value->user_id;
	$i++;
	
}

//print_r($listUsers);


$parentPromotorDetails=$pdao->fetchParentPromoters($user_id);
foreach($parentPromotorDetails as $value)
{
	$listUsers[$i]=$value->parent_user_id;
	$i++;
}



$childPromotorDetails=$pdao->fetchChildPromoters($user_id);
foreach($childPromotorDetails as $value)
{
	$listUsers[$i]=$value->child_user_id;
	$i++;
}



if($_POST['cmdSubmit']==1)
{
	$errors=array();
	
	if($_POST['username']=="select")
		$errors['username']="Please Select Username To Send Message";
		
	if($_POST['subject']=="")
		$errors['subject']="Subject Is Required";
		
	if($_POST['message']=="")
		$errors['message']="Message Can't Be Empty";
		
		
	if(count($errors)<=0)
	{
		$mvo=new messageVO();
		$mdao=new messageDAO();
		
		$mvo->message_from=$user_id;
		$mvo->message_to=trim($_POST['username']);
		$mvo->message_subject=trim($_POST['subject']);
		$mvo->message_details=trim($_POST['message']);
		$mvo->created_date=GetThisDate("Y-m-d G:i:s");
		$mvo->status=1;
		
		if($mdao->Insert($mvo))
		{
			$action="inserted";
		}
		else
		{
			$action="form";
			$data=$_POST;
			$errors['invalid']="Some Errors Occurred.Please Try Again";
		}
	}
	else
	{
		$data=$_POST;
	}
}

?>
