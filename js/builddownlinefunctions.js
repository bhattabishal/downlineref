var addonReq = new Array();
var buzztimer = new Array();
var stretchobjecttimer = null;
var stretching = false;
var shrinkobjecttimer = null;
var shrinking = false;

function trim(value) {
    value = value.replace(/^\s+/,''); 
    value = value.replace(/\s+$/,'');
    return value;
}

function tgflag(obj,mode,persistent) {
    var ext = obj.src.substring(obj.src.lastIndexOf('.'));
    var tmp = obj.src.replace(ext,'');
    tmp = tmp.replace('_gray','');
    if(mode==0 && persistent==0) obj.src = tmp+'_gray'+ext;
    else obj.src = tmp+ext;
}

function getajaxlist(type,div,callback,text,input_id) {
    getObject(div).style.display='';
    var params = '';
    params += '&type='+type;
    params += '&text='+text;
    params += '&div='+div;
    params += '&callback='+callback;
    params += '&input_id='+input_id;
    getaddon('/referrals/@suggest.php',params,div);
}

function startloadingdots(obj) {
    obj.innerHTML += (obj.innerHTML=='....') ? '.' : obj.innerHTML+'.';    
}

function isMouseLeave(e,handler) {
    if (e.type != 'mouseout' && e.type != 'mouseover') return false;
    var reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement;
    var found = false;
    while (reltg && !found) {
        if(reltg == handler) return true;
        reltg = reltg.parentNode;
    }
    return false;
}

function stretchobject(obj_id,newheight) {
    if(shrinking) return;
    var obj = getObject(obj_id);
    if(!obj) return;
    shrinkobjecttimer = null;
    var curheight = parseInt(obj.style.height);
    if(curheight>=newheight) {stretching=false; stretchobjecttimer=null; return;}
    stretching = true;
    var increment = 10;
    if((newheight-curheight)<10) increment = 1;
    else if((newheight-curheight)<30) increment = 3;
    stretchobjecttimer = window.setTimeout(function() {
        getObject(obj_id).style.height = parseInt(getObject(obj_id).style.height) + increment;
        stretchobject(obj_id,newheight);
    },10);
}
function shrinkobject(obj_id,newheight) {
    if(stretching) stretchobjecttimer = null;
    var obj = getObject(obj_id);
    if(!obj) return;
    var curheight = parseInt(obj.style.height);
    if(curheight<=newheight) {shrinking=false; shrinkobjecttimer=null; return;}
    shrinking = true;
    var decrement = 10;
    if((curheight-newheight)<10) decrement = 1;
    else if((curheight-newheight)<30) decrement = 3;
    shrinkobjecttimer = window.setTimeout(function() {
        getObject(obj_id).style.height = parseInt(getObject(obj_id).style.height) - decrement;
        shrinkobject(obj_id,newheight);
    },10);
}

function setform(formtrigger,formfield,formdata,page) {
    if(typeof page=="undefined") {page="";}
    if(formtrigger=='' && formfield=='' && formdata=='') return;
    getObject('trigger').name=formtrigger;
    getObject('data').name=formfield;
    getObject('data').value=formdata;
    document.scriptform.action=page;
    document.scriptform.submit();
}

function getaddon(url,parameters,showdiv,callback) {
    addonReq[showdiv] = null;
    addonReq[showdiv] = getXMLDoc();
    if(getObject('icon_processing'+showdiv)) getObject('icon_processing'+showdiv).style.visibility = 'visible';
    addonReq[showdiv].open("POST",url,true);
    addonReq[showdiv].setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    addonReq[showdiv].setRequestHeader("Content-length", parameters.length);
    addonReq[showdiv].setRequestHeader("Connection", "close");
    addonReq[showdiv].onreadystatechange = function() {
        if(addonReq[showdiv]) {
            if(addonReq[showdiv].readyState==4 && addonReq[showdiv].status==200) {
                var nodename = getObject(showdiv).nodeName;
                var response = addonReq[showdiv].responseText.split('-=-=-');
                if(nodename=="DIV") {
                    getObject(showdiv).innerHTML  = "<div id=icon_processing"+showdiv+" style='position:relative; visibility:hidden;'><div style='position:absolute; top:0%; left:50%;'><img src='/images/icon_processing.gif'></div></div>";
                    getObject(showdiv).innerHTML += response[0];
                } else if(nodename=="INPUT") {
                    getObject(showdiv).value = response[0];
                } else if(nodename=="TEXTAREA") {
                    getObject(showdiv).innerHTML = response[0];
                } else if(nodename=="IMAGE") {
                    getObject(showdiv).src = response[0];
                } else if(nodename=="SPAN") { //no icon
                    getObject(showdiv).innerHTML = response[0];
                }
                if(window[callback]) {                    
                    window[callback](response[1]);
                }
            }
        }
    }
    addonReq[showdiv].send(parameters);
}

function gototop() {
    scroll(0,0);
}

function wrapformdata(form) {
    var params = '';
    for(i=0; i<form.elements.length; i++) {
        var obj = form.elements[i];
        var name = (obj.type=='radio') ? obj.id : obj.name;
        var value = (obj.type=='checkbox' || obj.type=='radio') ? obj.checked : obj.value.replace('&','%26');
        if(name=='') continue;
        params += '&'+name+'='+value;
    }
    return params;
}
      
function getXMLDoc() {
    if(window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function getObject(elementID) {
    if (document.getElementById)
        return document.getElementById(elementID);
    else if (document.layers)
        return document.layers[elementID];
    else if (document.all)
        return document.all[elementID];
}

function buzz(obj,clear) {
    if(obj===undefined) return;
    if(clear===undefined) clear = false;
    if(buzztimer[obj.name]) window.clearTimeout(buzztimer[obj.name]);
    if(buzztimer[obj.id]) window.clearTimeout(buzztimer[obj.id]);
    obj.style.background = '#CC4444';
    obj.style.color = 'white';
    var tmpname = (obj.name=='') ? obj.id : obj.name;
    buzztimer[tmpname] = setTimeout(function() {
        if(obj.nodeName=='LABEL') obj.style.background = 'white';
        else if(obj.nodeName=='DIV') obj.style.background = '';
        else obj.style.background = '';
        obj.style.color='';
        if(obj.value && clear) obj.value='';
        buzztimer[tmpname] = null;
    },3000);
}

function callbackerror(status) {
    if(status===undefined) return false;
    status = status.split('###');
    if(status[0]=='ERR0') return true; /*message is shown*/
    if(status[0]=='ERR1') {getObject(status[1]).focus(); buzz(getObject(status[1])); return true;}
    if(status[0]=='RELOAD') {window.location.reload(); return true;}
    return false;
}


function parsecredits(obj) {
    obj.value = obj.value.replace(',','.');
}

function surfto(url) {
    document.location.href=url;
}

function hl(on,obj,back) {
    if(!obj) return;
    if(back===undefined || back=='') back = 'white';
    obj.style.background = (on==1) ? '#E9EBF2' : back; //DDE0ED
    obj.style.color = (on==1) ? '#2d2d2d' : '#4d4d4d';
}

function number_format(a,b) {
     if(!isDecimal(a)) return a;
     var b = (b===undefined) ? 2 : b;
     c = '.';
     d = ',';
     a = Math.round(a * Math.pow(10, b)) / Math.pow(10, b);
     e = a + '';
     f = e.split('.');
     if (!f[0]) {
      f[0] = '0';
     }
     if (!f[1]) {
      f[1] = '';
     }
     if (f[1].length < b) {
      g = f[1];
      for (i=f[1].length + 1; i <= b; i++) {
       g += '0';
      }
      f[1] = g;
     }
     if(d != '' && f[0].length > 3) {
      h = f[0];
      f[0] = '';
      for(j = 3; j < h.length; j+=3) {
       i = h.slice(h.length - j, h.length - j + 3);
       f[0] = d + i +  f[0] + '';
      }
      j = h.substr(0, (h.length % 3 == 0) ? 3 : (h.length % 3));
      f[0] = j + f[0];
     }
     c = (b <= 0) ? '' : c;
     return f[0] + c + f[1];
}

function isInteger(s) {
    if(s=='') return false;
    var valid = '0123456789';
    for(var i=0; i<s.length; i++)
        if(valid.indexOf(s.charAt(i)) < 0) return false;
    return true;
}

function isDecimal(s) {
    if(s=='') return false;
    var valid = '0123456789.';
    for(var i=0; i<s.length; i++)
        if(valid.indexOf(s.charAt(i)) < 0) return false;
    return true;
}

var highlightstimer = null;
var highlightscounter = 1;
var totalnumbers = 3;
var tempstoptimer = false;
document.onloaded = showhighlights(1,false);
document.onloaded = starthighlights();
function showhighlights(n,stop) {
    if(stop) stophighlights();
    if(tempstoptimer) return;
    for(i=1; i<=totalnumbers; i++) {
        if(getObject("promobox_content"+i)) {
            getObject("promobox_content"+i).style.display = (i==n) ? "" : "none";
            getObject("promobox_number"+i).className = (i==n) ? "selected" : "";
        }
    }
}          
function starthighlights() {
    highlightstimer = window.setInterval(function() {                  
        highlightscounter++;
        if(highlightscounter>totalnumbers) highlightscounter = 1;
        showhighlights(highlightscounter);
    }, 10000);
}
function stophighlights() {
    window.clearInterval(highlightstimer);
}

function docheckall(objname,onoff) {
    objname = objname.replace('[]','');
    for(i = 0; i < document.requestform.elements.length; i++) {
        if(document.requestform.elements[i].nodeName!='INPUT') continue;
        if((document.requestform.elements[i].name.indexOf(objname)) == -1) continue;
        document.requestform.elements[i].checked = onoff;
    }
}