//By: Chris Campbell
//Created: May 20, 2005
//Last Modified: June 27th, 2005
//www.particletree.com


window.onload = attachFormHandlers;

var gShow; //variable holding the id where feedback will be sent to.
var sUrl = "formvalidation.php?validationtype=ajax&val=";//url is the page which will be processing all of the information.  it is important to make sure validationtype is ajax
var gErrors = 0; //number of errors is set to none to begin with
var http = getHTTPObject();//don't worry about this



function attachFormHandlers()
{
	var form = document.getElementById('form1') 

	if (document.getElementsByTagName)//make sure were on a newer browser
	{
		var objInput = document.getElementsByTagName('input');
		for (var iCounter=0; iCounter<objInput.length; iCounter++)
		objInput[iCounter].onblur = function(){return validateMe(this);} //attach the onchange to each input field
	}
	form.onsubmit = function(){return validate();} //attach validate() to the form
}




/*validateMe is the function called with onblur each time the user leaves the input box
passed into it is the value entered, the rules (which you could create your own), and the id of the area the results will show in*/
function validateMe(objInput) {

	sVal = objInput.value; //get value inside of input field
	
	sRules = objInput.className.split(' '); // get all the rules from the input box classname
	sRequired = sRules[1]; // determines if field is required or not
	sTypeCheck = sRules[2]; //typecheck are additional validation rules (ie. email, phone, date)
    gShow = sRules[3]; //gShow is the td id where feedback is sent to.
  
	//sends the rules and value to the asp page to be validated
	http.open("GET", sUrl + (sVal) + "&sRequired=" + (sRequired) + "&sTypeCheck=" + sTypeCheck, true);
  
	http.onreadystatechange = handleHttpResponse; 	// handle what to do with the feedback 
	http.send(null);  
}


function handleHttpResponse() {
	//if the process is completed, decide to do with the returned data
	if (http.readyState == 4) 
  	{
		
  		sResults = http.responseText.split(","); //results is now whatever the feedback from the asp page was
		//whatever the variable glo_show's (usermsg for example) innerHTML holds, is now whatever  was returned by the asp page. 
    	document.getElementById(gShow).innerHTML = "";
		document.getElementById(gShow).appendChild(document.createTextNode(sResults[0]));
  	}
}


function validate()
{
var tables; 

tables = document.getElementsByTagName('td')

	for (i=0; i<tables.length; i++)//loop through all the <td> elements 
	{
		// if the class name of that td element is rules check to see if there are error warnings
		if (tables[i].className == "rules")
		{
			//if there is a thank you or its blank then it passes
			if (tables[i].innerHTML == 'Thank You' || tables[i].innerHTML == '' )
			{
				tables[i].style.color = '#000000';//the color is changed to black or stays black
			}
			else
			{
				gErrors = gErrors + 1; //the error count increases by 1
				tables[i].style.color = '#ff0000';//error messages are changed to red
			}
		}
	}
		
	if (gErrors > 0)
	{
		//if there are any errors give a message
		alert ("Please make sure all fields are properly completed.  Errors are marked in red!");
		gErrors = 0;// reset errors to 0
		return false;
	}
	else return true;

}


function getHTTPObject() {
	var xmlhttp;
	/*@cc_on
	@if (@_jscript_version >= 5)
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
      try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (E) {
        xmlhttp = false;
      }
    }
  @else
  xmlhttp = false;
  @end @*/
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
		try 
		{
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
		xmlhttp = false;
		}
	}
	return xmlhttp;
}