function makeDraggable(){
	new Draggable('polldiv',{revert:false});
}

function selectOption(obj) {
	option = $F(obj);
}

function submitPoll() {		
	$('result').innerHTML = 'Please wait...';
	
	var url = 'poll/poll.php';		
	var pars = 'option=' + option ;
	var myAjax = new Ajax.Request( url, { method: 'post', parameters: pars, onComplete: showResponse, onFailure: reportError });
}

function showResponse(originalRequest) {
	var res = originalRequest.responseText;
	if (res.indexOf('ip=') != -1) {
		var ip = res.substring(3);
		alert('You have already given your opinion for the day from:'+ip);
		$('result').innerHTML =  '';
	}
	else {
		$('result').innerHTML =  '';
		$('voteimg').innerHTML =  '';
		$('ques').innerHTML = 'Poll Results';
		$('options').innerHTML = res;
	}
}

function reportError(originalRequest) {
	$('result').innerHTML = originalRequest.responseText;
}	

function hidePoll() {
	$('polldiv').style.display = 'none';
	$('showdiv').style.display = 'block';
}

function showdiv() {
	$('polldiv').style.display = 'block';
	$('showdiv').style.display = 'none';
}