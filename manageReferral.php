<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];




//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();

$uDAO=new user_accountDAO();
$uVO=new user_accountVO();

$user_details=$sDAO->fetchDetails($user_id);
$refer_url=$user_details->referral_url;


$sDetails=$sDAO->fetchChild($user_id);


$sTotal=count($sDetails);


$pVO=new program_signup_childVO();
$pDAO=new program_signup_childDAO();



$referalSignups=$pDAO->fetchChildPromoters($user_id);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Manage Referral Sign-ups</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<link rel="stylesheet" type="text/css" href="css/sortertable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft" style="margin:5px; width:940px">
     	<div class="block" style="width:940px">
        <!-- Ttle (Start) -->
        <div class="ttle" style="background:url(images/h1bg2.jpg) no-repeat; width:940px">
          	<h1> 
				Manage Referral Sign-ups
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--info div staart-->
        <div id="form-container" style="margin-bottom:15px; width:910px">
            <header id="form-header" class="info" style="border:none; padding:10px; font-style:normal; font-weight:normal;">
             <ul style="list-style:none; font-weight:bold; ">
                        	<li><span style="margin-right:5px"></span>What's This ?</li>
                           
                        </ul>
                    	<h2 style="font-style:normal;font-size:110%">Here you can manage your referral sign-ups for the programs you are promoting at DownlineRefs. Click an entry from the list below to get more details about that sign-up. Here you can also terminate contracts if it turns out your referral is not active. <br />


						</h2>
                       
                    	<div></div>
                	</header>
            </div>
        <!--info div end-->
        
        
        <!--table starts-->
        <div id="tablewrapper">
		<div id="tableheader">
        	<div class="search">
                <select id="columns" onchange="sorter.search('query')"></select>
                <input type="text" id="query" onkeyup="sorter.search('query')" />
            </div>
            <span class="details">
				<div>Records <span id="startrecord"></span>-<span id="endrecord"></span> of <span id="totalrecords"></span></div>
        		<div><a href="javascript:sorter.reset()">reset</a></div>
        	</span>
        </div>
        <table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
            <thead>
                <tr>
                    <th class="nosort"><h3>Signup date</h3></th>
                    <th><h3>Program Name </h3></th>
                    <th><h3>Username  </h3></th>
                    <th><h3>Status </h3></th>
                    
                </tr>
            </thead>
            <tbody>
           <?php
		   if(count($referalSignups) > 0 )
		   {
		   foreach($referalSignups as $records)
		   {
		   	if($records->status != 0)
			{
		   		//signup program details
					$pvo=new program_signupVO();
					$pdao=new program_signupDAO();
					
					$programdetails=$pdao->fetchDetails($records->program_id);
					
					//userdetails
					$svo=new siteusersVO();
					$sdao=new siteusersDAO();
					$userinfo=$sdao->fetchDetails($records->child_user_id);
					$mem_name=$userinfo->username;
					$mem_type=$userinfo->mem_type;
					
					//check if terminated
					//$delimage="";
					
					//if($records->status==1)//only show terminate link if program is in progress
					//{
						//$delimage="<a href=\"manageReferral.php?id=$records->id\" onclick=\"return confirm('Are you sure you want to Terminate this Signup ?')\" ><img src=\"images/icon_del.png\" align=\"absmiddle\" style=\"margin-right:5px\" title=\"terminate\" /></a>";
					//}
					
		   ?>
        
                <tr>
                    <td><?php echo $records->created_date; ?></td>
                    <td><a href="editmyprogramsignup.php?sid=<?php echo $records->id; ?>&t=mr" title="show signup details" style="color:#006600"><?php echo $programdetails->program_name; ?></a></td>
                    <td> <a href="editmyprogramsignup.php?sid=<?php echo $records->id; ?>&t=mr" title="show signup details" style="color:#006600">
					<?php echo $mem_name; ?>
                    <?php
                                    if($mem_type==1)
                                    {
                                        echo '<img src="images/premium.png" align="bottom" alt="Premium Member" title="Premium Member" />';
                                        
                                    }
                                    else
                                    {
                                        echo '<img src="images/free.png" align="bottom" alt="Standard Member" title="Standard Member" />';
                                    }
                                ?>
                                </a></td>
                    <td><a href="editmyprogramsignup.php?sid=<?php echo $records->id; ?>&t=mr" title="show signup details" style="color:#006600"><b>
                     	<?php
							switch($records->status)
							{
								case 0:
								$status="Request Pending";
								break;
								
								case 1://means accepted.now calculate the time left
								//$pvvo=new program_signup_validityVO();
								$pvdao=new program_signup_validityDAO();
								$validitydetails=$pvdao->fetchSignupDetailsByUserId($records->child_user_id,$records->id);
								$date=$validitydetails->valid_to;
								$dateParts=dateParseFromFormat("Y-m-d H:i:s", $date);
								
								$CountDown = new CountDown;
								
								$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);
								
								
								$CountDown->format = "Weeks: {W} Days: {D} Hours: {H}";
								
								//$CountDown->finished = changeBannerAdvStatus($details->ban_id);
								if($out=='0')
								{
									//changeProgramSignUpStatus($records->id,'3');
									$status="<img src=\"images/icon14.png\"  align=\"middle \" style=\"margin-right:5px \"/>Completed";
									
								}
								else
								{
								
									$status="<img src=\"images/icon_status_in_progress.png\"  align=\"middle \" style=\"margin-right:5px \"/>In Progress...........";
								}
								
								
								break;
								
								case 2:
								$status="<img src=\"images/icon13.png\"  align=\"middle \" style=\"margin-right:5px \"/>Declined";
								break;
								
								case 3:
								$status="<img src=\"images/icon14.png\"  align=\"middle \" style=\"margin-right:5px \"/>Completed";
							break;
								
								case 4:
								$status="<img src=\"images/cancelled.png\"  align=\"middle \" style=\"margin-right:5px \"/>Contract Terminated";
								break;
								
								default:
								$status="n/a";
								
							}
							
							echo $status;
						?></b></a></td>
                    
                  
                </tr>
             
         <?php
		 } 
		 }
		 }
		 else
		 {
		 ?>
         <td colspan="4" align="center">No Records</td> 
         <?php
		 }
		 ?>
            </tbody>
        </table>
        <div id="tablefooter">
          <div id="tablenav">
            	<div>
                    <img src="images/shortertable/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/shortertable/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/shortertable/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/shortertable/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div>
                	<select id="pagedropdown"></select>
				</div>
                <div>
                	<a href="javascript:sorter.showall()">view all</a>
                </div>
            </div>
			<div id="tablelocation">
            	<div>
                    <select onchange="sorter.size(this.value)">
                    <option value="5">5</option>
                        <option value="10" selected="selected">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div>
            </div>
        </div>
    </div>


	<script type="text/javascript" src="js/sorterTable/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:7,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		//sum:[8],
		//avg:[6,7,8,9],
		//columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
           <!--table end-->
           <div class="buttons">
           
                	<button type="button" class="regular" name="Back" value="Back" onclick="location.href='dashboard.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
               
           </div>
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
           <div style="clear:both"></div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			//include_once("includes/rightcolumn.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
