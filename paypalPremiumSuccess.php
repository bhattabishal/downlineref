<?php
session_start();
//header('Refresh: 1; URL=purchagecredits.php');
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";

if(isset($_SESSION['SES_ID']) && $_SESSION['SES_ID']!='')
{
	$id=$_SESSION['SES_ID'];
}
else
{
	$id=0;
}
//$_SESSION['premium_type']=

$fvo=new premium_membersVO();
$fdao=new premium_membersDAO();

$svo=new siteusersVO();
$sdao=new siteusersDAO();

$svo->user_id=$id;
$svo->mem_type=1;

$fvo->user_id=$id;
$fvo->status=1;
$fvo->mem_start_dt=date('Y-m-d H:i:s',strtotime($_POST[payment_date]));
$fvo->mem_end_dt=date("Y-m-d H:i:s", strtotime($_POST[payment_date] . "+ ".$_SESSION['premium_type']." month")); 
$fvo->mem_month=$_SESSION['premium_type'];
//$d="10:05:34 May 31, 2011 PDT ";
//echo date("Y-m-d H:i:s", strtotime("2011-05-31 20:03:06". "+ 2 week"));
if($fdao->Insert($fvo) && $sdao->updateStatus($svo))
{
	unset($_SESSION['premium_type']);
	
	$url="premium.php?action=success";
	Redirect($url);
}
else
{
	$url="premium.php?action=error";
	Redirect($url);
}


	

?>

<html>
<head><title>::Thank You::</title>
<link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body bgcolor="ffffff">
<br>
<br>
<table width="500" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr> 
      <td align="left" valign="top" bgcolor="#333333"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
               <td align="center" bgcolor="#EEEEEE"> <p>&nbsp;</p>
                  <p>Thank you! Your order has been successfully processed.</p>
                  <p>&nbsp;</p></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
<table width="500" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr> 
      <td align="left" valign="top" bgcolor="#333333"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr align="left" valign="top"> 
               <td width="20%" bgcolor="#EEEEEE"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Order Number:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[txn_id]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Date:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[payment_date]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td width="20%" bgcolor="#EEEEEE"> First Name: </td>
                        <td width="80%" bgcolor="#EEEEEE"> 
                           <?=$_POST[first_name]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Last Name:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[last_name]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Email:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[payer_email]?>
                        </td>
                     </tr>
                  </table></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
</body>
</html>



