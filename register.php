<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
//require_once "autologin.php";
//require_once "logincheck.php";
//This code is for Re-captcha
require_once "./recaptcha/recaptcha_includes.php";


if(($_SESSION['SES_ID']) && intval($_SESSION['SES_ID'])!=0)
	{
			Redirect("index.php");

	}



$cDAO = new countryDAO();
$country=$cDAO->fetchAll();




if(isset($_SESSION['referral_id']) && $_SESSION['referral_id']!='')
{
	$referal_name=$_SESSION['referral_name'];
	$referal_id=$_SESSION['referral_id'];
	//echo "session set";
}


//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();

$ip=getRealIpAddr();
$ip;

$ipcheck=$sDAO->DuplicationIpcheck($ip);

$rVO=new referred_membersVO();
$rDAO=new referred_membersDAO();

$action="form";


if($_POST['cmdSubmit']==1)
{
	$errors=array();
	
	if ($_POST["recaptcha_response_field"]) 
	{
		$resp = recaptcha_check_answer ($privatekey,$_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);
	}
	
	if (!$resp->is_valid)
		$errors['security']="Incorrect Security Code";
	
	if($_POST['first_name']=="")
		$errors['first_name']="First Name Is Required";
		
	//if($_POST['gender']=="")
		//$errors['gender']="Gender Is Required";
	
	if($_POST['last_name']=="")
		$errors['last_name']="Last Name Is Required";
		
	if($_POST['username']=="")
		$errors['username']="Username Is Required";
		
	if($_POST['username']!='')
		{
			if(count($sDAO->DuplicationUserNamecheck($_POST['username']))>=1)
			{
				$errors['username']="This Username Is Already Taken.";
			}
		}
		
		if($_POST['email']!='')
		{
			if(count($sDAO->DuplicationEmailcheck($_POST['email']))>=1)
			{
				$errors['email']="This Email Is Already Taken.";
			}
		}
		
	if($_POST['password']=="")
		$errors['password']="Password Is Required";
		
	if($_POST['confirmpassword']=="")
		$errors['confirmpassword']="Confirm Password Is Required";
		
	if($_POST['confirmpassword']!="")
		{
			if($_POST['confirmpassword']!=$_POST['password'])
				$errors['confirmpassword']="Password Do Not Match";
		}
		
	if($_POST['email']=="" || $_POST['email']!=ValidateEmail($_POST['email']))
		$errors['email']="Valid Email Is Required";
		
	if($_POST['country']=="select")
		$errors['country']="Country Is Required";
		
		
	if(count($errors)<=0)
	{
		$sVO->first_name=$_POST['first_name'];
		$sVO->last_name=$_POST['last_name'];
		$sVO->username=$_POST['username'];
		$sVO->password=md5($_POST['password']);
		$sVO->email=$_POST['email'];
		$sVO->country=$_POST['country'];
		$sVO->gender="not required";
		 $sVO->referral_url=getWebsiteUrl()."?".$_POST['username'];
		
		$sVO->joined_date=GetThisDate("Y-m-d G:i:s");
		$sVO->status='1';
		$sVO->mem_type='0';
		$sVO->computer_ip=$_SERVER['REMOTE_ADDR'];
		$sVO->tree_id=0;
		$sVO->user_parent_id=0;
		$sVO->random_distributed='No';
		
		if($referal_id!='')
		{
			
			$sVO->user_parent_id=$referal_id;
		}
		
		$rVO->refer_by=$referal_id;
		
		$rVO->refer_date=GetThisDate("Y-m-d G:i:s");
		
		if($sDAO->Insert($sVO))
		{
			//$rVO->refer_to=mysql_insert_id();
			//$parentLevel=fetchDetails($referal_id);
			
			//if($referal_id!='' && $rDAO->Insert($rVO))
			//{
				$action="inserted";
				$_SESSION['referral_id']='';
				$_SESSION['referral_name']='';
			//}
			
		}
		else
		{
			//$sDAO->remove($rVO->refer_to);
			$action="form";
			$data=$_POST;
			$errors['invalid']="Some Errors Occurred.Please Try Again";
			
		}
	}
	else
	{
		$data=$_POST;
	}
	
		
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Register</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1> 
		  <?php if(isset($action) && $action=="form") {?>Registration Form <?php }
		 elseif($action=="inserted")
		 {
		 ?>
        Registration Successful
         <?php
		 }
		  ?></h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <?php
			if(!count($ipcheck) > 0)
			{
		?>
        <div style="clear:both"></div>
         <span class="errormsg"><?php echo $errors['invalid']; ?></span>
        <?php if(isset($action) && $action=="form") {?>
        <form name="basicregistration" action="" method="post" style="margin-top:10px">
        	<div class="input-block">
        		<div class="label">First Name :</div>
            	<div class="input"><input type="text" name="first_name" value="<?php echo $data['first_name']; ?>" maxlength="25"  /><span class="errormsg"><?php echo $errors['first_name']; ?></span></div>
        	</div>
            <div class="input-block">
        		<div class="label"> <b>Last Name :</b></div>
            	<div class="input">  <input type="text" name="last_name" value="<?php echo $data['last_name']; ?>" maxlength="25"  /><span class="errormsg"><?php echo $errors['last_name']; ?></span></div>
        	</div>
            <div class="input-block" >
        		<div class="label"><b> User Name :</b></div>
            	<div class="input"> <input type="text" name="username" value="<?php echo $data['username']; ?>" maxlength="15" /><span class="errormsg"><?php echo $errors['username']; ?></span></div>
        	</div>
            <div class="input-block">
        		<div class="label" ><b>Password :</b></div>
            	<div class="input"><input type="password" name="password" maxlength="15" /><span class="errormsg"><?php echo $errors['password']; ?></span></div>
        	</div>
            <div class="input-block" style="margin-top:10px">
        		<div class="label" style="margin-bottom:5px"><b>Verify Password :</b></div>
            	<div class="input"><input type="password" name="confirmpassword" maxlength="15" /><span class="errormsg"><?php echo $errors['confirmpassword']; ?></span></div>
        	</div>
            <?php if($referal_name!=''){ ?>
            <div class="input-block">
        		<div class="label" ><b>Referred By :</b></div>
            	<div class="input">
                	<input type="text" readonly="readonly" value="<?php echo $referal_name; ?>" />
                </div>
        	</div>
            <?php } ?>
           <!-- <div class="input-block">
        		<div class="label"><b>Gender :</b></div>
            	<div class="input">
                	<input type="radio" name="gender" value="male"  <?php// if($data['gender']=='male'){echo 'checked="checked"';} ?>/>Male
                    <input type="radio" name="gender" value="female" style="margin-left:5px" <?php// if($data['gender']=='female'){echo 'checked="checked"';} ?> />Female
                    <span class="errormsg"><?php// echo $errors['gender']; ?></span>
                </div>
        	</div>-->
            <div class="input-block">
        		<div class="label"><b>Email :</b></div>
            	<div class="input"><input type="text" name="email" value="<?php echo $data['email']; ?>" maxlength="50"/><span class="errormsg"><?php echo $errors['email']; ?></span></div>
        	</div>
            <div class="input-block">
        		<div class="label"><b>Country :</b></div>
            	<div class="input">
                 	<select name="country">
                    	<option>select</option>
                        <?php
							foreach($country as $value)
							{
						?>
                        	<option value="<?php echo $value->id; ?>" <?php if($value->id==$data['country']){echo 'selected="selected"';} ?>><?php echo $value->name; ?></option>
                        <?php
							}
						?>
                    </select>
                    <span class="errormsg"><?php echo $errors['country']; ?></span>
                </div>
        	</div>
            <div class="input-block">
        		<div class="label"><b>Security :</b></div>
            	<div class="input">
               <?php 
			   echo recaptcha_get_html($publickey, $error);
						
					 ?>
                     <span class="errormsg"><?php echo $errors['security']; ?></span>
                       <div style="clear:both;"></div>
                </div>
        	</div>
            <div class="button-block" style="margin-top:10px">
            	<!--<input type="submit" name="Submit" value="Submit" />
                <input type="reset" name="Cancel" value="Cancel"/>-->
                 <div class="buttons">
                <button type="submit" class="positive" name="Submit" value="Submit">
        			<img src="images/apply2.png" alt=""/>
        			Register
    			</button>
            	<button type="reset" class="negative">
                	<img src="images/cross.png" alt=""/>
                     Cancel
                </button>
                </div>
                <input name="cmdSubmit" type="hidden" id="cmdSubmit" value="1" />
            </div>
             <div style="clear:both;"></div>
         </form>
         <?php 
		 }
		 elseif($action=="inserted")
		 {
		 ?>
         <div class="blogtitle" style="margin-top:10px; font-weight:bold; margin-bottom:10px">Registration Completed</div>
                    <p style="font-size:12px; 
	 		color:#333;">Congratulations!</p>
            <ul style="font-size:12px; font-weight:normal; color:#333; ">
            <li style="list-style:none">you've successfully signed up.</li>
            <li style="list-style:none"><div id="">
				<p></p>
			<a href="login.php" class="topMenuAction"><b>Click here</b></a> to login
		</div>
         <?php
		 }
		  ?>
         
    	<!--inner content close-->
         <?php } else
		 {
		 ?>
         	<div style="color:#EC1334; font-weight:bold; background-color:#6C955C; padding:5px 10px">
            	
                <div>You Cannot Register 
                This computer, household or private network has already been assigned to an account.</div>
            </div>
         <?php
		 } 
		 ?>
      	</div>
       
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
