<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "alertpay.php";
require_once "logincheck.php";


$sellCreditVO=new sell_creditsVO();
$sellCreditDAO=new sell_creditsDAO();


$cvo=new code_valueVO();
$cdao=new code_valueDAO();

$code_label1="website_url";
$code_label2="alertpay_email";

$codevalues1=$cdao->fetchDetailsByLabel($code_label1);
$codevalues2=$cdao->fetchDetailsByLabel($code_label2);

$website_url=$codevalues1->code_value;
$alertpay_email=$codevalues2->code_value;

$allCredits=$sellCreditDAO->fetchAll('onlypublished');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Purchase Credit</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1> 
          	Purchage Credits
		 </h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin-left:15px; margin-top:15px">
        <div style="clear:both"></div>
        <?php
		if(isset($_GET['action']))
		{
		?>
        <div class="sucess-div"  style="padding:10px; margin:0 15px 10px 0; background-color:#000033; color:#FFFFFF">
        <?php 
		if($_GET['action']=="sucess")
		{
		?>
        	<b>Thank you! Your order has been successfully processed.</b>
         <?php
         }
		 elseif($_GET['action']=="faliure")
		 {
		 	?>
            	<b><font color="#FF0000">We're sorry, your credit card has been declined.</font></b>
            <?php
		 }
		?>
        </div>
        <?php
		}
		?>
        <!--<form name=purchaseform method=post>
      <input type=hidden name=dopurchase value=1>
      <input type=hidden name=package value=''>
      <input type=hidden name=processor value=''> </form>-->
	<form method="post" action="alertpay/process.php" name="alertpay" > 
    <input type="hidden" name="ap_purchasetype" value="item-auction"/> 
    <input type="hidden" name="ap_merchant" value="<?php echo $alertpay_email; ?>"/> 
    <input type="hidden" name="ap_itemname" value="credits"/> 
    <input type="hidden" name="ap_currency" value="USD"/> 
    <input type="hidden" name="ap_returnurl" value="<?php echo $website_url; ?>/payment-successful.php"/> 
    <input type="hidden" name="ap_itemcode" value="1"/> 
    <input type="hidden" name="ap_quantity" value="1"/> 
    <input type="hidden" name="ap_description" value="credits payed"/> 
    <input type="hidden" name="ap_amount" value="200"/> 
    <input type="hidden" name="ap_taxamount" value="0"/> 
    <input type="hidden" name="ap_shippingcharges" value="0"/> 
    <input type="hidden" name="ap_cancelurl" value="<?php echo $website_url; ?>/payment-cancelled.php"/> 
     <input type="hidden" name="item_id" value="0">
      <input type="hidden" name="action" value="no">
   
    </form>
    <!--form for paypal-->
   <!-- <form class="paypal" action="payments.php" method="post" id="paypal_form" target="_blank" name="paypal">    
	<input type="hidden" name="cmd" value="_xclick" /> 
    <input type="hidden" name="no_note" value="1" />
    <input type="hidden" name="lc" value="UK" />
    <input type="hidden" name="currency_code" value="GBP" />
    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
    <input type="hidden" name="first_name" value="Customer's First Name"  />
    <input type="hidden" name="last_name" value="Customer's Last Name"  />
    <input type="hidden" name="email" value="customer@example.com"  />
    <input type="hidden" name="item_number" value="123456" / >
   
	</form>-->
    <form method="post" action="php_paypal/process.php" name="paypal">
        <input type="hidden" name="amount" value="1">
        <input type="hidden" name="item_name" value="1">
        <input type="hidden" name="item_id" value="0">
        
		
        <!--<input type="submit" value=" Pay ">-->
    </form>
    <!--form ends-->
    <script>
      //function dopurchase(package,processor) {
         // document.purchaseform.package.value=package;
         // document.purchaseform.processor.value=processor;
          //document.purchaseform.submit();
      //}
    </script>   
    <script>
      function dopurchase(package,amount,item_name,item_id) {
	  	if(package=='paypal')
         {
		 	document.paypal.amount.value=amount;
			document.paypal.item_name.value=item_name;
			document.paypal.item_id.value=item_id;
          	document.paypal.submit();
		 }
		 else if(package=='alertpay')
		 {
		 	document.alertpay.ap_amount.value=amount;
			document.alertpay.ap_itemname.value=item_name;
			document.alertpay.item_id.value=item_id;
			document.alertpay.action.value='yes';
		 	document.alertpay.submit();
		 }
      }
    </script> 
    
    
   
       		<?php
				foreach($allCredits as $value)
				{
				?>
                 <div style="float:left; width:177px; height:177px; background-color:#d2e6ca; padding:10px; margin-bottom:10px; margin-right:10px">
					<div style="text-align:center"><b><?php echo strtoupper($value->credit_title)."</b><br/>"; ?></div>
					<div style="text-align:center"><?php echo $value->total_credits." "."credits<br/>"; ?></div>
					<div style="text-align:center"><?php echo "$".$value->total_price."<br/>"; ?></div>
                    <div style="text-align:center"><img src="images/paypal1.gif"  title='PayPal' style="margin:4px 0" onclick='dopurchase("paypal","<?php echo $value->total_price; ?>","<?php echo strtoupper($value->credit_title) ?>","<?php echo $value->credit_id; ?>");;' /></div>
                     <div style="text-align:center"><img src="images/big_pay_01.gif" title='AlertPay' style="margin-bottom:4px"  onclick='dopurchase("alertpay","<?php echo $value->total_price; ?>","<?php echo strtoupper($value->credit_title) ?>","<?php echo $value->credit_id; ?>");;'  /></div>
					<div style="text-align:center"><?php echo $value->remarks; ?></div>
				 </div>	
               
				<?php	
				}
				
            ?>
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
