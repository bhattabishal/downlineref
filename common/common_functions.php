<?php
//Common Functions Library
$keys=123;
//Hash functions
function GetUniqueID() 
	{
	return md5(uniqid(rand(),1));
	}

//Dates and Time functions
function GetThisDate($format, $daysOffset=0)
	{
	if($daysOffset<>0)
		$today= mktime(date("G")+1, date("i"), date("s"), date("m"), date("d")-$daysOffset, date("Y"));
	else
		$today= mktime(date("G")+1, date("i"), date("s"), date("m"), date("d"), date("Y"));
	return date($format, $today);
	}

function GetThisTimeStamp()
	{
	return mktime();
	}	
	
	function GetDays($sStartDate, $sEndDate){
  // Firstly, format the provided dates.
  // This function works best with YYYY-MM-DD
  // but other date formats will work thanks
  // to strtotime().
  $sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
  $sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

  // Start the variable off with the start date
  $aDays[] = $sStartDate;

  // Set a 'temp' variable, sCurrentDate, with
  // the start date - before beginning the loop
  $sCurrentDate = $sStartDate;

  // While the current date is less than the end date
  while($sCurrentDate < $sEndDate){
    // Add a day to the current date
    $sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));

    // Add this new day to the aDays array
    $aDays[] = $sCurrentDate;
  }

  // Once the loop has finished, return the
  // array of days.
  return $aDays;
}


// Returns the date adjusted by the number of hours specified.

  function AddHours($date, $hours)

  {

        $newdate = date("Y-m-d G:i:s", mktime($date["hours"] + $hours,

                                                $date["minutes"],

                                                $date["seconds"],

                                                $date["mon"],

                                                $date["mday"],

                                                $date["year"]));

        return $newdate;

  }

function GetNowDate()
	{
	$d = getdate(mktime());
	$datestamp  = $d["year"] . "-" . $d["mon"] . "-" . $d["mday"];
	return $datestamp;
	}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function GetDateForDb($screenDate) 

	{

	if((int)$screenDate<=0)

		return '0000-00-00';

	$d = split("/",$screenDate);

	 return $datestamp = $d["2"] . "-" . $d["0"] . "-" . $d["1"];

	}



function GetLongDateForDb($screenDate) 

	{

	if((int)$screenDate<=0)

		return '0000-00-00 00:00:00';

	

	$dt=explode(" ", $dbDate);

	

	$d = split("/",$dt[0]);

	 return $datestamp = $d["2"] . "-" . $d["0"] . "-" . $d["1"]." ".$dt[1];

	}

	

function GetScreenDate($dbDate) 

	{

	if((int)$dbDate<=0)

		return '';

	$d = split("-",$dbDate);

	 return $datestamp = $d["1"] . "/" . $d["2"] . "/" . $d["0"];

	}

	

function GetLongScreenDate($dbDate) 

	{

	if((int)$dbDate<=0)

		return '';

	$dt=explode(" ", $dbDate);

	

	$d = split("-",$dt[0]);

	 return $datestamp = $d["1"] . "/" . $d["2"] . "/" . $d["0"]." ".$dt[1];

	}

		

	

function GetExLongScreenDate($dbDate) 

	{

	if(strlen($dbDate)<=0)

		return '';

	$dt=explode(" ", $dbDate);

	

	$d = split("-",$dt[1]);

	 return $datestamp =$dt[0]." ".$d["1"] . "/" . $d["2"] . "/" . $d["0"];

	}

	

function GetDateDifference($date1, $date2='') 

	{

	if($date2=='')

		$date2=GetThisDate("Y-m-d");



	$d2Exp=explode("-",$date2);	

	$d2=mktime(0, 0, 0, $d2Exp[1], $d2Exp[2], $d2Exp[0]);		

	$d1Exp = explode("-",$date1);	

	$d1=mktime(0, 0, 0, $d1Exp[1], $d1Exp[2], $d1Exp[0]);	

	

	$dateDiff = $d1 - $d2;	

	$fullDays = floor($dateDiff/(60*60*24));

	

	return $fullDays;

	}	



function Redirect($url)

	{

	echo ('<meta content="url;'.$url.'" http-equiv="refresh">');

	echo ('<script language="javascript" type="text/javascript">

			document.location.href="'.$url.'"; 

			</script>');

	exit;

	}



function GetMonthsDropDown($thisMonth)

	{

	$html='';

	for($i=1;$i<=12;$i++)

		{

		$sel=($i==$thisMonth) ? ' selected="selected" ' : '';

		$html.="<option value='$i' $sel> $i</option>"; 

		}

	return $html;

	}



function GetYearsDropDown($thisYear)

	{

	$html='';

	$startYear=GetThisDate("Y")-10;

	$endYear=GetThisDate("Y");

	for($i=$startYear;$i<=$endYear;$i++)

		{

		$sel=($i==$thisYear) ? ' selected="selected" ' : '';

		$html.="<option value='$i' $sel> $i</option>"; 

		}

	return $html;

	}



//File Functions
/*
function IsFileExists($server_path, $_FILES, $fieldName) 

	{

	if (file_exists($server_path.$_FILES[$fieldName]['name'])) return true;

 	return false;

	}


*/


// HANDY LITTLE DEBUG FUNCTION **********************************************



function ppp($p, $vName = "") {	

	global $gDebug;

	$gDebug = true;	

	if ($gDebug) {

		echo "<PRE>\n";

		if (!empty($vName)) { echo $vName . "\n"; }

		print_r($p);

		echo "</PRE>\n";

	}

}

// **************************************************************************

function IncludePaging($page, $start, $limit , $num)

		{

		$html='';

		$thispage = $start + $limit; 

		$back = $start - $limit; 

		$next = $start + $limit; 

	

		if($num > $limit)

			{

			if($back >=0)

				{ 

				if($num<>0)

						$html='<a href="'.$page.'&start='.$back.'">&laquo; Prev</a>'; 

				} 

			$i=0;

			$l=1;

			$st=$start-(5*$limit);

			$en=$start+(5*$limit);

				for($i=0;$i < $num;$i=$i+$limit)

					{	

						if($i>=$st && $i<=$en)

							{						

							if($i <> $start)

								$html.='<a href="'.$page.'&start='.$i.'">'.$l.'</a>';

							else 

								$html.="<a href='javascript:void(0);' class='selected'>".$l."</a>";

							}

					$l=$l+1;

					}			

			if($thispage < $num) 

				$html.='<a href="'.$page.'&start='.$next.'">Next &raquo;</a>';

			}

		else

			$html.='';

		return $html;

	}

	

//Regular Expression Functions



function ValidateEmail($email)
{
	if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		return false;
	}
	else
	{
		return true;
	} 

}

	

function ValidateNumber($number)

	{

	if($number=is_numeric($number))

		{

			return $number;

		}

	}
	
//function ValidURL($url)
//{
 //$urlregex = "^((http:\/\/www\.)|(www\.)|(http:\/\/))[a-zA-Z0-9._-]+\.[a-zA-Z.]{2,5}$";
//if (eregi($urlregex, $url)) {return TRUE;} else {return FALSE;}  
//}  
function ValidURL($url)
{
 $urlregex = "^(https?|ftp)\:\/\/([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)*(\:[0-9]{2,5})?(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?(#[a-z_.-][a-z0-9+\$_.-]*)?\$";
if (eregi($urlregex, $url)) {return TRUE;} else {return FALSE;}  
}  
	
	

//encryption and decryption function

global $keys;

$keys=array();

function crypt_key($ckey)

	{

	global $keys;

	$c_key = base64_encode(sha1(md5($ckey)));

	$c_key = substr($c_key, 0, round(ord($ckey{0})/5));

	$c2_key = base64_encode(md5(sha1($ckey)));

	$last = strlen($ckey) - 1;

	$c2_key = substr($c2_key, 1, round(ord($ckey{$last})/7));

	$c3_key = base64_encode(sha1(md5($c_key).md5($c2_key)));

	$mid = round($last/2);

	$c3_key = substr($c3_key, 1, round(ord($ckey{$mid})/9));

	$c_key = $c_key.$c2_key.$c3_key;

	$c_key = base64_encode($c_key);

	for($i = 0; $i < strlen($c_key); $i++)

		$keys[] = $c_key[$i];

	} 

	

function encrypt($string)

	{

	global $keys;

	$string = base64_encode($string);

	$keys = $keys;

	for($i = 0; $i < strlen($string); $i++)

		{

		$id = $i % count($keys);

		$ord = ord($string{$i});

		$ord = $ord OR ord($keys[$id]);

		$id++;

		$ord = $ord AND ord($keys[$id]);

		$id++;

		$ord = $ord XOR ord($keys[$id]);

		$id++;

		$ord = $ord + ord($keys[$id]);

		$string{$i} = chr($ord);

		}

	return base64_encode($string);

	}

	

function decrypt($string)

	{

	global $keys;

	$string = base64_decode($string);

	$keys = $keys;

	for($i = 0; $i < strlen($string); $i++)

		{

		$id = $i % count($keys);

		$ord = ord($string{$i});

		$ord = $ord XOR ord($keys[$id]);

		$id++;

		$ord = $ord AND ord($keys[$id]);

		$id++;

		$ord = $ord OR ord($keys[$id]);

		$id++;

		$ord = $ord - ord($keys[$id]);

		$string{$i} = chr($ord);

		}

	return base64_decode($string);

	}
	
	
/** Input Parameters are
** $string- The string you want to encrypt
** $key- string(it�s like a password)
**/

function encrypt1($string,$key) {
$result = '';
for($i=0; $i<strlen($string); $i++) {
$char = substr($string, $i, 1);
$keychar = substr($key, ($i % strlen($key))-1, 1);
$char = chr(ord($char)+ord($keychar));
$result.=$char;
}

return base64_encode($result); 
}


/** Input Parameters are
** $string- The string you want to encrypt
** $key- string(it�s like a password)
**/

function decrypt1($string,$key) {
$result = '';
$string = base64_decode($string);

for($i=0; $i<strlen($string); $i++) {
$char = substr($string, $i, 1);
$keychar = substr($key, ($i % strlen($key))-1, 1);
$char = chr(ord($char)-ord($keychar));
$result.=$char;
}

return $result; 
}


//function convert($str,$ky='')
//{
//	if($ky=='')
//		return $str;
//	$ky=str_replace(chr(32),'',$ky);
//	if(strlen($ky)<8)
//		exit('key error');
//	$kl=strlen($ky)<32?strlen($ky):32;
//	$k=array();
//	for($i=0;$i<$k1;$i++)
//	{
//		$k[$i]=ord($ky{$i})&0*1F;
//	}
//	$j=0;
//	for($i=0;$i<strlen($str);$i++)
//	{
//		$e=ord($str{$i});
//		$str{$i}=$e&0xE0?chr($e^$k[$j]):chr($e);
//		$j++;$j=$j==$kl?0:$j;
//	}
//return $str;
//}

	
	function removePicture($file,$uploadPath)
	{	

	if(file_exists($uploadPath.$file) && $file!="")
		unlink($uploadPath.$file);
	}
	
	
	
	function resizeImage($filename, $max_width, $max_height, $newfilename="", $withSampling=true)   
{   
   $width = 0;  
   $height = 0;  
  
   $newwidth = 0;  
   $newheight = 0;  
  
   // If no new filename was specified then use the original filename  
   if($newfilename == "")   
   {  
      $newfilename = $filename;   
   }  
      
   // Get original sizes   
   list($width, $height) = getimagesize($filename);   
      
   if ($width > $height)  
   {  
      // We're dealing with max_width  
      if ($width > $max_width)  
      {  
         $newwidth = $width * ($max_width / $width);  
         $newheight = $height * ($max_width / $width);  
      }  
      else  
      {  
         // No need to resize  
         $newwidth = $width;  
         $newheight = $height;  
      }  
   }  
   else  
   {  
      // Deal with max_height  
      if ($height > $max_height)  
      {  
         $newwidth = $width * ($max_height / $height);  
         $newheight = $height * ($max_height / $height);  
      }  
      else  
      {  
         // No need to resize  
         $newwidth = $width;  
         $newheight = $height;  
      }  
   }  
  
   // Create a new image object   
   $thumb = imagecreatetruecolor($newwidth, $newheight);   
  
   // Load the original based on it's extension  
   $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));  
  
   if($ext=='jpg' || $ext=='jpeg')   
   {  
      $source = imagecreatefromjpeg($filename);   
   }  
   else if($ext=='gif')   
   {  
      $source = imagecreatefromgif($filename);   
   }  
   else if($ext=='png')  
   {   
      $source = imagecreatefrompng($filename);   
   }  
   else  
   {  
      // Fail because we only do JPG, JPEG, GIF and PNG  
      return FALSE;  
   }  
      
   // Resize the image with sampling if specified  
   if($withSampling)   
   {  
      imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);   
   }  
   else   
   {     
      imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);   
   }  
          
   // Save the new image   
   if($ext=='jpg' || $ext=='jpeg')   
   {  
      return imagejpeg($thumb, $newfilename);   
   }  
   else if($ext=='gif')   
   {  
      return imagegif($thumb, $newfilename);   
   }  
   else if($ext=='png')   
   {  
      return imagepng($thumb, $newfilename);   
   }  
}   

	/*end image upload functions*/
	
	
function dateDiff($dformat, $endDate, $beginDate)
{
$date_parts1=explode($dformat, $beginDate);
$date_parts2=explode($dformat, $endDate);
$start_date=gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
$end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
return ($end_date - $start_date);
}

/**
 * Simple function to take in a date format and return array of associated formats for each date element
 * @return array
 * @param string $strFormat
 *
 * Example: Y/m/d g:i:s becomes
 * Array
 * (
 *     [year] => Y
 *     [month] => m
 *     [day] => d
 *     [hour] => g
 *     [minute] => i
 *     [second] => s
 * )
 */
function dateParseFromFormat($stFormat, $stData)
{
    $aDataRet = array();
    $aPieces = split('[:/.\ \-]', $stFormat);
    $aDatePart = split('[:/.\ \-]', $stData);
    foreach($aPieces as $key=>$chPiece)   
    {
        switch ($chPiece)
        {
            case 'd':
            case 'j':
                $aDataRet['day'] = $aDatePart[$key];
                break;
               
            case 'F':
            case 'M':
            case 'm':
            case 'n':
                $aDataRet['month'] = $aDatePart[$key];
                break;
               
            case 'o':
            case 'Y':
            case 'y':
                $aDataRet['year'] = $aDatePart[$key];
                break;
           
            case 'g':
            case 'G':
            case 'h':
            case 'H':
                $aDataRet['hour'] = $aDatePart[$key];
                break;   
               
            case 'i':
                $aDataRet['minute'] = $aDatePart[$key];
                break;
               
            case 's':
                $aDataRet['second'] = $aDatePart[$key];
                break;           
        }
       
    }
    return $aDataRet;
}

?>