<?php



/**
 * Handle IRIs according to RFC 3987 <http://www.ietf.org/rfc/rfc3987.txt>
 * 
 * This class provides methods that allow IRI (URI, URL, URN, etc.) parsing, 
 * validation and other useful stuff according to the latest IRI specification 
 * in RFC 3987 <http://www.ietf.org/rfc/rfc3987.txt> and related 
 * scheme specific RFCs.
 * 
 * The class supports automatical IRI/URI scheme detection and forcing
 * a IRI/URI scheme which a passed IRI should be parsed or validated by.
 * Scheme specific parsing or validation will be executed automatically.
 * 
 * IRI/URI schemes that are currently supported:
 * - http (Hypertext Transfer Protocol, URL)
 * - https (Hypertext Transfer Protocol Secure, URL)
 * - ftp (File Transfer Protocol)
 * - urn (Uniform Resource Name, URN)
 * - file (Host-specific file names)
 * 
 * IRI/URI schemes that are planned to be implemented in future versions:
 * - dns (Domain Name System)
 * - mailto (Electronic Mail Address)
 * - tel (Telephone)
 * - other important IRI/URI schemes registered at IANA
 *   <http://www.iana.org/assignments/uri-schemes.html> 
 * 
 * For parsing IRI component "authority" the class supports all types of 
 * host names, including IPv4, IPv6, IPvFuture and domain names.
 * 
 * This class parses with the use of PHP configuration standard values, like:
 * - arg_separator.input (HTTP query segment separators, like '&' or ';')
 * - internal encoding settings
 * - other settings (a detailed list will follow)
 * So the behaviour of the parsing may differ under another configuration.
 * 
 * This class requires PHP >= 4.3.0.
 * 
 * If you find this software useful and/or use it in your own software,
 * please send me a message and/or donate some money for future development 
 * using my PayPal e-mail <mail@andreas-hahn.de>. Thank you very much!
 * 
 * Please report bugs to me via e-mail <mail@andreas-hahn.de>.
 * 
 * Copyright 2010 Andreas M. Hahn
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * @package AMH_IRI
 * @version 1.1
 * @internal Requires PHP >= 4.3.0
 * @author Andreas M. Hahn <mail@andreas-hahn.de>
 * @copyright Copyright 2010 Andreas M. Hahn
 * @license http://www.gnu.org/licenses/ GNU General Public License
 */



//error_reporting(0);



class AMH_IRI
{










	/*public*/ function AMH_IRI()
	{
	}










	/*public*/ function is_uri($string) // Multi-byte character safe URI string charset validation
	{	
		// Bytewise search for characters that are not supported by a URI according to RFC 3986
		// <http://www.ietf.org/rfc/rfc3986.txt>.
		// Aborts the search on match and returns FALSE

		for ($i = 0; $i < strlen($string); $i++) // >= PHP 4
		{
			$char = ord(substr($string, $i, 1)); // >= PHP 4
			if ($char < 32 || $char > 126)
			{
				return false;
			}
		}
		return true;
	}










	/*public*/ function encode($string)
	{
		// This function IRI-encodes a string according to RFC 3987 since PHP >= 4.0.

		$string = (string) $string;



		// PHP function 'rawurlencode' IRI-encodes a string correctly according to 
		// RFC 3987 since PHP >= 5.3. Before it incorrectly additionally IRI-encodes 
		// '~' (Tilde) to '%7E'.
	
		$string = rawurlencode($string); // PHP >= 4.0
	
	
	
		// Additionally incorrectly IRI-encoded '~' (Tilde) will be IRI-decoded
		// from case-insensitive '%7E' to '~'.
	
		$string = preg_replace('/%7E/i', '~', $string); // PHP >= 4.0
	
	
	
		// Return RFC 3987 conform IRI-encoded string
	
		return $string;
	}










	/*public*/ function decode($string)
	{
		// This function IRI-decodes a string according to RFC 3987 since PHP >= 4.0.

		$string = (string) $string;



		// PHP function 'rawurldecode' IRI-decodes a string correctly according to 
		// RFC 3987 since PHP >= 4.0.

		$string = rawurldecode($string); // PHP >= 4.0



		// Return RFC 3987 conform IRI-decoded string

		return $string;
	}










	/*public*/ function lowercase($string)
	{
		// This function makes a parsed IRI string component lowercase since PHP >= 4.3.0.

		$string = (string) $string;



		$encoding = mb_detect_encoding($string); // PHP >= 4.0.6

		$string = mb_strtolower($string, $encoding); // PHP >= 4.3.0



		return $string;
	}










	/*public*/ function parse($iri, $forced_scheme = '', $normalize = true, $type = true)
	{
		// Initialize variable that holds the "scheme" of the passed IRI

		$iri_scheme = '';



		// Initialize associative array that will hold the parsed IRI components

		$parsed_iri = array();



		// Store IRI in associative array
		
		$parsed_iri['IRI'] = $iri;



		// Store IS_URI value in associative arrays

		$parsed_iri['ISURI'] = $this->is_uri($iri);



		// The following regular expression matches IRI components of a well-formed IRI according to RFC 3987.
		// It doesn't validate a URI but matches any string.
		// It was copied from RFC 3986, Appendix B. <http://www.ietf.org/rfc/rfc3986.txt>

		preg_match('!^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?!', $iri, $component); // >= PHP 4



		// If IRI has a "scheme" component, store it in associative array

		if (isset($component[2]) && $component[2] != '') // >= PHP 4
		{
			$iri_scheme = $this->lowercase($this->decode($component[2]));

			$parsed_iri['SCHEME'] = ($normalize) ? $iri_scheme : $component[2];
		}



		// Initialize parse scheme
		
		$scheme = ($forced_scheme == '') ? $iri_scheme : $forced_scheme;



		// If requested scheme is not supported, trigger a warning

		if (!($scheme == 'http' || $scheme == 'https' || $scheme == '' || $scheme == 'ftp' || $scheme == 'urn' || $scheme == 'file'))
		{
			//trigger_error('IRI/URI scheme \'' . $scheme . '\' not supported. Set parameter \'forced_scheme\' to a supported IRI/URI scheme to force parsing.', E_USER_WARNING); // >= PHP 4.0.1
		}



		// If IRI has an "authority" component, store it in associative array

		if (isset($component[4]) && $component[4] != '') // >= PHP 4
		{
			$parsed_iri['AUTHORITY'] = $component[4];

			// Parse IRI "authority" component using current scheme

			$parsed_authority = $this->parse_authority($parsed_iri['AUTHORITY'], $scheme, $normalize);

			foreach ($parsed_authority as $key => $value)
			{
				$parsed_iri[$key] = $value;
			}
		}



		// Store IRI "path" component in associative array

		$parsed_iri['PATH'] = $component[5];

		// Parse IRI "path" component using current scheme

		$parsed_path = $this->parse_path($parsed_iri['PATH'], $scheme, $normalize);

		foreach ($parsed_path as $key => $value)
		{
			$parsed_iri[$key] = $value;
		}

		/*
		if ($normalize)
		{
			$parsed_iri['PATH'] = '';

			foreach ($parsed_iri['SEGMENT'] as $key => $value)
			{
				$parsed_iri['PATH'] .= '/' . $value;
			}
		}
		*/



		// If IRI has a "query" component, store it in associative array

		if (isset($component[7]) && $component[7] != '') // >= PHP 4
		{
			$parsed_iri['QUERY'] = $component[7];

			// Parse IRI "query" component using current scheme

			$parsed_query = $this->parse_query($parsed_iri['QUERY'], $scheme, $normalize);

			foreach ($parsed_query as $key => $value)
			{
				$parsed_iri[$key] = $value;
			}
		}



		// If IRI has a "fragment" component, store it in associative array

		if (isset($component[9]) && $component[9] != '') // >= PHP 4
		{
			$parsed_iri['FRAGMENT'] = ($normalize) ? $this->decode($component[9]) : $component[9];

			// Parse IRI "fragment" component using current scheme

			$parsed_fragment = $this->parse_fragment($parsed_iri['FRAGMENT'], $scheme, $normalize);

			foreach ($parsed_fragment as $key => $value)
			{
				$parsed_iri[$key] = $value;
			}
		}



		// Type IRI, if $type is true

		if ($type)
		{
			$typed_iri = $this->type($parsed_iri);

			foreach ($typed_iri as $key => $value)
			{
				$parsed_iri[$key] = $value;
			}
		}



		// Return associative array that holds the parsed IRI components

		return $parsed_iri;
	}










	/*public*/ function parse_authority($authority, $forced_scheme = '', $normalize = true)
	{
		// Initialize array variable that will contain the parsed IRI authority components

		$parsed_authority = array();



		// General IRI authority parsing (see below for scheme specific parsing)

		// The following regular expression matches IRI authority components of a well-formed IRI authority according to RFC 3987.
		// It doesn't validate a IRI authority but matches any string.
		// It supports IPv4, IPv6, IPvFuture and domain names.

		preg_match('!^(([^\@]+)\@)?(.*\]|[^:]*)(:(.*))?!', $authority, $component); // >= PHP 4



		// If IRI authority has a "userinfo" component, store it in associative array

		if (isset($component[2]) && $component[2] != '') // >= PHP 4
		{
			$parsed_authority['USERINFO'] = $component[2];

			$parsed_authority_userinfo = $this->parse_authority_userinfo($parsed_authority['USERINFO'], $forced_scheme, $normalize);

			foreach ($parsed_authority_userinfo as $key => $value)
			{
				$parsed_authority[$key] = $value;
			}
		}



		// Store IRI authority "host" component in associative array

		$parsed_authority['HOST'] = $component[3];

		$parsed_authority_host = $this->parse_authority_host($parsed_authority['HOST'], $forced_scheme, $normalize);

		foreach ($parsed_authority_host as $key => $value)
		{
			$parsed_authority[$key] = $value;
		}

		/*
		if ($normalize)
		{
			if (isset($parsed_authority['REGNAME']))
			{
				$parsed_authority['HOST'] = $parsed_authority['REGNAME'];
			}
		}
		*/


		// If IRI authority has a "port" component, store it in associative array

		if (isset($component[5]) && $component[5] != '') // >= PHP 4
		{
			$parsed_authority['PORT'] = ($normalize) ? $this->decode($component[5]) : $component[5];
		}



		// Scheme specific IRI authority parsing

		if ($forced_scheme == 'http' || $forced_scheme == 'https' || $forced_scheme == '')
		{
		}

		else if ($forced_scheme == 'ftp')
		{
		}

		else if ($forced_scheme == 'urn')
		{
		}

		else if ($forced_scheme == 'file')
		{
		}



		// Return associative array that holds the parsed IRI authority components

		return $parsed_authority;
	}










	/*public*/ function parse_authority_userinfo($userinfo, $forced_scheme = '', $normalize = true)
	{
		// Initialize array variable that will contain the parsed IRI authority userinfo components

		$parsed_userinfo = array();



		// General IRI authority userinfo parsing (see below for scheme specific parsing)



		// Scheme specific IRI authority userinfo parsing

		if ($forced_scheme == 'http' || $forced_scheme == 'https' || $forced_scheme == '' || $forced_scheme == 'ftp')
		{
			// The following regular expression matches HTTP authority userinfo components 
			// of a well-formed HTTP authority userinfo.
			// It doesn't validate a HTTP authority userinfo but matches any string.

			preg_match('!^([^:]*)(:(.*))?!', $userinfo, $component); // >= PHP 4

	
	
			// Store IRI authority userinfo "user" component in associative array

			$parsed_userinfo['USER'] = ($normalize) ? $this->decode($component[1]) : $component[1];



			// If IRI authority userinfo has a "pass" component, store it in associative array

			if (isset($component[3]) && $component[3] != '')
			{
				$parsed_userinfo['PASS'] = ($normalize) ? $this->decode($component[3]) : $component[3];
			}	
		}

		else if ($forced_scheme == 'urn')
		{
		}

		else if ($forced_scheme == 'file')
		{
		}



		// Return associative array that holds the parsed IRI authority userinfo components

		return $parsed_userinfo;
	}










	/*public*/ function parse_authority_host($host, $forced_scheme = '', $normalize = true)
	{
		// Initialize array variable that will contain the parsed IRI authority host components

		$parsed_host = array();



		// General IRI authority host parsing (see below for scheme specific parsing)

		// The following regular expression matches IRI authority host "IPliteral"
		// of a well-formed IRI authority host.

		preg_match('!^(\[(.*)\])\z!', $host, $ipliteral); // >= PHP 4



		if (isset($ipliteral[1]))
		{
			// If IRI authority host is an "IP-Literal", store it in associative array

			$parsed_host['IPLITERAL'] = $ipliteral[1];

			preg_match('!^\[([vV].*)\]\z!', $parsed_host['IPLITERAL'], $ipvfuture); // >= PHP 4

			if (isset($ipvfuture[1]))
			{
				// If IRI authority host is an "IPvFuture"

				$parsed_ipvfuture = $this->parse_ipvfuture($ipvfuture[1], $normalize);

				foreach ($parsed_ipvfuture as $key => $value)
				{
					$parsed_host[$key] = $value;
				}
			}

			else
			{
				// If IRI authority host is an "IPv6"

				$parsed_ipv6 = $this->parse_ipv6($ipliteral[2], $normalize);

				foreach ($parsed_ipv6 as $key => $value)
				{
					$parsed_host[$key] = $value;
				}
			}
		}

		else
		{
			$parsed_ipv4 = $this->parse_ipv4($host, $normalize);

			if (isset($parsed_ipv4['IPV4_INT']) && !isset($parsed_ipv4['IPV4_PREFIX']))
			{
				// If IRI authority host is an "IPv4"

				foreach ($parsed_ipv4 as $key => $value)
				{
					$parsed_host[$key] = $value;
				}
			}
			
			else
			{
				// If IRI authority host is a reg name

				$parsed_host['REGNAME'] = $host;

				$parsed_domain = $this->parse_domain($parsed_host['REGNAME'], $normalize);

				foreach ($parsed_domain as $key => $value)
				{
					$parsed_host[$key] = $value;
				}

				/*
				if ($normalize)
				{
					$parsed_host['REGNAME'] = $parsed_host['DOMAIN'];
				}
				*/

			}			

		}



		// Scheme specific IRI authority host parsing

		if ($forced_scheme == 'http' || $forced_scheme == 'https' || $forced_scheme == '')
		{
		}

		else if ($forced_scheme == 'ftp')
		{
		}

		else if ($forced_scheme == 'urn')
		{
		}

		else if ($forced_scheme == 'file')
		{
		}



		// Return associative array that holds the parsed IRI authority host components

		return $parsed_host;
	}










	/*public*/ function parse_ipvfuture($ipvfuture, $normalize = true)
	{
		// Initialize array variable that will contain the parsed IPvFuture components

		$parsed_ipvfuture = array();



		// Store IPvFuture in associative array

		$parsed_ipvfuture['IPVFUTURE'] = $ipvfuture;



		/* Not yet implemented.
		
		// The following regular expression matches IPvFuture components 
		// of a well-formed IPvFuture.
		// It doesn't validate a IPvFuture but matches any string.

		preg_match_all('!([^\:]+)!', $ipvfuture, $component); // >= PHP 4



		// Store every IPvFuture component in associative array

		foreach ($component[0] as $block)
		{
			$parsed_ipvfuture['IPVFUTURE_BLOCK'][] = $block;
		}
		*/



		// Return associative array that holds the parsed IPvFuture components

		return $parsed_ipvfuture;
	}










	/*public*/ function parse_ipv6($ipv6, $normalize = true)
	{
		// Initialize array variable that will contain the parsed IPv6 components

		$parsed_ipv6 = array();



		// Store IPv6 in associative array
		
		$parsed_ipv6['IPV6'] = $ipv6;



		/* Not yet implemented.
		
		// The following regular expression matches IPv6 components 
		// of a well-formed IPv6.
		// It doesn't validate a IPv6 but matches any string.

		preg_match_all('!([^\:]+)!', $ipv6, $component); // >= PHP 4



		// Store every IPv6 component in associative array

		foreach ($component[0] as $block)
		{
			$parsed_ipv6['IPV6_BLOCK'][] = $block;
		}
		*/



		// Return associative array that holds the parsed IPv6 components

		return $parsed_ipv6;
	}










	/*public*/ function parse_domain($domain, $normalize = true)
	{
		// Initialize array variable that will contain the parsed domain components

		$parsed_domain = array();



		// Store domain in associative array
		
		$parsed_domain['DOMAIN'] = $domain;



		// The following regular expression matches domain components 
		// of a well-formed domain.
		// It doesn't validate a domain but matches any string.

		preg_match_all('!([^\.]+)!', $domain, $component); // >= PHP 4



		// Store root domain label in associative array

		$parsed_domain['LABEL'][] = '';
		


		// Store every domain label in associative array

		$labels = array_reverse($component[0]); // >= PHP 4
		
		foreach ($labels as $label)
		{
			$parsed_domain['LABEL'][] = ($normalize) ? $this->lowercase($this->decode($label)) : $label;
		}
		
		/*
		if ($normalize)
		{
			$parsed_domain['DOMAIN'] = '';

			foreach ($parsed_domain['LABEL'] as $key => $value)
			{
				$parsed_domain['DOMAIN'] = $value . '.' . $parsed_domain['DOMAIN'];
			}
			
			$parsed_domain['DOMAIN'] = trim($parsed_domain['DOMAIN'], '.');
		}
		*/



		// Return associative array that holds the parsed domain components

		return $parsed_domain;
	}










	/*public*/ function parse_ipv4($ipv4, $normalize = true)
	{
		// Initialize array variable that will contain the parsed IPv4 components

		$parsed_ipv4 = array();



		// The following regular expression matches an IPv4 address.

		preg_match('!^((.*)\.)?(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})(\/(.*))?\z!', $ipv4, $component); // >= PHP 4

		if (isset($component[3]) && isset($component[4]) && isset($component[5]) && isset($component[6])
			&& $component[3] < 256 && $component[4] < 256 && $component[5] < 256 && $component[6] < 256)
		{
			// Store IPv4 in associative array
			
			$parsed_ipv4['IPV4'] = $ipv4;

			// Store IPv4 components in associative array

			$parsed_ipv4['IPV4_INT'][] = (int) $component[3];
			$parsed_ipv4['IPV4_INT'][] = (int) $component[4];
			$parsed_ipv4['IPV4_INT'][] = (int) $component[5];
			$parsed_ipv4['IPV4_INT'][] = (int) $component[6];

			if (isset($component[8]) && $component[8] != '')
			{
				$parsed_ipv4['IPV4_SUFFIX'][] = $component[8];
			}	

			if (isset($component[1]) && $component[1] != '')
			{
				$parsed_ipv4['IPV4_PREFIX'] = $component[1];
			}
		}



		// Return associative array that holds the parsed IPv4 components

		return $parsed_ipv4;
	}










	/*public*/ function parse_path($path, $forced_scheme = '', $normalize = true)
	{
		// Initialize array variable that will contain the parsed IRI path components

		$parsed_path = array();



		// Scheme specific IRI path pre-parsing

		if ($forced_scheme == 'ftp')
		{
			// The following regular expression matches FTP path components 
			// of a well-formed FTP path.
			// It doesn't validate a FTP path but matches any string.

			preg_match('!^([^;]*)(;(.*))?!', $path, $component); // >= PHP 4

	
	
			// If FTP path has a "ftptype" component

			if (isset($component[3]) && $component[3] != '')
			{
				preg_match('!^([^=]*)(=(.*))?!', $component[3], $type); // >= PHP 4

				// Store "ftptype" in associative array

				$parsed_path['FTPTYPE'] = ($normalize) ? $this->decode($type[3]) : $type[3];

				// Trim "ftptype" from FTP path

				$path = $component[1];
			}
		}



		// General IRI path parsing (see below or above for scheme specific parsing)

		// The following regular expression matches IRI path segments 
		// of a well-formed IRI path according to RFC 3987.
		// It doesn't validate a IRI path but matches any string.

		preg_match_all('!([^/]+)!', $path, $component); // >= PHP 4



		// Store every IRI path "segment" component in associative array

		if (sizeof($component[0]) > 0)
		{
			$parsed_path['SEGMENT'] = array();

			foreach ($component[0] as $segment)
			{
				/*
				if ($normalize)
				{
					$segment_normalized = $this->decode($segment);
					
					if ($segment_normalized != '.')
					{
						if ($segment_normalized == '..')
						{
							array_pop($parsed_path['SEGMENT']);
						}
						
						else
						{
							$parsed_path['SEGMENT'][] = $segment_normalized;
						}
					}
				}

				else
				{
				*/
					$parsed_path['SEGMENT'][] = $segment;
				/*
				}
				*/
			}
		}



		// Scheme specific IRI path parsing

		if ($forced_scheme == 'http' || $forced_scheme == 'https' || $forced_scheme == '')
		{
		}

		else if ($forced_scheme == 'ftp')
		{
		}

		else if ($forced_scheme == 'urn')
		{
			// The following regular expression matches IRI path NID and NSS 
			// of a well-formed IRN/URN according to RFC 2141.
			// It doesn't validate a IRI path but matches any string.

			preg_match_all('!^([^:]*)(:(.*))?!', $path, $component); // >= PHP 4



			// If IRI path has a "NID" component, store it in associative array

			if (isset($component[1]) && $component[1] != '')
			{
				$parsed_path['NID'] = ($normalize) ? $this->decode($component[1][0]) : $component[1][0];
			}



			// If IRI path has a "NSS" component, store it in associative array

			if (isset($component[3]) && $component[3] != '')
			{
				$parsed_path['NSS'] = ($normalize) ? $this->decode($component[3][0]) : $component[3][0];
			}
		}

		else if ($forced_scheme == 'file')
		{
		}



		// Return associative array that holds the parsed IRI path components

		return $parsed_path;
	}










	/*public*/ function parse_query($query, $forced_scheme = '', $normalize = true)
	{
		// Initialize array variable that will contain the parsed IRI query components

		$parsed_query = array();



		// General IRI query parsing (see below for scheme specific parsing)



		// Scheme specific IRI query parsing

		if ($forced_scheme == 'http' || $forced_scheme == 'https' || $forced_scheme == '')
		{
			// Substitude "&amp;" by "&", if $normalize = true

			if ($normalize)
			{
				$query = preg_replace('/&amp;/i', '&', $query); // PHP >= 4.0
			}


			/*

			// Get value of PHP directive 'arg_separator.input'

			$association_separators = ini_get('arg_separator.input'); // >= PHP 4.0.5



			// Set association separators to '&', if it was set incorrectly before

			if ($association_separators == '')
			{
				$association_separators = '&';
			}



			// The following regular expression matches HTTP query string segments of a well-formed HTTP query string.
			// It doesn't validate a HTTP query string but matches any string.

			preg_match_all('!([^' . $association_separators . ']+)!', $query, $component); // >= PHP 4



			// Store every IRI query "association" component in associative array

			foreach ($component[0] as $n => $association)
			{
				$parsed_query['ASSOCIATION'][$n] = $association;



				// The following regular expression matches HTTP query association parameters of a well-formed HTTP query segment.
				// It doesn't validate a HTTP query association but matches any string.

				preg_match('!^([^=]*)(=(.*))?!', $association, $association_component); // >= PHP 4

				$parsed_query['PARAMETER'][$n] = $association_component[1];

				$parsed_query['ARGUMENT'][$n] = ($normalize) ? $this->decode($association_component[3]) : $association_component[3];



				// The following section contains multiple errors, regarding handling of a IRI-encoded IRI 
				// query string and is therefore temporarily replaced by PHP standard function 'mb_parse_str'
				// which actually produces exactly the same array structure you'd find in $_GET variable.
				
				preg_match('!^([^\[]+)(\[(.*))?!', $association_component[1], $parameter_component); // >= PHP 4

				while (count($parameter_component) > 0)
				{
					$parsed_query['KEY'][$n][] = ($normalize) ? $this->decode($parameter_component[1]) : $parameter_component[1];

					preg_match('!^\[([^\]]*)\](.*)?!', $parameter_component[2], $parameter_component); // >= PHP 4
				}



				// For further implementation of a 'mb_parse_str' functionality with user defined association separators (e.g. '&', ';'), use this hint:
				//$parsed_query['GET'][$parsed_query['KEY'][$n][0]][$parsed_query['KEY'][$n][1]][$parsed_query['KEY'][$n][2]] = $parsed_query['ARGUMENT'][$n];
				
			}

			*/



			// Parse IRI query string and create an associative array, 
			// which has exactly the same structure like $_GET had
			// if you used your query string for the current HTTP request.
			// Always returns normalized string, even if $normalize == false
			
			mb_parse_str($query, $parsed_query['GET']); // >= PHP 4.0.6



		}

		else if ($forced_scheme == 'ftp')
		{
		}

		else if ($forced_scheme == 'urn')
		{
		}

		else if ($forced_scheme == 'file')
		{
		}



		// Return associative array that holds the parsed IRI query components

		return $parsed_query;
	}










	/*public*/ function parse_fragment($fragment, $forced_scheme = '', $normalize = true)
	{
		// Initialize array variable that will contain the parsed IRI fragment components

		$parsed_fragment = array();



		// General IRI fragment parsing (see below for scheme specific parsing)



		// Scheme specific IRI fragment parsing

		if ($forced_scheme == 'http' || $forced_scheme == 'https' || $forced_scheme == '')
		{
		}

		else if ($forced_scheme == 'ftp')
		{
		}

		else if ($forced_scheme == 'urn')
		{
		}

		else if ($forced_scheme == 'file')
		{
		}



		// Return associative array that holds the parsed IRI fragment components

		return $parsed_fragment;
	}










	/*public*/ function type($parsed_iri = array())
	{
		// Identify Google Search IRI
		// Must be updated regularily

		if ((isset($parsed_iri['LABEL'][2]) && $this->lowercase($parsed_iri['LABEL'][2]) == 'google')
			|| (isset($parsed_iri['LABEL'][3]) && $this->lowercase($parsed_iri['LABEL'][3]) == 'google')
			)
		{
			if (isset($parsed_iri['SEGMENT'][0]))
			{
				$google_segment_0 = $this->lowercase($parsed_iri['SEGMENT'][0]);

				if (in_array($google_segment_0, 
					array('search', 'url', 'ie', 'webhp', 'images', 'imgres', 'imghp', 'maps', 'place', 'm', 'custom', 'translate', 'thread', 'analytics')))
				{
					$parsed_iri['GOOGLESEARCH'] = $google_segment_0;
				}
			}
		}



		return $parsed_iri;
	}










	/*public*/ function javascript()
	{
		?>

        function AMH_IRI()
        {
            this.encode = function(str)
            {
                str = (str + '').toString();
                return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A'); // JS >= 1.5
            }
        
            this.decode = function(str)
            {
                return decodeURIComponent(str); // JS >= 1.5
            }
        }

		<?php
	}










}



?>