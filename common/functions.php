<?php 
//gets total credits of given user id
function GetTotalCreditsById($uid)
{
	if(isset($uid) && $uid!="" )
	{ 
			$id=$uid;
			$accountDAO=new user_accountDAO();
			$accountVO=new user_accountVO();
			
			 $programSignup=$accountDAO->fetchProgramSignUpCreditsByUserId($id);
			
			$clickingAds=$accountDAO->fetchAdvCreditsByUserId($id);
			//print_r($clickingAds);
			$referNewMemberClick=$accountDAO->fetchReferMemClickCreditsByUserId($id);
			$referNewMemberSignup=$accountDAO->fetchReferMemSignupCreditsByUserId($id);
			$purchageCredits=$accountDAO->fetchPurchagedCreditsByUserId($id);
			$referralRequets=$accountDAO->fetchReferralCreditsByUserId($id);
			$advPackages=$accountDAO->fetchAdvPackageCreditsByUserId($id);
			$jackpotGame=$accountDAO->fetchJackpotGamePackageCreditsByUserId($id);
			
			
			$earned=$programSignup+$clickingAds+$referNewMemberClick+referNewMemberSignup+$purchageCredits;
			$spend=$referralRequets+$advPackages+$jackpotGame;
			
			$totalCredits=$earned-$spend;
			if($totalCredits=='')
			{
				$totalCredits='0.00';
			}
			return $totalCredits;
	}
}
// get total earned credits of any given user
function GetTotalEarnedCreditsById($uid)
{
	if(isset($uid) && $uid!="" )
	{ 
			$id=$uid;
			$accountDAO=new user_accountDAO();
			$accountVO=new user_accountVO();
			
			 $programSignup=$accountDAO->fetchProgramSignUpCreditsByUserId($id);
			
			$clickingAds=$accountDAO->fetchAdvCreditsByUserId($id);
			//print_r($clickingAds);
			$referNewMemberClick=$accountDAO->fetchReferMemClickCreditsByUserId($id);
			$referNewMemberSignup=$accountDAO->fetchReferMemSignupCreditsByUserId($id);
			$purchageCredits=$accountDAO->fetchPurchagedCreditsByUserId($id);
			$referralRequets=$accountDAO->fetchReferralCreditsByUserId($id);
			$advPackages=$accountDAO->fetchAdvPackageCreditsByUserId($id);
			$jackpotGame=$accountDAO->fetchJackpotGamePackageCreditsByUserId($id);
			
			
			$earned=$programSignup+$clickingAds+$referNewMemberClick+referNewMemberSignup+$purchageCredits;
			$spend=$referralRequets+$advPackages+$jackpotGame;
			
			$totalCredits=$earned;
			
			return formatDoubleAmount($totalCredits);
	}
}

//get total spend credits of any given user
function GetTotalSpendCreditsById($uid)
{
	if(isset($uid) && $uid!="" )
	{ 
			$id=$uid;
			$accountDAO=new user_accountDAO();
			$accountVO=new user_accountVO();
			
			 $programSignup=$accountDAO->fetchProgramSignUpCreditsByUserId($id);
			
			$clickingAds=$accountDAO->fetchAdvCreditsByUserId($id);
			//print_r($clickingAds);
			$referNewMemberClick=$accountDAO->fetchReferMemClickCreditsByUserId($id);
			$referNewMemberSignup=$accountDAO->fetchReferMemSignupCreditsByUserId($id);
			$purchageCredits=$accountDAO->fetchPurchagedCreditsByUserId($id);
			$referralRequets=$accountDAO->fetchReferralCreditsByUserId($id);
			$advPackages=$accountDAO->fetchAdvPackageCreditsByUserId($id);
			$jackpotGame=$accountDAO->fetchJackpotGamePackageCreditsByUserId($id);
			
			
			$earned=$programSignup+$clickingAds+$referNewMemberClick+referNewMemberSignup+$purchageCredits;
			$spend=$referralRequets+$advPackages+$jackpotGame;
			
			$totalCredits=$spend;
			
			return formatDoubleAmount($totalCredits);
	}
}

//earned from referring member By memberid
function GetTotalEarnedFromMember($uid)
{
	if(isset($uid) && $uid!="" )
	{ 
			$percentageearning=getCodeValueByLabel($label="referral_earning_percentage");
			$id=$uid;
			$accountDAO=new user_accountDAO();
			$accountVO=new user_accountVO();
			
			 $programSignup=$accountDAO->fetchProgramSignUpCreditsByUserId($id);
			
			$clickingAds=$accountDAO->fetchAdvCreditsByUserId($id);
			//print_r($clickingAds);
			//$referNewMember=$accountDAO->fetchReferMemCreditsByUserId($id);
			//$purchageCredits=$accountDAO->fetchPurchagedCreditsByUserId($id);
			//$referralRequets=$accountDAO->fetchReferralCreditsByUserId($id);
			//$advPackages=$accountDAO->fetchAdvPackageCreditsByUserId($id);
			//$jackpotGame=$accountDAO->fetchJackpotGamePackageCreditsByUserId($id);
			
			
			$earned=($percentageearning/100)*$programSignup+($percentageearning/100)*$clickingAds;
			//$spend=$referralRequets+$advPackages+$jackpotGame;
			
			$totalCredits=$earned;
			if($totalCredits=='')
			{
				$totalCredits='0.00';
			}
			return  formatDoubleAmount($totalCredits);
	}
}

function getreferralEarningFromParticularMember($user_id,$txnid)
{
	
	$accountDAO=new user_accountDAO();
	$accountVO=new user_accountVO();
	
	$clickearning=$accountDAO->fetchTotalCreditsByTypeAndTrxnId($user_id,$txnid,$type='referring new members click');
	$signupearning=$accountDAO->fetchTotalCreditsByTypeAndTrxnId($user_id,$txnid,$type='referring new members signup');
	
	$total=$clickearning+$signupearning;
	
	return formatDoubleAmount($total);
}

//earned from referring member
function GetTotalReferralEarningsNewMember($uid)
{
	if(isset($uid) && $uid!="" )
	{ 
			$percentageearning=getCodeValueByLabel($label="referral_earning_percentage");
			$id=$uid;
			$accountDAO=new user_accountDAO();
			$accountVO=new user_accountVO();
			
			 $programSignup=$accountDAO->fetchReferMemSignupCreditsByUserId($id);
			
			$clickingAds=$accountDAO->fetchReferMemClickCreditsByUserId($id);
			//print_r($clickingAds);
			//$referNewMember=$accountDAO->fetchReferMemCreditsByUserId($id);
			//$purchageCredits=$accountDAO->fetchPurchagedCreditsByUserId($id);
			//$referralRequets=$accountDAO->fetchReferralCreditsByUserId($id);
			//$advPackages=$accountDAO->fetchAdvPackageCreditsByUserId($id);
			//$jackpotGame=$accountDAO->fetchJackpotGamePackageCreditsByUserId($id);
			
			
			$earned=$programSignup+$clickingAds;
			//$spend=$referralRequets+$advPackages+$jackpotGame;
			
			$totalCredits=$earned;
			if($totalCredits=='')
			{
				$totalCredits='0.00';
			}
			return  formatDoubleAmount($totalCredits);
	}
}

//returns website url
function getWebsiteUrl()
{
	$cvo=new code_valueVO();
	$cdao=new code_valueDAO();
	
	$code_label1="website_url";
	$codevalues1=$cdao->fetchDetailsByLabel($code_label1);
	$website_url=$codevalues1->code_value;
	return $website_url;
}

function changeBannerAdvStatus($ban_id)
{
	$fvo=new front_bannerVO();
	$fdao=new front_bannerDAO();
	$fdao->ActivateNdactivate($ban_id);
}

function getCodeValueByLabel($label)
{
	$cvo=new code_valueVO();
	$cdao=new code_valueDAO();
	$code_label1=$label;
	$codevalues1=$cdao->fetchDetailsByLabel($code_label1);
	$value=$codevalues1->code_value;
	return $value;
}

function formatDoubleAmount($amount)
{
	$formattedValue=number_format($amount, 2, '.', ',');
	return $formattedValue;
}

function isPremium($uid)
{
	
	$sVO=new siteusersVO();
	$sDAO=new siteusersDAO();
	
	$user_info=$sDAO->fetchDetails($uid);
	
	$membership_type=$user_info->mem_type;
	
	if($membership_type==1)
	{
		return true;
	}
	else if($membership_type==0)
	{
		return false;
	}
}



function checkMembershipRemainingTime($uid)
{
	if(isPremium($uid))
	{
		$pVO=new premium_membersVO();
		$pDAO=new premium_membersDAO();
		
		$details=$pDAO->fetchActiveMembershipDetails($uid);
		
		$endDate=$details->mem_end_dt;
		$dateParts=dateParseFromFormat("Y-m-d H:i:s", $endDate);
		//print_r($dateParts);
		
		$CountDown = new CountDown;
		
		$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);


$CountDown->format = "Weeks: {W} Days: {D} Hours: {H}";

if($out=='0')
{
	//change membership status
	$sVO=new siteusersVO();
	$sDAO=new siteusersDAO();
	
	$sVO->mem_type=0;
	$sVO->user_id=$uid;
	$sDAO->updateMemStatus($sVO);
	
	$pVO=new premium_membersVO();
	$pDAO=new premium_membersDAO();
	$pVO->status=0;
	$pVO->user_id=$uid;
	$pDAO->updateStatus($pVO);
	
}	
return $out;
	}
	
	
	
}

function changeAdvStatus()
	{
		$userdao=new user_accountDAO();
		$uservo=new user_accountVO();
		
		$cdao = new advertismentDAO();
		$cvo=new advertismentVO();
		
		$allAdv=$cdao->fetchAll($publish='onlypublished');
		
		foreach($allAdv as $advValue)
		{
			$advId=$advValue->id;
			
			$out=$userdao-> fetchAllAds24HrsInfo($advId,$cond='out');
			$in=$userdao-> fetchAllAds24HrsInfo($advId,$cond='in');
			$outTotal=count($out);
			$inTotal=count($in);
			
			$total=$outTotal+$inTotal;
			
			$advSize=$advValue->package_size;
			
			if($total >= $advSize )
			{
				$cdao->ActivateNdactivate($advId, $publish=0);
			}
		}

	}
	
	//function to change programsignup status
	function changeProgramSignUpStatus($signupid,$status)
	{
		$programsignupvo=new program_signup_childVO(); 
		$programsignupdao=new program_signup_childDAO(); 
		$programsignupdao->ActivateNdactivate($signupid,$status);
	}
	
	function getPromotorRating($user_id)
	{
		$votvo=new votingVO();
							$votdao=new votingDAO();
							$vote_type='promotor';
							
							$acceptedvotes=$votdao->fetchRatingsById($user_id,$vote_type,"accept");
							$rejectedvotes=$votdao->fetchRatingsById($user_id,$vote_type,"decline");
							$terminatedvotes=$votdao->fetchRatingsById($user_id,$vote_type,"terminate");
							$totalpromotorrating=$acceptedvotes-$rejectedvotes-$terminatedvotes;
							
								return $totalpromotorrating;
							
							
	}
	
	function getReferralRating($user_id)
	{
							$votvo=new votingVO();
							$votdao=new votingDAO();
							$vote_type='referral';
							
							$acceptedvotes=$votdao->fetchRatingsById($user_id,$vote_type,"accept");
							$rejectedvotes=$votdao->fetchRatingsById($user_id,$vote_type,"decline");
							$terminatedvotes=$votdao->fetchRatingsById($user_id,$vote_type,"terminate");
							$totalpromotorrating=$acceptedvotes-$rejectedvotes-$terminatedvotes;
							
								return $totalpromotorrating;
							
							
							
							 
	}
	
	
	//function to check from advertismemt id if today clicked or not,returns 1 if clicked otherwise 0
	function checkTodayClicked($adv_id)
	{
	$check=0;
	$userdao1=new user_accountDAO();
	$uservo1=new user_accountVO();
		if(isset($_SESSION['SES_ID']) && $_SESSION['SES_ID']!="" )
		{ 
			
			$value=$userdao1->fetchAllAdsByUserIdAdvId($_SESSION['SES_ID'],$adv_id);
			
			
			
			if(count($value) > 0)
			{			
				foreach($value as $record)
				{
					$date=$record->created_date;
					$newdate=date("Y-m-d H:i:s", strtotime($date . "+24 hour"));
					$curdate=date("Y-m-d H:i:s");
					
					if($newdate > $curdate)
					{
						$check=1;
						break;
					}
					else
					{
						$check=0;
					}
				}
			}
			else
			{
				$check=0;
			}
		}
		return $check;
	}
	
	//function to check from user id how much add clicked in 24 hours
	function totalTodayClicked($id)
	{
	$check=0;
	$userdao1=new user_accountDAO();
	$uservo1=new user_accountVO();
		if(isset($_SESSION['SES_ID']) && $_SESSION['SES_ID']!="" )
		{ 
			
			$value=$userdao1->fetchClickDetailsByUserId($id);
			//echo count($value);
			if(count($value) > 0)
			{			
				foreach($value as $record)
				{
					$date=$record->created_date;
					$newdate=date("Y-m-d H:i:s", strtotime($date . "+24 hour"));
					$curdate=date("Y-m-d H:i:s");
					
					if($newdate > $curdate)
					{
						$check=$check+1;
					
					}
					
				}
			}
			
		}
		return $check;
	}
	
	//to be called and runed in every page to calculate program signup credits to transfer
	function automatedCreditAddedForSignup()//not used
	{
		checkAndchangeSignUpStatus();	
	//fetch all signups in progress
	//$sdao=new program_signup_childDAO();
//	$activesignups=$sdao->fetchAllSignupsByStatus($s=1);
//	$signup_id=$activesignups->id;
//	
//	//for each active signups add the credit but make sure not added more than once in 24 hours
//	foreach($activesignups as $signups)
//	{
//		//get valid date infos
//		$validdao=new program_signup_validityDAO();
//		$validdetails=$validdao->fetchSignupValidityDetails($signup_id);
//		
//			$user_id=$validdetails->signed_user_id;//user who have signup
//			$signup_id=$validdetails->program_signup_id;//signed in which program child id
//			$valid_from=$validdetails->valid_from;// valid from
//			$valid_to=$validdetails->valid_to;// valid to
//			
//			
//			
//		
//	}
		
		$validdao=new program_signup_validityDAO();
		$validdetails=$validdao->fetchAllActive($publish='all');
		
		foreach($validdetails as $records)
		{
			$user_id=$records->signed_user_id;//user who have signup
			$signup_id=$records->program_signup_id;//signed in which program child id
			$valid_from=$records->valid_from;// valid from
			$valid_to=$records->valid_to;// valid to
			
			$sdao=new program_signup_childDAO();
			$signupdetails=$sdao->fetchDetails($signup_id);
			$program_id=$signupdetails->program_id;//program id
			
			$status=$signupdetails->status;// status
			
			$pdao=new program_signupDAO();
			$programdetails=$pdao->fetchDetails($program_id);
			
			$credit=$programdetails->total;//total credits in program
			$days=$programdetails->contract_duration; //contract duration
			$totalcreditstotransfer=(60/100)*$credit; //to transfer total credits
			$creditsperday=$totalcreditstotransfer/$days; //per day credits
			
			
			if($status == 1) //means
			{
				
					//check if for every credits payable day credits transferred or not
					//$date=date("Y-m-d H:i:s", strtotime("+$i days"));
					$accountdao=new user_accountDAO();
					$accountvo=new user_accountVO();
					$creditpayed=$accountdao->fetchSignUpByDate($user_id,$signup_id);
					//print_r($creditpayed);
					//die;
					if($creditpayed->account_id == 0 || $creditpayed->account_id=="")
					{
						$accountvo->user_id=$user_id;
						$accountvo->txn_id=$signup_id;
						$accountvo->created_date=date("Y-m-d H:i:s");
						$accountvo->credit_type="program signup";
						$accountvo->credits=$creditsperday;
						
						$accountdao->Insert($accountvo);
					}
					
				
			}
		}
	}
	
	function automatedSignupCreditTransfer()
	{
		$ssvo=new program_signup_childVO();
		$ssdao=new program_signup_childDAO();
		
		$signups=$ssdao->fetchAllSignupsByStatus($s='1');
		
		//for each sign ups get their validity info and change their status as completed if validity reached and transfer credit
		foreach($signups as $record)
		{
			$ppvvo=new program_signup_validityVO();
			$ppvdao=new program_signup_validityDAO();
			$validitydetails=$ppvdao->fetchSignupDetailsByUserId($record->child_user_id,$record->id);
			
			$valid_to=$validitydetails->valid_to;
			$valid_from=$validitydetails->valid_from;
			
			$currentdate=date("Y-m-d H:i:s");
			
			
			
			$user_id=$validitydetails->signed_user_id;//user who have signup
			$signup_id=$validitydetails->program_signup_id;//signed in which program child id
			
			
			$program_id=$record->program_id;//program id
			
			$status=$record->status;// status
			
			$pdao=new program_signupDAO();
			$programdetails=$pdao->fetchDetails($program_id);
			
			$credit=$programdetails->total;//total credits in program
			$days=$programdetails->contract_duration; //contract duration
			$totalcreditstotransfer=(60/100)*$credit; //to transfer total credits
			$creditsperday=$totalcreditstotransfer/$days; //per day credits
			
			
			//now add 24 hours to valid to until validity ends
			//$begindate=date("Y-m-d H:i:s", strtotime($valid_from . "+24 hour"));
			
			//$differencedate=GetDateDifference($valid_to, $valid_from) ;
			$olddate=$valid_from;
			$newdate="";
			
			for ($i=1;$i<=$days;$i++)
			{
				
				$newdate=date("Y-m-d H:i:s", strtotime($olddate . "+24 hour"));
				
				if($currentdate >= $newdate)
				{
					//check if credit already added
					$accountdao=new user_accountDAO();
					$accountvo=new user_accountVO();
					$creditpayed=$accountdao->fetchSignUpByDateTime($user_id,$signup_id,$newdate);
					if($creditpayed->account_id == 0 || $creditpayed->account_id=="")
					{
						$accountvo->user_id=$user_id;
						$accountvo->txn_id=$signup_id;
						$accountvo->created_date=$newdate;
						$accountvo->credit_type="program signup";
						$accountvo->credits=$creditsperday;
						
						$accountdao->Insert($accountvo);
					}
					
				}
				$olddate=$newdate;
			}
			
		}
	}
	
	//check all the signups which are in progress and change their status if date is ended
	function checkAndchangeSignUpStatus()
	{
		$ssvo=new program_signup_childVO();
		$ssdao=new program_signup_childDAO();
		
		$signups=$ssdao->fetchAllSignupsByStatus($s='1');
		
		//for each sign ups get their validity info and change their status as completed if validity reached
		foreach($signups as $record)
		{
			
			
			$ppvvo=new program_signup_validityVO();
			$ppvdao=new program_signup_validityDAO();
			$validitydetails=$ppvdao->fetchSignupDetailsByUserId($record->child_user_id,$record->id);
			
			$valid_to=$validitydetails->valid_to;
			$valid_from=$validitydetails->valid_from;
			$dateParts=dateParseFromFormat("Y-m-d H:i:s", $valid_to);
								
								$CountDown = new CountDown();
								
								$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);
								
								
								$CountDown->format = "Weeks: {W} Days: {D} Hours: {H}";
								//$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);
								//$CountDown->finished = changeBannerAdvStatus($details->ban_id);
								if($out=='0')
								{
									
									changeProgramSignUpStatus($record->id,'3');
									
								
									//also give 10% earning to their parents up to 5 level deep
									$child_id=$record->child_user_id;
									$program_id=$record->program_id;
									
									$pdao=new program_signupDAO();
									$programdetails=$pdao->fetchDetails($program_id);
			
									$credit=$programdetails->total;//total credits in program
									$days=$programdetails->contract_duration; //contract duration
									$totalcreditstotransfer=(60/100)*$credit; //to transfer total credits
									
									insertinToParents($child_id,$totalcreditstotransfer);
									
								}
		}
	}
	
	
	//distribute program signup credits to the parent 10%

	function insertinToParents($user_id,$credits)
{
	$percentageearning=getCodeValueByLabel($label="referral_earning_percentage");
	
	$parent="";
	$child=$user_id;
	
	$suvo=new siteusersVO();
			$sudao=new siteusersDAO();
			$details=$sudao->fetchDetails($child);
			
			$ifparent=$details->user_parent_id;
			
if($ifparent != 0)
{
	
	for($i=1;$i<=5;$i++)
	{
		
		$suvo=new siteusersVO();
			$sudao=new siteusersDAO();
			$details=$sudao->fetchDetails($child);
			
			$parent=$details->user_parent_id;
			if($parent==0)
				$parent="";
			
			if($parent != "" )
			{
				$dao=new user_accountDAO();
				$vo=new user_accountVO();
	
				$vo->user_id=$parent;
				$vo->txn_id=$user_id;
				$vo->created_date=date("Y-m-d H:i:s");
				$vo->credit_type="referring new members signup";
				$vo->credits=($percentageearning/100)*$credits;
				$child=$parent;
				$dao->Insert($vo);
				
			}
			else
			{
				break;
			}
	}
}
	
	
}
	
	//change status of user if their referral rating is -3 or less
	function blockAccountIfLessReferralRating()
	{
		//fetch all user details
		$sVO=new siteusersVO();
		$sDAO=new siteusersDAO();
		
		$userdetails=$sDAO->fetchAll($publish='onlypublished');
		
		foreach($userdetails as $record)
		{
			$id=$record->user_id;
			
			$rating=getReferralRating($id);
			
			if($rating <= -3)
			{
				//change user status
				$sDAO->ActivateNdactivate($id, $publish=0);
			}
			
			
		}
		
	}
	
	//automatically accept signups if not validated within 5 days
	function automaticallyAcceptSignup()
	{
		$svo=new program_signup_childVO();
		$sdao=new program_signup_childDAO();
		
		$signups=$sdao->fetchAllSignupsByStatus($s=0);
		
		foreach($signups as $record)
		{
			$signupdate=$record->created_date;
			$signup_id=$record->id;
			$program_id=$record->program_id;
			$child_user_id=$record->child_user_id;
			$uid=$record->parent_user_id;
			//now add 5 days to the signupdate
			$upto=date("Y-m-d H:i:s", strtotime($signupdate . "+5 day"));
			
			$curdate=date("Y-m-d H:i:s");
			
			if($upto==$curdate)
			{
								//change the status as accepted   
						if($sdao->ActivateNdactivate($signup_id,$status=1))
						{
							//set now details of validity of that signup and increase  ratings
							$pvo=new program_signupVO();
							$pdao=new program_signupDAO();
							
							$programdetails=$pdao->fetchDetails($program_id);
							
							$contract_duration=$programdetails->contract_duration;
							
							$curDate=date("Y-m-d H:i:s");
							
							$vvo=new program_signup_validityVO();
							$vdao=new program_signup_validityDAO();
							$vvo->signed_user_id=$child_user_id;
							$vvo->program_signup_id=$signup_id;
							$vvo->valid_from=$curDate;
							$vvo->valid_to=date("Y-m-d H:i:s", strtotime("+$contract_duration days"));
							
							
							
							$vdao->Insert($vvo);
							
							//rating
							$rvo=new votingVO();
							$rdao=new votingDAO();
							
							//promotor rating +1
							$rvo->vote_by=$child_user_id;
							$rvo->vote_to=$uid;
							$rvo->vote_type="promotor";
							$rvo->vote_value="accept";
							$rvo->vote=1;
							$rvo->vote_for=$signup_id;
							$rvo->created_date=$curDate;
							
							if($rdao->Insert($rvo))
							{
								//referral rating +1
								$rvo->vote_by=$uid;
								$rvo->vote_to=$child_user_id;
								$rvo->vote_type="referral";
								$rvo->vote_value="accept";
								$rvo->vote=1;
								$rvo->vote_for=$signup_id;
								$rvo->created_date=$curDate;
								
								$rdao->Insert($rvo);
								
							}
							
						
							
						}
			}
		}
	}
	
	
	function fetchFiveLevelDeepUserInfoById($id)
	{
		$uvo=new siteusersVO();
		$udao=new siteusersDAO();
		
		$list=array();
		$level1=array();
		$level2=array();
		$level3=array();
		$level4=array();
		$level5=array();
		
		$details=$udao->display_child_five_level($id);
		$i=0;
		foreach($details as $record)
		{
			if($record->lev1 != "")
				$level1[$i]=$record->lev1;
			if($record->lev2 != "")
				$level2[$i]=$record->lev2;
			if($record->lev3 != "")
				$level3[$i]=$record->lev3;
			if($record->lev4 != "")
				$level4[$i]=$record->lev4;
			if($record->lev5 != "")
				$level5[$i]=$record->lev5;
				
				$i++;
		}
		
		$list["1"]=$level1;
		$list["2"]=$level2;
		$list["3"]=$level3;
		$list["4"]=$level4;
		$list["5"]=$level5;
		
		return $list;
		
	}
	
	

	  ?>