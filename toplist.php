<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];




//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();

$pvo=new program_signupVO();
$pdao=new program_signupDAO();

//fetch 20 top program referrals
$programreferrals=$pdao->fetchMostReferralsAccepted();

//for 20 top promotor ratings
$vo=new votingVO();
$dao=new votingDAO();
$toppromotors=array();
$topreferrals=array();

$allpromotors=$dao->fetchAllTypeRatings($vote_type="promotor");
$allreferrals=$dao->fetchAllTypeRatings($vote_type="referral");

foreach($allpromotors as $value)
{
	$id=$value->vote_to;
	$rating=getPromotorRating($id);
	$toppromotors[$id]=$rating;
}
foreach($allreferrals as $value)
{
	$id=$value->vote_to;
	$rating=getReferralRating($id);
	$topreferrals[$id]=$rating;
}

arsort($toppromotors);

arsort($topreferrals);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Top 20 List</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<link rel="stylesheet" type="text/css" href="css/sortertable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
<style type="text/css">
table
{
	width:100%;
}
table tr td
{
	background-color:#6C955C;
	padding:3px;
	color:#ffffff;
}
</style>

</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft" style="margin:5px; width:940px">
     	<div class="block" style="width:940px">
        <!-- Ttle (Start) -->
        <div class="ttle" style="background:url(images/h1bg2.jpg) no-repeat; width:940px">
          	<h1> 
				Top 20 List
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--info div staart-->
        <div id="form-container" style="margin-bottom:15px; width:910px">
            <header id="form-header" class="info" style="border:none; padding:10px; font-style:normal; font-weight:normal;">
             <ul style="list-style:none; font-weight:bold; ">
                        	<li><span style="margin-right:5px"></span>What's This ?</li>
                           
                        </ul>
                    	<h2 style="font-style:normal;font-size:110%">The lists below show a top-20 of our community's best referral ratings, promotor ratings and members with the most program referrals earned using DownlineRefs. Just nice to see how everyone is keeping up.  <br />


						</h2>
                       
                    	<div></div>
                	</header>
            </div>
        <!--info div end-->
        
        
        <!--table starts-->
        
			<div style="margin-bottom:20px">
            	<div style="float:left; width:274px; height:auto; min-height:500px; background-color:#000066; margin-right:10px; padding:10px;background:none repeat scroll 0 0 #FFFFFF; border:1px solid #CCCCCC; box-shadow:0 0 5px rgba(0, 0, 0, 0.2);">
                
                	<table>
                    	<tr>
                        	<th colspan="3" align="left">
                            	Best Referral Rating
                            </th>
                        </tr>
                        <?php 
						$i=1;
							foreach($topreferrals as $key=>$value) 
							{
								if($i<=20 && $value >=0)
								{
									$details=$sDAO->fetchDetails($key);
									$name=$details->username;
									$mem_type=$details->mem_type;
						?>
                        <tr>
                        	<td style="width:15%" align="right"><?php echo $i; ?>.</td>
                            <td style="width:60%" align="left"><?php
							 echo $name; 
							if($mem_type==1)
				{
					echo '<img src="images/premium.png" align="top" alt="Premium Member" title="Premium Member" />';
					
				}
				else
				{
					echo '<img src="images/free.png" align="top" alt="Standard Member" title="Standard Member" />';
				}
							 ?></td>
                            <td style="25%" align="right">+<?php echo $value; ?></td>
                        </tr>
                        <?php
						}
						$i++;
						}
						?>
                    </table>
                </div>
                
                <div style="float:left; width:275px; height:auto; min-height:500px; background-color:#000066;margin-right:10px;padding:10px;background:none repeat scroll 0 0 #FFFFFF; border:1px solid #CCCCCC; box-shadow:0 0 5px rgba(0, 0, 0, 0.2);">
                	<table>
                    	<tr>
                        	<th colspan="3" align="left">
                            	Best Promotor Rating
                            </th>
                        </tr>
                         <?php 
						$i=1;
							foreach($toppromotors as $key=>$value) 
							{
								if($i<=20 && $value >=0)
								{
									$details=$sDAO->fetchDetails($key);
									$name=$details->username;
									$mem_type=$details->mem_type;
						?>
                        <tr>
                        	<td style="width:15%" align="right"><?php echo $i; ?>.</td>
                            <td style="width:60%" align="left"><?php
							 echo $name; 
							if($mem_type==1)
				{
					echo '<img src="images/premium.png" align="top" alt="Premium Member" title="Premium Member" />';
					
				}
				else
				{
					echo '<img src="images/free.png" align="top" alt="Standard Member" title="Standard Member" />';
				}
							 ?></td>
                            <td style="25%" align="right">+<?php echo $value; ?></td>
                        </tr>
                        <?php
						}
						$i++;
						}
						?>
                    </table>
                </div>
                
                <div style="float:left; width:275px; height:auto; min-height:500px; background-color:#000066;padding:10px;background:none repeat scroll 0 0 #FFFFFF; border:1px solid #CCCCCC; box-shadow:0 0 5px rgba(0, 0, 0, 0.2);">
                	<table style="width:100%">
                    	<tr>
                        	<th colspan="3" align="left">
                            	Most Program Referrals
                            </th>
                        </tr>
                        <?php 
							$k=1;
							foreach($programreferrals as $value) {
							$details=$sDAO->fetchDetails($value->vote_to);
							$name=$details->username;
							$mem_type=$details->mem_type;
						?>
                        <tr>
                        	<td style="width:15%" align="right"><?php echo $k; ?>.</td>
                            <td style="width:60%" align="left"><?php 
							echo $name; 
							if($mem_type==1)
				{
					echo '<img src="images/premium.png" align="top" alt="Premium Member" title="Premium Member" />';
					
				}
				else
				{
					echo '<img src="images/free.png" align="top" alt="Standard Member" title="Standard Member" />';
				}
							?></td>
                            <td style="25%" align="right"><?php echo $value->total; ?> refs</td>
                        </tr>
                        <?php 
						$k++;
						} ?>
                    </table>
                </div>
                <div style="clear:both"></div>
            </div>

	<div style="clear:both"></div>
           <!--table end-->
           <div class="buttons">
           
                	<button type="button" class="regular" name="Back" value="Back" onclick="location.href='dashboard.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
               
           </div>
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
           <div style="clear:both"></div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			//include_once("includes/rightcolumn.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
