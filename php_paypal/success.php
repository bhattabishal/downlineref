<?php
session_start();
include_once "global.php";
include_once "../".DIR_INCLUDES."/db_connection.php";
require_once "../".DIR_INCLUDES."/functions.php";

$id=$_SESSION['SES_ID'];
$item_id=$_SESSION['item_id'];

$vo=new paymentsVO();
$dao=new paymentsDAO();
/*
 * success.php
 *
 * PHP Toolkit for PayPal v0.51
 * http://www.paypal.com/pdn
 *
 * Copyright (c) 2004 PayPal Inc
 *
 * Released under Common Public License 1.0
 * http://opensource.org/licenses/cpl.php
 *
 */
?>

<html>
<head><title>::Thank You::</title>
<link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body bgcolor="ffffff">
<br>
<br>
<table width="500" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr> 
      <td align="left" valign="top" bgcolor="#333333"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
               <td align="center" bgcolor="#EEEEEE"> <p>&nbsp;</p>
                  <p>Thank you! Your order has been successfully processed.</p>
                  <p>&nbsp;</p></td>
            </tr>
         </table></td>
   </tr>
</table>
<?php


$vo->buyer_id=$id;
$vo->txnid=$_POST[txn_id];
$vo->buyer_firstname=$_POST[first_name];
$vo->buyer_lastname=$_POST[last_name];
$vo->buyer_email=$_POST[payer_email];
$vo->payment_amount=$_POST[payment_gross];
$vo->payment_status=$_POST[payment_type];
$vo->payment_type="paypal";
$vo->itemid=$item_id;
$createdtime=$_POST[payment_date];

$dao->Insert($vo);
 header('Refresh: 2; URL=../purchagecredits.php');

?>

<br>
<!--<table width="500" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr> 
      <td align="left" valign="top" bgcolor="#333333"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr align="left" valign="top"> 
               <td width="20%" bgcolor="#EEEEEE"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Order Number:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?php//echo $_POST[txn_id];?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Date:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?php//echo $_POST[payment_date]."item Id:".$item_id;?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td width="20%" bgcolor="#EEEEEE"> First Name: </td>
                        <td width="80%" bgcolor="#EEEEEE"> 
                           <?php//echo $_POST[first_name]."Payment_Gross:".$_POST[payment_gross];?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Last Name:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?php// echo $_POST[last_name]."User ID:".$id;?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Email:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?php// echo $_POST[payer_email]."Payment Status:".$_POST[payment_type]."Pending Reason:".$_POST[pending_reason];?>
                        </td>
                     </tr>
                  </table>--></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
</body>
</html>
