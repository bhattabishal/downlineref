<?php 
class code_valueDAO extends BaseDAO
{
	public $code_value;
	public $code_label;
	public $code_type;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO code_value
	SET
		code_value= '$vo->code_value',
		code_label= '$vo->code_label',
		code_type= '$vo->code_type'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM code_value ORDER BY code_value";
		else
			$this->sql = "SELECT * FROM code_value ORDER BY code_value";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new code_valueVO();
				$vo->code_value= $rows['code_value'];
				$vo->code_label= $rows['code_label'];
				$vo->code_type= $rows['code_type'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM code_value ORDER BY code_value LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM code_value ORDER BY code_value LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new code_valueVO();
				$vo->code_value= $rows['code_value'];
				$vo->code_label= $rows['code_label'];
				$vo->code_type= $rows['code_type'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE code_value
	SET
		code_value= '$vo->code_value',
		code_label= '$vo->code_label',
		code_type= '$vo->code_type'
	WHERE code_label= '$vo->code_label'";
		return $this->exec();
	}


	function fetchDetails($code_value)
	{
		$this->sql = "SELECT * FROM code_value WHERE code_value= '$code_value'";
		$this->exec();
		$vo = new code_valueVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->code_value= $rows['code_value'];
			$vo->code_label= $rows['code_label'];
			$vo->code_type= $rows['code_type'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchDetailsByLabel($code_label)
	{
		$this->sql = "SELECT code_value FROM code_value WHERE code_label= '$code_label'";
		$this->exec();
		$vo = new code_valueVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->code_value= $rows['code_value'];
			$vo->code_label= $rows['code_label'];
			$vo->code_type= $rows['code_type'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	


	function remove($code_value)
	{
		$vo=$this->fetchDetails($code_value);
		if($vo->code_value!=0)
		{
			$this->sql = "DELETE FROM code_value WHERE code_value= '$code_value'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

