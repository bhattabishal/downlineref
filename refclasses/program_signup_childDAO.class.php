<?php 
class program_signup_childDAO extends BaseDAO
{
	public $id;
	public $parent_user_id;
	public $child_user_id;
	public $username;
	public $email;
	public $program_id;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;
	public $status;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO program_signup_child
	SET
		parent_user_id= '$vo->parent_user_id',
		child_user_id= '$vo->child_user_id',
		username= '$vo->username',
		email= '$vo->email',
		program_id= '$vo->program_id',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count',
		status= '$vo->status'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_child ORDER BY id";
		else
			$this->sql = "SELECT * FROM program_signup_child ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	
	function fetchAllSignupsByStatus($s)
	{
		
			$this->sql = "SELECT * FROM program_signup_child WHERE status='$s' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllByDays($uid,$days)
	{
		
			$this->sql = "SELECT * FROM program_signup_child WHERE child_user_id='$uid' AND created_date BETWEEN DATE_SUB( CURDATE() ,INTERVAL $days DAY ) AND CURDATE()";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_child ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM program_signup_child ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}

function fetchAllByProgramId($publish='all',$pid)
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_child WHERE program_id='$pid' AND status='1' ORDER BY id";
		else
			$this->sql = "SELECT * FROM program_signup_child WHERE program_id='$pid' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllByProgramSignups($publish='all',$uid)
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_child WHERE child_user_id='$uid' AND status='1' ORDER BY id";
		else
			$this->sql = "SELECT * FROM program_signup_child WHERE child_user_id='$uid' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllMyReferralSignups($publish='all',$pid,$uid)
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_child WHERE parent_user_id='$uid' AND program_id='$pid' AND status='1' ORDER BY id";
		else
			$this->sql = "SELECT * FROM program_signup_child WHERE parent_user_id='$uid' AND program_id='$pid' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllMyPendingReferralSignups($pid,$uid)
	{
		
			$this->sql = "SELECT * FROM program_signup_child WHERE parent_user_id='$uid' AND program_id='$pid' AND status='0' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllMyDeclinedReferralSignups($pid,$uid)
	{
		
			$this->sql = "SELECT * FROM program_signup_child WHERE parent_user_id='$uid' AND program_id='$pid' AND status='2' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function update($vo)
	{
	$this->sql="UPDATE program_signup_child
	SET
		parent_user_id= '$vo->parent_user_id',
		child_user_id= '$vo->child_user_id',
		username= '$vo->username',
		email= '$vo->email',
		program_id= '$vo->program_id',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count',
		status= '$vo->status'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM program_signup_child WHERE id= '$id'";
		$this->exec();
		$vo = new program_signup_childVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->parent_user_id= $rows['parent_user_id'];
			$vo->child_user_id= $rows['child_user_id'];
			$vo->username= $rows['username'];
			$vo->email= $rows['email'];
			$vo->program_id= $rows['program_id'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchDetailsByProgramId($id)
	{
		$this->sql = "SELECT * FROM program_signup_child WHERE program_id= '$id'";
		$this->exec();
		$vo = new program_signup_childVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->parent_user_id= $rows['parent_user_id'];
			$vo->child_user_id= $rows['child_user_id'];
			$vo->username= $rows['username'];
			$vo->email= $rows['email'];
			$vo->program_id= $rows['program_id'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchChildPromotersByStatus($id,$status)
	{
	
			$this->sql = "SELECT * FROM program_signup_child WHERE parent_user_id='$id' AND status='$status'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchChildPromoters($id)
	{
	
			$this->sql = "SELECT * FROM program_signup_child WHERE parent_user_id='$id' ORDER BY created_date DESC";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	
	function fetchParentPromoters()
	{
		$this->sql = "SELECT * FROM program_signup_child WHERE child_user_id='$id'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_childVO();
				$vo->id= $rows['id'];
				$vo->parent_user_id= $rows['parent_user_id'];
				$vo->child_user_id= $rows['child_user_id'];
				$vo->username= $rows['username'];
				$vo->email= $rows['email'];
				$vo->program_id= $rows['program_id'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM program_signup_child WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function ActivateNdactivate($id,$status)
			{
			$vo= $this->fetchDetails($id);
			if($vo->id != 0)
				{
				$this->sql = "UPDATE program_signup_child SET status = '$status' WHERE id = '$vo->id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}


	function __destruct()
	{
		unset($this);
	}


}
?> 

