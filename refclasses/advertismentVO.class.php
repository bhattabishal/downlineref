<?php 
class advertismentVO 
{
	public $id;
	public $title;
	public $short_desc;
	public $credits;
	public $website_url;
	public $package_size;
	public $sub_total;
	public $span_fees;
	public $total;
	public $status;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id;
		$this->title= $this->title;
		$this->short_desc= addslashes(trim($this->short_desc));
		$this->credits= $this->credits;
		$this->website_url= $this->website_url;
		$this->package_size= $this->package_size;
		$this->sub_total= $this->sub_total;
		$this->span_fees= $this->span_fees;
		$this->total= $this->total;
		$this->status= $this->status;
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function formatFetchVariables()
	{
		$this->id;
		$this->title= $this->title;
		$this->short_desc= stripslashes(trim($this->short_desc));
		$this->credits= $this->credits;
		$this->website_url= $this->website_url;
		$this->package_size= $this->package_size;
		$this->sub_total= $this->sub_total;
		$this->span_fees= $this->span_fees;
		$this->total= $this->total;
		$this->status= $this->status;
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->title= $array['title'];
		$this->short_desc= $array['short_desc'];
		$this->credits= $array['credits'];
		$this->website_url= $array['website_url'];
		$this->package_size= $array['package_size'];
		$this->sub_total= $array['sub_total'];
		$this->span_fees= $array['span_fees'];
		$this->total= $array['total'];
		$this->status= $array['status'];
		$this->created_by= $array['created_by'];
		$this->created_date= $array['created_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->title);
		unset($this->short_desc);
		unset($this->credits);
		unset($this->website_url);
		unset($this->package_size);
		unset($this->sub_total);
		unset($this->span_fees);
		unset($this->total);
		unset($this->status);
		unset($this->created_by);
		unset($this->created_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this);
	}


}
?> 

