<?php 
class program_signupVO 
{
	public $program_id;
	public $program_name;
	public $user_id;
	public $referral_url;
	public $banner_url;
	public $program_type;
	public $earning_per_clk;
	public $earning_per_ref_clk;
	public $min_payout;
	public $instant_payout;
	public $payment_processor;
	public $downline_level;
	public $program_language;
	public $contract_duration;
	public $spend_credits;
	public $discount;
	public $total;
	public $task_desc;
	public $allowed_mem_rat;
	public $not_allowed_mem;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;
	public $status;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->program_id;
		$this->program_name=$this->program_name;
		$this->user_id= $this->user_id;
		$this->referral_url= $this->referral_url;
		$this->banner_url= $this->banner_url;
		$this->program_type= $this->program_type;
		$this->earning_per_clk= $this->earning_per_clk;
		$this->earning_per_ref_clk= $this->earning_per_ref_clk;
		$this->min_payout= $this->min_payout;
		$this->instant_payout= $this->instant_payout;
		$this->payment_processor= $this->payment_processor;
		$this->downline_level= $this->downline_level;
		$this->program_language= $this->program_language;
		$this->contract_duration= $this->contract_duration;
		$this->spend_credits= $this->spend_credits;
		$this->discount= $this->discount;
		$this->total= $this->total;
		$this->task_desc= addslashes(trim($this->task_desc));
		$this->allowed_mem_rat= $this->allowed_mem_rat;
		$this->not_allowed_mem= $this->not_allowed_mem;
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
		$this->status= $this->status;
	}


	function formatFetchVariables()
	{
		$this->program_id;
		$this->program_name=$this->program_name;
		$this->user_id= $this->user_id;
		$this->referral_url= $this->referral_url;
		$this->banner_url= $this->banner_url;
		$this->program_type= $this->program_type;
		$this->earning_per_clk= $this->earning_per_clk;
		$this->earning_per_ref_clk= $this->earning_per_ref_clk;
		$this->min_payout= $this->min_payout;
		$this->instant_payout= $this->instant_payout;
		$this->payment_processor= $this->payment_processor;
		$this->downline_level= $this->downline_level;
		$this->program_language= $this->program_language;
		$this->contract_duration= $this->contract_duration;
		$this->spend_credits= $this->spend_credits;
		$this->discount= $this->discount;
		$this->total= $this->total;
		$this->task_desc= stripslashes(trim($this->task_desc));
		$this->allowed_mem_rat= $this->allowed_mem_rat;
		$this->not_allowed_mem= $this->not_allowed_mem;
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
		$this->status= $this->status;
	}


	function getPostVars($array)
	{
		$this->program_id= $array['program_id'];
		$this->program_name=$array['program_name'];
		$this->user_id= $array['user_id'];
		$this->referral_url= $array['referral_url'];
		$this->banner_url= $array['banner_url'];
		$this->program_type= $array['program_type'];
		$this->earning_per_clk= $array['earning_per_clk'];
		$this->earning_per_ref_clk= $array['earning_per_ref_clk'];
		$this->min_payout= $array['min_payout'];
		$this->instant_payout= $array['instant_payout'];
		$this->payment_processor= $array['payment_processor'];
		$this->downline_level= $array['downline_level'];
		$this->program_language= $array['program_language'];
		$this->contract_duration= $array['contract_duration'];
		$this->spend_credits= $array['spend_credits'];
		$this->discount= $array['discount'];
		$this->total= $array['total'];
		$this->task_desc= $array['task_desc'];
		$this->allowed_mem_rat= $array['allowed_mem_rat'];
		$this->not_allowed_mem= $array['not_allowed_mem'];
		$this->created_by= $array['created_by'];
		$this->created_date= $array['created_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
		$this->status= $array['status'];
	}


	function __destruct()
	{
		unset($this->program_id);
		unset($this->program_name);
		unset($this->user_id);
		unset($this->referral_url);
		unset($this->banner_url);
		unset($this->program_type);
		unset($this->earning_per_clk);
		unset($this->earning_per_ref_clk);
		unset($this->min_payout);
		unset($this->instant_payout);
		unset($this->payment_processor);
		unset($this->downline_level);
		unset($this->program_language);
		unset($this->contract_duration);
		unset($this->spend_credits);
		unset($this->discount);
		unset($this->total);
		unset($this->task_desc);
		unset($this->allowed_mem_rat);
		unset($this->not_allowed_mem);
		unset($this->created_by);
		unset($this->created_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this->status);
		unset($this);
	}


}
?> 

