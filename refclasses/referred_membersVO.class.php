<?php 
class referred_membersVO 
{
	public $refer_id;
	public $refer_by;
	public $refer_to;
	public $refer_date;
	public $refer_level;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->refer_id;
		$this->refer_by= $this->refer_by;
		$this->refer_to= $this->refer_to;
		$this->refer_date= $this->refer_date;
		$this->refer_level= $this->refer_level;
	}


	function formatFetchVariables()
	{
		$this->refer_id;
		$this->refer_by= $this->refer_by;
		$this->refer_to= $this->refer_to;
		$this->refer_date= $this->refer_date;
		$this->refer_level= $this->refer_level;
	}


	function getPostVars($array)
	{
		$this->refer_id= $array['refer_id'];
		$this->refer_by= $array['refer_by'];
		$this->refer_to= $array['refer_to'];
		$this->refer_date= $array['refer_date'];
		$this->refer_level= $array['refer_level'];
	}


	function __destruct()
	{
		unset($this->refer_id);
		unset($this->refer_by);
		unset($this->refer_to);
		unset($this->refer_date);
		unset($this->refer_level);
		unset($this);
	}


}
?> 

