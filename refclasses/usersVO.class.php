<?php 
class usersVO
{
	public $user_id;
	public $full_name;
	public $username;
	public $password;
	public $designation;
	public $email;
	public $joined_date;
	public $status;
	public $remarks;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->user_id;
		$this->full_name= $this->full_name;
		$this->username= $this->username;
		$this->password= $this->password;
		$this->designation= $this->designation;
		$this->email= $this->email;
		$this->joined_date= $this->joined_date;
		$this->status= $this->status;
		$this->remarks= $this->remarks;
		$this->entered_by= $this->entered_by;
		$this->entered_date= $this->entered_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function formatFetchVariables()
	{
		$this->user_id;
		$this->full_name= $this->full_name;
		$this->username= $this->username;
		$this->password= $this->password;
		$this->designation= $this->designation;
		$this->email= $this->email;
		$this->joined_date= $this->joined_date;
		$this->status= $this->status;
		$this->remarks= $this->remarks;
		$this->entered_by= $this->entered_by;
		$this->entered_date= $this->entered_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function getPostVars($array)
	{
		$this->user_id= $array['user_id'];
		$this->full_name= $array['full_name'];
		$this->username= $array['username'];
		$this->password= $array['password'];
		$this->designation= $array['designation'];
		$this->email= $array['email'];
		$this->joined_date= $array['joined_date'];
		$this->status= $array['status'];
		$this->remarks= $array['remarks'];
		$this->entered_by= $array['entered_by'];
		$this->entered_date= $array['entered_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
	}


	function __destruct()
	{
		unset($this->user_id);
		unset($this->full_name);
		unset($this->username);
		unset($this->password);
		unset($this->designation);
		unset($this->email);
		unset($this->joined_date);
		unset($this->status);
		unset($this->remarks);
		unset($this->entered_by);
		unset($this->entered_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this);
	}


}
?> 