<?php 
class program_signupDAO extends BaseDAO
{
	public $program_id;
	public $program_name;
	public $user_id;
	public $referral_url;
	public $banner_url;
	public $program_type;
	public $earning_per_clk;
	public $earning_per_ref_clk;
	public $min_payout;
	public $instant_payout;
	public $payment_processor;
	public $downline_level;
	public $program_language;
	public $contract_duration;
	public $spend_credits;
	public $discount;
	public $total;
	public $task_desc;
	public $allowed_mem_rat;
	public $not_allowed_mem;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;
	public $status;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_program_signup
	SET
		user_id= '$vo->user_id',
		program_name= '$vo->program_name',
		referral_url= '$vo->referral_url',
		banner_url= '$vo->banner_url',
		program_type= '$vo->program_type',
		earning_per_clk= '$vo->earning_per_clk',
		earning_per_ref_clk= '$vo->earning_per_ref_clk',
		min_payout= '$vo->min_payout',
		instant_payout= '$vo->instant_payout',
		payment_processor= '$vo->payment_processor',
		downline_level= '$vo->downline_level',
		program_language= '$vo->program_language',
		contract_duration= '$vo->contract_duration',
		spend_credits= '$vo->spend_credits',
		discount= '$vo->discount',
		total= '$vo->total',
		task_desc= '$vo->task_desc',
		allowed_mem_rat= '$vo->allowed_mem_rat',
		not_allowed_mem= '$vo->not_allowed_mem',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count',
		status= '$vo->status'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_program_signup WHERE status='1' ORDER BY program_id";
		else
			$this->sql = "SELECT * FROM tbl_program_signup ORDER BY program_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllById($id)
	{
		
			$this->sql = "SELECT * FROM tbl_program_signup WHERE  created_by='$id'";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllActiveById($id,$status='1')
	{
		
			$this->sql = "SELECT * FROM tbl_program_signup WHERE  created_by='$id' AND status='1'";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchMostProgramReferrals()
	{
		
			$this->sql = "select tbl.user_id ,tbl.total from (select user_id,count(program_id) as total from tbl_program_signup as t1 group by user_id) as tbl

ORDER BY tbl.total desc  limit 20";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->user_id= $rows['user_id'];
				$vo->total= $rows['total'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchMostReferralsAccepted()
	{
		
			$this->sql = "select tbl.vote_to ,tbl.total from (select vote_to,count(id) as total from tbl_voting as t1 WHERE vote_type='promotor' AND vote_value='accept' group by vote_to) as tbl

ORDER BY tbl.total desc  limit 20";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->vote_to= $rows['vote_to'];
				$vo->total= $rows['total'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllByLastDays($id,$days)
	{
		
			$this->sql = "SELECT * FROM tbl_program_signup WHERE  created_by='$id' AND created_date BETWEEN DATE_SUB( CURDATE() ,INTERVAL $days DAY ) AND CURDATE()";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchProgramGroupByProgramName($program_type)
	{
		
			$this->sql = "SELECT
			tbl_program_signup.program_id,
			tbl_program_signup.program_name,
			tbl_program_signup.user_id,
			tbl_program_signup.referral_url,
			tbl_program_signup.banner_url,
			tbl_program_signup.program_type,
			tbl_program_signup.earning_per_clk,
			tbl_program_signup.earning_per_ref_clk,
			tbl_program_signup.min_payout,
			tbl_program_signup.instant_payout,
			tbl_program_signup.payment_processor,
			tbl_program_signup.downline_level,
			tbl_program_signup.program_language,
			tbl_program_signup.contract_duration,
			Max(tbl_program_signup.spend_credits) as spend_credits,
			tbl_program_signup.discount,
			tbl_program_signup.total,
			tbl_program_signup.task_desc,
			tbl_program_signup.allowed_mem_rat,
			tbl_program_signup.not_allowed_mem,
			tbl_program_signup.created_by,
			tbl_program_signup.created_date,
			tbl_program_signup.updated_by,
			tbl_program_signup.updated_date,
			tbl_program_signup.update_count,
			tbl_program_signup.`status`
			from tbl_program_signup
			where program_type='$program_type' AND status='1'
			GROUP BY
			tbl_program_signup.program_name";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllGroupByProgramType()
	{
		
			$this->sql = "select * from tbl_program_signup GROUP BY program_type";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllByProgramName($name)
	{
		
			$this->sql = "select * from tbl_program_signup where program_name='$name' AND status='1' ORDER BY user_id";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchUniqueByProgramName($name)
	{
		
			$this->sql = "SELECT
tbl_program_signup.program_id,
tbl_program_signup.program_name,
tbl_program_signup.user_id,
tbl_program_signup.referral_url,
tbl_program_signup.banner_url,
tbl_program_signup.program_type,
tbl_program_signup.earning_per_clk,
tbl_program_signup.earning_per_ref_clk,
tbl_program_signup.min_payout,
tbl_program_signup.instant_payout,
tbl_program_signup.payment_processor,
tbl_program_signup.downline_level,
tbl_program_signup.program_language,
tbl_program_signup.contract_duration,
Max(tbl_program_signup.spend_credits) as spend_credits,
tbl_program_signup.discount,
tbl_program_signup.total,
tbl_program_signup.task_desc,
tbl_program_signup.allowed_mem_rat,
tbl_program_signup.not_allowed_mem,
tbl_program_signup.created_by,
tbl_program_signup.created_date,
tbl_program_signup.updated_by,
tbl_program_signup.updated_date,
tbl_program_signup.update_count,
tbl_program_signup.`status`
from tbl_program_signup
where program_name='$name'  AND status='1'
GROUP BY
tbl_program_signup.user_id
";
		
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}

	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_program_signup ORDER BY program_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_program_signup ORDER BY program_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signupVO();
				$vo->program_id= $rows['program_id'];
				$vo->program_name= $rows['program_name'];
				$vo->user_id= $rows['user_id'];
				$vo->referral_url= $rows['referral_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->program_type= $rows['program_type'];
				$vo->earning_per_clk= $rows['earning_per_clk'];
				$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
				$vo->min_payout= $rows['min_payout'];
				$vo->instant_payout= $rows['instant_payout'];
				$vo->payment_processor= $rows['payment_processor'];
				$vo->downline_level= $rows['downline_level'];
				$vo->program_language= $rows['program_language'];
				$vo->contract_duration= $rows['contract_duration'];
				$vo->spend_credits= $rows['spend_credits'];
				$vo->discount= $rows['discount'];
				$vo->total= $rows['total'];
				$vo->task_desc= $rows['task_desc'];
				$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
				$vo->not_allowed_mem= $rows['not_allowed_mem'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_program_signup
	SET
		user_id= '$vo->user_id',
		program_name='$vo->program_name',
		referral_url= '$vo->referral_url',
		banner_url= '$vo->banner_url',
		program_type= '$vo->program_type',
		earning_per_clk= '$vo->earning_per_clk',
		earning_per_ref_clk= '$vo->earning_per_ref_clk',
		min_payout= '$vo->min_payout',
		instant_payout= '$vo->instant_payout',
		payment_processor= '$vo->payment_processor',
		downline_level= '$vo->downline_level',
		program_language= '$vo->program_language',
		contract_duration= '$vo->contract_duration',
		spend_credits= '$vo->spend_credits',
		discount= '$vo->discount',
		total= '$vo->total',
		task_desc= '$vo->task_desc',
		allowed_mem_rat= '$vo->allowed_mem_rat',
		not_allowed_mem= '$vo->not_allowed_mem',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count',
		status= '$vo->status'
	WHERE program_id= '$vo->program_id'";
		return $this->exec();
	}


	function fetchDetails($program_id)
	{
		$this->sql = "SELECT * FROM tbl_program_signup WHERE program_id= '$program_id'";
		$this->exec();
		$vo = new program_signupVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->program_name= $rows['program_name'];
			$vo->program_id= $rows['program_id'];
			$vo->user_id= $rows['user_id'];
			$vo->referral_url= $rows['referral_url'];
			$vo->banner_url= $rows['banner_url'];
			$vo->program_type= $rows['program_type'];
			$vo->earning_per_clk= $rows['earning_per_clk'];
			$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
			$vo->min_payout= $rows['min_payout'];
			$vo->instant_payout= $rows['instant_payout'];
			$vo->payment_processor= $rows['payment_processor'];
			$vo->downline_level= $rows['downline_level'];
			$vo->program_language= $rows['program_language'];
			$vo->contract_duration= $rows['contract_duration'];
			$vo->spend_credits= $rows['spend_credits'];
			$vo->discount= $rows['discount'];
			$vo->total= $rows['total'];
			$vo->task_desc= $rows['task_desc'];
			$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
			$vo->not_allowed_mem= $rows['not_allowed_mem'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchLatestHighestProgram()
	{
		$this->sql = "SELECT
*
FROM
tbl_program_signup
WHERE
tbl_program_signup.spend_credits=(select max(spend_credits) from tbl_program_signup ORDER BY
tbl_program_signup.created_date DESC Limit 1)";
		$this->exec();
		$vo = new program_signupVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->program_name= $rows['program_name'];
			$vo->program_id= $rows['program_id'];
			$vo->user_id= $rows['user_id'];
			$vo->referral_url= $rows['referral_url'];
			$vo->banner_url= $rows['banner_url'];
			$vo->program_type= $rows['program_type'];
			$vo->earning_per_clk= $rows['earning_per_clk'];
			$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
			$vo->min_payout= $rows['min_payout'];
			$vo->instant_payout= $rows['instant_payout'];
			$vo->payment_processor= $rows['payment_processor'];
			$vo->downline_level= $rows['downline_level'];
			$vo->program_language= $rows['program_language'];
			$vo->contract_duration= $rows['contract_duration'];
			$vo->spend_credits= $rows['spend_credits'];
			$vo->discount= $rows['discount'];
			$vo->total= $rows['total'];
			$vo->task_desc= $rows['task_desc'];
			$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
			$vo->not_allowed_mem= $rows['not_allowed_mem'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchDetailsByProgramName($program_name)
	{
		$this->sql = "SELECT
		tbl_program_signup.program_id,
		tbl_program_signup.program_name,
		tbl_program_signup.user_id,
		tbl_program_signup.referral_url,
		tbl_program_signup.banner_url,
		tbl_program_signup.program_type,
		tbl_program_signup.earning_per_clk,
		tbl_program_signup.earning_per_ref_clk,
		tbl_program_signup.min_payout,
		tbl_program_signup.instant_payout,
		tbl_program_signup.payment_processor,
		tbl_program_signup.downline_level,
		tbl_program_signup.program_language,
		tbl_program_signup.contract_duration,
		Max(tbl_program_signup.spend_credits) as spend_credits,
		tbl_program_signup.discount,
		tbl_program_signup.total,
		tbl_program_signup.task_desc,
		tbl_program_signup.allowed_mem_rat,
		tbl_program_signup.not_allowed_mem,
		tbl_program_signup.created_by,
		tbl_program_signup.created_date,
		tbl_program_signup.updated_by,
		tbl_program_signup.updated_date,
		tbl_program_signup.update_count,
		tbl_program_signup.`status`
		from tbl_program_signup
		
		where 
		tbl_program_signup.program_name='$program_name' AND status='1'
		GROUP BY
		tbl_program_signup.program_name";
		$this->exec();
		$vo = new program_signupVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->program_name= $rows['program_name'];
			$vo->program_id= $rows['program_id'];
			$vo->user_id= $rows['user_id'];
			$vo->referral_url= $rows['referral_url'];
			$vo->banner_url= $rows['banner_url'];
			$vo->program_type= $rows['program_type'];
			$vo->earning_per_clk= $rows['earning_per_clk'];
			$vo->earning_per_ref_clk= $rows['earning_per_ref_clk'];
			$vo->min_payout= $rows['min_payout'];
			$vo->instant_payout= $rows['instant_payout'];
			$vo->payment_processor= $rows['payment_processor'];
			$vo->downline_level= $rows['downline_level'];
			$vo->program_language= $rows['program_language'];
			$vo->contract_duration= $rows['contract_duration'];
			$vo->spend_credits= $rows['spend_credits'];
			$vo->discount= $rows['discount'];
			$vo->total= $rows['total'];
			$vo->task_desc= $rows['task_desc'];
			$vo->allowed_mem_rat= $rows['allowed_mem_rat'];
			$vo->not_allowed_mem= $rows['not_allowed_mem'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}

function ActivateNdactivate($id, $publish)
			{
			$vo= $this->fetchDetails($id);
			if($vo->program_id != 0)
				{
				$this->sql = "UPDATE tbl_program_signup SET status = '$publish' WHERE program_id = '$vo->program_id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}
			
	function remove($program_id)
	{
		$vo=$this->fetchDetails($program_id);
		if($vo->program_id!=0)
		{
			$this->sql = "DELETE FROM tbl_program_signup WHERE program_id= '$program_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

