<?php 
class premium_membersVO 
{
	public $id;
	public $user_id;
	public $mem_start_dt;
	public $mem_end_dt;
	public $mem_month;
	public $status;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id= $this->id;
		$this->user_id= $this->user_id;
		$this->mem_start_dt= $this->mem_start_dt;
		$this->mem_end_dt= $this->mem_end_dt;
		$this->mem_month= $this->mem_month;
		$this->status= $this->status;
	}


	function formatFetchVariables()
	{
		$this->id= $this->id;
		$this->user_id= $this->user_id;
		$this->mem_start_dt= $this->mem_start_dt;
		$this->mem_end_dt= $this->mem_end_dt;
		$this->mem_month= $this->mem_month;
		$this->status= $this->status;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->user_id= $array['user_id'];
		$this->mem_start_dt= $array['mem_start_dt'];
		$this->mem_end_dt= $array['mem_end_dt'];
		$this->mem_month= $array['mem_month'];
		$this->status= $array['status'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->user_id);
		unset($this->mem_start_dt);
		unset($this->mem_end_dt);
		unset($this->mem_month);
		unset($this->status);
		unset($this);
	}


}
?> 

