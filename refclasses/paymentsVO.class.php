<?php 
class paymentsVO 
{
	public $id;
	public $buyer_id;
	public $txnid;
	public $buyer_firstname;
	public $buyer_lastname;
	public $buyer_email;
	public $payment_amount;
	public $payment_status;
	public $payment_type;
	public $itemid;
	public $createdtime;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id;
		$this->buyer_id= $this->buyer_id;
		$this->txnid= $this->txnid;
		$this->buyer_firstname= $this->buyer_firstname;
		$this->buyer_lastname= $this->buyer_lastname;
		$this->buyer_email= $this->buyer_email;
		$this->payment_amount= $this->payment_amount;
		$this->payment_status= $this->payment_status;
		$this->payment_type= $this->payment_type;
		$this->itemid= $this->itemid;
		$this->createdtime= $this->createdtime;
	}


	function formatFetchVariables()
	{
		$this->id;
		$this->buyer_id= $this->buyer_id;
		$this->txnid= $this->txnid;
		$this->buyer_firstname= $this->buyer_firstname;
		$this->buyer_lastname= $this->buyer_lastname;
		$this->buyer_email= $this->buyer_email;
		$this->payment_amount= $this->payment_amount;
		$this->payment_status= $this->payment_status;
		$this->payment_type= $this->payment_type;
		$this->itemid= $this->itemid;
		$this->createdtime= $this->createdtime;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->buyer_id= $array['buyer_id'];
		$this->txnid= $array['txnid'];
		$this->buyer_firstname= $array['buyer_firstname'];
		$this->buyer_lastname= $array['buyer_lastname'];
		$this->buyer_email= $array['buyer_email'];
		$this->payment_amount= $array['payment_amount'];
		$this->payment_status= $array['payment_status'];
		$this->payment_type= $array['payment_type'];
		$this->itemid= $array['itemid'];
		$this->createdtime= $array['createdtime'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->buyer_id);
		unset($this->txnid);
		unset($this->buyer_firstname);
		unset($this->buyer_lastname);
		unset($this->buyer_email);
		unset($this->payment_amount);
		unset($this->payment_status);
		unset($this->payment_type);
		unset($this->itemid);
		unset($this->createdtime);
		unset($this);
	}


}
?> 

