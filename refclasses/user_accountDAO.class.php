<?php 
class user_accountDAO extends BaseDAO
{
	public $account_id;
	public $user_id;
	public $txn_id;
	public $created_date;
	public $credit_type;
	public $credits;
	


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_user_account
	SET
		user_id= '$vo->user_id',
		txn_id= '$vo->txn_id',
		created_date= '$vo->created_date',
		credit_type= '$vo->credit_type',
		credits= '$vo->credits'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_user_account ORDER BY account_id";
		else
			$this->sql = "SELECT * FROM tbl_user_account ORDER BY account_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllById($id)
	{
		
			$this->sql = "SELECT * FROM tbl_user_account WHERE user_id='$id' ORDER BY account_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	//function to get the total clicks plus member clicking the advertisment info
	function fetchAllClicksInfoById($id)
	{
		
			$this->sql = "SELECT
tbl_siteusers.username,
tbl_siteusers.mem_type,
count(tbl_siteusers.username) as totalclicks
FROM
tbl_user_account
INNER JOIN tbl_siteusers ON tbl_user_account.user_id = tbl_siteusers.user_id
WHERE
tbl_user_account.txn_id ='$id' AND credit_type='clicking ads' GROUP BY tbl_siteusers.username";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->username= $rows['username'];
				$vo->mem_type= $rows['mem_type'];
				$vo->totalclicks= $rows['totalclicks'];
				
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}



	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_user_account ORDER BY account_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_user_account ORDER BY account_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchLimitedById($id,$page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_user_account WHERE user_id='$id' ORDER BY account_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_user_account WHERE user_id='$id' ORDER BY account_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_user_account
	SET
		user_id= '$vo->user_id',
		txn_id= '$vo->txn_id',
		created_date= '$vo->created_date',
		credit_type= '$vo->credit_type',
		credits= '$vo->credits'
	WHERE account_id= '$vo->account_id'";
		return $this->exec();
	}


	function fetchDetails($account_id)
	{
		$this->sql = "SELECT * FROM tbl_user_account WHERE account_id= '$account_id'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits= $rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchParticularReferralPaymentById($uid,$pid)//fetches info of any user payed for particular program signup
	{
		$this->sql = "SELECT * FROM tbl_user_account WHERE user_id= '$uid' AND txn_id='$pid' AND credit_type='referral requests'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits= $rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	
	
function fetchCurrentDetailsByUserId($user_id,$adv_id)
	{
		$this->sql = "SELECT * FROM tbl_user_account INNER JOIN tbl_siteusers ON tbl_siteusers.user_id = tbl_user_account.user_id WHERE date(tbl_user_account.created_date) = date(curdate()) AND tbl_user_account.user_id ='$user_id' AND tbl_user_account.txn_id ='$adv_id' AND credit_type='clicking ads'";
		
		$this->exec();
		
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				if($rows!='')
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			
		}
		else
		{
			return 0;
		}
		
	}
	
	function fetchAllAdsByUserIdAdvId($user_id,$adv_id)
	{
		$this->sql = "SELECT
tbl_user_account.account_id,
tbl_user_account.user_id,
tbl_user_account.txn_id,
tbl_user_account.created_date,
tbl_user_account.credit_type,
tbl_user_account.credits
FROM tbl_user_account 
INNER JOIN tbl_siteusers ON tbl_siteusers.user_id = tbl_user_account.user_id
WHERE tbl_user_account.user_id ='$user_id' AND tbl_user_account.txn_id ='$adv_id' AND credit_type='clicking ads'";
		$this->exec();
		
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchCountCurrentDetailsByUserId($user_id)
	{
		$this->sql = "SELECT * FROM tbl_user_account INNER JOIN tbl_siteusers ON tbl_siteusers.user_id = tbl_user_account.user_id WHERE date(tbl_user_account.created_date) = date(curdate()) AND tbl_user_account.user_id ='$user_id' AND credit_type='clicking ads'";
		$this->exec();
		
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
		
	}
	
	function fetchAllAds24HrsInfo($adv_id,$cond='out')
	{
	if($cond=='in')
	{
		$this->sql = "SELECT * FROM tbl_user_account  WHERE date(created_date) = date(curdate()) AND txn_id ='$adv_id' AND credit_type='clicking ads'";
	}
	else
	{
		$this->sql = "SELECT * FROM tbl_user_account  WHERE date(created_date) != date(curdate()) AND txn_id ='$adv_id' AND credit_type='clicking ads'";
	}
	
		$this->exec();
		
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				
					$vo->account_id= $rows['account_id'];
					$vo->user_id= $rows['user_id'];
					$vo->txn_id= $rows['txn_id'];
					$vo->created_date= $rows['created_date'];
					$vo->credit_type= $rows['credit_type'];
					$vo->credits= $rows['credits'];
					$vo->formatFetchVariables();
					array_push($list, $vo);
				
			}
		}
		return $list;
		
	}
	
	function fetchTotalClicksByUserId($user_id)
	{
		$this->sql = "SELECT COUNT(credits) as clicks FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='clicking ads'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->clicks=$rows['clicks'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchTotalCreditsByType($user_id,$type)
	{
		$this->sql = "SELECT SUM(credits) as total FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='$type'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->total=$rows['total'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchTotalSignupCreditsByTypeBetweenDates($user_id,$txnid,$type='program signup',$from,$to)
	{
		$this->sql = "SELECT SUM(credits) as total FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='$type' AND txn_id='$txnid'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->total=$rows['total'];
			$vo->formatFetchVariables();
		}
		return $vo->total;
	}
	function fetchTotalSignupCreditsListByTypeBetweenDates($user_id,$txnid,$type='program signup',$from,$to)
	{
		$this->sql = "SELECT * FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='$type' AND txn_id='$txnid' AND created_date BETWEEN '$from' AND '$to'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchTotalCreditsByTypeAndTrxnId($user_id,$txnid,$type)
	{
		$this->sql = "SELECT SUM(credits) as total FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='$type' AND txn_id='$txnid'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->total=$rows['total'];
			$vo->formatFetchVariables();
		}
		return $vo->total;
	}
	
	function fetchTotalSignupByUserId($user_id)
	{
		$this->sql = "SELECT COUNT(credits) as signup FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='program signup'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->signup=$rows['signup'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchAdvCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='clicking ads'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchProgramSignUpCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='program signup'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchReferMemClickCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='referring new members click'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchReferMemSignupCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='referring new members signup'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchPurchagedCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='purchaged credits'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchReferralCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='referral requests'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchAdvPackageCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='advertisment packages'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchJackpotGamePackageCreditsByUserId($user_id)
	{
		$this->sql = "SELECT SUM(credits) as credits FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='jackpot game tickets'";
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo->credits;
	}
	
	function fetchClickDetailsByUserId($user_id)
	{
		$this->sql = "SELECT * FROM tbl_user_account WHERE user_id='$user_id' AND credit_type='clicking ads'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new user_accountVO();
				$vo->account_id= $rows['account_id'];
				$vo->user_id= $rows['user_id'];
				$vo->txn_id= $rows['txn_id'];
				$vo->created_date= $rows['created_date'];
				$vo->credit_type= $rows['credit_type'];
				$vo->credits= $rows['credits'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	//used to find if credit has been offered for the signup in the program in the given date
	function fetchSignUpByDate($user_id,$txn_id)
	{
		$this->sql = "SELECT *  FROM tbl_user_account WHERE user_id='$user_id' AND txn_id='$txn_id' AND credit_type='program signup' AND DATE(created_date) =CURDATE()";
		
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	//used to find if credit has been offered for the signup in the program in the given date time
	function fetchSignUpByDateTime($user_id,$txn_id,$date)
	{
		$this->sql = "SELECT *  FROM tbl_user_account WHERE user_id='$user_id' AND txn_id='$txn_id' AND credit_type='program signup' AND created_date ='$date'";
		
		$this->exec();
		$vo = new user_accountVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->account_id= $rows['account_id'];
			$vo->user_id= $rows['user_id'];
			$vo->txn_id= $rows['txn_id'];
			$vo->created_date= $rows['created_date'];
			$vo->credit_type= $rows['credit_type'];
			$vo->credits=$rows['credits'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}

	function remove($account_id)
	{
		$vo=$this->fetchDetails($account_id);
		if($vo->account_id!=0)
		{
			$this->sql = "DELETE FROM tbl_user_account WHERE account_id= '$account_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function removeReferral($uid,$txnid)
	{
		$vo=$this->fetchParticularReferralPaymentById($uid,$txnid);
		if($vo->account_id!=0)
		{
			$this->sql = "DELETE FROM tbl_user_account WHERE account_id= '$vo->account_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

