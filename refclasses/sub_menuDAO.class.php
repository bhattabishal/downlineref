<?php 
class sub_menuDAO extends BaseDAO
{
	public $sub_menu_id;
	public $name_en;
	public $name_np;
	public $sub_menu_index;
	public $menu_id;
	public $publish;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_sub_menu
	SET
		name_en= '$vo->name_en',
		sub_menu_index= '$vo->sub_menu_index',
		menu_id= '$vo->menu_id',
		publish= '$vo->publish',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'";
		
		return $this->exec();   
	}
	
	function fetchAll($parent_id, $par="all")
			{
			$this->sql = "SELECT * FROM tbl_sub_menu WHERE menu_id = '$parent_id' ORDER BY sub_menu_id";
			if($par != "all")
				$this->sql = "SELECT * FROM tbl_sub_menu WHERE menu_id = '$parent_id' AND publish='yes' ORDER BY sub_menu_id";
			
			
			$this->exec();//stores result in $this->result
			//echo $this->sql;
			
			$list = array();
			if($this->result)
				{
				while($rows = $this->fetch())//fetch result stored in $this->result
					{
					$vo = new sub_menuVO();
					
					$vo->sub_menu_id= $rows['sub_menu_id'];
					$vo->name_en= $rows['name_en'];
					$vo->name_np= $rows['name_np'];
					$vo->sub_menu_index= $rows['sub_menu_index'];
					$vo->menu_id= $rows['menu_id'];
					$vo->publish= $rows['publish'];
					$vo->entered_by= $rows['entered_by'];
					$vo->entered_date= $rows['entered_date'];
					$vo->updated_by= $rows['updated_by'];
					$vo->updated_date= $rows['updated_date'];
					$vo->update_count= $rows['update_count'];
					$vo->formatFetchVariables();
					array_push($list, $vo);
					}
				}
			return $list;
			}
			
		function fetchAllCond($cond="", $par="all")
			{
			$this->sql = "SELECT * FROM tbl_sub_menu".($cond!=""?" WHERE ".$cond:"")." ORDER BY sub_menu_id";
			if($par != "all")
				$this->sql = "SELECT * FROM tbl_sub_menu WHERE publish='yes'  ".($cond!=""?" AND ".$cond:"")." ORDER BY sub_menu_id";
			
			
			$this->exec();//stores result in $this->result
			
			$list = array();
			if($this->result)
				{
				while($rows = $this->fetch())//fetch result stored in $this->result
					{
					$vo = new sub_menuVO();
					
					$vo->sub_menu_id= $rows['sub_menu_id'];
					$vo->name_en= $rows['name_en'];
					$vo->name_np= $rows['name_np'];
					$vo->sub_menu_index= $rows['sub_menu_index'];
					$vo->menu_id= $rows['menu_id'];
					$vo->publish= $rows['publish'];
					$vo->entered_by= $rows['entered_by'];
					$vo->entered_date= $rows['entered_date'];
					$vo->updated_by= $rows['updated_by'];
					$vo->updated_date= $rows['updated_date'];
					$vo->update_count= $rows['update_count'];
					$vo->formatFetchVariables();
					array_push($list, $vo);
					}
				}
			return $list;
			}
	
/*
	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_sub_menu ORDER BY sub_menu_id";
		else
			$this->sql = "SELECT * FROM tbl_sub_menu ORDER BY sub_menu_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new sub_menuVO();
				$vo->sub_menu_id= $rows['sub_menu_id'];
				$vo->name= $rows['name'];
				$vo->sub_menu_index= $rows['sub_menu_index'];
				$vo->menu_id= $rows['menu_id'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}

*/
	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_sub_menu ORDER BY sub_menu_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_sub_menu ORDER BY sub_menu_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new sub_menuVO();
				$vo->sub_menu_id= $rows['sub_menu_id'];
				$vo->name_en= $rows['name_en'];
				$vo->name_np= $rows['name_np'];
				$vo->sub_menu_index= $rows['sub_menu_index'];
				$vo->menu_id= $rows['menu_id'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_sub_menu
	SET
		name_en= '$vo->name_en',
		sub_menu_index= '$vo->sub_menu_index',
		menu_id= '$vo->menu_id',
		publish= '$vo->publish',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'
	WHERE sub_menu_id= '$vo->sub_menu_id'";
		return $this->exec();
	}


	function fetchDetails($sub_menu_id)
	{
		$this->sql = "SELECT * FROM tbl_sub_menu WHERE sub_menu_id= '$sub_menu_id'";
		$this->exec();
		$vo = new sub_menuVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->sub_menu_id= $rows['sub_menu_id'];
			$vo->name_en= $rows['name_en'];
			$vo->name_np= $rows['name_np'];
			$vo->sub_menu_index= $rows['sub_menu_index'];
			$vo->menu_id= $rows['menu_id'];
			$vo->publish= $rows['publish'];
			$vo->entered_by= $rows['entered_by'];
			$vo->entered_date= $rows['entered_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}

	function publishNunpublish($sub_menu_id, $publishflag='yes')
		{
		$vo = $this->fetchDetails($sub_menu_id);
		
		if($vo->sub_menu_id != 0)
			{
			$this->sql = "UPDATE tbl_sub_menu SET publish='$publishflag' WHERE sub_menu_id = '$vo->sub_menu_id'";
			$this->exec();
			if($this->result)
				return true;
			else
				return false;
			}
		return false;
		}	

	function remove($sub_menu_id)
	{
		$vo=$this->fetchDetails($sub_menu_id);
		if($vo->sub_menu_id!=0)
		{
			$this->sql = "DELETE FROM tbl_sub_menu WHERE sub_menu_id= '$sub_menu_id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

