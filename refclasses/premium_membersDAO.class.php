<?php 
class premium_membersDAO extends BaseDAO
{
	public $id;
	public $user_id;
	public $mem_start_dt;
	public $mem_end_dt;
	public $mem_month;
	public $status;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_premium_members
	SET
		id= '$vo->id',
		user_id= '$vo->user_id',
		mem_start_dt= '$vo->mem_start_dt',
		mem_end_dt= '$vo->mem_end_dt',
		mem_month= '$vo->mem_month',
		status= '$vo->status'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_premium_members ORDER BY id";
		else
			$this->sql = "SELECT * FROM tbl_premium_members ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new premium_membersVO();
				$vo->id= $rows['id'];
				$vo->user_id= $rows['user_id'];
				$vo->mem_start_dt= $rows['mem_start_dt'];
				$vo->mem_end_dt= $rows['mem_end_dt'];
				$vo->mem_month= $rows['mem_month'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_premium_members ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_premium_members ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new premium_membersVO();
				$vo->id= $rows['id'];
				$vo->user_id= $rows['user_id'];
				$vo->mem_start_dt= $rows['mem_start_dt'];
				$vo->mem_end_dt= $rows['mem_end_dt'];
				$vo->mem_month= $rows['mem_month'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_premium_members
	SET
		id= '$vo->id',
		user_id= '$vo->user_id',
		mem_start_dt= '$vo->mem_start_dt',
		mem_end_dt= '$vo->mem_end_dt',
		mem_month= '$vo->mem_month',
		status= '$vo->status'
	WHERE id= '$vo->id'";
		return $this->exec();
	}
	
	function updateStatus($vo)
	{
	$this->sql="UPDATE tbl_premium_members
	SET
		status= '$vo->status'
	WHERE user_id= '$vo->user_id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM tbl_premium_members WHERE id= '$id'";
		$this->exec();
		$vo = new premium_membersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->user_id= $rows['user_id'];
			$vo->mem_start_dt= $rows['mem_start_dt'];
			$vo->mem_end_dt= $rows['mem_end_dt'];
			$vo->mem_month= $rows['mem_month'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchActiveMembershipDetails($id)
	{
		$this->sql = "SELECT
max(id) as id,user_id,mem_start_dt,mem_end_dt,mem_month,`status`
FROM
tbl_premium_members
WHERE
tbl_premium_members.user_id = '$id' AND status='1'";
		$this->exec();
		$vo = new premium_membersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->user_id= $rows['user_id'];
			$vo->mem_start_dt= $rows['mem_start_dt'];
			$vo->mem_end_dt= $rows['mem_end_dt'];
			$vo->mem_month= $rows['mem_month'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM tbl_premium_members WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

