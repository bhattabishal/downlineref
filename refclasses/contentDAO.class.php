<?php 
class contentDAO extends BaseDAO
{
	public $content_id;
	public $heading;
	public $heading_np;
	public $page_title;
	public $page_metatag;
	public $page_keywords;
	public $page_description;
	public $detail_desc_en;
	public $brief_desc_en;
	public $detail_desc_np;
	public $brief_desc_np;
	public $menu_id;
	public $sub_menu_id;
	public $publish;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;

	function __construct()
	{
	}


	function Insert($vo)
	{
		//$vo->formatInsertVariables();
	$this->sql="INSERT INTO tbl_content
	SET
		heading= '$vo->heading',
		heading_np= '$vo->heading_np',
		page_title= '$vo->page_title',
		page_metatag= '$vo->page_metatag',
		page_keywords= '$vo->page_keywords',
		page_description= '$vo->page_description',
		detail_desc_en= '$vo->detail_desc_en',
		brief_desc_en= '$vo->brief_desc_en',
		detail_desc_np= '$vo->detail_desc_np',
		brief_desc_np= '$vo->brief_desc_np',
		menu_id= '$vo->menu_id',
		sub_menu_id= '$vo->sub_menu_id',
		publish= '$vo->publish',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'";
		
		//die (" insert : ".$this->sql);
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_content ORDER BY content_id";
		else
			$this->sql = "SELECT * FROM tbl_content ORDER BY content_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new contentVO();
				$vo->content_id= $rows['content_id'];
				$vo->heading= $rows['heading'];
				$vo->heading_np= $rows['heading_np'];
				$vo->page_title= $rows['page_title'];
				$vo->page_metatag= $rows['page_metatag'];
				$vo->page_keywords= $rows['page_keywords'];
				$vo->page_description= $rows['page_description'];
				$vo->detail_desc_en= $rows['detail_desc_en'];
				$vo->brief_desc_en= $rows['brief_desc_en'];
				$vo->detail_desc_np= $rows['detail_desc_np'];
				$vo->brief_desc_np= $rows['brief_desc_np'];
				$vo->menu_id= $rows['menu_id'];
				$vo->sub_menu_id= $rows['sub_menu_id'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_content ORDER BY content_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_content ORDER BY content_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new contentVO();
				$vo->content_id= $rows['content_id'];
				$vo->heading= $rows['heading'];
				$vo->heading_np= $rows['heading_np'];
				$vo->page_title= $rows['page_title'];
				$vo->page_metatag= $rows['page_metatag'];
				$vo->page_keywords= $rows['page_keywords'];
				$vo->page_description= $rows['page_description'];
				$vo->detail_desc_en= $rows['detail_desc_en'];
				$vo->brief_desc_en= $rows['brief_desc_en'];
				$vo->detail_desc_np= $rows['detail_desc_np'];
				$vo->brief_desc_np= $rows['brief_desc_np'];
				$vo->menu_id= $rows['menu_id'];
				$vo->sub_menu_id= $rows['sub_menu_id'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_content
	SET
		heading= '$vo->heading',
		heading_np= '$vo->heading_np',
		page_title= '$vo->page_title',
		page_metatag= '$vo->page_metatag',
		page_keywords= '$vo->page_keywords',
		page_description= '$vo->page_description',
		detail_desc_en= '$vo->detail_desc_en',
		brief_desc_en= '$vo->brief_desc_en',
		detail_desc_np= '$vo->detail_desc_np',
		brief_desc_np= '$vo->brief_desc_np',
		menu_id= '$vo->menu_id',
		sub_menu_id= '$vo->sub_menu_id',
		publish= '$vo->publish',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count' 
	WHERE content_id= '$vo->content_id'";
	//echo $this->sql;
//	exit();
		//die (" update : ".$this->sql);

		return $this->exec();
	}


	function fetchDetails($content_id)
	{
		$this->sql = "SELECT * FROM tbl_content WHERE content_id= '$content_id'";
		$this->exec();
		$vo = new contentVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->content_id= $rows['content_id'];
			$vo->heading= $rows['heading'];
			$vo->heading_np= $rows['heading_np'];
			$vo->page_title= $rows['page_title'];
			$vo->page_metatag= $rows['page_metatag'];
			$vo->page_keywords= $rows['page_keywords'];
			$vo->page_description= $rows['page_description'];
			$vo->detail_desc_en= $rows['detail_desc_en'];
			$vo->brief_desc_en= $rows['brief_desc_en'];
			$vo->detail_desc_np= $rows['detail_desc_np'];
			$vo->brief_desc_np= $rows['brief_desc_np'];
			$vo->menu_id= $rows['menu_id'];
			$vo->sub_menu_id= $rows['sub_menu_id'];
			$vo->publish= $rows['publish'];
			$vo->entered_by= $rows['entered_by'];
			$vo->entered_date= $rows['entered_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


		function fetchByMenuIds($menu_id, $sub_menu_id)
			{
			if ($sub_menu_id != 0)
			{
				$this->sql = "SELECT * FROM tbl_content WHERE sub_menu_id = '$sub_menu_id' AND menu_id = '$menu_id'";
			}
			else
			{
				$this->sql = "SELECT * FROM tbl_content WHERE menu_id = '$menu_id'";
			}
			$this->exec();
			
			$vo = new contentVO();
			if($this->result)
				{
				$rows = $this->fetch();
				
				$vo->content_id= $rows['content_id'];
				$vo->heading= $rows['heading'];
				$vo->heading_np= $rows['heading_np'];
				$vo->page_title= $rows['page_title'];
				$vo->page_metatag= $rows['page_metatag'];
				$vo->page_keywords= $rows['page_keywords'];
				$vo->page_description= $rows['page_description'];
				$vo->detail_desc_en= $rows['detail_desc_en'];
				$vo->brief_desc_en= $rows['brief_desc_en'];
				$vo->detail_desc_np= $rows['detail_desc_np'];
				$vo->brief_desc_np= $rows['brief_desc_np'];
				$vo->menu_id= $rows['menu_id'];
				$vo->sub_menu_id= $rows['sub_menu_id'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				}
			
			return $vo;
			}
			
		function fetchByMenuType($menu_type)
			{
			
				$this->sql = "SELECT * FROM `tbl_content` WHERE menu_id=(select menu_id from tbl_menu where menu_type='$menu_type')";
			
			
			$this->exec();
			
			$vo = new contentVO();
			if($this->result)
				{
				$rows = $this->fetch();
				
				$vo->content_id= $rows['content_id'];
				$vo->heading= $rows['heading'];
				$vo->heading_np= $rows['heading_np'];
				$vo->page_title= $rows['page_title'];
				$vo->page_metatag= $rows['page_metatag'];
				$vo->page_keywords= $rows['page_keywords'];
				$vo->page_description= $rows['page_description'];
				$vo->detail_desc_en= $rows['detail_desc_en'];
				$vo->brief_desc_en= $rows['brief_desc_en'];
				$vo->detail_desc_np= $rows['detail_desc_np'];
				$vo->brief_desc_np= $rows['brief_desc_np'];
				$vo->menu_id= $rows['menu_id'];
				$vo->sub_menu_id= $rows['sub_menu_id'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				}
			
			return $vo;
			}
			
		function fetchByContentIds($sid,$pid)
			{
			$this->sql = "SELECT * FROM tbl_contents,tbl_submenu WHERE 
			tbl_contents.menu_id=tbl_submenu.mainmenu_id 
			AND tbl_contents.menu_id ='$sid' AND tbl_contents.parent_menu_id='$pid'";
			$this->exec();
			
			$vo = new ContentVO();
			if($this->result)
				{
				$rows = $this->fetch();
				
				$vo->id = $rows['id'];
				$vo->menu_id = $rows['menu_id'];
				$vo->parent_menu_id = $rows['parent_menu_id']; 
				$vo->content = $rows['content'];
				$vo->heading= $rows['heading'];
				$vo->heading_np= $rows['heading_np'];
				$vo->page_title= $rows['page_title'];
				$vo->page_metatag= $rows['page_metatag'];
				$vo->page_keywords= $rows['page_keywords'];
				$vo->page_description= $rows['page_description'];
				$vo->updated_date = $rows['updated_date'];
				$vo->created_date = $rows['created_date'];
				$vo->title = $rows['title'];
				
				$vo->formatFetchVariables();
				}
			
			return $vo;
			}


	function remove($content_id)
	{
		$vo=$this->fetchDetails($content_id);
		if($vo->content_id!=0)
		{
			$this->sql = "DELETE FROM tbl_content WHERE content_id= '$content_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

