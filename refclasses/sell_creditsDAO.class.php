<?php 
class sell_creditsDAO extends BaseDAO
{
	public $credit_id;
	public $credit_title;
	public $total_credits;
	public $total_price;
	public $remarks;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;
	public $status;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_sell_credits
	SET
		credit_title= '$vo->credit_title',
		total_credits= '$vo->total_credits',
		total_price= '$vo->total_price',
		remarks= '$vo->remarks',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count',
		status= '$vo->status'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_sell_credits WHERE status='1' ORDER BY credit_id ";
		else
			$this->sql = "SELECT * FROM tbl_sell_credits ORDER BY credit_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new sell_creditsVO();
				$vo->credit_id= $rows['credit_id'];
				$vo->credit_title= $rows['credit_title'];
				$vo->total_credits= $rows['total_credits'];
				$vo->total_price= $rows['total_price'];
				$vo->remarks= $rows['remarks'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_sell_credits ORDER BY credit_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_sell_credits ORDER BY credit_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new sell_creditsVO();
				$vo->credit_id= $rows['credit_id'];
				$vo->credit_title= $rows['credit_title'];
				$vo->total_credits= $rows['total_credits'];
				$vo->total_price= $rows['total_price'];
				$vo->remarks= $rows['remarks'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_sell_credits
	SET
		credit_title= '$vo->credit_title',
		total_credits= '$vo->total_credits',
		total_price= '$vo->total_price',
		remarks= '$vo->remarks',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count',
		status= '$vo->status'
	WHERE credit_id= '$vo->credit_id'";
		return $this->exec();
	}


	function fetchDetails($credit_id)
	{
		$this->sql = "SELECT * FROM tbl_sell_credits WHERE credit_id= '$credit_id'";
		$this->exec();
		$vo = new sell_creditsVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->credit_id= $rows['credit_id'];
			$vo->credit_title= $rows['credit_title'];
			$vo->total_credits= $rows['total_credits'];
			$vo->total_price= $rows['total_price'];
			$vo->remarks= $rows['remarks'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	
	
	function ActivateNdactivate($id, $publish)
			{
			$vo= $this->fetchDetails($id);
			if($vo->credit_id != 0)
				{
				$this->sql = "UPDATE tbl_sell_credits SET status = '$publish' WHERE credit_id = '$vo->credit_id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}
			
			


	function remove($credit_id)
	{
		$vo=$this->fetchDetails($credit_id);
		if($vo->credit_id!=0)
		{
			$this->sql = "DELETE FROM tbl_sell_credits WHERE credit_id= '$credit_id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

