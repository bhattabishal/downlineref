<?php 
class siteusersDAO extends BaseDAO
{
	public $user_id;
	public $tree_id;
	public $user_parent_id;
	public $first_name;
	public $last_name;
	public $username;
	public $password;
	public $computer_ip;
	public $country;
	public $email;
	public $joined_date;
	public $status;
	public $gender;
	public $mem_type;
	public $referral_url;
	public $random_distributed;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_siteusers
	SET
		first_name= '$vo->first_name',
		last_name= '$vo->last_name',
		tree_id= '$vo->tree_id',
		user_parent_id= '$vo->user_parent_id',
		username= '$vo->username',
		password= '$vo->password',
		computer_ip= '$vo->computer_ip',
		country= '$vo->country',
		email= '$vo->email',
		referral_url='$vo->referral_url',
		joined_date= '$vo->joined_date',
		mem_type= '$vo->mem_type',
		random_distributed= '$vo->random_distributed',
		gender= '$vo->gender',
		status= '$vo->status'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_siteusers WHERE status='1' ORDER BY user_id";
		else
			$this->sql = "SELECT * FROM tbl_siteusers ORDER BY user_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->user_id= $rows['user_id'];
				$vo->first_name= $rows['first_name'];
				$vo->tree_id= $rows['tree_id'];
				$vo->user_parent_id= $rows['user_parent_id'];
				$vo->last_name= $rows['last_name'];
				$vo->username= $rows['username'];
				$vo->password= $rows['password'];
				$vo->computer_ip= $rows['computer_ip'];
				$vo->country= $rows['country'];
				$vo->email= $rows['email'];
				$vo->referral_url= $rows['referral_url'];
				$vo->joined_date= $rows['joined_date'];
				$vo->status= $rows['status'];
				$vo->gender= $rows['gender'];
				$vo->mem_type= $rows['mem_type'];
				$vo->random_distributed= $rows['random_distributed'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_siteusers ORDER BY user_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_siteusers ORDER BY user_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new tbl_siteusersVO();
				$vo->user_id= $rows['user_id'];
				$vo->first_name= $rows['first_name'];
				$vo->tree_id= $rows['tree_id'];
				$vo->user_parent_id= $rows['user_parent_id'];
				$vo->last_name= $rows['last_name'];
				$vo->username= $rows['username'];
				$vo->password= $rows['password'];
				$vo->computer_ip= $rows['computer_ip'];
				$vo->country= $rows['country'];
				$vo->email= $rows['email'];
				$vo->referral_url= $rows['referral_url'];
				$vo->joined_date= $rows['joined_date'];
				$vo->status= $rows['status'];
					$vo->gender= $rows['gender'];
				$vo->mem_type= $rows['mem_type'];
				$vo->random_distributed= $rows['random_distributed'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_siteusers
	SET
		first_name= '$vo->first_name',
		last_name= '$vo->last_name',
		tree_id= '$vo->tree_id',
		user_parent_id= '$vo->user_parent_id',
		username= '$vo->username',
		password= '$vo->password',
		computer_ip= '$vo->computer_ip',
		country= '$vo->country',
		email= '$vo->email',
		referral_url='$vo->referral_url',
		joined_date= '$vo->joined_date',
		status= '$vo->status',
		gender= '$vo->gender',
		mem_type= '$vo->mem_type',
		random_distributed= '$vo->random_distributed'
						
	WHERE user_id= '$vo->user_id'";
		return $this->exec();
	}
	
	function updateMemStatus($vo)
	{
	$this->sql="UPDATE tbl_siteusers
	SET
	mem_type= '$vo->mem_type'								
	WHERE user_id= '$vo->user_id'";
		return $this->exec();
	}
	
	function updateStatus($vo)
	{
	$this->sql="UPDATE tbl_siteusers
	SET
		mem_type= '$vo->mem_type'						
	WHERE user_id= '$vo->user_id'";
		return $this->exec();
	}


	function fetchDetails($user_id)
	{
		$this->sql = "SELECT * FROM tbl_siteusers WHERE user_id= '$user_id'";
		$this->exec();
		$vo = new siteusersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->user_id= $rows['user_id'];
			$vo->tree_id= $rows['tree_id'];
			$vo->user_parent_id= $rows['user_parent_id'];
			$vo->first_name= $rows['first_name'];
			$vo->last_name= $rows['last_name'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			$vo->computer_ip= $rows['computer_ip'];
			$vo->country= $rows['country'];
			$vo->email= $rows['email'];
			$vo->referral_url=$rows['referral_url'];
			$vo->joined_date= $rows['joined_date'];
			$vo->status= $rows['status'];
			$vo->gender= $rows['gender'];
				$vo->mem_type= $rows['mem_type'];
				$vo->random_distributed= $rows['random_distributed'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchDetailsByName($user)
	{
		$this->sql = "SELECT * FROM tbl_siteusers WHERE username= '$user'";
		$this->exec();
		$vo = new siteusersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->user_id= $rows['user_id'];
			$vo->tree_id= $rows['tree_id'];
			$vo->user_parent_id= $rows['user_parent_id'];
			$vo->first_name= $rows['first_name'];
			$vo->last_name= $rows['last_name'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			$vo->computer_ip= $rows['computer_ip'];
			$vo->country= $rows['country'];
			$vo->email= $rows['email'];
			$vo->referral_url=$rows['referral_url'];
			$vo->joined_date= $rows['joined_date'];
			$vo->status= $rows['status'];
			$vo->gender= $rows['gender'];
				$vo->mem_type= $rows['mem_type'];
				$vo->random_distributed= $rows['random_distributed'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchChild($parentId)
	{
		
		$this->sql = "SELECT * FROM tbl_siteusers WHERE user_parent_id='$parentId'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->user_id= $rows['user_id'];
				$vo->first_name= $rows['first_name'];
				$vo->tree_id= $rows['tree_id'];
				$vo->user_parent_id= $rows['user_parent_id'];
				$vo->last_name= $rows['last_name'];
				$vo->username= $rows['username'];
				$vo->password= $rows['password'];
				$vo->computer_ip= $rows['computer_ip'];
				$vo->country= $rows['country'];
				$vo->email= $rows['email'];
				$vo->referral_url= $rows['referral_url'];
				$vo->joined_date= $rows['joined_date'];
				$vo->status= $rows['status'];
				$vo->gender= $rows['gender'];
				$vo->mem_type= $rows['mem_type'];
				$vo->random_distributed= $rows['random_distributed'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function display_child($parent, $level)
	{
		$this->sql = "SELECT * from tbl_siteusers WHERE user_parent_id='$parent'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				echo str_repeat('-',$level)." ".$rows['first_name']."<br />";
				$this->display_child($rows['user_id'], $level+1);
			}
			
		}
		
	}
	
	function display_child_five_level($parent)
	{
		$this->sql = "SELECT  t2.user_id as lev1, t3.user_id as lev2, t4.user_id as lev3,t5.user_id as lev4,t6.user_id as lev5
FROM tbl_siteusers AS t1
LEFT JOIN tbl_siteusers AS t2 ON t2.user_parent_id = t1.user_id
LEFT JOIN tbl_siteusers AS t3 ON t3.user_parent_id = t2.user_id
LEFT JOIN tbl_siteusers AS t4 ON t4.user_parent_id = t3.user_id
LEFT JOIN tbl_siteusers AS t5 ON t5.user_parent_id = t4.user_id
LEFT JOIN tbl_siteusers AS t6 ON t6.user_parent_id = t5.user_id
WHERE t1.user_id = '$parent'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->lev1= $rows['lev1'];
				$vo->lev2= $rows['lev2'];
				$vo->lev3= $rows['lev3'];
				$vo->lev4= $rows['lev4'];
				$vo->lev5= $rows['lev5'];
				
				//$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
		
	}
	
	function fetchParent($id)
	{
		$this->sql = "SELECT * FROM tbl_siteusers WHERE user_id='$id' AND  user_parent_id != '0'";
		
		$this->exec();
		$vo = new siteusersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->user_id= $rows['user_id'];
			$vo->tree_id= $rows['tree_id'];
			$vo->user_parent_id= $rows['user_parent_id'];
			$vo->first_name= $rows['first_name'];
			$vo->last_name= $rows['last_name'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			$vo->computer_ip= $rows['computer_ip'];
			$vo->country= $rows['country'];
			$vo->email= $rows['email'];
			$vo->referral_url=$rows['referral_url'];
			$vo->joined_date= $rows['joined_date'];
			$vo->status= $rows['status'];
			$vo->gender= $rows['gender'];
				$vo->mem_type= $rows['mem_type'];
				$vo->random_distributed= $rows['random_distributed'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	


function Duplicationcheck($username,$email)
		{
		
			 $this->sql="SELECT * FROM tbl_siteusers WHERE username='$username' or email='$email'";
				
				$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->id= $rows['id'];
			
			$vo->email= $rows['email'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			
			$vo->status=$rows['status'];
		$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function DuplicationIpcheck($ip)
		{
		
			$this->sql="SELECT * FROM tbl_siteusers WHERE computer_ip='$ip'";
				
				$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->id= $rows['id'];
			
			$vo->email= $rows['email'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			
			$vo->status=$rows['status'];
		$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function DuplicationUserNamecheck($username)
		{
		
			 $this->sql="SELECT * FROM tbl_siteusers WHERE username='$username'";
				
				$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->id= $rows['id'];
			
			$vo->email= $rows['email'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			
			$vo->status=$rows['status'];
		$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function DuplicationEmailcheck($email)
		{
		
			 $this->sql="SELECT * FROM tbl_siteusers WHERE email='$email'";
				
				$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new siteusersVO();
				$vo->id= $rows['id'];
			
			$vo->email= $rows['email'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			
			$vo->status=$rows['status'];
		$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	
	function ActivateNdactivate($id, $publish)
			{
			$vo= $this->fetchDetails($id);
			if($vo->user_id != 0)
				{
				$this->sql = "UPDATE tbl_siteusers SET status = '$publish' WHERE user_id = '$vo->user_id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}
	
	function checkPremium($id)
			{
			$vo= $this->fetchDetails($id);
			if($vo->user_id != 0)
				{
				$this->sql = "SELECT mem_type FROM tbl_siteusers WHERE user_id = '$vo->user_id' AND mem_type='1'";
				$this->exec();
					if($this->result)
					{
						if($this->row_count($this->result)==1)
						{
							return true;
						}
					}
					else
					{
						return false;
					}
				}
			else
				return false;
			}
	
	function authenticate($vo)
	{
			$this->sql = "SELECT * FROM tbl_siteusers WHERE username = '$vo->username' 
			 AND password = '$vo->password' AND status = '1'";
			//echo $this->sql;
			//exit();
			$this->exec();//stores result in $this->result
			
			if($this->result)
			{
				if($this->row_count($this->result)==1)
				{
					$rows = $this->fetch();
					$id = intval($rows['user_id']);
					$vo = new siteusersVO();
					if($id != 0)
					{
						$vo->id = $rows['user_id'];
						
						$vo->username= $rows['username'];
						$vo->password= $rows['password'];
						
						$vo->email= $rows['email'];
						$vo->status = "valid";
						
						$vo->formatFetchVariables();
						return $vo->id;
					}
					else
					{
						$vo->status = "invalid";
						return $id;
					}
				}
				else
				{
					$vo->status = "invalid";
					return $id;
				}
			}
			else
			{
				$vo->status = "invalid";
				return $id;
			}
		
	}


	function remove($user_id)
	{
		$vo=$this->fetchDetails($user_id);
		if($vo->user_id!=0)
		{
			$this->sql = "DELETE FROM tbl_siteusers WHERE user_id= '$user_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

