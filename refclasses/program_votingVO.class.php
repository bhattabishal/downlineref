<?php 
class program_votingVO 
{
	public $id;
	public $vote_by;
	public $vote_value;
	public $vote_for;
	public $created_date;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id;
		$this->vote_by= $this->vote_by;
		$this->vote_value= $this->vote_value;
		$this->vote_for= $this->vote_for;
		$this->created_date= $this->created_date;
	}


	function formatFetchVariables()
	{
		$this->id;
		$this->vote_by= $this->vote_by;
		$this->vote_value= $this->vote_value;
		$this->vote_for= $this->vote_for;
		$this->created_date= $this->created_date;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->vote_by= $array['vote_by'];
		$this->vote_value= $array['vote_value'];
		$this->vote_for= $array['vote_for'];
		$this->created_date= $array['created_date'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->vote_by);
		unset($this->vote_value);
		unset($this->vote_for);
		unset($this->created_date);
		unset($this);
	}


}
?> 

