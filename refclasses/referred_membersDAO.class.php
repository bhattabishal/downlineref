<?php 
class referred_membersDAO extends BaseDAO
{
	public $refer_id;
	public $refer_by;
	public $refer_to;
	public $refer_date;
	public $refer_level;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_referred_members
	SET
		refer_by= '$vo->refer_by',
		refer_to= '$vo->refer_to',
		refer_level= '$vo->refer_level',
		refer_date= '$vo->refer_date'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_referred_members ORDER BY refer_id";
		else
			$this->sql = "SELECT * FROM tbl_referred_members ORDER BY refer_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new referred_membersVO();
				$vo->refer_id= $rows['refer_id'];
				$vo->refer_by= $rows['refer_by'];
				$vo->refer_to= $rows['refer_to'];
				$vo->refer_date= $rows['refer_date'];
				$vo->refer_level= $rows['refer_level'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_referred_members ORDER BY refer_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_referred_members ORDER BY refer_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new referred_membersVO();
				$vo->refer_id= $rows['refer_id'];
				$vo->refer_by= $rows['refer_by'];
				$vo->refer_to= $rows['refer_to'];
				$vo->refer_date= $rows['refer_date'];
				$vo->refer_level= $rows['refer_level'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_referred_members
	SET
		refer_by= '$vo->refer_by',
		refer_to= '$vo->refer_to',
		refer_level= '$vo->refer_level',
		refer_date= '$vo->refer_date'
	WHERE refer_id= '$vo->refer_id'";
		return $this->exec();
	}


	function fetchDetails($refer_id)
	{
		$this->sql = "SELECT * FROM tbl_referred_members WHERE refer_id= '$refer_id'";
		$this->exec();
		$vo = new referred_membersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->refer_id= $rows['refer_id'];
			$vo->refer_by= $rows['refer_by'];
			$vo->refer_to= $rows['refer_to'];
			$vo->refer_date= $rows['refer_date'];
			$vo->refer_level= $rows['refer_level'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function GetRefererById($id)
	{
	$this->sql = "SELECT * FROM tbl_referred_members WHERE refer_by= '$refer_by'";
		$this->exec();
		$vo = new referred_membersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->refer_id= $rows['refer_id'];
			$vo->refer_by= $rows['refer_by'];
			$vo->refer_to= $rows['refer_to'];
			$vo->refer_level= $rows['refer_level'];
			$vo->refer_date= $rows['refer_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function GetLastChildTreeIdByCategoryId($id)
	{
	
		$this->sql = "SELECT refer_level FROM tbl_referred_members WHERE refer_by='$id' ORDER BY refer_level DESC";
		$this->exec();
		$vo = new referred_membersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$lastChild=$rows[0];
			$lastTree=explode("-",$lastChild['tree_id']);
			$lastTree=array_reverse($lastTree);
			$lastTree[0]=$lastTree[0]+1;
			
			
		}
		return $lastTree[0];
	
	
	}
	
	function fetchDetailsByReferralId($id)
	{
		$this->sql = "SELECT * FROM tbl_referred_members WHERE refer_by= '$id'";
		$this->exec();
		
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new referred_membersVO();
				$vo->refer_id= $rows['refer_id'];
				$vo->refer_by= $rows['refer_by'];
				$vo->refer_to= $rows['refer_to'];
				$vo->refer_date= $rows['refer_date'];
				$vo->refer_level= $rows['refer_level'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function remove($refer_id)
	{
		$vo=$this->fetchDetails($refer_id);
		if($vo->refer_id!=0)
		{
			$this->sql = "DELETE FROM tbl_referred_members WHERE refer_id= '$refer_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

