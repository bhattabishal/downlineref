<?php 
class menuDAO extends BaseDAO
{
	public $menu_id;
	public $name_en;
	public $name_np;
	public $menu_type;
	public $menu_index;
	public $has_content;
	public $publish;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_menu
	SET
		name_en= '$vo->name_en',
		name_np= '$vo->name_np',
		menu_type= '$vo->menu_type',
		menu_index= '$vo->menu_index',
		has_content= '$vo->has_content',
		publish= '$vo->publish',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_menu WHERE publish='yes' ORDER BY menu_index";
		else
			$this->sql = "SELECT * FROM tbl_menu ORDER BY menu_id";

		
		//exit();
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new menuVO();
				$vo->menu_id= $rows['menu_id'];
				$vo->name_en= $rows['name_en'];
				$vo->name_np= $rows['name_np'];
				$vo->menu_type= $rows['menu_type'];
				$vo->menu_index= $rows['menu_index'];
				$vo->has_content= $rows['has_content'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_menu ORDER BY menu_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_menu ORDER BY menu_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new menuVO();
				$vo->menu_id= $rows['menu_id'];
				$vo->name_en= $rows['name_en'];
				$vo->name_np= $rows['name_np'];
				$vo->menu_type= $rows['menu_type'];
				$vo->menu_index= $rows['menu_index'];
				$vo->has_content= $rows['has_content'];
				$vo->publish= $rows['publish'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_menu
	SET
		name_en= '$vo->name_en',
		name_np= '$vo->name_np',
		menu_type= '$vo->menu_type',
		menu_index= '$vo->menu_index',
		has_content= '$vo->has_content',
		publish= '$vo->publish',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'
	WHERE menu_id= '$vo->menu_id'";
	
	return $this->exec();
	}


	function fetchDetails($menu_id)
	{
		$this->sql = "SELECT * FROM tbl_menu WHERE menu_id= '$menu_id'";
		$this->exec();
		$vo = new menuVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->menu_id= $rows['menu_id'];
			$vo->name_en= $rows['name_en'];
			$vo->name_np= $rows['name_np'];
			$vo->menu_type= $rows['menu_type'];
			$vo->menu_index= $rows['menu_index'];
			$vo->has_content= $rows['has_content'];
			$vo->publish= $rows['publish'];
			$vo->entered_by= $rows['entered_by'];
			$vo->entered_date= $rows['entered_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($menu_id)
	{
		$vo=$this->fetchDetails($menu_id);
		if($vo->menu_id!=0)
		{
			$this->sql = "DELETE FROM tbl_menu WHERE menu_id= '$menu_id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function publishNunpublish($menu_id, $publishflag='yes')
		{
		$vo = $this->fetchDetails($menu_id);
		
		if($vo->menu_id != 0)
			{
			$this->sql = "UPDATE tbl_menu SET publish='$publishflag' WHERE menu_id = '$vo->menu_id'";
			$this->exec();
			if($this->result)
				return true;
			else
				return false;
			}
		return false;
		}	


	function hasSubmenus($id)
		{
		$this->sql = "SELECT * FROM tbl_sub_menu WHERE menu_id = '$id'";
		$this->exec();
		if($this->result)
			{
			if($this->row_count($this->result)>0)
				return true;
			else
				return false;
			}
		else
			return false;
		}	


	function __destruct()
	{
		unset($this);
	}


}
?> 

