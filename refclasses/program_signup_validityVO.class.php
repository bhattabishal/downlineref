<?php 
class program_signup_validityVO 
{
	public $id;
	public $signed_user_id;
	public $program_signup_id;
	public $valid_from;
	public $valid_to;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id;
		$this->signed_user_id= $this->signed_user_id;
		$this->program_signup_id= $this->program_signup_id;
		$this->valid_from= $this->valid_from;
		$this->valid_to= $this->valid_to;
	}


	function formatFetchVariables()
	{
		$this->id;
		$this->signed_user_id= $this->signed_user_id;
		$this->program_signup_id= $this->program_signup_id;
		$this->valid_from= $this->valid_from;
		$this->valid_to= $this->valid_to;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->signed_user_id= $array['signed_user_id'];
		$this->program_signup_id= $array['program_signup_id'];
		$this->valid_from= $array['valid_from'];
		$this->valid_to= $array['valid_to'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->signed_user_id);
		unset($this->program_signup_id);
		unset($this->valid_from);
		unset($this->valid_to);
		unset($this);
	}


}
?> 

