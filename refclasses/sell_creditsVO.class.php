<?php 
class sell_creditsVO 
{
	public $credit_id;
	public $credit_title;
	public $total_credits;
	public $total_price;
	public $remarks;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;
	public $status;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->credit_id;
		$this->credit_title= $this->credit_title;
		$this->total_credits= $this->total_credits;
		$this->total_price= $this->total_price;
		$this->remarks= addslashes(trim($this->remarks));
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
		$this->status= $this->status;
	}


	function formatFetchVariables()
	{
		$this->credit_id;
		$this->credit_title= $this->credit_title;
		$this->total_credits= $this->total_credits;
		$this->total_price= $this->total_price;
		$this->remarks= stripslashes(trim($this->remarks));
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
		$this->status= $this->status;
	}


	function getPostVars($array)
	{
		$this->credit_id= $array['credit_id'];
		$this->credit_title= $array['credit_title'];
		$this->total_credits= $array['total_credits'];
		$this->total_price= $array['total_price'];
		$this->remarks= $array['remarks'];
		$this->created_by= $array['created_by'];
		$this->created_date= $array['created_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
		$this->status= $array['status'];
	}


	function __destruct()
	{
		unset($this->credit_id);
		unset($this->credit_title);
		unset($this->total_credits);
		unset($this->total_price);
		unset($this->remarks);
		unset($this->created_by);
		unset($this->created_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this->status);
		unset($this);
	}


}
?> 

