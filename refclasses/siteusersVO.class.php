<?php 
class siteusersVO 
{
	public $user_id;
	public $first_name;
	public $last_name;
	public $gender;
	public $mem_type;
	public $username;
	public $password;
	public $computer_ip;
	public $country;
	public $email;
	public $joined_date;
	public $status;
	public $referral_url;
	public $tree_id;
	public $user_parent_id;
	
	public $random_distributed;



	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->user_id;
		$this->first_name= $this->first_name;
		$this->last_name= $this->last_name;
		$this->tree_id= $this->tree_id;
		$this->user_parent_id= $this->user_parent_id;
		$this->gender= $this->gender;
		$this->mem_type= $this->mem_type;
		$this->username= $this->username;
		$this->password= $this->password;
		$this->computer_ip= $this->computer_ip;
		$this->country= $this->country;
		$this->email= $this->email;
		$this->referral_url= $this->referral_url;
		$this->joined_date= $this->joined_date;
		$this->status= $this->status;
		$this->random_distributed=$this->random_distributed;
	}


	function formatFetchVariables()
	{
		$this->user_id;
		$this->tree_id= $this->tree_id;
		$this->user_parent_id= $this->user_parent_id;
		$this->first_name= $this->first_name;
		$this->last_name= $this->last_name;
		$this->gender= $this->gender;
		$this->mem_type= $this->mem_type;
		$this->username= $this->username;
		$this->password= $this->password;
		$this->computer_ip= $this->computer_ip;
		$this->country= $this->country;
		$this->email= $this->email;
		$this->referral_url= $this->referral_url;
		$this->joined_date= $this->joined_date;
		$this->status= $this->status;
		$this->random_distributed=$this->random_distributed;
	}


	function getPostVars($array)
	{
		$this->user_id= $array['user_id'];
		$this->tree_id= $array['tree_id'];
		$this->user_parent_id= $array['user_parent_id'];
		$this->first_name= $array['first_name'];
		$this->last_name= $array['last_name'];
		$this->username= $array['username'];
		$this->gender=$array['gender'];
		$this->mem_type=$array['mem_type'];
		$this->password= $array['password'];
		$this->computer_ip= $array['computer_ip'];
		$this->country= $array['country'];
		$this->email= $array['email'];
		$this->referral_url=$array['referral_url'];
		$this->joined_date= $array['joined_date'];
		$this->status= $array['status'];
		$this->random_distributed=$array['random_distributed'];
	}


	function __destruct()
	{
		unset($this->user_id);
		unset($this->first_name);
		unset($this->tree_id);
		unset($this->user_parent_id);
		unset($this->last_name);
		unset($this->username);
		unset($this->mem_type);
		unset($this->gender);
		unset($this->password);
		unset($this->computer_ip);
		unset($this->country);
		unset($this->email);
		unset($this->referral_url);
		unset($this->joined_date);
		unset($this->status);
		unset($this->random_distributed);
		unset($this);
	}


}
?> 

