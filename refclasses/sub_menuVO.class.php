<?php 
class sub_menuVO 
{
	public $sub_menu_id;
	public $name_en;
	public $name_np;
	public $sub_menu_index;
	public $menu_id;
	public $publish;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	
	}


	function formatInsertVariables()
	{
		$this->sub_menu_id;
		$this->name_en= $this->name_en;
		$this->name_np= $this->name_np;
		$this->sub_menu_index= $this->sub_menu_index;
		$this->menu_id= $this->menu_id;
		$this->publish= $this->publish;
		$this->entered_by= $this->entered_by;
		$this->entered_date= $this->entered_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function formatFetchVariables()
	{
		$this->sub_menu_id;
		$this->name_en= $this->name_en;
		$this->name_np= $this->name_np;
		$this->sub_menu_index= $this->sub_menu_index;
		$this->menu_id= $this->menu_id;
		$this->publish= $this->publish;
		$this->entered_by= $this->entered_by;
		$this->entered_date= $this->entered_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function getPostVars($array)
	{
		$this->sub_menu_id= $array['sub_menu_id'];
		$this->name_en= $array['name_en'];
		$this->name_np= $array['name_np'];
		$this->sub_menu_index= $array['sub_menu_index'];
		$this->menu_id= $array['menu_id'];
		$this->publish= $array['publish'];
		$this->entered_by= $array['entered_by'];
		$this->entered_date= $array['entered_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
	}


	function __destruct()
	{
		unset($this->sub_menu_id);
		unset($this->name_en);
		unset($this->name_np);
		unset($this->sub_menu_index);
		unset($this->menu_id);
		unset($this->publish);
		unset($this->entered_by);
		unset($this->entered_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this);
	}


}
?> 

