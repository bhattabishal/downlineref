<?php 
class advertismentDAO extends BaseDAO
{
	public $id;
	public $title;
	public $short_desc;
	public $credits;
	public $website_url;
	public $package_size;
	public $sub_total;
	public $span_fees;
	public $total;
	public $status;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}
	
	function ActivateNdactivate($id, $publish)
			{
			$vo= $this->fetchDetails($id);
			if($vo->id != 0)
				{
				$this->sql = "UPDATE tbl_advertisment SET status = '$publish' WHERE id = '$vo->id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_advertisment
	SET
		title= '$vo->title',
		short_desc= '$vo->short_desc',
		credits= '$vo->credits',
		website_url= '$vo->website_url',
		package_size= '$vo->package_size',
		sub_total= '$vo->sub_total',
		span_fees= '$vo->span_fees',
		total= '$vo->total',
		status= '$vo->status',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_advertisment WHERE status='1' ORDER BY id";
		else
			$this->sql = "SELECT * FROM tbl_advertisment ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new advertismentVO();
				$vo->id= $rows['id'];
				$vo->title= $rows['title'];
				$vo->short_desc= $rows['short_desc'];
				$vo->credits= $rows['credits'];
				$vo->website_url= $rows['website_url'];
				$vo->package_size= $rows['package_size'];
				$vo->sub_total= $rows['sub_total'];
				$vo->span_fees= $rows['span_fees'];
				$vo->total= $rows['total'];
				$vo->status= $rows['status'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	//function to fetch all advertisments submitted by the given user
	function fetchAllByUserId($id)
	{
		
		$this->sql = "SELECT * FROM tbl_advertisment WHERE created_by='$id' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new advertismentVO();
				$vo->id= $rows['id'];
				$vo->title= $rows['title'];
				$vo->short_desc= $rows['short_desc'];
				$vo->credits= $rows['credits'];
				$vo->website_url= $rows['website_url'];
				$vo->package_size= $rows['package_size'];
				$vo->sub_total= $rows['sub_total'];
				$vo->span_fees= $rows['span_fees'];
				$vo->total= $rows['total'];
				$vo->status= $rows['status'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_advertisment ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_advertisment ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new advertismentVO();
				$vo->id= $rows['id'];
				$vo->title= $rows['title'];
				$vo->short_desc= $rows['short_desc'];
				$vo->credits= $rows['credits'];
				$vo->website_url= $rows['website_url'];
				$vo->package_size= $rows['package_size'];
				$vo->sub_total= $rows['sub_total'];
				$vo->span_fees= $rows['span_fees'];
				$vo->total= $rows['total'];
				$vo->status= $rows['status'];
				$vo->created_by= $rows['created_by'];
				$vo->created_date= $rows['created_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_advertisment
	SET
		title= '$vo->title',
		short_desc= '$vo->short_desc',
		credits= '$vo->credits',
		website_url= '$vo->website_url',
		package_size= '$vo->package_size',
		sub_total= '$vo->sub_total',
		span_fees= '$vo->span_fees',
		total= '$vo->total',
		status= '$vo->status',
		created_by= '$vo->created_by',
		created_date= '$vo->created_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM tbl_advertisment WHERE id= '$id'";
		$this->exec();
		$vo = new advertismentVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->title= $rows['title'];
			$vo->short_desc= $rows['short_desc'];
			$vo->credits= $rows['credits'];
			$vo->website_url= $rows['website_url'];
			$vo->package_size= $rows['package_size'];
			$vo->sub_total= $rows['sub_total'];
			$vo->span_fees= $rows['span_fees'];
			$vo->total= $rows['total'];
			$vo->status= $rows['status'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}

function fetchDetailsByUrl($url)
	{
		$this->sql = "SELECT * FROM tbl_advertisment WHERE website_url= '$url'";
		$this->exec();
		$vo = new advertismentVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->title= $rows['title'];
			$vo->short_desc= $rows['short_desc'];
			$vo->credits= $rows['credits'];
			$vo->website_url= $rows['website_url'];
			$vo->package_size= $rows['package_size'];
			$vo->sub_total= $rows['sub_total'];
			$vo->span_fees= $rows['span_fees'];
			$vo->total= $rows['total'];
			$vo->status= $rows['status'];
			$vo->created_by= $rows['created_by'];
			$vo->created_date= $rows['created_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}

	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM tbl_advertisment WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

