<?php 
class msg_sentboxDAO extends BaseDAO
{
	public $id;
	public $msg_id;
	public $sent_by;
	public $sent_to;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO msg_sentbox
	SET
		msg_id= '$vo->msg_id',
		sent_by= '$vo->sent_by',
		sent_to= '$vo->sent_to'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM msg_sentbox ORDER BY id";
		else
			$this->sql = "SELECT * FROM msg_sentbox ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new msg_sentboxVO();
				$vo->id= $rows['id'];
				$vo->msg_id= $rows['msg_id'];
				$vo->sent_by= $rows['sent_by'];
				$vo->sent_to= $rows['sent_to'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchSentAllById($id)
	{
		
			$this->sql = "SELECT * FROM msg_sentbox WHERE sent_by='$id' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new msg_sentboxVO();
				$vo->id= $rows['id'];
				$vo->msg_id= $rows['msg_id'];
				$vo->sent_by= $rows['sent_by'];
				$vo->sent_to= $rows['sent_to'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM msg_sentbox ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM msg_sentbox ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new msg_sentboxVO();
				$vo->id= $rows['id'];
				$vo->msg_id= $rows['msg_id'];
				$vo->sent_by= $rows['sent_by'];
				$vo->sent_to= $rows['sent_to'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE msg_sentbox
	SET
		msg_id= '$vo->msg_id',
		sent_by= '$vo->sent_by',
		sent_to= '$vo->sent_to'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM msg_sentbox WHERE id= '$id'";
		$this->exec();
		$vo = new msg_sentboxVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->msg_id= $rows['msg_id'];
			$vo->sent_by= $rows['sent_by'];
			$vo->sent_to= $rows['sent_to'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM msg_sentbox WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

