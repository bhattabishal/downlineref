<?php 
class contentVO 
{
	public $content_id;
	public $heading;
	public $heading_np;
	public $page_title;
	public $page_metatag;
	public $page_keywords;
	public $page_description;
	public $detail_desc_en;
	public $brief_desc_en;
	public $detail_desc_np;
	public $brief_desc_np;
	public $menu_id;
	public $sub_menu_id;
	public $publish;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->content_id;
		$this->heading= $this->heading;
		$this->heading_np= $this->heading_np;
		$this->page_title= $this->page_title;
		$this->page_metatag= $this->page_metatag;
		$this->page_keywords= $this->page_keywords;
		$this->page_description= $this->page_description;
		$this->detail_desc_en= addslashes(trim($this->detail_desc_en));
		$this->brief_desc_en= addslashes(trim($this->brief_desc_en));
		$this->detail_desc_np= addslashes(trim($this->detail_desc_np));
		$this->brief_desc_np= addslashes(trim($this->brief_desc_np));
		$this->menu_id= $this->menu_id;
		$this->sub_menu_id= $this->sub_menu_id;
		$this->publish= $this->publish;
		$this->entered_by= $this->entered_by;
		$this->entered_date= $this->entered_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function formatFetchVariables()
	{
		$this->content_id;
		$this->heading= $this->heading;
		$this->heading_np= $this->heading_np;
		$this->page_title= $this->page_title;
		$this->page_metatag= $this->page_metatag;
		$this->page_keywords= $this->page_keywords;
		$this->page_description= $this->page_description;
		$this->detail_desc_en= stripslashes(trim($this->detail_desc_en));
		$this->brief_desc_en= stripslashes(trim($this->brief_desc_en));
		$this->detail_desc_np= stripslashes(trim($this->detail_desc_np));
		$this->brief_desc_np= stripslashes(trim($this->brief_desc_np));
		$this->menu_id= $this->menu_id;
		$this->sub_menu_id= $this->sub_menu_id;
		$this->publish= $this->publish;
		$this->entered_by= $this->entered_by;
		$this->entered_date= $this->entered_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
	}


	function getPostVars($array)
	{
		$this->content_id= $array['content_id'];
		$this->heading= $array['heading'];
		$this->heading_np= $array['heading_np'];
		$this->page_title= $array['page_title'];
		$this->page_metatag= $array['page_metatag'];
		$this->page_keywords= $array['page_keywords'];
		$this->page_description= $array['page_description'];
		$this->detail_desc_en= $array['detail_desc_en'];
		$this->brief_desc_en= $array['brief_desc_en'];
		$this->detail_desc_np= $array['detail_desc_np'];
		$this->brief_desc_np= $array['brief_desc_np'];
		$this->menu_id= $array['menu_id'];
		$this->sub_menu_id= $array['sub_menu_id'];
		$this->publish= $array['publish'];
		$this->entered_by= $array['entered_by'];
		$this->entered_date= $array['entered_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
	}


	function __destruct()
	{
		unset($this->content_id);
		unset($this->heading);
		unset($this->heading_np);
		unset($this->page_title);
		unset($this->page_metatag);
		unset($this->page_keywords);
		unset($this->page_description);
		unset($this->detail_desc_en);
		unset($this->brief_desc_en);
		unset($this->detail_desc_np);
		unset($this->brief_desc_np);
		unset($this->menu_id);
		unset($this->sub_menu_id);
		unset($this->publish);
		unset($this->entered_by);
		unset($this->entered_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this);
	}


}
?> 

