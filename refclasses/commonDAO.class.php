<?php
class commonDAO extends BaseDAO
{
	function __construct()
	{
	}
	
	function fetchAllCountries()
	{
		$this->sql="Select * from country";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$list['id']= $rows['id'];
				$list['name']= $rows['name'];
			}
		}
		return $list;
	}
}
?>