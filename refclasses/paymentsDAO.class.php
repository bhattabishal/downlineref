<?php 
class paymentsDAO extends BaseDAO
{
	public $id;
	public $buyer_id;
	public $txnid;
	public $buyer_firstname;
	public $buyer_lastname;
	public $buyer_email;
	public $payment_amount;
	public $payment_status;
	public $payment_type;
	public $itemid;
	public $createdtime;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO payments
	SET
		buyer_id= '$vo->buyer_id',
		txnid= '$vo->txnid',
		buyer_firstname= '$vo->buyer_firstname',
		buyer_lastname= '$vo->buyer_lastname',
		buyer_email= '$vo->buyer_email',
		payment_amount= '$vo->payment_amount',
		payment_status= '$vo->payment_status',
		payment_type= '$vo->payment_type',
		itemid= '$vo->itemid',
		createdtime= '$vo->createdtime'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM payments ORDER BY id";
		else
			$this->sql = "SELECT * FROM payments ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new paymentsVO();
				$vo->id= $rows['id'];
				$vo->buyer_id= $rows['buyer_id'];
				$vo->txnid= $rows['txnid'];
				$vo->buyer_firstname= $rows['buyer_firstname'];
				$vo->buyer_lastname= $rows['buyer_lastname'];
				$vo->buyer_email= $rows['buyer_email'];
				$vo->payment_amount= $rows['payment_amount'];
				$vo->payment_status= $rows['payment_status'];
				$vo->payment_type= $rows['payment_type'];
				$vo->itemid= $rows['itemid'];
				$vo->createdtime= $rows['createdtime'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM payments ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM payments ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new paymentsVO();
				$vo->id= $rows['id'];
				$vo->buyer_id= $rows['buyer_id'];
				$vo->txnid= $rows['txnid'];
				$vo->buyer_firstname= $rows['buyer_firstname'];
				$vo->buyer_lastname= $rows['buyer_lastname'];
				$vo->buyer_email= $rows['buyer_email'];
				$vo->payment_amount= $rows['payment_amount'];
				$vo->payment_status= $rows['payment_status'];
				$vo->payment_type= $rows['payment_type'];
				$vo->itemid= $rows['itemid'];
				$vo->createdtime= $rows['createdtime'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE payments
	SET
		buyer_id= '$vo->buyer_id',
		txnid= '$vo->txnid',
		buyer_firstname= '$vo->buyer_firstname',
		buyer_lastname= '$vo->buyer_lastname',
		buyer_email= '$vo->buyer_email',
		payment_amount= '$vo->payment_amount',
		payment_status= '$vo->payment_status',
		payment_type= '$vo->payment_type',
		itemid= '$vo->itemid',
		createdtime= '$vo->createdtime'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM payments WHERE id= '$id'";
		$this->exec();
		$vo = new paymentsVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->buyer_id= $rows['buyer_id'];
			$vo->txnid= $rows['txnid'];
			$vo->buyer_firstname= $rows['buyer_firstname'];
			$vo->buyer_lastname= $rows['buyer_lastname'];
			$vo->buyer_email= $rows['buyer_email'];
			$vo->payment_amount= $rows['payment_amount'];
			$vo->payment_status= $rows['payment_status'];
			$vo->payment_type= $rows['payment_type'];
			$vo->itemid= $rows['itemid'];
			$vo->createdtime= $rows['createdtime'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchPurchasedCreditsById($buyer_id)
	{
		$this->sql = "SELECT Sum(payments.payment_amount) AS Amount FROM payments WHERE payments.buyer_id ='$buyer_id'";
		$this->exec();
		$vo;
		if($this->result)
		{
			$rows = $this->fetch();
			//$vo->id= $rows['id'];
			
			$vo['payment_amount']= $rows['Amount'];
			
			//$vo->formatFetchVariables();
		}
		return $vo;
	}




	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM payments WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

