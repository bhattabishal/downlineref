<?php 
class program_signup_childVO 
{
	public $id;
	public $parent_user_id;
	public $child_user_id;
	public $username;
	public $email;
	public $program_id;
	public $created_by;
	public $created_date;
	public $updated_by;
	public $updated_date;
	public $update_count;
	public $status;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id;
		$this->parent_user_id= $this->parent_user_id;
		$this->child_user_id= $this->child_user_id;
		$this->username= $this->username;
		$this->email= $this->email;
		$this->program_id= $this->program_id;
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
		$this->status= $this->status;
	}


	function formatFetchVariables()
	{
		$this->id;
		$this->parent_user_id= $this->parent_user_id;
		$this->child_user_id= $this->child_user_id;
		$this->username= $this->username;
		$this->email= $this->email;
		$this->program_id= $this->program_id;
		$this->created_by= $this->created_by;
		$this->created_date= $this->created_date;
		$this->updated_by= $this->updated_by;
		$this->updated_date= $this->updated_date;
		$this->update_count= $this->update_count;
		$this->status= $this->status;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->parent_user_id= $array['parent_user_id'];
		$this->child_user_id= $array['child_user_id'];
		$this->username= $array['username'];
		$this->email= $array['email'];
		$this->program_id= $array['program_id'];
		$this->created_by= $array['created_by'];
		$this->created_date= $array['created_date'];
		$this->updated_by= $array['updated_by'];
		$this->updated_date= $array['updated_date'];
		$this->update_count= $array['update_count'];
		$this->status= $array['status'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->parent_user_id);
		unset($this->child_user_id);
		unset($this->username);
		unset($this->email);
		unset($this->program_id);
		unset($this->created_by);
		unset($this->created_date);
		unset($this->updated_by);
		unset($this->updated_date);
		unset($this->update_count);
		unset($this->status);
		unset($this);
	}


}
?> 

