<?php 
class usersDAO extends BaseDAO
{
	public $user_id;
	public $full_name;
	public $username;
	public $password;
	public $designation;
	public $email;
	public $joined_date;
	public $status;
	public $remarks;
	public $entered_by;
	public $entered_date;
	public $updated_by;
	public $updated_date;
	public $update_count;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_users
	SET
		full_name= '$vo->full_name',
		username= '$vo->username',
		password= '$vo->password',
		designation= '$vo->designation',
		email= '$vo->email',
		joined_date= '$vo->joined_date',
		status= '$vo->status',
		remarks= '$vo->remarks',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_users ORDER BY user_id";
		else
			$this->sql = "SELECT * FROM tbl_users ORDER BY user_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new usersVO();
				$vo->user_id= $rows['user_id'];
				$vo->full_name= $rows['full_name'];
				$vo->username= $rows['username'];
				$vo->password= $rows['password'];
				$vo->designation= $rows['designation'];
				$vo->email= $rows['email'];
				$vo->joined_date= $rows['joined_date'];
				$vo->status= $rows['status'];
				$vo->remarks= $rows['remarks'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_users ORDER BY user_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_users ORDER BY user_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new usersVO();
				$vo->user_id= $rows['user_id'];
				$vo->full_name= $rows['full_name'];
				$vo->username= $rows['username'];
				$vo->password= $rows['password'];
				$vo->designation= $rows['designation'];
				$vo->email= $rows['email'];
				$vo->joined_date= $rows['joined_date'];
				$vo->status= $rows['status'];
				$vo->remarks= $rows['remarks'];
				$vo->entered_by= $rows['entered_by'];
				$vo->entered_date= $rows['entered_date'];
				$vo->updated_by= $rows['updated_by'];
				$vo->updated_date= $rows['updated_date'];
				$vo->update_count= $rows['update_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_users
	SET
		full_name= '$vo->full_name',
		username= '$vo->username',
		password= '$vo->password',
		designation= '$vo->designation',
		email= '$vo->email',
		joined_date= '$vo->joined_date',
		status= '$vo->status',
		remarks= '$vo->remarks',
		entered_by= '$vo->entered_by',
		entered_date= '$vo->entered_date',
		updated_by= '$vo->updated_by',
		updated_date= '$vo->updated_date',
		update_count= '$vo->update_count'
		WHERE user_id= '$vo->user_id'";
		return $this->exec();
	}


	function fetchDetails($user_id)
	{
		$this->sql = "SELECT * FROM tbl_users WHERE user_id= '$user_id'";
		$this->exec();
		$vo = new usersVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->user_id= $rows['user_id'];
			$vo->full_name= $rows['full_name'];
			$vo->username= $rows['username'];
			$vo->password= $rows['password'];
			$vo->designation= $rows['designation'];
			$vo->email= $rows['email'];
			$vo->joined_date= $rows['joined_date'];
			$vo->status= $rows['status'];
			$vo->remarks= $rows['remarks'];
			$vo->entered_by= $rows['entered_by'];
			$vo->entered_date= $rows['entered_date'];
			$vo->updated_by= $rows['updated_by'];
			$vo->updated_date= $rows['updated_date'];
			$vo->update_count= $rows['update_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function ForgotPassword($email)
			{
			$this->sql = "SELECT * FROM tbl_users WHERE email = '$email'";
			
			$this->exec();//stores result in $this->result
			
			$vo = new usersVO();
			if($this->result)
				{
				$rows = $this->fetch();
				
				//$vo = new UsersVO();
				
				
					$vo->user_id= $rows['user_id'];
					$vo->full_name= $rows['full_name'];
					$vo->username= $rows['username'];
					$vo->password= $rows['password'];
					$vo->designation= $rows['designation'];
					$vo->email= $rows['email'];
					$vo->joined_date= $rows['joined_date'];
					$vo->status= $rows['status'];
					$vo->remarks= $rows['remarks'];
					$vo->entered_by= $rows['entered_by'];
					$vo->entered_date= $rows['entered_date'];
					$vo->updated_by= $rows['updated_by'];
					$vo->updated_date= $rows['updated_date'];
					$vo->update_count= $rows['update_count'];
				
				$vo->formatFetchVariables();
				}
			return $vo;
			}
	
	
	function authenticate($vo)
	{
			$this->sql = "SELECT * FROM tbl_users WHERE username = '$vo->username' 
			 AND password = '$vo->password' AND status = '1'";
			//echo $this->sql;
			//exit();
			$this->exec();//stores result in $this->result
			
			if($this->result)
			{
				if($this->row_count($this->result)==1)
				{
					$rows = $this->fetch();
					$id = intval($rows['user_id']);
					$vo = new usersVO();
					if($id != 0)
					{
						$vo->user_id = $rows['user_id'];
						$vo->full_name= $rows['full_name'];
						$vo->username= $rows['username'];
						$vo->password= $rows['password'];
						$vo->designation= $rows['designation'];
						$vo->email= $rows['email'];
						$vo->status = "valid";
						
						$vo->formatFetchVariables();
						return $vo->user_id;
					}
					else
					{
						$vo->status = "invalid";
						return $id;
					}
				}
				else
				{
					$vo->status = "invalid";
					return $id;
				}
			}
			else
			{
				$vo->status = "invalid";
				return $id;
			}
		
	}
		
	function changePassword($password, $id)
		{
		$vo = $this->fetchDetails($id);
		if($vo->id != 0)
			{
			$this->sql = "UPDATE tbl_admins SET password = '$password' WHERE id = '$vo->id'";
			$this->exec();
			if($this->result)
				return true;
			else
				return false;
			}
		else
			return false;
		}				
	
	
	function ActivateNdactivate($id, $publish)
			{
			$vo= $this->fetchDetails($id);
			if($vo->user_id != 0)
				{
				$this->sql = "UPDATE tbl_users SET status = '$publish' WHERE user_id = '$vo->user_id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}
	
	
	
	function remove($user_id)
	{
		$vo=$this->fetchDetails($user_id);
		if($vo->user_id!=0)
		{
			$this->sql = "DELETE FROM tbl_users WHERE user_id= '$user_id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

