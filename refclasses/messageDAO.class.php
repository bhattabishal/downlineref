<?php 
class messageDAO extends BaseDAO
{
	public $message_id;
	public $message_from;
	public $message_to;
	public $message_subject;
	public $message_details;
	public $created_date;
	public $status;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO message
	SET
		message_from= '$vo->message_from',
		message_to= '$vo->message_to',
		message_subject= '$vo->message_subject',
		message_details= '$vo->message_details',
		created_date= '$vo->created_date',
		status= '$vo->status'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM message ORDER BY message_id";
		else
			$this->sql = "SELECT * FROM message ORDER BY message_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new messageVO();
				$vo->message_id= $rows['message_id'];
				$vo->message_from= $rows['message_from'];
				$vo->message_to= $rows['message_to'];
				$vo->message_subject= $rows['message_subject'];
				$vo->message_details= $rows['message_details'];
				$vo->created_date= $rows['created_date'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllSent($id)
	{
		
			$this->sql = "SELECT * FROM message WHERE message_from='$id'  ORDER BY message_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new messageVO();
				$vo->message_id= $rows['message_id'];
				$vo->message_from= $rows['message_from'];
				$vo->message_to= $rows['message_to'];
				$vo->message_subject= $rows['message_subject'];
				$vo->message_details= $rows['message_details'];
				$vo->created_date= $rows['created_date'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllReceived($id)
	{
		
			$this->sql = "SELECT * FROM message WHERE message_to='$id'  ORDER BY message_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new messageVO();
				$vo->message_id= $rows['message_id'];
				$vo->message_from= $rows['message_from'];
				$vo->message_to= $rows['message_to'];
				$vo->message_subject= $rows['message_subject'];
				$vo->message_details= $rows['message_details'];
				$vo->created_date= $rows['created_date'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM message ORDER BY message_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM message ORDER BY message_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new messageVO();
				$vo->message_id= $rows['message_id'];
				$vo->message_from= $rows['message_from'];
				$vo->message_to= $rows['message_to'];
				$vo->message_subject= $rows['message_subject'];
				$vo->message_details= $rows['message_details'];
				$vo->created_date= $rows['created_date'];
				$vo->status= $rows['status'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE message
	SET
		message_from= '$vo->message_from',
		message_to= '$vo->message_to',
		message_subject= '$vo->message_subject',
		message_details= '$vo->message_details',
		created_date= '$vo->created_date',
		status= '$vo->status'
	WHERE message_id= '$vo->message_id'";
		return $this->exec();
	}


	function fetchDetails($message_id)
	{
		$this->sql = "SELECT * FROM message WHERE message_id= '$message_id'";
		
		$this->exec();
		$vo = new messageVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->message_id= $rows['message_id'];
			$vo->message_from= $rows['message_from'];
			$vo->message_to= $rows['message_to'];
			$vo->message_subject= $rows['message_subject'];
			$vo->message_details= $rows['message_details'];
			$vo->created_date= $rows['created_date'];
			$vo->status= $rows['status'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($message_id)
	{
		$vo=$this->fetchDetails($message_id);
		if($vo->message_id!=0)
		{
			$this->sql = "DELETE FROM message WHERE message_id= '$message_id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

