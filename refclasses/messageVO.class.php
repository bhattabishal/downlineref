<?php 
class messageVO 
{
	public $message_id;
	public $message_from;
	public $message_to;
	public $message_subject;
	public $message_details;
	public $created_date;
	public $status;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->message_id;
		$this->message_from= $this->message_from;
		$this->message_to= $this->message_to;
		$this->message_subject= $this->message_subject;
		$this->message_details= addslashes(trim($this->message_details));
		$this->created_date= $this->created_date;
		$this->status= $this->status;
	}


	function formatFetchVariables()
	{
		$this->message_id;
		$this->message_from= $this->message_from;
		$this->message_to= $this->message_to;
		$this->message_subject= $this->message_subject;
		$this->message_details= stripslashes(trim($this->message_details));
		$this->created_date= $this->created_date;
		$this->status= $this->status;
	}


	function getPostVars($array)
	{
		$this->message_id= $array['message_id'];
		$this->message_from= $array['message_from'];
		$this->message_to= $array['message_to'];
		$this->message_subject= $array['message_subject'];
		$this->message_details= $array['message_details'];
		$this->created_date= $array['created_date'];
		$this->status= $array['status'];
	}


	function __destruct()
	{
		unset($this->message_id);
		unset($this->message_from);
		unset($this->message_to);
		unset($this->message_subject);
		unset($this->message_details);
		unset($this->created_date);
		unset($this->status);
		unset($this);
	}


}
?> 

