<?php 
class countryDAO extends BaseDAO
{
	public $id;
	public $name;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO country
	SET
		name= '$vo->name'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM country ORDER BY id";
		else
			$this->sql = "SELECT * FROM country ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new countryVO();
				$vo->id= $rows['id'];
				$vo->name= $rows['name'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM country ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM country ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new countryVO();
				$vo->id= $rows['id'];
				$vo->name= $rows['name'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE country
	SET
		name= '$vo->name'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM country WHERE id= '$id'";
		$this->exec();
		$vo = new countryVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->name= $rows['name'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM country WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

