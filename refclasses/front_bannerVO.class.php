<?php 
class front_bannerVO 
{
	public $ban_id;
	public $purchased_by;
	public $payment_type;
	public $txn_id;
	public $target_url;
	public $banner_url;
	public $purchaser_email;
	public $ban_spot;
	public $ban_weeks;
	public $ban_status;
	public $created_date;
	public $valid_date;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->ban_id;
		$this->purchased_by= $this->purchased_by;
		$this->payment_type= $this->payment_type;
		$this->txn_id= $this->txn_id;
		$this->target_url= $this->target_url;
		$this->banner_url= $this->banner_url;
		$this->purchaser_email= $this->purchaser_email;
		$this->ban_spot= $this->ban_spot;
		$this->ban_weeks= $this->ban_weeks;
		$this->ban_status= $this->ban_status;
		$this->created_date= $this->created_date;
		$this->valid_date= $this->valid_date;
	}


	function formatFetchVariables()
	{
		$this->ban_id;
		$this->purchased_by= $this->purchased_by;
		$this->payment_type= $this->payment_type;
		$this->txn_id= $this->txn_id;
		$this->target_url= $this->target_url;
		$this->banner_url= $this->banner_url;
		$this->purchaser_email= $this->purchaser_email;
		$this->ban_spot= $this->ban_spot;
		$this->ban_weeks= $this->ban_weeks;
		$this->ban_status= $this->ban_status;
		$this->created_date= $this->created_date;
		$this->valid_date= $this->valid_date;
	}


	function getPostVars($array)
	{
		$this->ban_id= $array['ban_id'];
		$this->purchased_by= $array['purchased_by'];
		$this->payment_type= $array['payment_type'];
		$this->txn_id= $array['txn_id'];
		$this->target_url= $array['target_url'];
		$this->banner_url= $array['banner_url'];
		$this->purchaser_email= $array['purchaser_email'];
		$this->ban_spot= $array['ban_spot'];
		$this->ban_weeks= $array['ban_weeks'];
		$this->ban_status= $array['ban_status'];
		$this->created_date= $array['created_date'];
		$this->valid_date= $array['valid_date'];
	}


	function __destruct()
	{
		unset($this->ban_id);
		unset($this->purchased_by);
		unset($this->payment_type);
		unset($this->txn_id);
		unset($this->target_url);
		unset($this->banner_url);
		unset($this->purchaser_email);
		unset($this->ban_spot);
		unset($this->ban_weeks);
		unset($this->ban_status);
		unset($this->created_date);
		unset($this->valid_date);
		unset($this);
	}


}
?> 

