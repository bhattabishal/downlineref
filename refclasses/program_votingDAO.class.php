<?php 
class program_votingDAO extends BaseDAO
{
	public $id;
	public $vote_by;
	public $vote_value;
	public $vote_for;
	public $created_date;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_program_voting
	SET
		vote_by= '$vo->vote_by',
		vote_value= '$vo->vote_value',
		vote_for= '$vo->vote_for',
		created_date= '$vo->created_date'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_program_voting ORDER BY id";
		else
			$this->sql = "SELECT * FROM tbl_program_voting ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchRatingsById($program,$vote_value)
	{
		
		$this->sql = "SELECT * FROM tbl_program_voting WHERE vote_for='$program' AND  vote_value='$vote_value'";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_program_voting ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_program_voting ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_program_voting
	SET
		vote_by= '$vo->vote_by',
		vote_value= '$vo->vote_value',
		vote_for= '$vo->vote_for',
		created_date= '$vo->created_date'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM tbl_program_voting WHERE id= '$id'";
		$this->exec();
		$vo = new program_votingVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->vote_by= $rows['vote_by'];
			$vo->vote_value= $rows['vote_value'];
			$vo->vote_for= $rows['vote_for'];
			$vo->created_date= $rows['created_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchIfInserted($vote_by,$vote_for)
	{
		$this->sql = "SELECT * FROM tbl_program_voting WHERE vote_by= '$vote_by' AND vote_for='$vote_for'";
		$this->exec();
		$vo = new program_votingVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->vote_by= $rows['vote_by'];
			$vo->vote_value= $rows['vote_value'];
			$vo->vote_for= $rows['vote_for'];
			$vo->created_date= $rows['created_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM tbl_program_voting WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

