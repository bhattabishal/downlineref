<?php 
class front_bannerDAO extends BaseDAO
{
	public $ban_id;
	public $purchased_by;
	public $payment_type;
	public $txn_id;
	public $target_url;
	public $banner_url;
	public $purchaser_email;
	public $ban_spot;
	public $ban_weeks;
	public $ban_status;
	public $created_date;
	public $valid_date;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_front_banner
	SET
		purchased_by= '$vo->purchased_by',
		payment_type= '$vo->payment_type',
		txn_id= '$vo->txn_id',
		target_url= '$vo->target_url',
		banner_url= '$vo->banner_url',
		purchaser_email= '$vo->purchaser_email',
		ban_spot= '$vo->ban_spot',
		ban_weeks= '$vo->ban_weeks',
		ban_status= '$vo->ban_status',
		created_date= '$vo->created_date',
		valid_date= '$vo->valid_date'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_front_banner WHERE ban_status='1' ORDER BY ban_id";
		else
			$this->sql = "SELECT * FROM tbl_front_banner ORDER BY ban_id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new front_bannerVO();
				$vo->ban_id= $rows['ban_id'];
				$vo->purchased_by= $rows['purchased_by'];
				$vo->payment_type= $rows['payment_type'];
				$vo->txn_id= $rows['txn_id'];
				$vo->target_url= $rows['target_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->purchaser_email= $rows['purchaser_email'];
				$vo->ban_spot= $rows['ban_spot'];
				$vo->ban_weeks= $rows['ban_weeks'];
				$vo->ban_status= $rows['ban_status'];
				$vo->created_date= $rows['created_date'];
				$vo->valid_date= $rows['valid_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_front_banner ORDER BY ban_id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_front_banner ORDER BY ban_id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new front_bannerVO();
				$vo->ban_id= $rows['ban_id'];
				$vo->purchased_by= $rows['purchased_by'];
				$vo->payment_type= $rows['payment_type'];
				$vo->txn_id= $rows['txn_id'];
				$vo->target_url= $rows['target_url'];
				$vo->banner_url= $rows['banner_url'];
				$vo->purchaser_email= $rows['purchaser_email'];
				$vo->ban_spot= $rows['ban_spot'];
				$vo->ban_weeks= $rows['ban_weeks'];
				$vo->ban_status= $rows['ban_status'];
				$vo->created_date= $rows['created_date'];
				$vo->valid_date= $rows['valid_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_front_banner
	SET
		purchased_by= '$vo->purchased_by',
		payment_type= '$vo->payment_type',
		txn_id= '$vo->txn_id',
		target_url= '$vo->target_url',
		banner_url= '$vo->banner_url',
		purchaser_email= '$vo->purchaser_email',
		ban_spot= '$vo->ban_spot',
		ban_weeks= '$vo->ban_weeks',
		ban_status= '$vo->ban_status',
		created_date= '$vo->created_date',
		valid_date= '$vo->valid_date'
	WHERE ban_id= '$vo->ban_id'";
		return $this->exec();
	}


	function fetchDetails($ban_id)
	{
		$this->sql = "SELECT * FROM tbl_front_banner WHERE ban_id= '$ban_id'";
		$this->exec();
		$vo = new front_bannerVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->ban_id= $rows['ban_id'];
			$vo->purchased_by= $rows['purchased_by'];
			$vo->payment_type= $rows['payment_type'];
			$vo->txn_id= $rows['txn_id'];
			$vo->target_url= $rows['target_url'];
			$vo->banner_url= $rows['banner_url'];
			$vo->purchaser_email= $rows['purchaser_email'];
			$vo->ban_spot= $rows['ban_spot'];
			$vo->ban_weeks= $rows['ban_weeks'];
			$vo->ban_status= $rows['ban_status'];
			$vo->created_date= $rows['created_date'];
			$vo->valid_date= $rows['valid_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchDetailsByActiveSpot($ban_spot)
	{
		$this->sql = "SELECT * FROM tbl_front_banner WHERE ban_spot='$ban_spot' AND ban_status='1'";
		$this->exec();
		$vo = new front_bannerVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->ban_id= $rows['ban_id'];
			$vo->purchased_by= $rows['purchased_by'];
			$vo->payment_type= $rows['payment_type'];
			$vo->txn_id= $rows['txn_id'];
			$vo->target_url= $rows['target_url'];
			$vo->banner_url= $rows['banner_url'];
			$vo->purchaser_email= $rows['purchaser_email'];
			$vo->ban_spot= $rows['ban_spot'];
			$vo->ban_weeks= $rows['ban_weeks'];
			$vo->ban_status= $rows['ban_status'];
			$vo->created_date= $rows['created_date'];
			$vo->valid_date= $rows['valid_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
function ActivateNdactivate($id)
			{
			$vo= $this->fetchDetails($id);
			if($vo->ban_id != 0)
				{
				$this->sql = "UPDATE tbl_front_banner SET ban_status = '0' WHERE ban_id = '$vo->ban_id'";
				$this->exec();
				if($this->result)
					return true;
				else
					return false;
				}
			else
				return false;
			}

	function remove($ban_id)
	{
		$vo=$this->fetchDetails($ban_id);
		if($vo->ban_id!=0)
		{
			$this->sql = "DELETE FROM tbl_front_banner WHERE ban_id= '$ban_id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

