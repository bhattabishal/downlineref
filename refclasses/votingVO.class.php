<?php 
class votingVO 
{
	public $id;
	public $vote_by;
	public $vote_to;
	public $vote_type;
	public $vote_value;
	public $vote_for;
	public $vote;
	public $created_date;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->id;
		$this->vote_by= $this->vote_by;
		$this->vote_to= $this->vote_to;
		$this->vote_type= $this->vote_type;
		$this->vote_value= $this->vote_value;
		$this->vote_for= $this->vote_for;
		$this->vote= $this->vote;
		$this->created_date= $this->created_date;
	}


	function formatFetchVariables()
	{
		$this->id;
		$this->vote_by= $this->vote_by;
		$this->vote_to= $this->vote_to;
		$this->vote_type= $this->vote_type;
		$this->vote_value= $this->vote_value;
		$this->vote_for= $this->vote_for;
		$this->vote= $this->vote;
		$this->created_date= $this->created_date;
	}


	function getPostVars($array)
	{
		$this->id= $array['id'];
		$this->vote_by= $array['vote_by'];
		$this->vote_to= $array['vote_to'];
		$this->vote_type= $array['vote_type'];
		$this->vote_value= $array['vote_value'];
		$this->vote_for= $array['vote_for'];
		$this->vote= $array['vote'];
		$this->created_date= $array['created_date'];
	}


	function __destruct()
	{
		unset($this->id);
		unset($this->vote_by);
		unset($this->vote_to);
		unset($this->vote_type);
		unset($this->vote_value);
		unset($this->vote_for);
		unset($this->vote);
		unset($this->created_date);
		unset($this);
	}


}
?> 

