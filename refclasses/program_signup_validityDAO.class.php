<?php 
class program_signup_validityDAO extends BaseDAO
{
	public $id;
	public $signed_user_id;
	public $program_signup_id;
	public $valid_from;
	public $valid_to;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO program_signup_validity
	SET
		signed_user_id= '$vo->signed_user_id',
		program_signup_id= '$vo->program_signup_id',
		valid_from= '$vo->valid_from',
		valid_to= '$vo->valid_to'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_validity ORDER BY id";
		else
			$this->sql = "SELECT * FROM program_signup_validity ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_validityVO();
				$vo->id= $rows['id'];
				$vo->signed_user_id= $rows['signed_user_id'];
				$vo->program_signup_id= $rows['program_signup_id'];
				$vo->valid_from= $rows['valid_from'];
				$vo->valid_to= $rows['valid_to'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllActive($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_validity ORDER BY id";
		else
			$this->sql = "SELECT * FROM program_signup_validity WHERE  CURDATE() > DATE(valid_from)  AND CURDATE() <=  DATE(valid_to)  ORDER BY id DESC";
			
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_validityVO();
				$vo->id= $rows['id'];
				$vo->signed_user_id= $rows['signed_user_id'];
				$vo->program_signup_id= $rows['program_signup_id'];
				$vo->valid_from= $rows['valid_from'];
				$vo->valid_to= $rows['valid_to'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM program_signup_validity ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM program_signup_validity ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new program_signup_validityVO();
				$vo->id= $rows['id'];
				$vo->signed_user_id= $rows['signed_user_id'];
				$vo->program_signup_id= $rows['program_signup_id'];
				$vo->valid_from= $rows['valid_from'];
				$vo->valid_to= $rows['valid_to'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE program_signup_validity
	SET
		signed_user_id= '$vo->signed_user_id',
		program_signup_id= '$vo->program_signup_id',
		valid_from= '$vo->valid_from',
		valid_to= '$vo->valid_to'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM program_signup_validity WHERE id= '$id'";
		$this->exec();
		$vo = new program_signup_validityVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->signed_user_id= $rows['signed_user_id'];
			$vo->program_signup_id= $rows['program_signup_id'];
			$vo->valid_from= $rows['valid_from'];
			$vo->valid_to= $rows['valid_to'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchSignupDetailsByUserId($uid,$pid)
	{
		$this->sql = "SELECT * FROM program_signup_validity WHERE signed_user_id= '$uid' AND program_signup_id='$pid'";
		$this->exec();
		$vo = new program_signup_validityVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->signed_user_id= $rows['signed_user_id'];
			$vo->program_signup_id= $rows['program_signup_id'];
			$vo->valid_from= $rows['valid_from'];
			$vo->valid_to= $rows['valid_to'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchSignupValidityDetails($id)
	{
		$this->sql = "SELECT * FROM program_signup_validity WHERE program_signup_id='$id' AND valid_to >= CURDATE()";
		$this->exec();
		$vo = new program_signup_validityVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->signed_user_id= $rows['signed_user_id'];
			$vo->program_signup_id= $rows['program_signup_id'];
			$vo->valid_from= $rows['valid_from'];
			$vo->valid_to= $rows['valid_to'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM program_signup_validity WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

