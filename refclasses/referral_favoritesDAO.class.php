<?php 
class referral_favoritesDAO extends BaseDAO
{
	public $id;
	public $user_id;
	public $url;
	public $created_date;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_referral_favorites
	SET
		user_id= '$vo->user_id',
		url= '$vo->url',
		created_date= '$vo->created_date'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_referral_favorites ORDER BY id";
		else
			$this->sql = "SELECT * FROM tbl_referral_favorites ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new referral_favoritesVO();
				$vo->id= $rows['id'];
				$vo->user_id= $rows['user_id'];
				$vo->url= $rows['url'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllById($id)
	{
		
			$this->sql = "SELECT * FROM tbl_referral_favorites WHERE user_id='$id' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new referral_favoritesVO();
				$vo->id= $rows['id'];
				$vo->user_id= $rows['user_id'];
				$vo->url= $rows['url'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_referral_favorites ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_referral_favorites ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new referral_favoritesVO();
				$vo->id= $rows['id'];
				$vo->user_id= $rows['user_id'];
				$vo->url= $rows['url'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_referral_favorites
	SET
		user_id= '$vo->user_id',
		url= '$vo->url',
		created_date= '$vo->created_date'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM tbl_referral_favorites WHERE id= '$id'";
		$this->exec();
		$vo = new referral_favoritesVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->user_id= $rows['user_id'];
			$vo->url= $rows['url'];
			$vo->created_date= $rows['created_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM tbl_referral_favorites WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

