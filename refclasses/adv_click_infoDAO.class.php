<?php 
class adv_click_infoDAO extends BaseDAO
{
	public $id;
	public $adv_id;
	public $adv_click_count;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO adv_click_info
	SET
		adv_id= '$vo->adv_id',
		adv_click_count= '$vo->adv_click_count'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM adv_click_info ORDER BY id";
		else
			$this->sql = "SELECT * FROM adv_click_info ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new adv_click_infoVO();
				$vo->id= $rows['id'];
				$vo->adv_id= $rows['adv_id'];
				$vo->adv_click_count= $rows['adv_click_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM adv_click_info ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM adv_click_info ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new adv_click_infoVO();
				$vo->id= $rows['id'];
				$vo->adv_id= $rows['adv_id'];
				$vo->adv_click_count= $rows['adv_click_count'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE adv_click_info
	SET
		adv_id= '$vo->adv_id',
		adv_click_count= '$vo->adv_click_count'
	WHERE id= '$vo->id';
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM adv_click_info WHERE id= '$id'";
		$this->exec();
		$vo = new adv_click_infoVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->adv_id= $rows['adv_id'];
			$vo->adv_click_count= $rows['adv_click_count'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM adv_click_info WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return ture;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

