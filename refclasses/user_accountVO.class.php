<?php 
class user_accountVO 
{
	public $account_id;
	public $user_id;
	public $txn_id;
	public $created_date;
	public $credit_type;
	public $credits;


	function __construct()
	{
	}


	function formatInsertVariables()
	{
		$this->account_id;
		$this->user_id= $this->user_id;
		$this->txn_id= $this->txn_id;
		$this->created_date= $this->created_date;
		$this->credit_type= $this->credit_type;
		$this->credits= $this->credits;
	}


	function formatFetchVariables()
	{
		$this->account_id;
		$this->user_id= $this->user_id;
		$this->txn_id= $this->txn_id;
		$this->created_date= $this->created_date;
		$this->credit_type= $this->credit_type;
		$this->credits= $this->credits;
	}


	function getPostVars($array)
	{
		$this->account_id= $array['account_id'];
		$this->user_id= $array['user_id'];
		$this->txn_id= $array['txn_id'];
		$this->created_date= $array['created_date'];
		$this->credit_type= $array['credit_type'];
		$this->credits= $array['credits'];
	}


	function __destruct()
	{
		unset($this->account_id);
		unset($this->user_id);
		unset($this->txn_id);
		unset($this->created_date);
		unset($this->credit_type);
		unset($this->credits);
		unset($this);
	}


}
?> 

