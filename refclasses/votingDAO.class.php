<?php 
class votingDAO extends BaseDAO
{
	public $id;
	public $vote_by;
	public $vote_to;
	public $vote_type;
	public $vote_value;
	public $vote_for;
	public $vote;
	public $created_date;


	function __construct()
	{
	}


	function Insert($vo)
	{
	$this->sql="INSERT INTO tbl_voting
	SET
		vote_by= '$vo->vote_by',
		vote_to= '$vo->vote_to',
		vote_type= '$vo->vote_type',
		vote_value= '$vo->vote_value',
		vote_for= '$vo->vote_for',
		vote= '$vo->vote',
		created_date= '$vo->created_date'";
		return $this->exec();   
	}


	function fetchAll($publish='all')
	{
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_voting ORDER BY id";
		else
			$this->sql = "SELECT * FROM tbl_voting ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_to= $rows['vote_to'];
				$vo->vote_type= $rows['vote_type'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->vote= $rows['vote'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllRatingsById($id,$vote_type)
	{
		
			$this->sql = "SELECT * FROM tbl_voting WHERE vote_to='$id' AND vote_type='$vote_type' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_to= $rows['vote_to'];
				$vo->vote_type= $rows['vote_type'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->vote= $rows['vote'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllRatingsByUserId($id)
	{
		
			$this->sql = "SELECT * FROM tbl_voting WHERE vote_to='$id' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_to= $rows['vote_to'];
				$vo->vote_type= $rows['vote_type'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->vote= $rows['vote'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchAllTypeRatings($vote_type)
	{
		
			$this->sql = "SELECT * FROM tbl_voting WHERE  vote_type='$vote_type' ORDER BY id";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_to= $rows['vote_to'];
				$vo->vote_type= $rows['vote_type'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->vote= $rows['vote'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchRatingsById($vote_to,$vote_type,$vote_value)
	{
		
		$this->sql = "SELECT COALESCE(sum(vote),0) as vote FROM tbl_voting WHERE vote_to='$vote_to' AND vote_type='$vote_type' AND vote_value='$vote_value'";
		$this->exec();
		$vo = new votingVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->vote_by= $rows['vote_by'];
			$vo->vote_to= $rows['vote_to'];
			$vo->vote_type= $rows['vote_type'];
			$vo->vote_value= $rows['vote_value'];
			$vo->vote_for= $rows['vote_for'];
			$vo->vote= $rows['vote'];
			$vo->created_date= $rows['created_date'];
			$vo->formatFetchVariables();
		}
		return $vo->vote;
	}


	function fetchLimited($page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_voting ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_voting ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_to= $rows['vote_to'];
				$vo->vote_type= $rows['vote_type'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->vote= $rows['vote'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}
	
	function fetchLimitedByUserId($id,$page, $perpage, $publish='all')
	{
		$from = ($page-1)*$perpage;
		if($publish == 'onlypublished')
			$this->sql = "SELECT * FROM tbl_voting WHERE vote_to='$id' ORDER BY id LIMIT $from, $perpage";
		else
			$this->sql = "SELECT * FROM tbl_voting WHERE vote_to='$id' ORDER BY id LIMIT $from, $perpage";
		$this->exec();
		$list = array();
		if($this->result)
		{
			while($rows = $this->fetch())
			{
				$vo = new votingVO();
				$vo->id= $rows['id'];
				$vo->vote_by= $rows['vote_by'];
				$vo->vote_to= $rows['vote_to'];
				$vo->vote_type= $rows['vote_type'];
				$vo->vote_value= $rows['vote_value'];
				$vo->vote_for= $rows['vote_for'];
				$vo->vote= $rows['vote'];
				$vo->created_date= $rows['created_date'];
				$vo->formatFetchVariables();
				array_push($list, $vo);
			}
		}
		return $list;
	}


	function update($vo)
	{
	$this->sql="UPDATE tbl_voting
	SET
		vote_by= '$vo->vote_by',
		vote_to= '$vo->vote_to',
		vote_type= '$vo->vote_type',
		vote_value= '$vo->vote_value',
		vote_for= '$vo->vote_for',
		vote= '$vo->vote',
		created_date= '$vo->created_date'
	WHERE id= '$vo->id'";
		return $this->exec();
	}


	function fetchDetails($id)
	{
		$this->sql = "SELECT * FROM tbl_voting WHERE id= '$id'";
		$this->exec();
		$vo = new votingVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->vote_by= $rows['vote_by'];
			$vo->vote_to= $rows['vote_to'];
			$vo->vote_type= $rows['vote_type'];
			$vo->vote_value= $rows['vote_value'];
			$vo->vote_for= $rows['vote_for'];
			$vo->vote= $rows['vote'];
			$vo->created_date= $rows['created_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}
	
	function fetchDetailsByProgramId($id)
	{
		$this->sql = "SELECT * FROM tbl_voting WHERE vote_for= '$id'";
		$this->exec();
		$vo = new votingVO();
		if($this->result)
		{
			$rows = $this->fetch();
			$vo->id= $rows['id'];
			$vo->vote_by= $rows['vote_by'];
			$vo->vote_to= $rows['vote_to'];
			$vo->vote_type= $rows['vote_type'];
			$vo->vote_value= $rows['vote_value'];
			$vo->vote_for= $rows['vote_for'];
			$vo->vote= $rows['vote'];
			$vo->created_date= $rows['created_date'];
			$vo->formatFetchVariables();
		}
		return $vo;
	}


	function remove($id)
	{
		$vo=$this->fetchDetails($id);
		if($vo->id!=0)
		{
			$this->sql = "DELETE FROM tbl_voting WHERE id= '$id'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function removeByProgramId($pid)
	{
		
			$this->sql = "DELETE FROM tbl_voting WHERE vote_for= '$pid' AND vote_type='referral'";
			$this->exec();
			if($this->result)
			{
				return true;
			}
			else
			{
				return false;
			}
		
	}


	function __destruct()
	{
		unset($this);
	}


}
?> 

