<?php
 
// WPTB Mass Cronjob Script
// http://wptweetbomb.com/strategy/fast-cron-job-setup
 
// How To Use This Script
//	Save this code as a PHP file. Something like "wptb_cron.php" will do.
//	Enter your WPTB cron urls below, one per line in the "$data" variable.
//	Upload it to your server. Any server, in any location.
//	Setup ONE cronjob to run this script on that same server.
 
 
$data = '
 
http://www.web2bizz.net/downlinerefnew/scheduledtask.php

 
';
 
 
// Don't edit below here unless you feel like tweaking some code.
 
 
$urls = explode( "\n", trim($data) );
 
foreach( $urls as $url )
{
	get( $url );
}
 
function get( $url )
{
	$curl = curl_init();
	curl_setopt( $curl, CURLOPT_URL, $url );
	curl_setopt( $curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" );
	curl_setopt( $curl, CURLOPT_HTTPHEADER, array("Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5", "Cache-Control: max-age=0", "Connection: keep-alive", "Keep-Alive: 300", "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7", "Accept-Language: en-us,en;q=0.5", "Pragma: ") );
	curl_setopt( $curl, CURLOPT_ENCODING, "gzip,deflate" );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $curl, CURLOPT_TIMEOUT, 1 );
	curl_setopt( $curl, CURLOPT_AUTOREFERER, 1 );
	curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, 1 );
	$html = curl_exec( $curl );
	curl_close( $curl );
	return $html;
}
 
?>