<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "logincheck.php";

$vo=new votingVO();
$dao=new votingDAO();

//fetch 



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Referral- and Promotor Ratings</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<link rel="stylesheet" type="text/css" href="css/sortertable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<script type="text/javascript" src="js/ajaxviewrating.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<!--<script type="text/javascript" src="js/wufoo.js"></script>-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft" style="margin:5px; width:940px">
     	<div class="block" style="width:940px">
        <!-- Ttle (Start) -->
        <div class="ttle" style="background:url(images/h1bg2.jpg) no-repeat; width:940px">
          	<h1> 
				Referral- and Promotor Ratings
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--info div staart-->
        <div id="form-container" style="margin-bottom:15px; width:910px">
            <header id="form-header" class="info" style="border:none; padding:10px; font-style:normal; font-weight:normal;">
             <ul style="list-style:none; font-weight:bold; ">
                        	<li><span style="margin-right:5px"></span>What's This ?</li>
                           
                        </ul>
                    	<h2 style="font-style:normal;font-size:110%">Each member has two ratings that reflect their 'trustworthyness' here on DownlineRefs:<br />
                      
<div style="margin-left:10px">A promotor rating, which tells you how reliable a member is as a promotor.</div>
<div style="margin-left:10px">And a referral rating, which tells you how reliable a member is as a referral.</div>

The higher a member's rating, the better. More information about ratings can be found in our <a href="support.php">Support Center and Help.</a> <br />


						</h2>
                       
                    	<div></div>
                	</header>
            </div>
        <!--info div end-->
        
        
        <!--table starts-->
        <div style="background:none repeat scroll 0 0 #FFFFFF; border:1px solid #CCCCCC; box-shadow:0 0 5px rgba(0, 0, 0, 0.2); padding:10px 10px 20px 10px; margin-bottom:10px">
        <form method="post" action="" onsubmit="return false">
        	<div style="font-size:14px; font-weight:bold; padding:10px">View member ratings</div>
            <input type="text" name="memname" id="memname" />
            
            <input type="submit" name="memname" value="View Rating"  onclick="viewrating(document.getElementById('memname').value);"/>
        </form>
        </div>
		<div id="validateresult">
        	
        </div>
        
        
           <!--table end-->
           <div class="buttons">
           
                	<button type="button" class="regular" name="Back" value="Back" onclick="location.href='dashboard.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
               
           </div>
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
           <div style="clear:both"></div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			//include_once("includes/rightcolumn.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
