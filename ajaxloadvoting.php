<?php
session_start();
include_once("common/functions.php");
include_once("common/common_functions.php");
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
$uid=$_SESSION['SES_ID'];

$program_name=trim($_GET['program_name']);

$votvo=new program_votingVO();
$votdao=new program_votingDAO();

$spamVotes=$votdao->fetchRatingsById($program_name,"scam");
$trustfulVotes=$votdao->fetchRatingsById($program_name,"trustful");
$totalVotes=count($spamVotes)+count($trustfulVotes);

if($totalVotes == "" )
{
	$totalVotes=0;
}

$scamPercentge=@((count($spamVotes)/($totalVotes))*100);

if($scamPercentge == "")
{
	$scamPercentge=0;
}

$trustfulPercentage=@((count($trustfulVotes)/($totalVotes))*100);

if($trustfulPercentage== "")
{
	$trustfulPercentage=0;
}

//voting ends
?>

<div style="background-color:#6C955C; padding:5px 5px 10px 5px">
                            	<div style="padding:5px"><b>PTC SCAM Meter</b></div>
                                <div style="text-align:center; background-color:#FFFFFF; padding:5px">
                                	<div id="programname" style="font-weight:bold">
                                    	<?php echo $program_name; ?>
                                    </div>
                                	<img src="images/chart.png" />
                                    <div>
                                    	<font color="#FF0000"><b><?php echo $scamPercentge; ?>%</b> Scam</font> | <b><?php echo $trustfulPercentage; ?>%</b> Trustful<br />
                                       <font color="#999999" size="1"> (Results Based On <?php echo $totalVotes; ?> Votes)</font>
                                    </div>
                                    <div style="margin-top:5px">
                                    Want To Vote Yourself ?<br />
                                    <input type="button" value="Scam" style="color:#FF0000" onclick="vote('scam','<?php  echo $uid;?>','<?php  echo $program_name;?>');" />
                                     <input type="button" value="Trustful" style="color:#009900" onclick="vote('trustful','<?php  echo $uid;?>','<?php  echo $program_name;?>');" />
                                    </div>
                                </div>
                            </div>