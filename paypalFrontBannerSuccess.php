<?php
session_start();
//header('Refresh: 1; URL=purchagecredits.php');
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";

if(isset($_SESSION['SES_ID']) && $_SESSION['SES_ID']!='')
{
	$id=$_SESSION['SES_ID'];
}
else
{
	$id=0;
}


$fvo=new front_bannerVO();
$fdao=new front_bannerDAO();

$fvo->purchased_by=$id;
$fvo->payment_type="paypal";
$fvo->txn_id=$_POST[txn_id];
$fvo->target_url=$_SESSION['target_url'];
$fvo->banner_url=$_SESSION['banner_url'];
$fvo->purchaser_email=$_SESSION['purchaser_email'];
$fvo->ban_spot=$_SESSION['ban_spot'];
$fvo->ban_weeks=$_SESSION['ban_weeks'];
$fvo->ban_status=1;
$fvo->created_date=date('Y-m-d H:i:s',strtotime($_POST[payment_date]));
$fvo->valid_date=date("Y-m-d H:i:s", strtotime($fvo->created_date . "+ ".$_SESSION['ban_weeks']." week")); 
//$d="10:05:34 May 31, 2011 PDT ";
//echo date("Y-m-d H:i:s", strtotime("2011-05-31 20:03:06". "+ 2 week"));
if($fdao->Insert($fvo))
{
	unset($_SESSION['target_url']);
	unset($_SESSION['banner_url']);
	unset($_SESSION['purchaser_email']);
	unset($_SESSION['ban_spot']);
	unset($_SESSION['ban_weeks']);
	$url="frontadv.php?action=success";
	Redirect($url);
}
else
{
	$url="frontadv.php?action=error";
	Redirect($url);
}


	

?>

<html>
<head><title>::Thank You::</title>
<link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body bgcolor="ffffff">
<br>
<br>
<table width="500" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr> 
      <td align="left" valign="top" bgcolor="#333333"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
               <td align="center" bgcolor="#EEEEEE"> <p>&nbsp;</p>
                  <p>Thank you! Your order has been successfully processed.</p>
                  <p>&nbsp;</p></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
<table width="500" border="0" align="center" cellpadding="1" cellspacing="0">
   <tr> 
      <td align="left" valign="top" bgcolor="#333333"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr align="left" valign="top"> 
               <td width="20%" bgcolor="#EEEEEE"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Order Number:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[txn_id]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Date:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[payment_date]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td width="20%" bgcolor="#EEEEEE"> First Name: </td>
                        <td width="80%" bgcolor="#EEEEEE"> 
                           <?=$_POST[first_name]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Last Name:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[last_name]?>
                        </td>
                     </tr>
                     <tr align="left" valign="top"> 
                        <td bgcolor="#EEEEEE">Email:</td>
                        <td bgcolor="#EEEEEE"> 
                           <?=$_POST[payer_email]?>
                        </td>
                     </tr>
                  </table></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
</body>
</html>



