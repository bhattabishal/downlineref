<?php
session_start();
require_once "global.php";
require_once DIR_INCLUDES."/db_connection.php";
require_once DIR_COMMON."/common_functions.php";
$user_id = intval($_SESSION['user_id']);
$logintime =  $_SESSION['logintime'];



unset($_SESSION['SES_USER']);
unset($_SESSION['SES_ID']);
unset($_SESSION['auth']);

/**
 * Delete cookies - the time must be in the past,
 * so just negate what you added when creating the
 * cookie.
 */
//if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookpass'])){
   //setcookie("cookname", "", time()-60*60*24*100, "/");
   //setcookie("cookpass", "", time()-60*60*24*100, "/");
//}


Redirect("index.php");
?>