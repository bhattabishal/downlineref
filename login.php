<?php
session_start();

require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
//This code is for Re-captcha
require_once "./recaptcha/recaptcha_includes.php";

//require_once "autologin.php";
//require_once "logincheck.php";

if(($_SESSION['SES_ID']) && intval($_SESSION['SES_ID'])!=0)
	{
			Redirect("index.php");

	}



//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();
$action="form";


if($_POST['cmdSubmit']==1)
{
	$errors=array();
	
	if ($_POST["recaptcha_response_field"]) 
	{
		$resp = recaptcha_check_answer ($privatekey,$_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);
	}
	
	
	if(!isset($_COOKIE['cookname']) || $_COOKIE['cookname']='' )
	{
	
	if (!$resp->is_valid)
		$errors['security']="Incorrect Security Code";
	}
	
	
	if($_POST['username']=="")
		$errors['username']="Username Is Required";
		
	if($_POST['password']=="")
		$errors['password']="Password Is Required";
		
	
		
		
	if(count($errors)<=0)
	{
		
		$sVO->username=$_POST['username'];
		$sVO->password=md5($_POST['password']);
		
		$sVO->status='1';
		
		$sVO->computer_ip=$_SERVER['REMOTE_ADDR'];
		
		if($id = $sDAO->authenticate($sVO))
		{
			$action="inserted";
			$_SESSION['SES_USER']=$_POST['username'];
			$_SESSION['SES_ID']=$id;
			$_SESSION['auth']='true';
			//if($_POST['autologin'] == 1)
//			{
//				$cookie_name = 'siteAuth';
//
//				$cookie_time = (3600 * 24 * 1); // 1 days
//				$password_hash = md5($_POST['password']); // will result in a 32 characters hash
//

//setcookie("cookpass", $password_hash, time()+ $cookie_time);
//				
//			}
			Redirect("dashboard.php");
		}
		else
		{
			$action="form";
			$data=$_POST;
			$errors['invalid']="Incorrect Username Or Password.Please Try Again";
		}
	}
	else
	{
		$data=$_POST;
	}
	
		
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Log in
        	</h1>
        </div>
         <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
         <div style="clear:both"></div>
           <span class="errormsg"><?php echo $errors['invalid']; ?></span>
        <form name="basicregistration" action="" method="post" style="margin-top:10px">
        	 <div class="input-block">
        		<div class="label"> User Name :</div>
            	<div class="input"> 
                	<input type="text" name="username" maxlength="15"  />
                    <span class="errormsg"><?php echo $errors['username']; ?></span>
                </div>
        	</div>
            <div class="input-block">
        		<div class="label">Password :</div>
            	<div class="input">
                	<input type="password" name="password" maxlength="15" />
                     <span class="errormsg"><?php echo $errors['password']; ?></span>
                </div>
        	</div>
            <!-- <div class="input-block">
        		<div class="label">Remember Me :</div>
            	<div class="input">
                	<input type="checkbox"  name="autologin" value="1" style="margin-left:10px" align="bottom" />
                </div>
        	</div>-->
            <?php
				if(!isset($_COOKIE['cookname']) || $_COOKIE['cookname']='' )
				{
			?>
           
            <div class="input-block">
        		<div class="label">Security :</div>
            	<div class="input">
               <?php 
			   echo recaptcha_get_html($publickey, $error);
						
					 ?>
                      <span class="errormsg"><?php echo $errors['security']; ?></span>
                       <div style="clear:both;"></div>
                </div>
        	</div>
            <?php
			}
			?>
           <!-- <div class="button-block">
            	<input type="submit" name="Submit" value="Submit" />
                <input type="reset" name="Cancel" value="Cancel"/>
                <input name="cmdSubmit" type="hidden" id="cmdSubmit" value="1" />
            </div>-->
            <div style="clear:both"></div>
            <div class="button-block" style="margin-top:10px">
            <div class="buttons">
    			<button type="submit" class="positive" name="Submit" value="Submit">
        			<img src="images/apply2.png" alt=""/>
        			Log In
    			</button>
            	<button type="reset" class="negative">
                	<img src="images/cross.png" alt=""/>
                     Cancel
                </button>              
                 <input name="cmdSubmit" type="hidden" id="cmdSubmit" value="1" />
            </div>
            </div>
             <div style="clear:both;"></div>
         </form>
    	<!--inner content close-->
      	</div>
       
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
