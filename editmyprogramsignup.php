<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];
$signup_id=trim(intval($_GET['sid']));
$mr=trim($_GET['t']);



//code to get the rating if promotor terminates the request
	$changepromotorstatus=true;
	$prating=getPromotorRating($user_id);
	//do not change promotor rating if
			if(isPremium($user_id) && $prating <= 0 )
			{
				$changepromotorstatus=false;
				$trat=$prating;
			}
			if($changepromotorstatus==true)
			{
				//rating
				
				if(isPremium($user_id) && $prating == 1)
				{
					$trat=$prating-1;
				}
				else
				{
					$trat=$prating-2;
				}
				
			}
//end of code to get promotor terminating rating



//terminate contract
//terminate the signup
if(isset($_POST['did']) && $_POST['did'] != "")
{
	$pVO=new program_signup_childVO();
$pDAO=new program_signup_childDAO();
	//get details of the signups
	$signupdetails=$pDAO->fetchDetails($_POST['did']);
	$signup_child=$signupdetails->child_user_id;
	$signup_id=$_POST['did'];
	$program_id=$signupdetails->program_id;
	$sts=$signupdetails->status;
	
	$pvo=new program_signupVO();
	$pdao=new program_signupDAO();
	$pvvo=new program_signup_validityVO();
								$pvdao=new program_signup_validityDAO();
								$validitydetails=$pvdao->fetchSignupDetailsByUserId($signup_child,$signup_id);
								$valid_to=$validitydetails->valid_to;
								$valid_from=$validitydetails->valid_from;
								
								
	$programdetails=$pdao->fetchDetails($program_id);
	$days=$programdetails->contract_duration;
	$spend_credits=$programdetails->total;
	$totcredits=(60/100)*$programdetails->total;
	$fee=$spend_credits-$totcredits;
	
	if($sts != 4)
	{
	if($pDAO->ActivateNdactivate($_POST['did'],$status=4))
	{
		//decrease promotor rating to 2 if condition meet
		$changepromotorstatus=true;
		//check promotor rating
		$prating=getPromotorRating($user_id);
		
		//do not change promotor rating if
			if(isPremium($user_id) && $prating <= 0 )
			{
				$changepromotorstatus=false;
			}
			if($changepromotorstatus==true)
			{
				//rating
				$rvo=new votingVO();
				$rdao=new votingDAO();
				$curDate=date("Y-m-d H:i:s");
				//promotor rating -1
				$rvo->vote_by=$signup_child;
				$rvo->vote_to=$user_id;
				$rvo->vote_type="promotor";
				$rvo->vote_value="terminate";
				$rvo->vote_for=$signup_id;
				if(isPremium($user_id) && $prating == 1)
				{
					$rvo->vote=1;
				}
				else
				{
					$rvo->vote=2;
				}
				$rvo->created_date=$curDate;
				$rdao->Insert($rvo);
			}
			//also refund the credits
			$uDAO=new user_accountDAO();
			$uVO=new user_accountVO();
			$earnedtillnow=$uDAO->fetchTotalSignupCreditsByTypeBetweenDates($signup_child,$signup_id,$type='program signup',$valid_from,$valid_to);
			$earneddays=$uDAO->fetchTotalSignupCreditsListByTypeBetweenDates($signup_child,$signup_id,$type='program signup',$valid_from,$valid_to);
			
			$uVO->user_id=$user_id;
			$uVO->txn_id=$signup_id;
			$uVO->created_date=date("Y-m-d H:i:s");
			$uVO->credit_type="program signup";
			$totalrefund=($totcredits-$earnedtillnow)+((count($earneddays)/$days)*$fee);
			$uVO->credits=$totalrefund;
			
			$uDAO->Insert($uVO);
			
	}
}
}
//end contract termination
//fetch signup details
$svo=new program_signup_childVO();
$sdao=new program_signup_childDAO();

$signupdetails=$sdao->fetchDetails($signup_id);
$program_id=$signupdetails->program_id;
$child=$signupdetails->child_user_id;

//signup program details
$pvo=new program_signupVO();
$pdao=new program_signupDAO();

$programdetails=$pdao->fetchDetails($program_id);


//parent userdetails
$suvo=new siteusersVO();
$sudao=new siteusersDAO();
$userinfo=$sudao->fetchDetails($signupdetails->parent_user_id);
$mem_name=$userinfo->username;
$mem_type=$userinfo->mem_type;





//voting starts
$votvo=new program_votingVO();
$votdao=new program_votingDAO();

$spamVotes=$votdao->fetchRatingsById($programdetails->program_name,"scam");
$trustfulVotes=$votdao->fetchRatingsById($programdetails->program_name,"trustful");
$totalVotes=count($spamVotes)+count($trustfulVotes);

if($totalVotes == "" )
{
	$totalVotes=0;
}

$scamPercentge=@((count($spamVotes)/($totalVotes))*100);

if($scamPercentge == "")
{
	$scamPercentge=0;
}

$trustfulPercentage=@((count($trustfulVotes)/($totalVotes))*100);

if($trustfulPercentage== "")
{
	$trustfulPercentage=0;
}

//voting ends


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>My Program Sign-ups</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<script type="text/javascript" src="js/ajaxVote.js"></script>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				My Program Sign-ups
        	</h1>
        </div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
        	<div class="container-table">
                    <div class="wrap-table"> 
                    	<table cellspacing="0" cellpadding="0" border="0" width="610" >
                            <tbody>
                                <tr>
                                    <td class="r_name no_line">&nbsp;</td>
                                     <td class="r_name no_line" colspan="2">Contract details</td>
                                     
                                </tr>
                                <tr>
                                	<td rowspan="11" class="c_left" style="width:auto; padding:0px!important; float:none" align="center">
                                     <div class="graph">
                                        	<table class="small" cellspacing="0" border="0" style="border:none; float:none">
<tbody>
<tr>
<td colspan="2" rowspan="3" style="border:none; padding:15px 0 0px 0!important">
<img width="20" height="8" src="images/graph_bar_gray.png">
<img width="20" height="17" src="images/graph_bar_gray.png">
<img width="20" height="25" src="images/graph_bar_gray.png">
<img width="20" height="33" src="images/graph_bar_gray.png">
<img width="20" height="42" src="images/graph_bar_gray.png">
<img width="20" height="50" src="images/graph_bar_gray.png">
<img width="20" height="58" src="images/graph_bar_gray.png">
<img width="20" height="67" src="images/graph_bar_gray.png">
<img width="20" height="75" src="images/graph_bar_gray.png">

</td>
<td valign="top" height="25%" style="border:none; padding:2px!important"><?php echo formatDoubleAmount((60/100)*$programdetails->total); ?></td>
</tr>
<tr>
<td height="50%" style="border:none; padding:2px!important">credits</td>
</tr>
<tr>
<td height="25%" style="border:none; padding:2px!important">&nbsp;</td>
</tr>
<!--<tr>
<td colspan="2" style="border:none; padding:0px!important">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
<img width="20" height="10" src="images/graph_bar_gray_mirror.png">
</td>
</tr>-->
<tr>
<td style="border:none; padding:0px!important">1</td>
<td align="right" style="border:none; padding:0px!important"><?php  echo $programdetails->contract_duration;?></td>
<td style="border:none; padding:0px!important"></td>
</tr>
</tbody>
</table>
<br>
                                        </div><!--graph ends-->
                                    	
                                    	<!--voting starts-->
                                        	<div class="rightdiv" id="rightdiv" style="width:200px;">
                        	
                        	<div style="background-color:#6C955C; padding:5px 5px 10px 5px">
                            	<div style="padding:5px"><b>PTC SCAM Meter</b></div>
                                <div style="text-align:center; background-color:#FFFFFF; padding:5px">
                                	<div id="programname" style="font-weight:bold">
                                    	<?php echo $programdetails->program_name; ?>
                                    </div>
                                	<img src="images/chart.png" />
                                    <div>
                                    	<font color="#FF0000"><b><?php echo $scamPercentge; ?>%</b> Scam</font> | <b><?php echo $trustfulPercentage; ?>%</b> Trustful<br />
                                       <font color="#999999" size="1"> (Results Based On <?php echo $totalVotes; ?> Votes)</font>
                                    </div>
                                    <div style="margin-top:5px">
                                    Want To Vote Yourself ?<br />
                                    <input type="button" value="Scam" style="color:#FF0000" onclick="vote('scam','<?php  echo $user_id;?>','<?php  echo $programdetails->program_name;?>');" />
                                     <input type="button" value="Trustful" style="color:#009900" onclick="vote('trustful','<?php  echo $user_id;?>','<?php  echo $programdetails->program_name;?>');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div style="clear:both"></div>
                                    </td>
                                    <td>
                                    	Promotor
                                    </td>
                                    <td>
                                    	<?php echo $mem_name; ?>
										<?php
                                            if($mem_type==1)
                                            {
                                                echo '<img src="images/premium.png" align="bottom" alt="Premium Member" title="Premium Member" />';
                                                
                                            }
                                            else
                                            {
                                                echo '<img src="images/free.png" align="bottom" alt="Standard Member" title="Standard Member" />';
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td>Program Name</td>
                                    <td><?php  echo $programdetails->program_name;?></td>
                                </tr>
                                <tr>
                                	<td>Signup Date</td>
                                    <td><?php echo $signupdetails->created_date; ?></td>
                                </tr>
                                <tr>
                                	<td>Contract Status</td>
                                    <td>
                                    <?php
									//earned till now
									$avo=new user_accountVO();
									$adao=new user_accountDAO();
								
								
								
								
								$pvvo=new program_signup_validityVO();
								$pvdao=new program_signup_validityDAO();
								$validitydetails=$pvdao->fetchSignupDetailsByUserId($child,$signupdetails->id);
								$valid_to=$validitydetails->valid_to;
								$valid_from=$validitydetails->valid_from;
								
								$earnedtillnow=$adao->fetchTotalSignupCreditsByTypeBetweenDates($child,$signup_id,$type='program signup',$valid_from,$valid_to);
									//end earned till now	
									
							switch($signupdetails->status)
							{
								case 0:
								$status="Request Pending";
								break;
								
								case 1://means accepted.now calculate the time left
								//now for earned credits of program signup
								$avo=new user_accountVO();
								$adao=new user_accountDAO();
								
								
								
								
								$pvvo=new program_signup_validityVO();
								$pvdao=new program_signup_validityDAO();
								$validitydetails=$pvdao->fetchSignupDetailsByUserId($child,$signupdetails->id);
								$valid_to=$validitydetails->valid_to;
								$valid_from=$validitydetails->valid_from;
								
								$earnedtillnow=$adao->fetchTotalSignupCreditsByTypeBetweenDates($child,$signup_id,$type='program signup',$valid_from,$valid_to);
								
								$dateParts=dateParseFromFormat("Y-m-d H:i:s", $valid_to);
								
								$CountDown = new CountDown;
								
								$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);
								
								
								$CountDown->format = "Weeks: {W} Days: {D} Hours: {H}";
								
								
								if($out=='0')
								{
									//changeProgramSignUpStatus($signupdetails->id,'3');
									$status="completed";
								}
								else
								{
								
								
									$status="in progress";
								}
								
								break;
								
								case 2:
								$status="declined";
								break;
								
								case 3:
								$status="completed";
								break;
								
								case 4:
								$status="terminated";
								break;
								
								default:
								$status="n/a";
								
							}
							
							echo $status;
						?>
                                    </td>
                                </tr>
                                <tr>
                                	<td>Contract Duration</td>
                                    <td><?php  echo $programdetails->contract_duration;?> days</td>
                                </tr>
                                <tr>
                                	<td>Contract End Date</td>
                                    <td><?php if($valid_to==""){ echo "n/a";}else{ echo $valid_to;} ?></td>
                                </tr>
                                <tr>
                                	<td>Task Description</td>
                                    <td>
                                    <textarea readonly="readonly"><?php echo $programdetails->task_desc; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="r_name">Earnings</td>
                                </tr>
                                <tr>
                                	<td>Earned</td>
                                    <td>
                                    <?php
									echo formatDoubleAmount($earnedtillnow);
									?>
                                     out of <?php echo formatDoubleAmount((60/100)*$programdetails->total); ?> Credits</td>
                                  <!--  fetchTotalSignupCreditsByTypeBetweenDates($user_id,$txnid,$type='program signup',$from,$to)-->
                                </tr>
                                <tr>
                                	<td>Reserved</td>
                                    <td><?php
									if($status=="in progress")
									{
									 echo formatDoubleAmount((60/100)*$programdetails->total-$earnedtillnow); 
									}
									else
									{
										echo "0.00";
									}
									 ?> Credits</td>
                                </tr>
                                <tr>
                                	<td>Total</td>
                                    <td><?php echo formatDoubleAmount((60/100)*$programdetails->total); ?> Credits</td>
                                </tr>
                                <?php
									if($signupdetails->status== 1 && (isset($mr) && $mr !=""))
									{
								?>
                              <tr>
                                    <td colspan="3" class="r_name">
                                    Terminate Contract
                                    <form name="terminate" method="post" action="">
                                    <div style="font-size:11px; font-weight:normal">
                                    	Only if your referral is not active you can terminate this contract.Remaining credits will be refunded to your account.Contract fees are not refunded<br />
                                       <span style="color:#FF0000; font-style:italic">important</span> Your promotor rating will be reverted and subsequently decreased by 1
                                    </div>
                                   <div class="buttons" style=" margin-left:170px" >
                                    <button type="submit" class="regular" onclick="return confirm('Make Sure Before You Terminate This Contract !')"><img src="images/terminate_all_co.png" alt="terminate" /><span style="color:#FF0000;">Terminate contract <br /> your new promotor rating will be <?php if($trat < 0){ echo $trat;}else{echo "+".$trat;} ?></span></button>
                                     </div>
                               <div style="clear:both"></div>
                               <input type="hidden" name="did" value="<?php echo $signup_id; ?>" />
                               </form>
                                    </td>
                                </tr>
                                <?php
								}
								?>
                                <!--  <tr>
                                    <td class="c_left">Videos</td>
                                    <td width="520">MP4, MPEG-4, H.264, HD H.264</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Audios</td>
                                    <td width="520">MP3, M4A, AAC</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Images</td>
                                    <td width="520">BMP, GIF, JPEG, PNG</td>
                                </tr>-->
                            </tbody>
                        </table> 
                        		
                        </div>
                </div>
           <!--table end-->
        	 <div class="buttons" style="float:right">
           
                                        <button type="button" class="regular" name="Back" value="Back" onclick="location.href='<?php if(isset($mr) && $mr !=""){ echo "manageReferral.php";}else{ echo "myProgramSignUp.php"; } ?>'" ><img src="images/arrow_back.png" alt=""/>Back</button>
                                   
                               </div>
                               <div style="clear:both"></div>
                              
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
