<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];




//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();
$user_details=$sDAO->fetchDetails($user_id);
$action="form";


if($_POST['cmdSubmit']==1)
{
	$errors=array();
		
	if($user_details->username=="")
		$errors['user_name']="User Name Is Required";
		
	if($_POST['first_name']=="")
		$errors['first_name']="First Name Is Required";
		
	if($_POST['last_name']=="")
		$errors['last_name']="Last Name Is Required";
	
		
	if($_POST['oldpassword']=="")
		$errors['oldpassword']="Old Password Is Required";
		
	if($_POST['newpassword']=="")
		$errors['newpassword']="New Password Is Required";
		
	if($_POST['oldpassword']!="")
	{
		
		if($user_details->password!=$_POST['oldpassword'])
		$errors['oldpassword']="Old Password Do Not Match";
	}
		
	if($_POST['confirmpassword']=="")
		$errors['confirmpassword']="Confirm New Password Is Required";
		
	if($_POST['confirmpassword']!="")
		{
			if($_POST['confirmpassword']!=$_POST['newpassword'])
				$errors['confirmpassword']="Password Do Not Match";
		}
		
	if($_POST['email']=="" || $_POST['email']!=ValidateEmail($_POST['email']))
		$errors['email']="Valid Email Is Required";
		
	if($_POST['referral_url']=="")
		$errors['referral_url']="Required";
		
		
	if(count($errors)<=0)
	{
		$sVO->user_id=$user_id;
		$sVO->first_name=$_POST['first_name'];
		$sVO->last_name=$_POST['last_name'];
		$sVO->username=$user_details->username;
		$sVO->password=$_POST['newpassword'];
		$sVO->email=$_POST['email'];
		$sVO->country=$user_details->country;
		$sVO->gender=$user_details->gender;;
		$sVO->referral_url=$_POST['referral_url'];
		$sVO->joined_date=$user_details->joined_date;;
		$sVO->status=$user_details->status;
		$sVO->mem_type=$user_details->mem_type;
		$sVO->computer_ip=$user_details->computer_ip;
		
		if($sDAO->update($sVO))
		{
			$action="updated";
			$user_details=$sDAO->fetchDetails($user_id);
		}
		else
		{
			$action="error";
			$data=$_POST;
			
		}
	}
	else
	{
		$data=$_POST;
	}
	
		
}




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Account Statistics</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Account Statistics And Overview
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
         
            <div class="message-block">
            	<?php if($action=="updated"){  ?>
                Your Account Details Have Been Successfully Updated
                <?php }elseif($action=="error"){ ?>
                <font color="#FF0000">Some Error Occured.Please Try Again</font>
                <?php } ?>
                
            </div>
        	<div class="container-table">
                    <div class="wrap-table"> 
                   
                    	<form name="editAccount" method="post" action="">
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="r_name no_line">Your Account Details</td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td>
										<?php echo $user_details->username; ?>
                                         <span class="errormsg"><?php echo $errors['user_name']; ?></span>
                                    </td>
                                </tr>
                                 <tr>
                                   <td>First Name</td>
                                   <td>
                                   	<input type="text" name="first_name" value="<?php echo $user_details->first_name; ?>" style="width:250px"/>
                                    <span class="errormsg"><?php echo $errors['first_name']; ?></span>
                                    </td>
                                </tr>
                                 <tr>
                                   <td>Last Name</td>
                                   <td><input type="text" name="last_name" value="<?php echo $user_details->last_name; ?>" style="width:250px"/> <span class="errormsg"><?php echo $errors['last_name']; ?></span></td>
                                </tr>
                                 <tr>
                                   <td>Email Address</td>
                                   <td><input type="text" name="email" value="<?php echo $user_details->email; ?>" style="width:250px"/> <span class="errormsg"><?php echo $errors['email']; ?></span></td>
                                </tr>
                                 <tr>
                                   <td>Old Password</td>
                                   <td>
                                   	<input type="password" name="oldpassword" style="width:250px"/>
                                     <span class="errormsg"><?php echo $errors['oldpassword']; ?></span>
                                   </td>
                                </tr>
                                <tr>
                                   <td>New Password</td>
                                   <td>
                                   		<input name="newpassword" type="password" style="width:250px"/>
                                         <span class="errormsg"><?php echo $errors['newpassword']; ?></span>
                                  	</td>
                                </tr>
                                 
                                 <tr>
                                   <td>Confirm New Password</td>
                                   <td>
                                   		<input name="confirmpassword" type="password" style="width:250px" />
                                         <span class="errormsg"><?php echo $errors['confirmpassword']; ?></span>
                                   </td>
                                </tr>
                                 <tr>
                                   <td>Your Upline</td>
                                   <td>Downlinerefs.com</td>
                                </tr>
                                <tr>
                                	<td colspan="2" class="r_name">Referral earnings</td>
                                </tr>
                                 <tr>
                                   <td>Your Referral URL</td>
                                   <td><input type="text" name="referral_url" value="<?php echo $user_details->referral_url; ?>" style="width:350px"   readonly="readonly"/> <span class="errormsg"><?php echo $errors['referral_url']; ?></span></td>
                                </tr>
                                 <tr>
                                   <td>Downline Earnings</td>
                                   <td>10% - 10% - 10% - 10% - 10%</td>
                                </tr>
                                <tr>
                                    <td  class="r_name">
                                     <div class="buttons">
                                    	<button  id="submitbutton" class="positive" name="Submit" value="Submit" onclick="doSubmit(this.form);"><img src="images/apply2.png" alt=""/>Save changes</button>
                                        </div>
                                    </td>
                                    <td  class="r_name">
                                    	<div class="buttons">
                                    		<button type="button" class="regular" name="Back" value="Back" onclick="location.href='accountdetails.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
                                        </div>
                                    </td>
                                </tr>
                                  
                
            </div>
                               
                            </tbody>
                        </table>
                         <input name="cmdSubmit" type="hidden" id="cmdSubmit" value="1" />
                        </form>
                        </div>
                </div>
           <!--table end-->
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
         
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
