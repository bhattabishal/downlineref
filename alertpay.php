<?php
/*
  $Id: alertpay.php,v 1 2006/05/08

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License

*/


  class alertpay {
    var $code, $title, $description, $enabled;

// class constructor
    function alertpay() {
      $this->code = 'alertpay';
      $this->title = MODULE_PAYMENT_ALERTPAY_TEXT_TITLE;
      $this->description = MODULE_PAYMENT_ALERTPAY_TEXT_DESCRIPTION;
      $this->sort_order = MODULE_PAYMENT_ALERTPAY_SORT_ORDER;
      $this->enabled = ((MODULE_PAYMENT_ALERTPAY_STATUS == 'True') ? true : false);

      if ((int)MODULE_PAYMENT_ALERTPAY_ORDER_STATUS_ID > 0) {
        $this->order_status = MODULE_PAYMENT_ALERTPAY_ORDER_STATUS_ID;
      }
      $this->form_action_url = 'https://www.alertpay.com/PayProcess.aspx';
    }

// class methods
    function javascript_validation() {
      return false;
    }

    function selection() {
      return array('id' => $this->code,
                   'module' => $this->title);
    }

    function pre_confirmation_check() {
      return false;
    }

    function confirmation() {
      return false;
    }

    function process_button() {
      global $order, $currencies;

      //alertpay accepted currency
      $alert_cur = array('CAD', 'EUR', 'GBP', 'USD', 'AUD');
      $CUR = $order->info['currency'];
      if (!in_array($CUR,$alert_cur)) {
        $CUR = 'USD';
      }
      
	   //to show complete list of product on alertpay payment gateway page
	   for($i=0;$i<count($order->products);$i++)
		{
			if($i > 0) $postfix = "_".$i;
			
			$hiddenBox .= tep_draw_hidden_field('ap_itemname'.$postfix, $order->products[$i]['name']) .
			tep_draw_hidden_field('ap_amount'.$postfix, $order->products[$i]['price']) .
			tep_draw_hidden_field('ap_quantity'.$postfix, $order->products[$i]['qty']);
		}
    
       $process_button_string = tep_draw_hidden_field('ap_merchant', MODULE_PAYMENT_ALERTPAY_ACCOUNT) .
                               //tep_draw_hidden_field('ap_amount', number_format(($order->info['total']) * $currencies->currencies[$CUR]['value'], $currencies->currencies[$CUR]['decimal_places'])) .
                               tep_draw_hidden_field('ap_currency', $CUR) .
                               tep_draw_hidden_field('ap_purchasetype', 'Item') .
							   
							   $hiddenBox .
							   
							   //pass shipping information to alertpay payment gateway
							  // updated by parorrey 2011/03/03
					 tep_draw_hidden_field('ap_shippingcharges', number_format($order->info['shipping_cost'], 2, '.', '')) .
							   
                               //tep_draw_hidden_field('ap_itemname', MODULE_PAYMENT_ALERTPAY_PRODUCT_TYPE) .
                               tep_draw_hidden_field('ap_returnurl', tep_href_link(FILENAME_CHECKOUT_PROCESS, '', 'SSL')) .
                               tep_draw_hidden_field('ap_cancelurl', tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));

      return $process_button_string;
    }

    function before_process() {
      return false;
    }

    function after_process() {
      return false;
    }

    function output_error() {
      return false;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_ALERTPAY_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable AlertPay Module', 'MODULE_PAYMENT_ALERTPAY_STATUS', 'True', 'Do you want to accept AlertPay payments?', '6', '3', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order of display.', 'MODULE_PAYMENT_ALERTPAY_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('General product description.', 'MODULE_PAYMENT_ALERTPAY_PRODUCT_TYPE', '', 'Needed to complete the AlertPay payment process.', '6', '5', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('AlertPay Account Number', 'MODULE_PAYMENT_ALERTPAY_ACCOUNT', 'sales@yourshop.com', 'Your AlertPay account number to which the payment is to be made.', '6', '4', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Set Order Status', 'MODULE_PAYMENT_ALERTPAY_ORDER_STATUS_ID', '0', 'Set the status of orders made with this payment module to this value', '6', '0', 'tep_cfg_pull_down_order_statuses(', 'tep_get_order_status_name', now())");
    }

/*    function remove() {
      $keys = '';
      $keys_array = $this->keys();
      for ($i=0; $i<sizeof($keys_array); $i++) {
        $keys .= "'" . $keys_array[$i] . "',";
      }
      $keys = substr($keys, 0, -1);

      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in (" . $keys . ")");
    }
*/
    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_PAYMENT_ALERTPAY_STATUS', 'MODULE_PAYMENT_ALERTPAY_SORT_ORDER', 'MODULE_PAYMENT_ALERTPAY_PRODUCT_TYPE', 'MODULE_PAYMENT_ALERTPAY_ACCOUNT', 'MODULE_PAYMENT_ALERTPAY_ORDER_STATUS_ID');
    }
  }
?>
