<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";

require_once "alertpay.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];

//check if active if not active change status
checkMembershipRemainingTime($user_id);

$cvo=new code_valueVO();
$cdao=new code_valueDAO();

$alertpay_email=getCodeValueByLabel("alertpay_email");
$website_url=getCodeValueByLabel("website_url");

$referralReqDiscount=getCodeValueByLabel("premium_referral_request_discount");
$standardUserPromotablePrograms=getCodeValueByLabel("standard_user_promotable_programs");
$creditPurchageBonus=getCodeValueByLabel("premium_credit_purchase_bonus");

$premiumSixMonthPrice=getCodeValueByLabel("premium_sixmonth");
$premiumSixMonthDiscountPrice=getCodeValueByLabel("premium_sixmonth_discount");
if($premiumSixMonthDiscountPrice == "" || $premiumSixMonthDiscountPrice == 0)
	$premiumSixMonthDiscountPrice=$premiumSixMonthPrice;
$premiumOneYearPrice=getCodeValueByLabel("premium_oneyear");
$premiumOneYearDiscountPrice=getCodeValueByLabel("premium_oneyear_discount");
if($premiumOneYearDiscountPrice == "" || $premiumOneYearDiscountPrice == 0)
	$premiumOneYearDiscountPrice=$premiumOneYearPrice;

$perMonthOfSixMonth=formatDoubleAmount($premiumSixMonthDiscountPrice/6);
$perMonthOfOneYear=formatDoubleAmount($premiumOneYearDiscountPrice/12);




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Premium Membership</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<script type="text/javascript">
function dopurchase(package,price,type)
{
//alert("hi");
	if(type==6)
	{
		var label="Six Month Premium Membership";
	}
	if(type==12)
	{
		var label="One Year Premium Membership";
	}
	if(package=='paypal')
         {
		 	document.paypal.amount.value=price;
			document.paypal.item_name.value=label;
			document.paypal.type.value=type;
          	document.paypal.submit();
		 }
		 else if(package=='alertpay')
		 {
		 	document.alertpay.ap_amount.value=price;
			document.alertpay.ap_itemname.value=label;
			document.alertpay.type.value=type;
			document.alertpay.action.value='yes';
		 	document.alertpay.submit();
		 }
	

}
</script>
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
</head>
<body>
<form method="post" action="premium_alertpay/process.php" name="alertpay" > 
    <input type="hidden" name="ap_purchasetype" value="item-auction"/> 
    <input type="hidden" name="ap_merchant" value="<?php echo $alertpay_email; ?>"/> 
    <input type="hidden" name="ap_itemname" value="membership"/> 
    <input type="hidden" name="ap_currency" value="USD"/> 
    <input type="hidden" name="ap_returnurl" value="<?php echo $website_url; ?>/premiumAlertpaySuccessful.php"/> 
    <input type="hidden" name="ap_itemcode" value="1"/> 
    <input type="hidden" name="ap_quantity" value="1"/> 
    <input type="hidden" name="ap_description" value="Premium Membership"/> 
    <input type="hidden" name="ap_amount" value="5"/> 
    <input type="hidden" name="ap_taxamount" value="0"/> 
    <input type="hidden" name="ap_shippingcharges" value="0"/> 
    <input type="hidden" name="ap_cancelurl" value="<?php echo $url->code_value; ?>/premiumAlertpayCancelled.php"/> 
     <input type="hidden" name="type" value="0">
      <input type="hidden" name="action" value="no">
   
    </form>
  
    <form method="post" action="premium_paypal/process.php" name="paypal">
        <input type="hidden" name="amount" value="1">
        <input type="hidden" name="item_name" value="1">
        <input type="hidden" name="type" value="0">
       
        
		
        <!--<input type="submit" value=" Pay ">-->
    </form>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Premium Membership
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
         <div id="form-container">
            <header id="form-header" class="info" style="border:none; padding:10px; font-style:normal; font-weight:normal;">
                    	<h2 style="font-style:normal;font-size:110%">Being a Premium Member has certain privileges in our community. We offer Premium Memberships for only $<?php echo $perMonthOfOneYear; ?> USD per month. The list below shows the differences between a Standard- and a Premium Membership. Get the most out of your membership and become Premium today! We have limited offers available.


						</h2>
                    	<div></div>
                	</header>
            </div>
            <?php
		if(isset($_GET['action']))
		{
		?>
        <div class="sucess-div" id="sucess-div"  style="padding:10px; margin:15px 15px 0px 15px; background-color:#000033; color:#FFFFFF">
        <?php 
		if($_GET['action']=="success")
		{
		?>
        	<b>Thank you! Your order has been successfully processed.</b>
         <?php
         }
		 elseif($_GET['action']=="faliure")
		 {
		 	?>
            	<b><font color="#FF0000">We're sorry, your credit card has been declined.</font></b>
            <?php
		 }
		?>
        </div>
        <?php
		}
		?>
        	<div class="container-table">
                    <div class="wrap-table"> 
                   
                    	
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                    <td class="r_name no_line">&nbsp;</td>
                                     <td class="r_name no_line">Standard</td>
                                      <td class="r_name no_line">Premium</td>
                                       <td class="r_name no_line">Explanation</td>
                                </tr>
                               
                                <tr>
                                	<td>Referral Requests listed first</td>
                                    <td><font style="font-weight:bold;" color="darkred">No</font></td>
                                    <td><font style="font-weight:bold;" color="darkgreen">Yes</font></td>
                                    <td class="small disabled">Referral requests of Premium Members will be listed first</td>
                               </tr>
      							<tr>
                                	<td>Discount on Referral Requests</td>
                                    <td><font style="font-weight:bold;" color="darkred">No</font></td>
                                    <td><font color="darkgreen"><b>Yes, <?php echo $referralReqDiscount; ?>%</b></font></td>
                                    <td class="small disabled">Example: spend 100 credits, we charge you 
									<?php echo 100-(100*$referralReqDiscount)/100; ?> credits only</td>
                                </tr>
      							<tr>
                                	<td>Promotable programs</td>
                                    <td><?php echo $standardUserPromotablePrograms; ?></td>
                                    <td><font color="darkgreen"><b>Unlimited</b></font></td>
                                    <td class="small disabled">Premium Members can promote an unlimited number of programs using Referral Requests</td>
                               </tr>

      							<tr>
                                	<td>Prevent members from signing up</td>
                                    <td><font style="font-weight:bold;" color="darkred">No</font></td>
                                    <td><font color="darkgreen"><b>Yes</b></font></td><td class="small disabled">Block specific members to prevent them from signing up to your programs</td>
                               </tr>
      						<tr>
                            	<td>Bonus on credit purchase</td>
                                <td><font style="font-weight:bold;" color="darkred">No</font></td>
                                <td><font color="darkgreen"><b><font style="font-weight:bold;" color="darkgreen">
								<?php 
									if($creditPurchageBonus == "" || $creditPurchageBonus ==0 ) 
									{
										echo "No";
									}
									else
									{
										echo "Yes";
									}
								?></font>, 
								<?php
								 echo $creditPurchageBonus; ?>%</b></font></td>
                                <td class="small disabled">Example: buy 800 credits, receive <?php echo 800 + (800*$creditPurchageBonus)/100;  ?> credits</td>
                           </tr>
      						<tr>
                            	<td>Random referrals</td>
                                <td><font style="font-weight:bold;" color="darkred">No</font></td>
                                <td><font style="font-weight:bold;" color="darkgreen">Yes</font></td>
                                <td class="small disabled">Unreferred DownlineRefs.com members will be distributed among Premium Members</td>
                          </tr>

     					 <tr>
                         	<td>Secure your ratings</td>
                            <td><font style="font-weight:bold;" color="darkred">No</font></td>
                            <td><font style="font-weight:bold;" color="darkgreen">Yes</font></td>
                            <td class="small disabled">Premium Members will always have a promotor rating of +0 or higher</td>
                        </tr>
                                  
                
            </div>
                               
                            </tbody>
                        </table>
                         
                        </form>
                        </div>
                </div>
           <!--table end-->
           
			
				
          <div style="clear:both"></div>
          <?php if(!isPremium($user_id)){ ?>
          <!--pay start-->
          <div class="preminumpay" style=" padding-left:100px; text-align:center">
                 <div style="float:left; width:177px; height:190px; background-color:#d2e6ca; padding:10px; margin-bottom:10px; margin-right:10px">
					<div style="text-align:center"><b>Premium Membership</b></div>
					<div style="text-align:center"><b>6 months</b></div>
					<div style="text-align:center">Offer: from <span style="text-decoration:line-through; color:#FF0000">$ <?php echo $premiumSixMonthPrice; ?> USD</span> for</div>
                    <div style="text-align:center"><b>$ <?php echo $premiumSixMonthDiscountPrice; ?> USD</b></div>
                    <div style="text-align:center"><img src="images/paypal1.gif"  title='PayPal' style="margin:4px 0" onclick='dopurchase("paypal",<?php echo $premiumSixMonthDiscountPrice; ?>,6);;' /></div>
                     <div style="text-align:center"><img src="images/big_pay_01.gif" title='AlertPay' style="margin-bottom:4px"  onclick='dopurchase("alertpay",<?php echo $premiumSixMonthDiscountPrice; ?>,6);;'  /></div>
					<div style="text-align:center">= $<?php echo $perMonthOfSixMonth; ?> USD per month</div>
				 </div>	
                
                  <div style="float:left; width:177px; height:190px; background-color:#d2e6ca; padding:10px; margin-bottom:10px; margin-right:10px">
					<div style="text-align:center"><b>Premium Membership</b></div>
					<div style="text-align:center"><b>1 year</b></div>
					<div style="text-align:center">Offer: from <span style="text-decoration:line-through; color:#FF0000">$ <?php echo $premiumOneYearPrice; ?>  USD</span> for</div>
                    <div style="text-align:center"><b>$ <?php echo $premiumOneYearDiscountPrice; ?> USD</b></div>
                    <div style="text-align:center"><img src="images/paypal1.gif"  title='PayPal' style="margin:4px 0" onclick='dopurchase("paypal",<?php echo $premiumOneYearDiscountPrice; ?>,12);;' /></div>
                     <div style="text-align:center"><img src="images/big_pay_01.gif" title='AlertPay' style="margin-bottom:4px"  onclick='dopurchase("alertpay",<?php echo $premiumOneYearDiscountPrice; ?>,12);;'  /></div>
					<div style="text-align:center">= $<?php echo $perMonthOfOneYear; ?> USD per month</div>
				 </div>	
                  <div style="clear:both"></div>
               </div>
               <span style="text-align:center; display:block">
               	Note: Offers are valid until stated otherwise. All purchases will be processed instantly.
               </span>
               <!--pay close-->
               <?php } ?>
    	<!--inner content close-->
      	</div>
         
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
