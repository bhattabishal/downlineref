<?php
session_start();
include_once("common/functions.php");
include_once("common/common_functions.php");
//include_once("common/class.amh_iri.php");
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";


$name=trim($_GET['name']);
//fetch all record of this member
$suvo=new siteusersVO();
$sudao=new siteusersDAO();
$rdao=new votingDAO();

$userdetails=$sudao->fetchDetailsByName($name);

$user_id=$userdetails->user_id;

//fetch all promotor ratings of that user
$promotorratings=$rdao->fetchAllRatingsById($user_id,$vote_type='promotor');

//fetch all referral ratings of that user
$referralratings=$rdao->fetchAllRatingsById($user_id,$vote_type='referral')

?>
<div>

            	<div style="font-size:14px; font-weight:bold; padding:5px">Ratings for member <?php echo $name; ?></div>
                <div >
                	<div style="font-size:14px; font-weight:bold; padding:5px">Promotor rating</div>
                    <div style="padding:5px">The table below shows all validation decisions of member <b><?php echo $name; ?></b> when someone joined his/her program.</div>
                    <table style="width:100%">
                    	<tr>
                        	<th style="background-color:#006600; color:#FFFFFF; padding:5px">Date</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Opposing member</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Opposing member rating</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Validation decision</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Adjustment</th>
                        </tr>
                        <?php foreach($promotorratings as $value){ 
                        switch($value->vote_value)
                        {
							case "accept":
							$sign="+";
							break;
							
							case "decline":
								$sign="-";
								break;
								
							case "terminate":
								$sign="-";
								break;
								
							default:
								$sign="+";
								break;
                        }
						
						$oppmemname=$sudao->fetchDetails($value->vote_by);
						$mem_type=$oppmemname->mem_type;
						?>
                        <tr>
                        	<td style=" padding:5px" align="center"><?php echo $value->created_date; ?></td>
                            <td style=" padding:5px" align="center"><?php echo $oppmemname->username;
							if($mem_type==1)
				{
					echo '<img src="images/premium.png" align="top" alt="Premium Member" title="Premium Member" />';
					
				}
				else
				{
					echo '<img src="images/free.png" align="top" alt="Standard Member" title="Standard Member" />';
				}
							 ?></td>
                            <td style=" padding:5px" align="center"><?php if(getReferralRating($value->vote_by) < 0){ echo getReferralRating($value->vote_by);} else { echo "+".getReferralRating($value->vote_by); } ?></td>
                            <td style=" padding:5px" align="center"><?php echo $value->vote_value; ?></td>
                            <td style=" padding:5px" align="center"><?php echo $sign." ".$value->vote; ?></td>
                        </tr>
                        
                       
                        <?php } ?>
                         <tr >
                        	<td colspan="4" align="right" style="font-size:14px; font-weight:bold;background-color:#009900; padding:5px; color:#FFFFFF">Total rating</td>
                            <td style="background-color:#009900; padding:5px; color:#FFFFFF">
                             
 								 <b><?php if(getPromotorRating($user_id) < 0){ echo getPromotorRating($user_id);} else { echo "+".getPromotorRating($user_id); } ?></b> (promotor rating)
                            </td>
                        </tr>
                    </table>
                </div>
                
                <!--start referral rating-->
                	<div style="margin:20px 0 10px 0" >
                	<div style="font-size:14px; font-weight:bold; padding:5px">Referral rating</div>
                    <div style="padding:5px">The table below shows all validation decisions of promotors when member <b><?php echo $name; ?></b> has joined a program.</div>
                    <table style="width:100%">
                    	<tr>
                        	<th style="background-color:#006600; color:#FFFFFF; padding:5px">Date</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Opposing member</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Opposing member rating</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Validation decision</th>
                            <th style="background-color:#006600; color:#FFFFFF; padding:5px">Adjustment</th>
                        </tr>
                        <?php foreach($referralratings as $value) { 
						switch($value->vote_value)
                        {
							case "accept":
							$sign="+";
							break;
							
							case "decline":
								$sign="-";
								break;
								
							case "terminate":
								$sign="-";
								break;
								
							default:
								$sign="+";
								break;
                        }
						$oppmemname=$sudao->fetchDetails($value->vote_by);
						$mem_type=$oppmemname->mem_type;
						?>
                        <tr>
                        	<td style=" padding:5px" align="center"><?php echo $value->created_date; ?></td>
                            <td style=" padding:5px" align="center"><?php  echo $oppmemname->username;
							if($mem_type==1)
				{
					echo '<img src="images/premium.png" align="top" alt="Premium Member" title="Premium Member" />';
					
				}
				else
				{
					echo '<img src="images/free.png" align="top" alt="Standard Member" title="Standard Member" />';
				}
							 ?></td>
                            <td style=" padding:5px" align="center"><?php if(getPromotorRating($value->vote_by) < 0){ echo getPromotorRating($value->vote_by);} else { echo "+".getPromotorRating($value->vote_by); } ?></td>
                            <td style=" padding:5px" align="center"><?php echo $value->vote_value; ?></td>
                            <td style=" padding:5px" align="center"><?php echo $sign." ".$value->vote; ?></td>
                        </tr>
                        
                        <?php } ?>
                        <tr >
                        	<td colspan="4" align="right" style="font-size:14px; font-weight:bold;background-color:#009900; padding:5px; color:#FFFFFF">Total rating</td>
                            <td style="background-color:#009900; padding:5px; color:#FFFFFF">
                             
 								<b><?php if(getReferralRating($value->vote_to) < 0){ echo getReferralRating($value->vote_to);}else{ echo "+".getReferralRating($value->vote_to);} ?></b> (referral rating)
                            </td>
                        </tr>
                    </table>
                </div>
                <!--end referral rating-->
            </div>