<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_INCLUDES."/menuids.php";

$mid=trim($_GET['id']);
$sid=trim($_GET['sid']);

if(!isset($sid))
{
	$sid=0;
}



$cvo=new contentVO();
$cdao=new contentDAO();

$contents=$cdao->fetchByMenuIds($mid, $sid);

$home=$contents->detail_desc_en;
$home_heading=$contents->heading;

//$page_title=$contents->page_title;
$page_title="Downlineref:".$home_heading;

$page_metatag=$contents->page_metatag;
$page_keywords=$contents->page_keywords;
$page_description=$contents->page_description;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $page_title;?></title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<style type="text/css">
.content li
{
	margin:0 30px;
	padding:5px;
}
</style>
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  <?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  <?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
 	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1><?php echo $home_heading; ?></h1>
        </div>
        <div style="clear:both;"> </div>
        <!--inner content start-->
        <div class="content" style="margin:15px; min-height:635px">
        <!-- Ttle (End) -->
      <!-- Block_a (Start) -->
        <!-- ======== S T A R T   C O N T E N T ======== -->
		<?php echo $home; ?>
<!-- ======== E N D   C O N T E N T ======== -->
      <!-- Block_a (End) -->
        
      </div>
     <div style="clear:both;"> </div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
   <?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   <?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
