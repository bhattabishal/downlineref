<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_INCLUDES."/menuids.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Help & Support</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<style type="text/css">
	a:link,a:visited
	{
		color:#676052;
	}
	.fakea
	{
		cursor:pointer;
	}
	
</style>
<script type="text/javascript">
	function gototop()
	{
		location.href="#1";
	}
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
 <?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
   <?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  <?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1> Support Center and Help</h1>
        </div>
        <!--inner content start-->
        <div style="clear:both"></div>
        <div style="margin:15px">
        <!-- Ttle (End) -->
      <!-- Block_a (Start) -->
        <!-- ======== S T A R T   C O N T E N T ======== -->

   
    
  
    
  
<br style='font-size:5px;'>
<b>Advertising / Clicking ads</b><br>
<div style='padding:5px; padding-top:0px;'>
    <a href='#1' >Is exposing my link cheap?</a>
    <br>
    <a href='#2'>How can I expose my referral links to all members of DownlineRefs?</a>
    <br>
</div>
<br style='font-size:5px;'><b>Concept explained</b><br>
<div style='padding:5px; padding-top:0px;'>
    <a href='#3' >How does the system work?</a><br>
    <a href='#4' >What is a referral request?</a><br>
</div>
<br style='font-size:5px;'><b>Credits and money</b><br>
<div style='padding:5px; padding-top:0px;'>
	<a href='#5'>I have purchased credits or a Premium Membership. Can you refund the transaction?</a><br>
</div>
<br style='font-size:5px;'><b>Getting referrals</b><br>
<div style='padding:5px; padding-top:0px;'>
    <a href='#6' >A member has signed up to my program, but stopped clicking or being active in that program. What can I do?</a><br>
    <a href='#7' >If I decline a sign-up, will I get a full refund of the spend credits?</a><br>
    <a href='#8' >A member claims he/she has signed up to my program, but I can't find that person in my downline.</a><br>
    <a href='#9'>How can I get referrals for my programs?</a><br>
</div>
<br style='font-size:5px;'><b>Hints and suggestions</b><br>
<div style='padding:5px; padding-top:0px;'>
	<a href='#10' >I want to get the most out of your service. How?</a><br>
</div>
<br style='font-size:5px;'><b>Ratings</b><br>
<div style='padding:5px; padding-top:0px;'>
    <a href='#11' >Rating pitfalls, thresholds and more...</a><br>
    <a href='#12'>How are my ratings calculated?</a><br>
    <a href='#13'>What do my ratings represent?</a><br>
    </div><br style='font-size:5px;'><b>Signing up to programs</b><br>
<div style='padding:5px; padding-top:0px;'>
	<a href='#14'>I have signed up to a program. When will I receive my credits?</a><br>
    <a href='#15'>How can I sign up to programs to earn credits?</a><br>
</div><br>
<hr size=1 color=#c0c0c0>
    <a name=1></a><br><br style='font-size:10px;'>
    <h2>Advertising / Clicking ads</h2>

        <a name=1></a>
        <br class=br5>
        <div id=div1 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>Is exposing my link cheap?</div>
          <div style='color:#666'>You bet it is! If you earned credits by signing up to programs or clicking ads, obviously it free! Alternatively, you can buy credits and spend them on advertisement packages to expose your link. Currently we offer 2000 credits for 10 USD only, which is equivalent to 0.005 USD per credit. An advertisement package of <b>1000 clicks</b> being worth 0.05 credits each will cost you 50 credits, or <b>0.25 USD only</b>!</div>

          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    
        <a name=2></a>
        <br class=br5>
        <div id=div2 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>How can I expose my referral links to all members of DownlineRefs?</div>

          <div style='color:#666'>You can expose your link by clicking 'Expose your link' from the DashBoard. Fill out an advertisement and indicate how many (guaranteed) clicks you want for your link. The credit amount that you specify on reflects the number of credits a single user gets when clicking on your link.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    <br><br style='font-size:10px;'><h2>Concept explained</h2>
        <a name=3></a>

        <br class=br5>
        <div id=div3 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>How does the system work?</div>
          <div style='color:#666'>The DownlineRefs community contains users from over the world, all looking for referrals and willing to sign up to new programs. Our system is based on a simple but well-proven concept using 'credits' to get referrals for your programs.<br />
<br />
<b>Step 1 - Earn or purchase credits</b><br />
Credits can be obtained by <b>joining programs</b>, <b>clicking banner advertisements</b> or <b>referring new members to DownlineRefs</b>. Additionally, you can buy credits at low prices. Most often we have special offers for credits packages.<br />

<br />
<b>Step 2 - Spend credits on 'buying' referrals for your programs</b><br />
Once you have these credits, you can spend them on so called 'referral requests' to get referrals in your program(s). Members in our community are willing to sign up under you, as they need the credits to get referrals for their own programs. Click Build Your Downline from the DashBoard and fill out a request form. You will receive a message when a member has joined under you.<br />
<br />
<b>Step 3 - Validate sign-ups</b><br />
If a user has signed up under you, you will automatically receive a message. Check your program to see if the member has indeed signed up to your program. To validate, click Pending Requests from the DashBoard and locate the request in the list. Click on it and follow the instructions.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>

    
        <a name=4></a>
        <br class=br5>
        <div id=div4 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>What is a referral request?</div>
          <div style='color:#666'>A <b>referral request</b> is a filled out form in which you request a referral in one of your programs. Each request relates to one sign-up <span class='small disabled'>(i.e. one referral)</span>. Thus, if you want 10 referrals in your program, click <i>Build your Downline</i> from the DashBoard, fill out the form and click the submit button 10 times.</div>

          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    <br><br style='font-size:10px;'><h2>Credits and money</h2>
        <a name=5></a>
        <br class=br5>
        <div id=div5 style='padding:8px;'>

          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>I have purchased credits or a Premium Membership. Can you refund the transaction?</div>
          <div style='color:#666'>No, all purchases are permanent and cannot be refunded.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    <br><br style='font-size:10px;'><h2>Getting referrals</h2>

        <a name=6></a>
        <br class=br5>
        <div id=div6 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>A member has signed up to my program, but stopped clicking or being active in that program. What can I do?</div>
          <div style='color:#666'>Your referral should be active in your program for the duration of the contract. If not, you are allowed to terminate the contract. Contract termination will result in:<br />
<br />
[*] A drop in your promotor rating of -2.<br />
[*] A refund of the remaining credit offer plus a partial refund of fees according to the progress of the contract.<br />

<br />
<span class=disabled><b>Example refund calculation:</b><br />
Transfered 40.000 out of 100.000 credits. Fees: 60.000 credits. Contract day 10 of 30.<br />
Your refund will be: (100.000 - 40.000) + 10/30 * 60.000 = 80.000 credits.</span><br />
<br />
To terminate a contract, click Manage Referral Sign-ups from the Dashboard, click the contract from the list and click the button Terminate Contract.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>

    
        <a name=7></a>
        <br class=br5>
        <div id=div7 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>If I decline a sign-up, will I get a full refund of the spend credits?</div>
          <div style='color:#666'>No, instead the status of your request will be set to 'available' again. If you want a full refund, click Pending/Validate Requests from the Dashboard and cancel the request by clicking the red cross in front of it.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>

        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    
        <a name=8></a>
        <br class=br5>
        <div id=div8 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>A member claims he/she has signed up to my program, but I can't find that person in my downline.</div>
          <div style='color:#666'>If a member claims he/she signed up to your program, but you can't find that person in your downline list of that program, you can either do one of the following:<br />
<br />

[*] Contact the person by sending a message (click Message Center from the Dashboard) to find out what happened. It could have been a slight mistake by using a wrong referral link for example. Pass on the right referral link in a message and let the member sign up again.<br />
<br />
[*] Decline the sign-up. This option sets the status of your request back to 'available'. However, declining sign-ups may affect your promotor rating. See elsewhere on this page for more information about ratings.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    
        <a name=9></a>
        <br class=br5>

        <div id=div9 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>How can I get referrals for my programs?</div>
          <div style='color:#666'><b>Option 1 - Referral Requests</b><br />
Click Build Your Downline from the DashBoard and fill out a request form, in which you indicate how many credits you offer if someone from our community signs up under you in your program. As life is not forever, indicate how long you want this new referral to be signed up to your program.<span class='disabled small'>Tip: Make sure your offer is worth more than others are offering for the same program! Click Program Signup from the DashBoard to see the list.</span> You will receive a message when a member has signed up to your program. Validate the sign-up by clicking Pending Requests from the Dashboard, locate and click the request and follow the instructions.<br />
<br />
<b>Option 2 - Expose your link</b><br />
Expose your link to hundreds or thousands of members at the same time. Each member, who are potential referrals, will click your link in exchange for some credits. Click Expose Your Link from the DashBoard and follow the instructions.<br />

<br />
<b>Option 3 - Advertise on our frontpage</b><br />
Daily we receive thousands of visitors to DownlineRefs, who will end up at our front page. Advertise on the front page at low costs and increase your chance of getting referrals for your programs. We offer competitively priced advertisement packages starting from 4 USD only! To order an advertisement spot, surf to the front page (click the logo on top of the page) and click an empty advertisement spot if available.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    <br><br style='font-size:10px;'><h2>Hints and suggestions</h2>

        <a name=10></a>
        <br class=br5>
        <div id=div10 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>I want to get the most out of your service. How?</div>
          <div style='color:#666'><u>The first</u> thing you can do is become a Premium Member. This membership has a bunch of privileges that Standard Members don't have. For instance, your referral requests will be located on top of the list, so you will get your referrals sooner. Also, as a Premium Member you will receive a discount on referral requests (-10%) and an extra credit bonus (+25%) upon purchasing credits. There is a link on the DashBoard that can provide you a Premium Membership.<br />
<br />
<u>Second</u>, try to increase you promotor rating, so members from our community are more willing to sign up under you, as your trustworthyness is high. <span class='small disabled'>(<i>Hint:</i> Premium Members can prevent unreliable members from signing up!)</span><br />

<br />
<u>Third</u>, carefully keep an eye on what other members are offering for the program you are promoting. Obviously, if you are offering 50 credits, and others are offering 90 credits for the same program, you're likely not to receive referrals. For the best results, we advise to increase your credit offer in such a way that you end up in the top 3 of the list.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    <br><br style='font-size:10px;'><h2>Ratings</h2>
        <a name=11></a>

        <br class=br5>
        <div id=div11 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>Rating pitfalls, thresholds and more...</div>
          <div style='color:#666'>Apart from the general exception <span class='small disabled'>(if the opposing party has a rating less than 0, your rating will not be changed)</span>, there are some rating thresholds at which our services to your membership will be limited:<br />
<br />
<b>Concerning your promotor rating</b><br />
If your promotor rating is <u>-5 or less</u>, you have to accept future sign ups for any pending referral requests in order to increase your promotor rating.<br />

<br />
<b>Concerning your referral rating</b><br />
If your referral rating is <u>-3 or less</u>, your account will be irreversibly blocked. Remaining credits will be forfeited.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    
        <a name=12></a>

        <br class=br5>
        <div id=div12 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>How are my ratings calculated?</div>
          <div style='color:#666'><b>Referral rating</b><br />
Each time your sign up to a program and your sign-up is <b>approved</b>, your referral rating will <b>increase</b> with 1.<br />

Each time your sign up to a program and your sign-up is <b>declined</b>, your referral rating will <b>decrease</b> with 1.<br />
<br />
<b>Promotor rating</b><br />
Each time you <b>approve</b> a new referral in your program, your promotor rating will <b>increase</b> with 1.<br />

Each time you <b>decline</b> a new referral in your program, your promotor rating will <b>decrease</b> with 1.<br />
<br />
<i>However...no rules without exceptions:</i><br />
The general exception is: if the rating of the opposing party is <b>less than 0</b>, your rating will <u>not</u> be changed. A second exception is that Premium Members always have a promotor rating of 0 or higher.<br />

<br />
<span class='small disabled'><u>Example #1:</u> You were promoting a program, but your new referral did not sign up. He/she has a referral rating of -1. If you decline this user, your promotor rating will not be changed.<br />
<br />
<u>Example #2:</u> You signed up to a program, but the promotor had falsely declined your sign-up. He/she had a promotor rating of -1. Your referral rating will not be changed.<br />
<br />
<u>Example #3:</u> You were promoting a program, but your new referral did not sign up. You have a referral rating of +0 and you are a Premium Member. Your promotor rating will not change if you decline this user.</span></div>
          <br class=br5>

        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    
        <a name=13></a>
        <br class=br5>
        <div id=div13 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>What do my ratings represent?</div>
          <div style='color:#666'>Ratings are divided into two groups: <b>promotor ratings</b> and <b>referral ratings</b>. They indicate your 'trustworthyness' in our community as being either a promotor or referral. The higher your rating, the better.<br />

<br />
<b>Promotor rating</b><br />
A positive promotor rating tells you that this promotor has accepted more sign-ups than he/she declined.<br />
A negative promotor rating tells you that this promotor has declined more sign-ups than he/she accepted.<br />
<br />
<b>Referral rating</b><br />
A positive referral rating tells you that this member was accepted for a sign-up more than he/she was declined.<br />
A negative referral rating tells you that this member was declined for a sign-up more than he/she was accepted.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>

        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    <br><br style='font-size:10px;'><h2>Signing up to programs</h2>
        <a name=14></a>
        <br class=br5>
        <div id=div14 style='padding:8px;'>
          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>I have signed up to a program. When will I receive my credits?</div>
          <div style='color:#666'>You will start receiving credits 24 hours after your sign-up has been accepted by the promotor. You will receive each day an equal share of the total offer. For example, if the offer is 60 credits with a contract duration is 20 days, you will receive 60 / 20 = 3 credits daily. This 'credit transfer' is fully automated and does not require intervention of the promotor.<br />

Hint: To see an actual status of the sign-up contract, click My Program Sign-ups from the DashBoard.</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>
    
        <a name=15></a>
        <br class=br5>
        <div id=div15 style='padding:8px;'>

          <div style='padding:3px; font-weight:bold; margin-bottom:3px; font-size:14px; background:#c0c0c0; color:white;'>How can I sign up to programs to earn credits?</div>
          <div style='color:#666'>You will earn credits by joining and being active in other programs. Click Program Signup from the DashBoard to see a list of programs that are currently offered by our members. Pick a program that you like, and sign up under a user that makes a good offer. Most likely you want to make a trade off between the credit offer, the contract duration and the trustworthyness <span class='disabled small'>(promotor rating, higher=better)</span> of the user. Click on the user's offer to proceed to the next page. Sign up to the program using the referral-link displayed (a new window will open). After you have joined the program, return to DownlineRefs and indicate which username you have used for your sign-up. Then, click Request Validation. Now your sign-up will be validated within 5 days by the member (=promotor) who was promoting that program. If your sign-up is not validated within 5 days it will be automatically accepted!</div>
          <br class=br5>
        </div>        
        <div class=br5 style='border-top:1px dashed #c0c0c0;'></div>
        <div class=small style='padding-left:5px; margin-top:2px; margin-bottom:10px;'><img src='/images/faq_top.gif' align=absmiddle> <a class=fakea onclick='gototop();'>go to top</a></div>

    
    <script>
        init();
        function init() {
            if(document.location.hash=='') return;
            var tmp = document.location.hash;
            tmp = tmp.replace('#','');
            if(tmp<1 || tmp>15) return;
            tmp = parseInt(tmp);
            faqhigh(tmp);
        }
    </script>
<!-- ======== E N D   C O N T E N T ======== -->
      <!-- Block_a (End) -->
      </div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
   <?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   <?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
