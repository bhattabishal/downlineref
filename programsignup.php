<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "./".DIR_COMMON."/numbertotext.php";
require_once "logincheck.php";


$pvo=new program_signupVO();
$pdao=new program_signupDAO();

$programs=$pdao->fetchAllGroupByProgramType();
$totalprograms=count($programs);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Program Signup</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="js/ajaxExposeLink.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>
<script type="text/javascript" src="js/exposelink.js"></script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(document).ready(function(){
        $("#exposelink").validationEngine('attach');
       });
    </script>

<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>-->
<!-- inline validation ends -->
<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<script type="text/javascript">
	 
      
     
      
      function update() {
          var size = getObject('packagesize').value;
          var worth = getObject('worth').value;
          if(!isDecimal(size) || !isDecimal(worth)) {
              getObject('spansubtotal').innerHTML='0.0';
              getObject('spanfees').innerHTML='0.0';
              getObject('spantotal').innerHTML='0.0';
              return;
          }
          
          var subtotal = parseFloat(size) * parseFloat(worth);
          var fees = parseFloat(subtotal * 0.2);
          var total = parseFloat(subtotal + fees);
          
          getObject('transactionoverview').style.visibility='visible';
          getObject('spansubtotal').innerHTML = number_format(subtotal,4);
          getObject('spanfees').innerHTML = number_format(fees,4);
          getObject('spantotal').innerHTML = number_format(total,4)+' credits';
      }
      
         
</script>
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
<form method=post name=scriptform>
  <input id=trigger type=hidden name=spacer value=TRUE>
  <input id=data type=hidden name=spacer value=spacer>
</form>
<form method=post name=surftoform></form>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--for tabs-->
<link rel="stylesheet" media="all" type="text/css" href="css/mini-tabbed-pages.css" />

<script src="js/click_menu.js" type="text/javascript"></script>
<!--tabs end-->

</head>
<body onload="clickMenu('tabs')">
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1> 
          	Program Signup
		 </h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        	<div id="form-container">
            	<form  id="exposelink" name="exposelink" class="wufoo topLabel page"  autocomplete="off" enctype="multipart/form-data" method="post" action="exposelinkvalidate.php" onsubmit="return false;">
                	<header id="form-header" class="info">
                    	<h2 style="font-size:110%">
<div style="font-weight:bold">How does this work</div>
<ul>
	<li style="width:100%">1.<span style="position:absolute; margin-left:5px">Click one of the programs below, and choose a promotor that offers most credits.</span></li>
    <li style="width:100%; margin-top:10px">2.<span style="position:absolute;margin-left:5px"> Follow the instructions to sign up to the program.</span></li>
    <li style="width:100%; margin:10px 0px;">3.<span style="position:absolute;margin-left:5px"> The promotor will validate your sign-up within 5 days. Each day you will receive an equal share of credits for the duraction of the contract.</span></li>
</ul>
<font color="#FF0000">Important!</font> You need to be active in the program you sign up to!
						</h2>
                    	<div></div>
                	</header>
                   <!-- <h2>Search</h2>
                	<table>
                    	
                        <tr>
                        	<td>Minimum offer</td>
                            <td>
                            	<select>
                                	<option>Select</option>
                                    <?php 
									//$i=30015;
										//while($i>=25)
										//{
										//$i=$i-5;
											?>
                                            	<option value="<?php //echo $i; ?>"><?php //echo $i; ?></option>
                                            <?php
										//}
									 ?>
                                </select>
                                Credits
                            </td>
                            <td>
                            	Payment processor
                            </td>
                            <td>
                            	<select>
                                	<option>Alertpay</option>
                                    <option>Paypal</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td>Program Search</td>
                            <td>
                            	<input type="text" />
                            </td>
                            <td>
                            	Program type
                            </td>
                            <td>
                            	<select id="programtype" onchange="programtype=this.value; getprogramlist();">

                                    <option selected="selected"></option>
                                    <option value="ptc">Paid to Click</option>
                                    <option value="freeoffer">Free Offer website</option>
                                    <option value="downlinebuilder">Downline Builder</option>
                                    <option value="trafficexchange">Traffic Exchange</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td>Instant payout</td>
                            <td>
                            	<select id="instantpayout" onchange="instantpayout=this.value; getprogramlist();">
        <option selected="selected"></option>
        <option value="yes">Yes</option>
        <option value="no">No</option>
    </select>
                            </td>
                            <td>
                            	Program language
                            </td>
                            <td>
                            	<select>
                            	        <option selected="selected"></option>
                                        <option value="english">English</option>
                                        <option value="nederlands">Nederlands</option>
                                        <option value="francais">Francais</option>
                                        <option value="deutsch">Deutsch</option>
                                        <option value="espagnol">Espagnol</option>
                                
                                        <option value="other">-Other-</option>
                               </select>
                            </td>
                        </tr>
                    </table>-->
                    	
            </div>
        
       		<!--programs start-->
         	<div style="float:left; width:590px; height:auto; min-height:200px; background-color:#000066; margin-right:10px; padding:10px;background:none repeat scroll 0 0 #FFFFFF; border:1px solid #CCCCCC; box-shadow:0 0 5px rgba(0, 0, 0, 0.2); margin-top:10px">
            		<?php
						if($totalprograms > 0)
						{
							foreach($programs as $value)
							{
								
					?>
            		
            		<div><!--main program contents-->
                    	<div style="margin-bottom:10px"><b>
						<?php
						 switch($value->program_type)
						 {
						 	case 'ptc':
							echo "Paid to Click";
							break;
							
							case 'freeoffer':
							echo "Free Offer website";
							break;
							
							case 'downlinebuilder':
							echo "Downline Builder";
							break;
							
							case 'trafficexchange':
							echo "Traffic Exchange";
							break;
						 } 
						 ?></b></div>
                        <?php
							$nameslist=$pdao->fetchProgramGroupByProgramName($value->program_type);
							foreach($nameslist as $name)
							{
						?>
                        <a href="programsignupdetails.php?name=<?php echo $name->program_name; ?>" style="color:#006600">
                        <div class="program-div" style="float:left; width:188px;height:100px; margin:0 5px 5px 0; padding:5px">
                        	<div class="program-head" style="padding:2px 2px 5px 5px;font-size:14px">
                                <span>
                                    
                                    <b><?php echo $name->program_name; ?></b>
                                </span>
                            </div>
                            <div class="program-max" style="padding:2px 2px 5px 5px; font-size:14px">
                                Max Offer: <?php echo formatDoubleAmount((60/100)*$name->total); ?> Credits
                            </div>
                            <div class="program-type" style="padding:2px 2px 5px 5px;">
                                Type: <?php echo strtoupper($name->program_type); ?>(<?php echo $name->program_language; ?>)
                            </div>
                             <?php
								if($name->payment_processor != '')
								{
								?>
									<div class="payment-icon" style="padding:2px 2px 5px 5px;">
									<?php 
										if($name->payment_processor=='alertpay')
										{
									?>
										<img src="images/alertpay.gif" align="alertpay" />
										
									 <?php
										}
										elseif($name->payment_processor=='paypal')
										{
									 ?>
										<img src="images/az_paypal_icon.gif"  alt="paypal" />
									 <?php
										}
									 ?>
									</div>
								<?php
								}
							?>
                        </div><!--program div ends-->
                        </a>
                 		<?php
							}
						?>
                     
                        
                    </div><!--end main program contents-->
                       <div style="clear:both"></div>
                   
                  <?php } }?> 
                 
            </div>
 			<div style="clear:both"></div>
            <!-- programs ends -->
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->

</body>
</html>
