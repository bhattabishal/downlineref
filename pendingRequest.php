<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "./".DIR_COMMON."/countdown.class.php";
require_once "logincheck.php";

$pvo=new program_signupVO();
$pdao=new program_signupDAO();

$cpvo=new program_signup_childVO();
$cpdao=new program_signup_childDAO();

$uvo=new user_accountVO();
$udao=new user_accountDAO();

$vdao=new votingDAO();



$user_id=$_SESSION['SES_ID'];
if(isset($_GET['id']) && $_GET['id']!="")
{
	$udao->removeReferral($user_id,trim($_GET['id']));
	$pdao->remove(trim($_GET['id']));
	//$vdao->removeByProgramId($_GET['id']);
}



//fetch all programs submitted by the user


$programList=$pdao->fetchAllById($user_id);








?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pending Requests</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<link rel="stylesheet" type="text/css" href="css/sortertable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft" style="margin:5px; width:940px">
     	<div class="block" style="width:940px">
        <!-- Ttle (Start) -->
        <div class="ttle" style="background:url(images/h1bg2.jpg) no-repeat; width:940px">
          	<h1> 
				Pending Requests
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--info div staart-->
        <div id="form-container" style="margin-bottom:15px; width:910px">
            <header id="form-header" class="info" style="border:none; padding:10px; font-style:normal; font-weight:normal;">
             <ul style="list-style:none; font-weight:bold; ">
                        	<li><span style="margin-right:5px"></span>What's This ?</li>
                           
                        </ul>
                    	<h2 style="font-style:normal;font-size:110%">The list below shows your referral requests that are still available. As soon as a member has signed up to your program, you will be notified by e-mail and the status will change. Click an entry from the list to change the details (if available) or validate the sign-up (if waiting validation). To cancel a request, click the red cross on the right - spend credits will be refunded into your account. <br />


						</h2>
                       
                    	<div></div>
                	</header>
            </div>
        <!--info div end-->
        
        
        <!--table starts-->
        <div id="tablewrapper">
		<div id="tableheader">
        	<div class="search">
                <select id="columns" onchange="sorter.search('query')"></select>
                <input type="text" id="query" onkeyup="sorter.search('query')" />
            </div>
            <span class="details">
				<div>Records <span id="startrecord"></span>-<span id="endrecord"></span> of <span id="totalrecords"></span></div>
        		<div><a href="javascript:sorter.reset()">reset</a></div>
        	</span>
        </div>
        <table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
            <thead>
                <tr>
                	
                    <th class="nosort"><h3>Timestamp</h3></th>
                    <th><h3>Program Name </h3></th>
                    <th><h3>Offer </h3></th>
                    <th><h3>Status </h3></th>
                    <th><h3>Time Left  </h3></th>
                </tr>
            </thead>
            <tbody>
           <?php
		   		foreach($programList as $value)
				{
					//only list programs where any child has not signup
					$avai=0;
					$pending=0;
					$delimage="";
					
					if($value->status == 1)
					{
						$status="<img src='images/icon_status_available.png' align='absmiddle' /><b>available</b>";
						$timeleft="n/a";
						$avai=1;
					
						$delimage="<a href=\"pendingRequest.php?id=$value->program_id\" onclick=\"return confirm('Are you sure you want to Delete this program ?')\" ><img src=\"images/icon_del.png\" align=\"absmiddle\" style=\"margin-right:5px\" title=\"delete\" /></a>";
					}
					else // if not available check if the signup child for that program is waiting validation also declined ones
					{
						
						$childSignups=$cpdao->fetchAllMyPendingReferralSignups($value->program_id,$user_id);
						$childDeclinedSignups=$cpdao->fetchAllMyDeclinedReferralSignups($value->program_id,$user_id);
						
						if(count($childSignups) > 0)
						{
							$avai=1;
							$status="<img src='images/icon_status_waiting.png' align='absmiddle' /><b>Pending</b>";
							$timeleft="n/a";
							$pending=1;
							
						}
						
						if(count($childDeclinedSignups) > 0)
						{
						$status="<img src='images/icon_status_available.png' align='absmiddle' /><b>available</b>";
						$timeleft="n/a";
						$avai=1;
					
						$delimage="<a href=\"pendingRequest.php?id=$value->program_id\" onclick=\"return confirm('Are you sure you want to Delete this program ?')\" ><img src=\"images/icon_del.png\" align=\"absmiddle\" style=\"margin-right:5px\" title=\"delete\" /></a>";
							
						}
						
					}
					
					if($avai==1)
					{
					
					?>
                     <td><?php echo $delimage.$value->created_date; ?></td>
                      <td>
                    <?php
					if($delimage != "")
					{
					?>
                    	<a href="editpendingrequest.php?id=<?php echo $value->program_id; ?>">
                    <?php }else{ ?> <a href="signups.php?pid=<?php echo $value->program_id; ?>"> <?php } ?>
						<?php echo $value->program_name; ?>
                         
                        
                        </a>
                    
                    </td>
                    <td><?php echo formatDoubleAmount((60/100)*$value->total); ?> Credits</td>
                    <td><?php echo $status; ?></td>
                    <td><?php echo $timeleft; ?></td>
                     </tr>
                    <?php
					}
			 	}
			 ?>
              
            </tbody>
        </table>
        <div id="tablefooter">
          <div id="tablenav">
            	<div>
                    <img src="images/shortertable/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/shortertable/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/shortertable/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/shortertable/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div>
                	<select id="pagedropdown"></select>
				</div>
                <div>
                	<a href="javascript:sorter.showall()">view all</a>
                </div>
            </div>
			<div id="tablelocation">
            	<div>
                    <select onchange="sorter.size(this.value)">
                    <option value="5">5</option>
                        <option value="10" selected="selected">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div>
            </div>
        </div>
    </div>


	<script type="text/javascript" src="js/sorterTable/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:7,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		//sum:[8],
		//avg:[6,7,8,9],
		//columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
           <!--table end-->
             <div style="clear:both"></div>
           <div class="buttons">
           
                	<button type="button" class="regular" name="Back" value="Back" onclick="location.href='dashboard.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
               
           </div>
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
           <div style="clear:both"></div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			//include_once("includes/rightcolumn.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
