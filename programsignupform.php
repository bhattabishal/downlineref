<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];




//code to insert records in to the database
if(isset($_GET['pid']) && intval($_GET['pid']) !='')
	$program_id=$_GET['pid'];
	
if(isset($_GET['uid']) && intval($_GET['uid']) !='')
	$promotor_id=$_GET['uid'];


$pvo=new program_signupVO();
$pdao=new program_signupDAO();
$programs=$pdao->fetchDetails($program_id);
$program_name=$programs->program_name;

$ratingallowed=$programs->allowed_mem_rat;
$blockuserlist=$programs->not_allowed_mem;
$blockusers=explode(",",$blockuserlist);



$svo=new siteusersVO();
$sdao=new siteusersDAO();

$userinfo=$sdao->fetchDetails($promotor_id);
$mem_name=$userinfo->username;
$mem_type=$userinfo->mem_type;


//for promotor rating
$votvo=new votingVO();
$votdao=new votingDAO();

$totalpromotorrating=getPromotorRating($promotor_id);


if(isset($_POST['save']) && $_POST['save']=='true')
{
	$cvo=new program_signup_childVO();
	$cdao=new program_signup_childDAO();
	
	
	
	
	
	$errors=array();
	
	if($_POST['user-email']=="")
	{
		$errors['user-email']="valid username and/or email address is required";
	}
		
	
	
		$ch1 = $_POST['checkbox1'];
		if($ch1 != "checkbox1")
		{
			$errors['checkbox1']="Please check the checkbox to agree";
		}
	
	
	
	
		$ch2=$_POST['checkbox2'];
		if($ch2 !="checkbox2")
		{
			$errors['checkbox2']="Please check the checkbox to agree";
		}
	
	
	if(count($errors)<=0)
	{
		$cvo->parent_user_id=$promotor_id;
		$cvo->child_user_id=$user_id;
		$cvo->username=trim($_POST['user-email']);
		$cvo->email=trim($_POST['user-email']);
		$cvo->program_id=$program_id;
		$cvo->created_by=$user_id;
		$cvo->created_date=date("Y-m-d H:i:s");
		$cvo->updated_by="";
		$cvo->updated_date="";
		$cvo->update_count=0;
		$cvo->status=0;//status 0-waiting validation,1-accepted,2-declined,3-completed,4-terminated
		
		//also check before inserting that if any user has already signup or not in the same program
		$pvo=new program_signupVO();
		$pdao=new program_signupDAO();
		$programs=$pdao->fetchDetails($program_id);
		$alreadyjoined=false;
		$blocked=false;
		$notallowedrat=false;
		if($programs->status == 0)
		{
			$alreadyjoined=true;
		}
		
		foreach ($blockusers as $key=>$value )
		{
			if(trim($_SESSION['SES_USER']) == trim($value))
			{
				$blocked=true;
				break;
			}
		}
		$refrating=getReferralRating($user_id);
		if($refrating < $ratingallowed)
		{
			$notallowedrat=true;
		}
		
		//$cdao=new program_signup_childDAO();
		if($alreadyjoined==false && $blocked==false && $notallowedrat==false)
		{
			if($cdao->Insert($cvo))
			{
				$msg="Your Request Has Been Send Successfully";
				$check=1;
				
				//change status of program to 0
				$pdao->ActivateNdactivate($program_id,0);
				
				//now send one message to the promotor regarding your signup
				$mvo=new messageVO();
				$mdao=new messageDAO();
				
				
				$mvo->message_from=0;
				$mvo->message_to=$promotor_id;
				$mvo->message_subject="Program Signup";
				
				$mvo->created_date=GetThisDate("Y-m-d G:i:s");
				$mvo->message_details="Hello $mem_name,"."<br />" ."A Member Has Signup to your program"." "."<b>". $program_name."</b>"." " ."on $mvo->created_date .Click on " ."<a href=\"pendingRequest.php\">"."pending/validate</a> link from Dashboard page and follow the instruction to validate the signup.You need to validate this sign-up within 5 days";
				$mvo->status=1;
				$mdao->Insert($mvo);
				
			}
			else
			{
				$msg="Some Error Occured.Please Try Again";
			}
		}
		else
		{
			$msg="Currently You Can Not Signup To This Promotor.Please Choose Another";
		}
	}
		
	
	
	
	
}


//voting starts
$votvo=new program_votingVO();
$votdao=new program_votingDAO();

$spamVotes=$votdao->fetchRatingsById($programs->program_name,"scam");
$trustfulVotes=$votdao->fetchRatingsById($programs->program_name,"trustful");
$totalVotes=count($spamVotes)+count($trustfulVotes);

if($totalVotes == "" )
{
	$totalVotes=0;
}

$scamPercentge=@((count($spamVotes)/($totalVotes))*100);

if($scamPercentge == "")
{
	$scamPercentge=0;
}

$trustfulPercentage=@((count($trustfulVotes)/($totalVotes))*100);

if($trustfulPercentage== "")
{
	$trustfulPercentage=0;
}

//voting ends




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php
	//if(isset($check) && $check==1)
	//{
	?>
   <!-- <meta http-equiv="refresh" content="2; URL=programsignup.php">-->
    <?php
	//}
?>
<title>Progrm Signup</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<script type="text/javascript" src="js/ajaxVote.js"></script>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
<form method=post name=scriptform>
  <input id=trigger type=hidden name=spacer value=TRUE>
  <input id=data type=hidden name=spacer value=spacer>
</form>
<form method=post name=surftoform></form>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!--for poll-->
	<!--<link rel="stylesheet" href="poll/stylesheet.css" type="text/css"/>
	<SCRIPT src="poll/prototype.js" type="text/javascript"></SCRIPT>
	<SCRIPT src="poll/controls.js" type="text/javascript"></SCRIPT>
	<SCRIPT src="poll/dragdrop.js" type="text/javascript"></SCRIPT>
	<SCRIPT src="poll/effects.js" type="text/javascript"></SCRIPT>
	
	<SCRIPT src="poll/javascript.js" type="text/javascript"></SCRIPT>-->
<!--poll ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Program Signup
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
         
            <div class="message-block">
            	
                
            </div>
        	<div class="container-table">
                    <div class="wrap-table"> 
                   
                    	
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                    <td class="r_name no_line"><img src="images/globe-Vista-icon.png" width="24" height="24" align="absmiddle" style="margin-right:10px" />Program</td>
                                     <td class="r_name no_line"><img src="images/business_user.png" width="24" height="24" align="absmiddle" style="margin-right:5px" />Promotor</td>
                                </tr>
                               <tr>
                               		<td>Program Name: <?php echo $programs->program_name; ?></td>
                                    <td>
                                    	Promoter:
                                        <?php echo $mem_name; ?>
										<?php
                                            if($mem_type==1)
                                            {
                                                echo '<img src="images/premium.png" align="bottom" alt="Premium Member" title="Premium Member" />';
                                                
                                            }
                                            else
                                            {
                                                echo '<img src="images/free.png" align="bottom" alt="Standard Member" title="Standard Member" />';
                                            }
                                        ?>
                                    </td>
                               </tr>
                               <tr>
                               		<td>Program Type: <?php echo $programs->program_type; ?></td>
                                    <td>Rating:  <?php if($totalpromotorrating > 0) { echo "+";} ?><?php echo $totalpromotorrating; ?></td>
                               </tr>
                               <tr>
                               		<td>Instant Payout: <?php echo $programs->instant_payout; ?></td>
                                    <td>Contract Duration: <?php echo $programs->contract_duration; ?> days</td>
                               </tr>
                               <tr>
                               		<td>Payment Processor: <?php if($programs->payment_processor=='' ){ echo "unknown"; }else{ echo $programs->payment_processor;} ?></td>
                                    <td>Sign-up Offer: <?php echo formatDoubleAmount((60/100)*$programs->total); ?> credits</td>
                               </tr>
                               <tr>
                               		<td colspan="2" class="r_name no_line" align="center">
                                    <div class="leftdiv" style="float:left; width:320px; text-align:left; margin-top:70px; font-size:16px"><!--left part ends-->
                                    	You will receive <?php echo formatDoubleAmount((60/100)*$programs->total); ?> credits when joining this program for <?php echo $programs->contract_duration; ?> days <br/> <a href="<?php echo $programs->referral_url; ?>" ><?php echo $programs->referral_url; ?></a>
                                        </div>
                                        <!--voting starts-->
                                        	<div class="rightdiv" id="rightdiv" style="width:200px; float:right;">
                        	
                        	<div style="background-color:#6C955C; padding:5px 5px 10px 5px">
                            	<div style="padding:5px"><b>PTC SCAM Meter</b></div>
                                <div style="text-align:center; background-color:#FFFFFF; padding:5px">
                                	<div id="programname" style="font-weight:bold">
                                    	<?php echo $programs->program_name; ?>
                                    </div>
                                	<img src="images/chart.png" />
                                    <div>
                                    	<font color="#FF0000"><b><?php echo $scamPercentge; ?>%</b> Scam</font> | <b><?php echo $trustfulPercentage; ?>%</b> Trustful<br />
                                       <font color="#999999" size="1"> (Results Based On <?php echo $totalVotes; ?> Votes)</font>
                                    </div>
                                    <div style="margin-top:5px">
                                    Want To Vote Yourself ?<br />
                                    <input type="button" value="Scam" style="color:#FF0000" onclick="vote('scam','<?php  echo $user_id;?>','<?php  echo $programs->program_name;?>');" />
                                     <input type="button" value="Trustful" style="color:#009900" onclick="vote('trustful','<?php  echo $user_id;?>','<?php  echo $programs->program_name;?>');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div style="clear:both"></div>
                                        <!--voting ends-->

                                    </td>
                               </tr>
                                
                                  
                
            </div>
                               
                            </tbody>
                        </table>
                         
                       
                        </div>
                </div>
           <!--table end-->
           
           
          <div style="clear:both"></div>
           <!--poll start-->
                        	<!--<SPAN id="showdiv" onclick="showdiv()">Show Poll</SPAN>
	<DIV id="polldiv"  style="margin-bottom:10px" >
		<IMG src="cross.jpg" alt="Close" class="cross" id="cross" onclick="hidePoll()" title="Close poll" />
		<CENTER><SPAN class="header">{header}</SPAN></CENTER>
		<BR />
		<SPAN class="question" id="ques">{question}</SPAN>
		<BR />
		<SPAN id="options" class="options">
		<BR />
			{options}			
		<BR />
		</SPAN>
		<SPAN id="voteimg">&nbsp;<IMG src="vote.gif" alt="Vote" onclick="return submitPoll()" /></SPAN>
		<SPAN id="result" class="answer"></SPAN>
	</DIV>-->
                        <!--poll ends-->
          <div id="form-container">
            	<form  id="exposelink" name="exposelink" class="wufoo topLabel page"  autocomplete="off" enctype="multipart/form-data" method="post" action="">
                	
                            <div style="clear:both"></div>
                	
                    <ul>
                    <?php
					if(isset($msg))
					{
					?>
                    	<div style=" padding:10px; color:#FFFFFF; background-color:#000033; font-weight:bold">
							<?php echo $msg; ?>
                        </div>
                        <?php
						}
                    	 ?>
                 
                    	<li id="foli3" 		class="complex" >
                            <label class="desc" id="title3" for="Field3">
                                1 Sign up to <?php echo $programs->program_name; ?>
                            </label>
                            <div>
                                <span class="full addr1">
                               Click the link above to sign up to <a href="<?php echo $programs->referral_url; ?>" ><?php echo $programs->program_name; ?></a> under member <?php echo $mem_name; ?>.
                                
                                </span>
                                <div style="text-align:center">
                                <?php
									if($programs->banner_url !="" && $programs->banner_url !="http://" )
									{
										?>
                                        	<a href="<?php echo $programs->referral_url; ?>" title="open in new window" target="_blank"><img src="<?php echo $programs->banner_url ?>" alt="<?php echo $programs->program_name; ?>" align="absbottom" /></a>
                                        <?php
									}
								?>
                               </div>
                            </div>
                            
                            </li>
                            
                            <li id="foli9" 		class="     " >
                        		<label class="desc" id="title9" for="Field9">
                            		2. Your sign-up details
                                </label>
                        		<div>
                            		
                                    <label for="Field3">What username and/or email address did you use? </label>
                                    My feedback for promotor:<input  id="user-email" name="user-email" type=text   	value=""   style="width:250px"  />  <label for="Field3">Important! Your promotor will validate your sign-up based on the information you provide here. </label>
                        		</div>
                        	</li>
                         
                            <li id="foli16" 		class="     ">
                            <fieldset>
                            <![if !IE | (gte IE 8)]>
                            
                            <![endif]>
                            <!--[if lt IE 8]>
                            <label id="title16" class="desc">
                                Requirements
                                    </label>
                            <![endif]-->
                            <div>
                                <span>
                            <input id="Field16" 		name="checkbox1" 		type="checkbox" value="checkbox1"/>
                            <label class="choice" for="Field16">I have signed up to the program using the URL above.</label>
                            </span>
                                <span>
                            <input id="Field17" 		name="checkbox2" 		type="checkbox" value="checkbox2" style="margin-top:10px" />
                            <label class="choice" for="Field17">I will actively participate in this program for the duration of the contract.</label>
                            </span>
                                
                                </div>
                            </fieldset>
                            </li>
                            <?php
								if(isset($errors['user-email']) || isset($errors['checkbox1']) || isset($errors['checkbox2']))
								{
							?>
                             <div style="background-color:#000033; color:#FF0000; padding:10px">
                            	<?php
									if(isset($errors['user-email']))
										echo $errors['user-email'];
										
									elseif(isset($errors['checkbox1']))
										echo $errors['checkbox1'];
										
									elseif(isset($errors['checkbox2']))
										echo $errors['checkbox2'];
								 ?>
                            </div>
                            <?php
							}
							?>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		3. Task description
                                </label>
                                <label>This is what the promotor asks you to do after you have signed up:</label>
                        		<div>
                            		 <textarea disabled="disabled"><?php echo $programs->task_desc; ?></textarea>
                        		</div>
                        	</li>
                           
                           
                            <li class="buttons ">
                            <div>
                            	<input type="hidden" name="save" value="false" />
                                      <input type="submit"  name="submit" value="Request Validation"  onclick="this.form.save.value='true'"  />
                                      <input type="reset"  name="submit" value="Cancel"    />
                                                
                                    
                           </div>
                        </li>
                    </ul>
                   
                </form>
                <script>getObject('user-email').focus();</script>
            </div>
            <!--form closed-->
    	<!--inner content close-->
      	</div>
         
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
