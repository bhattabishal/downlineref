<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "logincheck.php";
//This code is for Re-captcha
$user_id=$_SESSION['SES_ID'];






//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();
$uDAO=new user_accountDAO();
$user_details=$sDAO->fetchDetails($user_id);
//$credits_detail=$uDAO->fetchDetailsByUserId($user_id);

$account_type=$user_details->mem_type;

///if($credits_detail->credits!=''){$credits=$credits_detail->credits; }else {$credits="0.00";}

//program referrals details
$programvo=new program_signupVO();
$programdao=new program_signupDAO();
$programtotalreferrals=$programdao->fetchAllById($user_id);
$programlastweekreferrals=$programdao->fetchAllByLastDays($user_id,$days=7);

//referral signups details
$childprogramvo=new program_signup_childVO();
$childprogramdao=new program_signup_childDAO();
$totalChildsignups=$childprogramdao->fetchAllByProgramSignups($publish='all',$user_id);
$childlastweeksignups=$childprogramdao->fetchAllByDays($user_id,$days=7);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Account Statistics</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Account Statistics And Overview
        	</h1>
        </div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
        	<div class="container-table">
                    <div class="wrap-table"> 
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="r_name no_line">Your Account</td>
                                </tr>
                                <tr >
                                    <td style="font-weight:bold">Membership</td>
                                    <td style="font-weight:bold"><?php if($account_type==1)
				{
					echo '<img src="images/premium.png" align="top" alt="Premium Member" title="Premium Member" /> Premium';
					
				}
				else
				{
					echo '<img src="images/free.png" align="top" alt="Standard Member" title="Standard Member" /> Standard';
				} ?></td>
                                </tr>
                                 <tr>
                                   <td style="border-top:none; vertical-align:top">Expires</td>
                                   <td style="border-top:none">
								   <?php if($account_type==1)
								   { 
								   	$pvo=new premium_membersVO();
									$pdao=new premium_membersDAO();
								   	$premium_details=$pdao->fetchActiveMembershipDetails($user_id);
									 echo $premium_details->mem_end_dt;
									}else 
									{ echo "never";} ?>
                                   </td>
                                </tr>
                                 <tr>
                                   <td style="font-weight:bold">Program Referrals</td>
                                   <td style="font-weight:bold"><?php echo count($programtotalreferrals); ?></td>
                                </tr>
                                 <tr>
                                   <td style="border-top:none">Change over the last week</td>
                                   <td style="border-top:none"><?php echo count($programlastweekreferrals); ?></td>
                                </tr>
                                 <tr>
                                   <td style="font-weight:bold">DownlineRefs Referrals</td>
                                   <td style="font-weight:bold"><?php echo count($totalChildsignups); ?></td>
                                </tr>
                                <tr>
                                   <td style="border-top:none">Change over the last week</td>
                                   <td style="border-top:none"><?php echo count($childlastweeksignups); ?></td>
                                </tr>
                                 <tr>
                                   <td style="font-weight:bold">Account Balance</td>
                                   <td style="font-weight:bold"><?php echo $totalCredits; ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="r_name">Purchages</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="font-weight:bold">
                                    <p><a href="purchagecredits.php" style="color:#6C955C">Purchage Credits</a></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="border-top:none">
                                    <p>Save time and purchage credits directly</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="font-weight:bold">
                                    <p><a href="premium.php" style="color:#6C955C">Upgrade to Premium</a></p>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="border-top:none">
                                    <p>Get the most out of your membership-lots of advantages</p>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                   	 	
                                        <div class="buttons">
                                    		<button type="button" class="regular" name="Delete Account" value="Delete Account" onclick="location.href='accDelete.php'"><img src="images/cross.png" alt=""/>Delete Account</button>
                                        </div>
                                    </td>
                                    <td>
                                   	 	
                                        <div class="buttons">
                                    		<button type="button" class="regular" name="Edit Account" value="Edit Account" onclick="location.href='accEdit.php'"><img src="images/edit-icon.png" alt=""/>Edit Account</button>
                                        </div>
                                    </td>
                                </tr>
                               <!-- <tr>
                                    <td colspan="2" class="r_name">Output File Formats Supported</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Videos</td>
                                    <td width="520">MP4, MPEG-4, H.264, HD H.264</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Audios</td>
                                    <td width="520">MP3, M4A, AAC</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Images</td>
                                    <td width="520">BMP, GIF, JPEG, PNG</td>
                                </tr>-->
                            </tbody>
                        </table> 
                        </div>
                </div>
           <!--table end-->
        
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
