<?php
session_start();
require_once("alertpay/EPDDecryptor.php");
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";

if(isset($_SESSION['SES_ID']) && $_SESSION['SES_ID']!='')
{
	$id=$_SESSION['SES_ID'];
}
else
{
	$id=0;
}
$ipn=getCodeValueByLabel("alertpay_ipn_code");





//A constant holding the value is the Security Code generated from the IPN section of your AlertPay account. 

    define("IPN_SECURITY_CODE", $ipn); //***Please change it to yours***
	
	//Create a EPDDecryptor object and pass the IPN Security Code to it

    $decryptor = new EPDDecryptor(IPN_SECURITY_CODE);
	//Pass the cypher text received from $_GET['AP'] to the decrypt function of the EPDDecryptor object

    $ipnData = $decryptor->decrypt($_GET['AP']);
	//var_dump($_GET['AP']);
	
	    

   // var_dump($ipnData); //simply display the decrypted text using var_dump
	


$fvo=new front_bannerVO();
$fdao=new front_bannerDAO();

$fvo->purchased_by=$id;
$fvo->payment_type="alertpay";
$fvo->txn_id=$ipnData["ap_referencenumber"];
$fvo->target_url=$_SESSION['target_url'];
$fvo->banner_url=$_SESSION['banner_url'];
$fvo->purchaser_email=$_SESSION['purchaser_email'];
$fvo->ban_spot=$_SESSION['ban_spot'];
$fvo->ban_weeks=$_SESSION['ban_weeks'];
$fvo->ban_status=1;
$fvo->created_date=date('Y-m-d H:i:s',strtotime($ipnData["ap_transactiondate"]));
$fvo->valid_date=date("Y-m-d H:i:s", strtotime($fvo->created_date . "+ ".$_SESSION['ban_weeks']." week")); 
//$d="10:05:34 May 31, 2011 PDT ";
//echo date("Y-m-d H:i:s", strtotime("2011-05-31 20:03:06". "+ 2 week"));
if($fdao->Insert($fvo))
{
	unset($_SESSION['target_url']);
	unset($_SESSION['banner_url']);
	unset($_SESSION['purchaser_email']);
	unset($_SESSION['ban_spot']);
	unset($_SESSION['ban_weeks']);
	$url="frontadv.php?action=success";
	Redirect($url);
}
else
{
	$url="frontadv.php?action=error";
	Redirect($url);
}
	


?>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payment Successful</title>
</head>

<body>
<h1>Thank You</h1>
<p>Your payment was successful. Thank you.</p>
<?php// echo $_POST['ap_itemname'];?>
</body>
</html>-->
