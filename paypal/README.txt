June 14, 2007

Purpose of this Software
========================

The purpose of this software is to provide you the developer with an easy-to-use interface 
for accessing the Paypal Website Payment Pro Webs service using the Website Payments Pro SOAP 1.1 API.

phpPaypalPro currently supports 6 major operations available for the Website Payments Pro SOAP API namely:

(a) DoDirectPayment
(b) SetExpressCheckout
(c) GetExpressCheckoutDetails
(d) DoExpressCheckoutPayment
(e) TransactionSearch
(f) GetTransactionDetails

Please be on the lookout as more operations are scheduled to be added in the immediate future.

phpPaypalPro's easy-to-use interface allows you to easily select the operation you wish to call, set the parameters
and then execute the call in just a few lines of code. Unlike some other SDK's it allows you add Payment Items easily
when executing the DoDirectPayment or the DoExpressCheckoutPayment operations. 

List of Requirements:
=====================

1. PHP Version 5.1 or better. Though it is always better to get the lastest
version of PHP5 because of continuous upgrades to the SOAP extension.
2. On UNIX systems, this extension is only available if PHP was configured with --enable-soap.
OpenSSL also must be compiled into PHP. If you are running PHP on Windows, then the OpenSSL and SOAP extensions must be enabled
by uncommenting the appropriate .dll files in the php.ini configuration file.
3. The Webservice 3-token authentication (Username, Password, Signature) is needed.
4. If making calls on behalf of another person, then the Subject of this person is needed.

When implementing the framework, I would recommend you set max_execution_time runtime option to zero (0).
This sets the maximum time in seconds your script is allowed to run before it is terminated by the parser to unlimited.

This is necessary because sometimes, the Paypal Server may take longer than expected to respond.

This Software is released according to the revised BSD License. 

You should have received a copy of the revised BSD License along with this program.

If not please feel free to contact Israel Ekpo at perfectvista@users.sourceforge.net.


How to Interprete the Paypal Response
=====================================

The $Paypal->success() method returns true if there was no fatal error
while the application was trying to contact the paypal server. By fatal
error I mean a SoapFault being returned. $Paypal->success() can 
still return true when the response from Paypal indicates a failed transactions.
When a SoapFault is being return from the Paypal Server the $Paypal->success()
method returns false and the $Paypal->getAPIException() gets populated instead
with the results of the SoapFault.

What you have to do is to check the $Paypal->getAPIResponse() method when $Paypal->success()
is true to retrieve what $Paypal->getAPIResponse() is returning. 
Look specifically for the "Ack" property since it is an object not an array.

The Ack property returns one of the following values and these are their
meanings.

(1) Success - Request Processing Succeeded
(2) Failure - Request Processing Failed
(3) Warning - Request processing completed with warning information included in the response message
(4) SuccessWithWarning - Request processing completed with some warning information that could be useful 
rthe requesting application to process and/or record.
(5) FailureWithWarning - Request processing failed with some error 
and warnining nformation that requesting application should process to determine cause(s) of failure.

Also take a look at the error codes and thier meanings at this page
https://www.paypal.com/IntegrationCenter/ic_api-errors.html

You can also get more details about the API at:
https://www.paypal.com/IntegrationCenter/ic_pro_home.html

You should only pass the result if Ack is either Success or SuccessWithWarning. 
Anything else should be treated as an error. I hope this helps.

So what you should focus on is the value of the Ack property and not the
return value of the $Paypal->success() method.

As you can see if paypal returns no SOAPFault but the value of the '->Ack' property in the response was a
'Failure', 'Warning', or 'FailureWithWarning', $Paypal->success() will still return true.

What I think you should do will be to check the Ack property if it is not Success or SuccessWithWarning 
then send them to the failure page otherwise send them to the success page.
