<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";
$uid=$_SESSION['SES_ID'];




//get all program details
$pvo=new program_signupVO();
$pdao=new program_signupDAO();

$program_id=trim($_GET['id']);
$programdetails=$pdao->fetchDetails($program_id);

//end program details

$uVO=new siteusersVO();
$uDAO=new siteusersDAO();
$usrs_list=$uDAO->fetchAll('onlypublished');


$isPremium=$uDAO->checkPremium($uid);

if(!$isPremium){$prmchk=1;}else {$prmchk=2;}


$program_name=$programdetails->program_name;

$votvo=new program_votingVO();
$votdao=new program_votingDAO();

$spamVotes=$votdao->fetchRatingsById($program_name,"scam");
$trustfulVotes=$votdao->fetchRatingsById($program_name,"trustful");
$totalVotes=count($spamVotes)+count($trustfulVotes);

if($totalVotes == "" )
{
	$totalVotes=0;
}

$scamPercentge=@((count($spamVotes)/($totalVotes))*100);

if($scamPercentge == "")
{
	$scamPercentge=0;
}

$trustfulPercentage=@((count($trustfulVotes)/($totalVotes))*100);

if($trustfulPercentage== "")
{
	$trustfulPercentage=0;
}

//voting ends


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pending Requests</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="js/builddownlinefunctions.js"></script>
<script type="text/javascript" src="js/loadlist.js"></script>
<script type="text/javascript" src="js/ajaxeditbuilddownline.js"></script>
<script type="text/javascript" src="js/ajaxVote.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>
<!--<script type="text/javascript" src="js/exposelink.js"></script>-->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(document).ready(function(){
        $("#exposelink").validationEngine('attach');
       });
    </script>

<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>-->
<!-- inline validation ends -->
<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
 <script>
    
	
	
     
       
    function submitForm()
	{
		document.buildDownline.submit();
	}
      
    </script>
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
<form method=post name=scriptform>
  <input id=trigger type=hidden name=spacer value=TRUE>
  <input id=data type=hidden name=spacer value=spacer>
</form>
<form method=post name=surftoform></form>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1> 
          Pending Requests
		 </h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        	<div id="form-container">
            	<form  id="buildDownline" name="buildDownline" class="wufoo topLabel page"  autocomplete="off" enctype="multipart/form-data" method="post" action="" onsubmit="return false;">
                	<header id="form-header" class="info">
                    	<h2 style="font-size:110%">Below is one of your referral requests. You can update this request if you want to makes changes in it. Currently you cannot change the offer. If you do want to change the offer, a work-around would be to delete this request and add a new one. </a><br /><br />

						</h2>
                    	<div></div>
                	</header>
                	<header id="form-header" class="info">
                    	<h2>Request Details For <?php echo $programdetails->program_name; ?></h2>
                    	<div></div>
                	</header>
                    <ul>
                    	<div class="rightdiv" id="rightdiv" style="width:200px; float:right;">
                        	
                        	<div style="background-color:#6C955C; padding:5px 5px 10px 5px">
                            	<div style="padding:5px"><b>PTC SCAM Meter</b></div>
                                <div style="text-align:center; background-color:#FFFFFF; padding:5px">
                                	<div id="programname" style="font-weight:bold">
                                    	<?php echo $program_name; ?>
                                    </div>
                                	<img src="images/chart.png" />
                                    <div>
                                    	<font color="#FF0000"><b><?php echo $scamPercentge; ?>%</b> Scam</font> | <b><?php echo $trustfulPercentage; ?>%</b> Trustful<br />
                                       <font color="#999999" size="1"> (Results Based On <?php echo $totalVotes; ?> Votes)</font>
                                    </div>
                                    <div style="margin-top:5px">
                                    Want To Vote Yourself ?<br />
                                    <input type="button" value="Scam" style="color:#FF0000" onclick="vote('scam','<?php  echo $uid;?>','<?php  echo $program_name;?>');" />
                                     <input type="button" value="Trustful" style="color:#009900" onclick="vote('trustful','<?php  echo $uid;?>','<?php  echo $program_name;?>');" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    	<div class="leftdiv" style="float:left; width:300px"><!--left part ends-->
                    	<li id="foli3" 		class="complex">
                            <label class="desc" id="title3" for="Field3">
                                Referral URL:
                            </label>
                            <div>
                                <span class="full addr1">
                               <input name="referral_url" id="referral_url" type="text"  maxlength="80" value="<?php echo $programdetails->referral_url; ?>" style="width:300px"  >
                               
                                </span>
                                
                               
                            </div>
                            
                            </li>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		Banner URL:
                                </label>
                        		<div>
                            		 <input name="banner_url" id="banner_url" type="text" style=' width:300px; font-size:11px;' value="<?php echo $programdetails->banner_url; ?>" >
                        		</div>
                        	</li>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		Program language:
                                </label>
                        		<div>
                            		 <select name="program_language" id="program_language">
                                     	<option  selected="selected" value="">Select</option>
                                        <option value="english" <?php if($programdetails->program_language=="english"){ echo 'selected="selected"';} ?>>English</option>
                                        <option value="nederlands" <?php if($programdetails->program_language=="nederlands"){ echo 'selected="selected"';} ?>>Nederlands</option>
                                
                                        <option value="francais" <?php if($programdetails->program_language=="francais"){ echo 'selected="selected"';} ?>>Francais</option>
                                        <option value="deutsch" <?php if($programdetails->program_language=="deutsch"){ echo 'selected="selected"';} ?>>Deutsch</option>
                                        <option value="espagnol" <?php if($programdetails->program_language=="espagnol"){ echo 'selected="selected"';} ?>>Espagnol</option>
                                        <option value="other" <?php if($programdetails->program_language=="other"){ echo 'selected="selected"';} ?>>-Other-</option>
                                     </select>
                        		</div>
                        	</li>
                            
                             
                           
                             </div><!--left part ends-->
                           <li id="foli9" 		class="">
                        		<label class="desc" id="title9" for="Field9">
                            		Program type:
                                </label>
                        		<div>
                            		 <select  name="program_type">
                                     	<option selected="selected" value="">Select</option>
                                        <option  value="ptc" <?php if($programdetails->program_type=="ptc"){ echo 'selected="selected"';} ?>>Paid to Click</option>
                                        <option value="freeoffer" <?php if($programdetails->program_type=="freeoffer"){ echo 'selected="selected"';} ?>>Free Offer website</option>
                                
                                        <option value="downlinebuilder" <?php if($programdetails->program_type=="downlinebuilder"){ echo 'selected="selected"';} ?>>Downline Builder</option>
                                        <option value="trafficexchange" <?php if($programdetails->program_type=="trafficexchange"){ echo 'selected="selected"';} ?>>Traffic Exchange</option>
                                     </select>
                        		</div>
                        	</li>
                           
                            <div id="ptcoptions" style="background-color:#6C955C">
                            <li>
                            	
                            	<table style="font-size:10px; color:#FFFFFF">
                                	<tr>
                                    	<td colspan="4">Additional PTC Information</td>
                                    </tr>
                                    <tr>
                                    	<td>Earnings per click:</td>
                                        <td>$<input type="text" style="width:100px" name="earning_per_clk"  value="<?php echo $programdetails->earning_per_clk; ?>" /></td>
                                        <td>Instant payout:</td>
                                        <td>
                                        <select id="instant_payout" name="instant_payout">
                                            <option selected="selected" value="">select</option>
                                            <option  value="Yes" <?php if($programdetails->instant_payout=="Yes"){ echo 'selected="selected"';} ?>>Yes</option>
                                            <option value="No" <?php if($programdetails->instant_payout=="No"){ echo 'selected="selected"';} ?>>No</option>
                                        </select>
                                        </td>
                                    </tr>
                                     <tr>
                                    	<td>Earnings per referral click:</td>
                                        <td>$<input type="text" style="width:100px" name="earning_per_ref_clk" id="earning_per_ref_clk"  value="<?php echo $programdetails->earning_per_ref_clk; ?>"/></td>
                                        <td>Payment processor:</td>
                                        <td>
                                        <select id="payment_processor" name="payment_processor">
                                            <option selected="selected" value="">Select</option>
                                            <option  value="alertpay" <?php if($programdetails->payment_processor=="alertpay"){ echo 'selected="selected"';} ?>>Alertpay</option>
                                            <option value="paypal" <?php if($programdetails->payment_processor=="paypal"){ echo 'selected="selected"';} ?>>Paypal</option>
                                        </select>
                                        </td>
                                    </tr>
                                     <tr>
                                    	<td>Payout minimum:</td>
                                        <td>$<input type="text" style="width:100px" name="min_payout" id="min_payout"  value="<?php echo $programdetails->min_payout; ?>"/></td>
                                        <td>Downline levels:</td>
                                        <td>
                                        <select id="downline_level" name="downline_level">
                                            <option  selected="selected" value="">Select</option>
                                            <option value="1" <?php if($programdetails->downline_level=="1"){ echo 'selected="selected"';} ?>>1</option>
                                            <option value="2" <?php if($programdetails->downline_level=="2"){ echo 'selected="selected"';} ?>>2</option>
                                            <option value="3" <?php if($programdetails->downline_level=="3"){ echo 'selected="selected"';} ?>>3</option>
                                            <option value="4" <?php if($programdetails->downline_level=="4"){ echo 'selected="selected"';} ?>>4</option>
                                            <option value="5" <?php if($programdetails->downline_level=="5"){ echo 'selected="selected"';} ?>>5</option>
                                            <option value="6" <?php if($programdetails->downline_level=="6"){ echo 'selected="selected"';} ?>>6</option>
                                            <option value="7" <?php if($programdetails->downline_level=="7"){ echo 'selected="selected"';} ?>>7</option>
                                            <option value="8" <?php if($programdetails->downline_level=="8"){ echo 'selected="selected"';} ?>>8</option>
                                            <option value="9" <?php if($programdetails->downline_level=="9"){ echo 'selected="selected"';} ?>>9</option>
                                            <option value="10" <?php if($programdetails->downline_level=="10"){ echo 'selected="selected"';} ?>>10</option>
                                        </select>
                                        </td>
                                    </tr>
                                </table>
                                
                            </li>
                            </div>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		Contract duration:
                                </label>
                        		<div>
                            		 <input type="text" name="contract_duration" id="contract_duration" style="width:50px" value="<?php echo $programdetails->contract_duration; ?>" />days (Minimum is 3 days. Maximum is 120 days.)
                        		</div>
                        	</li>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		Spend credits:<span style="font-size:10px; font-weight:normal"></span>
                                </label>
                        		<div class=b><input style='width:50px;' readonly="readonly" type=text name=spend id=spend class=credits value="<?php echo $programdetails->spend_credits; ?>">&nbsp;credits</div>
                        	</li>
                            <li>
                            	<table style="font-size:10px">
                                	
                                	<tr>
                                    	<td>Discount:</td>
                                        <td>
                                        	<div class=fl style='width:54px; text-align:center; border-bottom:1px solid #000000; float:left' id=discountspan><?php echo $programdetails->discount; ?></div><div style="vertical-align:top; width:100px; float:left">credits</div>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td>Total charge:</td>
                                        <td valign="top"><div class=fl style='width:54px; text-align:center; font-weight:bold; float:left' id=totalchargespan><?php echo $programdetails->total; ?></div><div style="vertical-align:top; width:100px; float:left; font-weight:bold">credits</div></td>
                                    </tr>
                                     <tr>
                                    	<td><b>Offer:</b></td>
                                        <td valign="top"><div class=fl style='width:54px; text-align:center; font-weight:bold; float:left' id=totalchargespan><?php echo formatDoubleAmount((60/100)*$programdetails->spend_credits);?></div><div style="vertical-align:top; width:100px; float:left; font-weight:bold">credits</div></td>
                                    </tr>
                                </table>
                            </li>
                            <li>
                            	<span id="validateresult"></span>
                            </li>
                           
                             <li id="foli111" class="">
								<label class="desc" id="title111" for="Field111">
                            	Task description
                               <span style="font-size:10px; font-weight:normal">(Enter a brief description of what you expect your referral to do after signing up (optional)).</span>

                                    </label>
                            
                            	<div>
                                    <textarea id="task" name="task" class="field textarea medium" spellcheck="true" rows="10" cols="50" ><?php echo $programdetails->task_desc; ?></textarea>
                                </div>
                            
                            </li>
                            
                            <li id="foli111" class="">
								<label class="desc" id="title111" for="Field111">
                            	Premium Member options
                               

                                    </label>
                            
                            	
                                	<table style="font-size:10px">
                                    	<tr>
                                        	<td colspan="2">Rating:</td>
                                        </tr>
                                        <tr>
                                        	<td>&nbsp;</td>
                                            <td>
                                                    Only members with a referral rating of
                                                 <!--   <input type="text" name="rating" <?php// if(!$isPremium){ ?> disabled="disabled" <?php// } ?> style="width:100px" />-->
                                                <select name="rating" <?php if(!$isPremium){ ?> disabled="disabled" <?php } ?>>
                                                	<option value="">select</option>
                                                    <option value="10" <?php if($programdetails->allowed_mem_rat=="10"){ echo 'selected="selected"';} ?>>+10</option>
                                                    <option value="9" <?php if($programdetails->allowed_mem_rat=="9"){ echo 'selected="selected"';} ?>>+9</option>
                                                    <option value="8" <?php if($programdetails->allowed_mem_rat=="8"){ echo 'selected="selected"';} ?>>+8</option>
                                                    <option value="7" <?php if($programdetails->allowed_mem_rat=="7"){ echo 'selected="selected"';} ?>>+7</option>
                                                    <option value="6" <?php if($programdetails->allowed_mem_rat=="6"){ echo 'selected="selected"';} ?>>+6</option>
                                                     <option value="5" <?php if($programdetails->allowed_mem_rat=="5"){ echo 'selected="selected"';} ?>>+5</option>
                                                    <option value="4" <?php if($programdetails->allowed_mem_rat=="4"){ echo 'selected="selected"';} ?>>+4</option>
                                                     <option value="3" <?php if($programdetails->allowed_mem_rat=="3"){ echo 'selected="selected"';} ?>>+3</option>
                                                      <option value="2" <?php if($programdetails->allowed_mem_rat=="2"){ echo 'selected="selected"';} ?>>+2</option>
                                                      <option value="1" <?php if($programdetails->allowed_mem_rat=="1"){ echo 'selected="selected"';} ?>>+1</option>
                                                       <option value="-5" <?php if($programdetails->allowed_mem_rat=="-5"){ echo 'selected="selected"';} ?>>-5</option>
                                                       <option value="-4" <?php if($programdetails->allowed_mem_rat=="-4"){ echo 'selected="selected"';} ?>>-4</option>
                                                       <option value="-3" <?php if($programdetails->allowed_mem_rat=="-3"){ echo 'selected="selected"';} ?>>-3</option>
                                                       <option value="-2" <?php if($programdetails->allowed_mem_rat=="-2"){ echo 'selected="selected"';} ?>>-2</option>
                                                       <option value="-1" <?php if($programdetails->allowed_mem_rat=="-1"){ echo 'selected="selected"';} ?>>-1</option>
                                                      
                                                </select>
                                                or more are allowed to sign up.
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td colspan="2">Prevent sign-ups from:</td>
                                        </tr>
                                        <tr>
                                        	<td>&nbsp;</td>
                                            <td>
                                             <select name="insert" style="min-width:200px; vertical-align:top" <?php if(!$isPremium){ ?> disabled="disabled" <?php } ?>>
                                             	<?php
													foreach($usrs_list as $value)
													{
														?>
                                                        <option><?php echo $value->username; ?></option>
                                                        <?php
													}
												?>
                                             </select>
                                               
                                              <!--  <img align="top" src="images/icon_add_gray.png" title="add" />-->
                                              <input type="button" value="Insert"
 onclick=" insertOldSchool(this.form.not_allowed_mem,this.form.insert.value, this.form.insert.value);" style="vertical-align:top" <?php if(!$isPremium){ ?> disabled="disabled" <?php } ?> />
                                               
                                               <select name="not_allowed_mem" size="5" multiple="multiple" style="min-width:200px; min-height:100px" <?php if(!$isPremium){ ?> disabled="disabled" <?php } ?> >
                                               <?php 
											    $notAllowedList=explode(',',$programdetails->not_allowed_mem);
												foreach($notAllowedList as $key=>$value)
												{
													echo "<option value=\"$value\">$value</option>";
												}
												?>
                                                   
                                              </select>
                                                    <input type="button" value="Remove"
                                                     onclick="removeOldSchool(this.form.not_allowed_mem);"  <?php if(!$isPremium){ ?> disabled="disabled" <?php } ?>/>
                                            </td>
                                        </tr>
                                    </table>
                                	
                                	
                                    
                               
                            
                            </li>

                           
                            <li class="buttons ">
                            <div>
                            			<input type="hidden" name="programid" id="programid" value="<?php echo $programdetails->program_id; ?>" />
                                      <input type="submit"  name="submit" value="submit"  onclick="downlineEditValidate()" />
                                                
                                    
                           </div>
                        </li>
                    </ul>
                   
                </form>
                
                <script>getObject('referral_url').focus();</script>
            </div>
        
       		
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->

</body>
</html>
