<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";


$sellCreditVO=new sell_creditsVO();
$sellCreditDAO=new sell_creditsDAO();

$allCredits=$sellCreditDAO->fetchAll('onlypublished');

$minCreditPerClick=getCodeValueByLabel("min_credits_per_adv_click");
$minPackageSize=getCodeValueByLabel("min_adv_package_size");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Expose Your Link</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="js/ajaxExposeLink.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>
<script type="text/javascript" src="js/exposelink.js"></script>
   

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(document).ready(function(){
        $("#exposelink").validationEngine('attach');
       });
    </script>

<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>-->
<!-- inline validation ends -->
<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>

<script type="text/javascript">
	 
      
     
      
      function update() {
          var size = getObject('packagesize').value;
          var worth = getObject('worth').value;
          if(!isDecimal(size) || !isDecimal(worth)) {
              getObject('spansubtotal').innerHTML='0.0';
              getObject('spanfees').innerHTML='0.0';
              getObject('spantotal').innerHTML='0.0';
              return;
          }
          
          var subtotal = parseFloat(size) * parseFloat(worth);
          var fees = parseFloat(subtotal * 0.2);
          var total = parseFloat(subtotal + fees);
          
          getObject('transactionoverview').style.visibility='visible';
          getObject('spansubtotal').innerHTML = number_format(subtotal,4);
          getObject('spanfees').innerHTML = number_format(fees,4);
          getObject('spantotal').innerHTML = number_format(total,4)+' credits';
      }
      
      function example() {
          getObject('worth').value = '0.10';
          getObject('text1').value = 'Brand new PTC';
          getObject('text2').value = 'Join this PTC now, serving over 8 ads per day!';
          getObject('websiteURL').value = 'http://www.neobux.com/?ref=username';
          getObject('packagesize').value = '1000';
          update();
      }
      
      function doclear() {
          getObject('worth').value = '';
          getObject('text1').value = '';
          getObject('text2').value = '';
          getObject('websiteURL').value = 'http://';
          getObject('packagesize').value = '';
          update();
          getObject('text1').focus();
      }      
</script>
<!-- CSS -->
<link rel="stylesheet" href="css/structure.css" type="text/css" />
<link rel="stylesheet" href="css/form.css" type="text/css" />

<!-- JavaScript -->
<script type="text/javascript" src="js/wufoo.js"></script>
<form method=post name=scriptform>
  <input id=trigger type=hidden name=spacer value=TRUE>
  <input id=data type=hidden name=spacer value=spacer>
</form>
<form method=post name=surftoform></form>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          <h1> 
          	Expose Your Link
		 </h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        	<div id="form-container">
            	<form  id="exposelink" name="exposelink" class="wufoo topLabel page"  autocomplete="off" enctype="multipart/form-data" method="post" action="exposelinkvalidate.php" onsubmit="return false;">
                	<header id="form-header" class="info">
                    	<h2 style="font-size:110%">On many PTC sites you have to pay money for advertising your banner or program on their website. Here we allow you to add advertisement packages using your credits. Click 'fill out an example' on the right to see how you should submit this form. Your advertising package will be online immediately after pressing the Submit Package button. Your ads will be shown at our Click <a href="advertisment.php">Advertisements page.</a><br /><br />

<font color="#FF0000">Hint!</font> The more credits you offer, the higher you will end up in the advertisement list!
						</h2>
                    	<div></div>
                	</header>
                	<header id="form-header" class="info">
                    	<h2>Submit Your Advertisment Package</h2>
                    	<div></div>
                	</header>
                    
                    <ul>
                    	<li>
                        	<span><a style="cursor:pointer; color:#0000FF; text-decoration:underline"  onclick="example();">fill out an example</a></span><span style="margin-left:10px"><a style="cursor:pointer;color:#0000FF; text-decoration:underline" onclick="doclear();">clear</a></span>
                        </li>
                    	<li id="foli3" 		class="complex      ">
                            <label class="desc" id="title3" for="Field3">
                                1 Fill out your advertisment
                            </label>
                            <div>
                                <span class="full addr1">
                               <input name="text1" id="text1" type="text" style='width:100%;' maxlength="80" class="validate[required] ">
                                <label for="Field3"></label>
                                </span>
                                <span class="full addr2">
                                <input name="user" id="text2" type="text" style='width:100%; font-size:11px;' maxlength="80"  class="validate[required]" >
                                <label for="Field4">Advertisement</label>
                                </span>
                                <span class="full addr2">
                                 <input type="text" id="worth" name="worth" style='width:50px; font-size:11px;' onkeyup='parsecredits(this); update();' onblur='update();' class="validate[required,custom[number],min[0.70]]"  />Credits: number of credits per click, minimum=<?php echo formatDoubleAmount($minCreditPerClick); ?> credits
                              
                                </span>
                               
                            </div>
                            
                            </li>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		2. Enter the target URL or your referral URL
                                </label>
                        		<div>
                            		 <input name="websiteURL" id="websiteURL" type="text" style='width:400px; font-size:11px;' value='http://' class="validate[required,custom[url]] ">
                        		</div>
                        	</li>
                            <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		3. Indicate your package size
                                </label>
                        		<div>
                            		<input  id="packagesize" name="packagesize" type=text onkeyup='parsecredits(this); update();' onblur='update();'  	value=""  class="validate[required,custom[integer],min[100]]" style="width:50px"  /> 
                                    <label for="Field3">clicks, minimum=<?php echo $minPackageSize; ?> clicks </label>
                        		</div>
                        	</li>
                            <li id="foli16" 		class="     ">
                            <fieldset>
                            <![if !IE | (gte IE 8)]>
                            
                            <![endif]>
                            <!--[if lt IE 8]>
                            <label id="title16" class="desc">
                                Requirements
                                    </label>
                            <![endif]-->
                            <div>
                                <span>
                            <input id="Field16" 		name="agree1" 		type="checkbox" 		class="validate[required]" 		value="Frame"/>
                            <label class="choice" for="Field16">I declare this URL does not contain a framebreaker.</label>
                            </span>
                                <span>
                            <input id="Field17" 		name="agree2" 		type="checkbox" 		class="validate[required]" 		value="Declare" style="margin-top:10px" />
                            <label class="choice" for="Field17">I declare this URL does not contain inappropriate or abusive content.</label>
                            </span>
                                
                                </div>
                            </fieldset>
                            </li>
                             <li id="foli9" 		class="     ">
                        		<label class="desc" id="title9" for="Field9">
                            		Transaction Overview
                                </label>
                        		<div>
                                	 <span id=transactionoverview>        
              <table border=0>
                <tr class=nowrap><td width=85><span class="disabledbold" style="font-size:12px; font-weight:bold">Sub total</span></td><td><span id=spansubtotal>0.0</span></td></tr>
                <tr class=nowrap><td><span class="disabledbold" style="font-size:12px; font-weight:bold">Upline fees</span></td><td><span id=spanfees style='border-bottom:1px solid #c0c0c0;'>0.0</span></td></tr>
                <tr class=nowrap><td><span class="disabledbold" style="font-size:12px; font-weight:bold">Total</span></td><td><b><span id="spantotal">0.0</span></b></td></tr>

              </table>
            </span>
                            		
                                     
                        		</div>
                        	</li>
                            <li>
                            	<span id="validateresult"></span>
                            </li>
                            <li class="buttons ">
                            <div>
                                      <input type="submit"  name="submit" value="submit"  onclick="exposeValidate()"  />
                                                
                                    
                           </div>
                        </li>
                    </ul>
                    
                   
                </form>
                <script>getObject('text1').focus();</script>
            </div>
        
       		
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->

</body>
</html>
