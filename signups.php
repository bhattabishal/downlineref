<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/countdown.class.php";
require_once "logincheck.php";

if(isset($_GET['pid']) && $_GET['pid']!= "")
{
	$pid=$_GET['pid'];
}

if(isset($_SESSION['SES_ID']) && $_SESSION['SES_ID'] != "")
{
	$user_id=$_SESSION['SES_ID'];
	
	$pVO=new program_signup_childVO();
	$pDAO=new program_signup_childDAO();
	$allPrograms=$pDAO->fetchAllByProgramId($publish='all',$pid);
	
}




//fetch all signups 







?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Validate Program Sign-ups</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<link rel="stylesheet" type="text/css" href="css/sortertable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>

<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft" style="margin:5px; width:940px">
     	<div class="block" style="width:940px">
        <!-- Ttle (Start) -->
        <div class="ttle" style="background:url(images/h1bg2.jpg) no-repeat; width:940px">
          	<h1> 
				Validate Program Sign-ups
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        
        
        
        <!--table starts-->
        <div id="tablewrapper">
		<div id="tableheader">
        	<div class="search">
                <select id="columns" onchange="sorter.search('query')"></select>
                <input type="text" id="query" onkeyup="sorter.search('query')" />
            </div>
            <span class="details">
				<div>Records <span id="startrecord"></span>-<span id="endrecord"></span> of <span id="totalrecords"></span></div>
        		<div><a href="javascript:sorter.reset()">reset</a></div>
        	</span>
        </div>
        <table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
            <thead>
                <tr>
                    <th class="nosort"><h3>Signup date</h3></th>
                    <th><h3>Program Name </h3></th>
                    <th><h3>User Name</h3></th>
                    <th><h3>Contract earnings </h3></th>
                    <th><h3>Days left  </h3></th>
                   
                </tr>
            </thead>
            <tbody>
            <?php
				foreach($allPrograms as $value)
				{
				
					//signup program details
					$pvo=new program_signupVO();
					$pdao=new program_signupDAO();
					
					$programdetails=$pdao->fetchDetails($value->program_id);
					
					//userdetails
					$svo=new siteusersVO();
					$sdao=new siteusersDAO();
					$userinfo=$sdao->fetchDetails($value->child_user_id);
					$mem_name=$userinfo->username;
					$mem_type=$userinfo->mem_type;
			?>
        
                <tr>
                    <td><a href="validatesignups.php?sid=<?php echo $value->id; ?>"  title="validate signup" style="color:#006600; cursor:pointer"><?php echo $value->created_date; ?></a>
                    </td>
                    <td><a href="validatesignups.php?sid=<?php echo $value->id; ?>"  title="validate signup" style="color:#006600; cursor:pointer"><?php echo $programdetails->program_name; ?></a></td>
                    <td>
                  <a href="validatesignups.php?sid=<?php echo $value->id; ?>"  title="validate signup" style="color:#006600; cursor:pointer">
					<?php echo $mem_name; ?>
                    <?php
                                    if($mem_type==1)
                                    {
                                        echo '<img src="images/premium.png" align="bottom" alt="Premium Member" title="Premium Member" />';
                                        
                                    }
                                    else
                                    {
                                        echo '<img src="images/free.png" align="bottom" alt="Standard Member" title="Standard Member" />';
                                    }
                                ?>
                                </a>
                    </td>
                    <td><a href="validatesignups.php?sid=<?php echo $value->id; ?>"  title="validate signup" style="color:#006600; cursor:pointer"><?php echo formatDoubleAmount((60/100)*$programdetails->spend_credits); ?> Credits</a></td>
                     <td><a href="validatesignups.php?sid=<?php echo $value->id; ?>"  title="validate signup" style="color:#006600; cursor:pointer"><b>
                     	<?php
							switch($value->status)
							{
								case 0:
								$status="Request Pending";
								break;
								
								case 1://means accepted.now calculate the time left
								$pvvo=new program_signup_validityVO();
								$pvdao=new program_signup_validityDAO();
								$validitydetails=$pvdao->fetchSignupDetailsByUserId($value->child_user_id,$value->id);
								$date=$validitydetails->valid_to;
								$dateParts=dateParseFromFormat("Y-m-d H:i:s", $date);
								
								$CountDown = new CountDown;
								
								$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);
								
								
								$CountDown->format = "Weeks: {W} Days: {D} Hours: {H}";
								
								
								//if($out==0)
								//{
									//changeProgramSignUpStatus($value->id,'3');
									
								//}
								$status=$out;
								break;
								
								case 2:
								$status="declined";
								break;
								
								case 3:
								$status="completed";
								break;
								
								case 4:
								$status="terminated";
								break;
								
								default:
								$status="n/a";
								
							}
							
							echo $status;
						?></b></a>
                     </td>
                  
                </tr>
              
              <?php
			  	}
			  ?>
            </tbody>
        </table>
        <div id="tablefooter">
          <div id="tablenav">
            	<div>
                    <img src="images/shortertable/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/shortertable/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/shortertable/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/shortertable/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div>
                	<select id="pagedropdown"></select>
				</div>
                <div>
                	<a href="javascript:sorter.showall()">view all</a>
                </div>
            </div>
			<div id="tablelocation">
            	<div>
                    <select onchange="sorter.size(this.value)">
                    <option value="5">5</option>
                        <option value="10" selected="selected">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div>
            </div>
        </div>
    </div>


	<script type="text/javascript" src="js/sorterTable/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:7,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		//sum:[8],
		//avg:[6,7,8,9],
		//columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
           <!--table end-->
           <div class="buttons">
           
                	<button type="button" class="regular" name="Back" value="Back" onclick="location.href='pendingRequest.php'"><img src="images/arrow_back.png" alt=""/>Back</button>
               
           </div>
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
           <div style="clear:both"></div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			//include_once("includes/rightcolumn.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
