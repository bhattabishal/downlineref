<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/countdown.class.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];
$signup_id=trim(intval($_GET['sid']));
$mr=trim($_GET['t']);
//fetch signup details
$svo=new program_signup_childVO();
$sdao=new program_signup_childDAO();

$signupdetails=$sdao->fetchDetails($signup_id);
$program_id=$signupdetails->program_id;
$child_id=$signupdetails->child_user_id;

//signup program details
$pvo=new program_signupVO();
$pdao=new program_signupDAO();

$programdetails=$pdao->fetchDetails($program_id);


//parent userdetails
$suvo=new siteusersVO();
$sudao=new siteusersDAO();
$userinfo=$sudao->fetchDetails($signupdetails->child_user_id);
$mem_name=$userinfo->username;
$mem_type=$userinfo->mem_type;





//voting starts
$votvo=new program_votingVO();
$votdao=new program_votingDAO();

$spamVotes=$votdao->fetchRatingsById($programdetails->program_name,"scam");
$trustfulVotes=$votdao->fetchRatingsById($programdetails->program_name,"trustful");
$totalVotes=count($spamVotes)+count($trustfulVotes);

if($totalVotes == "" )
{
	$totalVotes=0;
}

$scamPercentge=@((count($spamVotes)/($totalVotes))*100);

if($scamPercentge == "")
{
	$scamPercentge=0;
}

$trustfulPercentage=@((count($trustfulVotes)/($totalVotes))*100);

if($trustfulPercentage== "")
{
	$trustfulPercentage=0;
}

//voting ends


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Validate Sign-ups</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<!--<script type="text/javascript" src="js/ajaxVote.js"></script>-->
<script type="text/javascript" src="js/ajaxvalidatesignups.js"></script>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<script>
	function validaterequest(x,y)
	{
		var a;
		if(y=="accept")
		{
			a=confirm("Are You Sure You Want To Accept This Signup ?");
		}
		else if(y=="decline")
		{
			a=confirm("Are You Sure You Want To Decline This Signup ?");
		}
		
		if(a==true)
		{
			//alert("callajax");
			acceptrequest(x,y);
		}
	}
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Validate Sign-ups
        	</h1>
        </div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
      
        
        <!--table starts-->
        
        	<div class="container-table">
                    <div class="wrap-table"> 
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                   
                                     <td class="r_name no_line">Contract details</td>
                                      <td class="r_name no_line">&nbsp;</td>
                                </tr>
                                <tr>
                                	
                                    <td>
                                    	User
                                    </td>
                                    <td>
                                    	<?php echo $mem_name; ?>
										<?php
                                            if($mem_type==1)
                                            {
                                                echo '<img src="images/premium.png" align="bottom" alt="Premium Member" title="Premium Member" />';
                                                
                                            }
                                            else
                                            {
                                                echo '<img src="images/free.png" align="bottom" alt="Standard Member" title="Standard Member" />';
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td>Program Name</td>
                                    <td><?php  echo $programdetails->program_name;?></td>
                                </tr>
                                <tr>
                                	<td>Signup Date</td>
                                    <td><?php echo $signupdetails->created_date; ?></td>
                                </tr>
                                <tr>
                                	<td>Contract Status</td>
                                    <td>
                                    <?php
							switch($signupdetails->status)
							{
								case 0:
								$status="Request Pending";
								break;
								
								case 1://means accepted.now calculate the time left
								//now for earned credits of program signup
								$avo=new user_accountVO();
								$adao=new user_accountDAO();
								
								
								
								
								
								$pvvo=new program_signup_validityVO();
								$pvdao=new program_signup_validityDAO();
								$validitydetails=$pvdao->fetchSignupDetailsByUserId($child_id,$signupdetails->id);
								$valid_to=$validitydetails->valid_to;
								$valid_from=$validitydetails->valid_from;
								
								$earnedtillnow=$adao->fetchTotalSignupCreditsByTypeBetweenDates($child_id,$signup_id,$type='program signup',$valid_from,$valid_to);
								
								$dateParts=dateParseFromFormat("Y-m-d H:i:s", $valid_to);
								
								$CountDown = new CountDown;
								
								$out = $CountDown->Give($dateParts['hour'],$dateParts['minute'],$dateParts['second'],$dateParts['month'],$dateParts['day'],$dateParts['year']);
								
								
								$CountDown->format = "Weeks: {W} Days: {D} Hours: {H}";
								
								
								if($out=='0')
								{
									//changeProgramSignUpStatus($signupdetails->id,'3');
									$status="completed";
									
								}
								else
								{
									$status=$out;
								}
								
								$signupaccepteddate=$valid_from;
								
								
								break;
								
								case 2:
								$status="declined";
								break;
								
								case 3:
								$status="completed";
								break;
								
								case 4:
								$status="terminated";
								break;
								
								default:
								$status="n/a";
								
							}
							
							echo $status;
						?>
                                    </td>
                                </tr>
                                <tr>
                                	<td>Contract Duration</td>
                                    <td><?php  echo $programdetails->contract_duration;?> days</td>
                                </tr>
                                <?php if(isset($signupaccepteddate) && $signupaccepteddate!=""){ ?>
                                <tr>
                                	<td>Contract Start Date</td>
                                    <td><?php echo $signupaccepteddate; ?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                	<td>Contract End Date</td>
                                    <td><?php if($valid_to==""){ echo "n/a";}else{ echo $valid_to;} ?></td>
                                </tr>
                                <tr>
                                	<td>Task Description</td>
                                    <td>
                                    <textarea readonly="readonly"><?php echo $programdetails->task_desc; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                	<td class="r_name">
                                    username and/or email address used by the user to sign-up? 
                                    </td>
                                    <td class="r_name">
                                    	<input type="text" value="<?php echo $signupdetails->email; ?>" readonly="readonly" />
                                        <input type="hidden" value="0" name="pagereload" id="pagereload"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="r_name">Earnings</td>
                                </tr>
                                <tr>
                                	<td>Earned</td>
                                    <td>
                                    <?php
									echo formatDoubleAmount($earnedtillnow);
									?>
                                     out of <?php echo formatDoubleAmount((60/100)*$programdetails->total); ?> Credits</td>
                                  <!--  fetchTotalSignupCreditsByTypeBetweenDates($user_id,$txnid,$type='program signup',$from,$to)-->
                                </tr>
                                <tr>
                                	<td>Reserved</td>
                                    <td><?php echo formatDoubleAmount((60/100)*$programdetails->total-$earnedtillnow); ?> Credits</td>
                                </tr>
                                <tr>
                                	<td>Total</td>
                                    <td><?php echo formatDoubleAmount((60/100)*$programdetails->total); ?> Credits</td>
                                </tr>
                                <tr>
                                	<td colspan="2" class="r_name">
                                      <!--for ajax message-->
                                    <div id="result">
                                    </div>
                                    <div id="validateresult"></div>
                                    <?php
									if($signupdetails->status==0)
									{
									?>
                                    <div class="buttons" style="float:left">
           
                                        <button type="button" class="regular" name="Accept" value="Accept" onclick="validaterequest(<?php echo $signupdetails->id; ?>,'accept');" ><img src="images/icon_msg.png" alt=""/>Accept Request</button>
                                        
                                         <button type="button" class="regular" name="Decline" value="Decline" onclick="validaterequest(<?php echo $signupdetails->id; ?>,'decline');" ><img src="images/icon_status_declined.png" alt=""/>Decline Request</button>
                                   
                               </div>
                               <?php } ?>
                                    </td>
                                </tr>
                               <!-- <tr>
                                    <td colspan="2" class="r_name">Output File Formats Supported</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Videos</td>
                                    <td width="520">MP4, MPEG-4, H.264, HD H.264</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Audios</td>
                                    <td width="520">MP3, M4A, AAC</td>
                                </tr>
                                <tr>
                                    <td class="c_left">Images</td>
                                    <td width="520">BMP, GIF, JPEG, PNG</td>
                                </tr>-->
                            </tbody>
                        </table> 
                        		
                        </div>
                </div>
           <!--table end-->
        	 <div class="buttons" style="float:right">
           
                                        <button type="button" class="regular" name="Back" value="Back" onclick="location.href='signups.php?pid=<?php echo $program_id; ?>'" ><img src="images/arrow_back.png" alt=""/>Back</button>
                                   
                               </div>
    	<!--inner content close-->
      	</div>
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
