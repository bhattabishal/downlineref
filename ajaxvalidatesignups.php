<?php
session_start();
include_once("common/functions.php");
include_once("common/common_functions.php");
//include_once("common/class.amh_iri.php");
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
$uid=$_SESSION['SES_ID'];
$signup_id=trim($_GET['signup_id']);
$type=trim($_GET['type']);

$pvo=new program_signupVO();
$pdao=new program_signupDAO();



$vo=new program_signup_childVO();
$dao=new program_signup_childDAO();

$details=$dao->fetchDetails($signup_id);
$child_user_id=$details->child_user_id;
$program_id=$details->program_id;
$status=$details->status;




//if accepted
if($status==0)
{
$curDate=date("Y-m-d H:i:s");
	if($type=="accept")
	{
		//change the status as accepted   
		if($dao->ActivateNdactivate($signup_id,$status=1))
		{
		
		
			
			
			//set now details of validity of that signup and increase  ratings
			$pvo=new program_signupVO();
			$pdao=new program_signupDAO();
			
			$programdetails=$pdao->fetchDetails($program_id);
			
			$contract_duration=$programdetails->contract_duration;
			
			$curDate=date("Y-m-d H:i:s");
			
			$vvo=new program_signup_validityVO();
			$vdao=new program_signup_validityDAO();
			$vvo->signed_user_id=$child_user_id;
			$vvo->program_signup_id=$signup_id;
			$vvo->valid_from=$curDate;
			$vvo->valid_to=date("Y-m-d H:i:s", strtotime("+$contract_duration days"));
			
			
			
			$vdao->Insert($vvo);
			
			
			//send message to the referral about his request accepted
				$mvo=new messageVO();
				$mdao=new messageDAO();
				$svo=new siteusersVO();
				$sdao=new siteusersDAO();

				$userinfo=$sdao->fetchDetails($promotor_id);
				$mem_name=$userinfo->username;
				$program_name=$programdetails->program_name;
				
				
				
				$mvo->message_from=0;
				$mvo->message_to=$child_user_id;
				$mvo->message_subject="Request Accepted";
				
				$mvo->created_date=GetThisDate("Y-m-d G:i:s");
				$mvo->message_details="Hello $mem_name,"."<br />" ."Your Request has been accepted for program "."<b>". $program_name."</b>" ." on $mvo->created_date .you will receive an equal share of credits for the duraction of the contract.Click on "."<a href=\"myProgramSignUp.php\">my program signup</a>"." "."link from dashboard to view your credit transferred details";
				$mvo->status=1;
				$mdao->Insert($mvo);
			
			//message ends
			
			//rating
			$rvo=new votingVO();
			$rdao=new votingDAO();
			
			//promotor rating +1
			$rvo->vote_by=$child_user_id;
			$rvo->vote_to=$uid;
			$rvo->vote_type="promotor";
			$rvo->vote_value="accept";
			$rvo->vote=1;
			$rvo->vote_for=$signup_id;
			$rvo->created_date=$curDate;
			
			if($rdao->Insert($rvo))
			{
				//referral rating +1
				$rvo->vote_by=$uid;
				$rvo->vote_to=$child_user_id;
				$rvo->vote_type="referral";
				$rvo->vote_value="accept";
				$rvo->vote=1;
				$rvo->vote_for=$signup_id;
				$rvo->created_date=$curDate;
				
				$rdao->Insert($rvo);
				
			}
			
			echo '<div style=" padding:10px;margin:3px; color:#FFFFFF; background-color:#6C955C; font-weight:bold; margin-top:10px">You Have Accepted The Request</div>';
			
		}
		else
		{
			
			echo '<div style=" padding:10px;;margin:3px; color:#FFFFFF; background-color:#6C955C; font-weight:bold; margin-top:10px">Error occured on accepting request</div>';
		}
	}
	elseif($type=="decline")//if declined
	{
		$changepromotorstatus=true;
		$changereferralstatus=true;
		//change the status as declined   
		if($dao->ActivateNdactivate($signup_id,$status=2))
		{
		
			//set program id to 1 i.e active
			$pvo=new program_signupVO();
			$pdao=new program_signupDAO();
			
			$pdao->ActivateNdactivate($program_id,1);
			
			
			
			//check promotor rating
			$prating=getPromotorRating($uid);
			$rrating=getReferralRating($child_user_id);
			
			
			
			//check premium
			//isPremium($uid);
			
			//do not change promotor rating if
			if((isPremium($uid) && $prating <= 0 ) || $rrating < 0)
			{
				$changepromotorstatus=false;
			}
			
			//do not change referral rating if
			if($prating < 0 || (isPremium($child_user_id) && $rrating <= 0))
			{
				$changereferralstatus=false;
			}
			
			if($changepromotorstatus==true)
			{
				//rating
				$rvo=new votingVO();
				$rdao=new votingDAO();
				
				//promotor rating -1
				$rvo->vote_by=$child_user_id;
				$rvo->vote_to=$uid;
				$rvo->vote_type="promotor";
				$rvo->vote_value="decline";
				$rvo->vote_for=$signup_id;
				$rvo->vote=1;
				$rvo->created_date=$curDate;
				$rdao->Insert($rvo);
			}
			else
			{
				//rating
				$rvo=new votingVO();
				$rdao=new votingDAO();
				
				//promotor rating -1
				$rvo->vote_by=$child_user_id;
				$rvo->vote_to=$uid;
				$rvo->vote_type="promotor";
				$rvo->vote_value="decline";
				$rvo->vote_for=$signup_id;
				$rvo->vote=0;
				$rvo->created_date=$curDate;
				$rdao->Insert($rvo);
			}
			
			if($changereferralstatus==true)
			{
				//rating
				$rvo=new votingVO();
				$rdao=new votingDAO();
				
				//referral rating -1
				$rvo->vote_by=$uid;
				$rvo->vote_to=$child_user_id;
				$rvo->vote_type="referral";
				$rvo->vote_value="decline";
				$rvo->vote=1;
				$rvo->vote_for=$signup_id;
				$rvo->created_date=$curDate;
				$rdao->Insert($rvo);
			}
			else
			{
				//rating
				$rvo=new votingVO();
				$rdao=new votingDAO();
				
				//referral rating -1
				$rvo->vote_by=$uid;
				$rvo->vote_to=$child_user_id;
				$rvo->vote_type="referral";
				$rvo->vote_value="decline";
				$rvo->vote=0;
				$rvo->vote_for=$signup_id;
				$rvo->created_date=$curDate;
				$rdao->Insert($rvo);
			}
			
			
			echo '<div style=" padding:10px; color:#FFFFFF; background-color:#6C955C; font-weight:bold; margin-top:10px">Request Declined</div>';
		}
		else
		{
			echo '<div style=" padding:10px; color:#FFFFFF; background-color:#6C955C; font-weight:bold; margin-top:10px">Error occured on declining request</div>';
			
		}
	}


}
else
{
	echo '<div style=" padding:10px;;margin:3px; color:#FFFFFF; background-color:#6C955C; font-weight:bold; margin-top:10px">You Have Already Validated The Request</div>';
}




?>

