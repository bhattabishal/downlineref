<?php

include_once "../".DIR_INCLUDES."/functions.php";
$delId=$_GET['upId'];
$upvo = new UserPaymentVO();
$updao = new UserPaymentDAO();

	
$count = 1;
	if(isset($_GET['upId']) && intval($_GET['upId'])!=0)
		{
		$rUP = new UserPaymentDAO();
		$flag = $rUP->remove($_GET['upId']);
		if($flag)
			$msg = "Selected User Payment has been removed successfully.";
		else
			$msg = "Some error prevented User Payment from being removed.";
		}

	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<?php 

$year=$_REQUEST['year'];
$month=$_REQUEST['month'];
?>
<script language="javascript">
function redirect(url)
{
	var search_value = "";
	if(document.getElementById("user_id").value!="")
		search_value += ("&uid="+ document.getElementById("user_id").value +"");
		
	if(document.getElementById("year").value!="")
	{
		//if(search_value!="")
			//search_value += " AND ";
		search_value += "&yr="+ document.getElementById("year").value +"";
	}
		
	if(document.getElementById("month").value!="")
	{
		//if(search_value!="")
			//search_value += " AND ";
		search_value += "&mt="+ document.getElementById("month").value +"";
	}
		
		
	var newurl=url+search_value;
	alert(newurl);
	window.open(newurl,"_parent");
}
</script>
<?php 
	$upvo->user_id=$_REQUEST['uid'];
	$upvo->year=$_REQUEST['yr'];
	$upvo->month=$_REQUEST['mt'];     
	
	           
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="28%" class="ptitle"><strong>Manage User Payment:</strong></td>
    <td width="72%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
   <tr>
    <td colspan="2" class="ptitle">
    <table>
        <tr>
          <th >Filter By </th>
          <th > User Name</th>
          <td>
            <select name="user_id" id="user_id" class="field" valiclass="select" valimessage="Please select User Name.">
              <option value="0" <?php echo ($upvo->user_id==""?"selected":""); ?>>Select </option>
              <?php 
			  $userDAO = new UsersDAO();
			  $userVO = new UsersVO();
			  $list = $userDAO->FetchAll();
				     
			   foreach($list as $up)
			   { ?>
<option value="<?php echo $up->user_id; ?>"<?php if($up->user_id==$upvo->user_id) echo "selected"; ?>><?php echo $up->first_name.' '.$up->sur_name ?></option>
<?php
			   }
			   
			   ?>
            </select>
            </td>
          <th>Year:</th>
          <td>
          
          <select name="year"  id="year">
              <option  value="0" <?php echo ($upvo->year==""?"selected":"");?>>Year</option>
              <?php
              for($i=1900;$i<=2100;$i++)
                 echo '<option value="'.$i.'">'.$i.'</option>';
                
              ?>

  		</select>
          </td>
          <th >Month:</th>
          <td>
          <select name="month"  id="month">
            <option value="0" <?php echo ($upvo->month==""?"selected":"");?>>Month</option>
            <option value="January" <?php echo ($upvo->month=="January"?"selected":"");?>>January</option>
            <option value="February" <?php echo ($upvo->month=="February"?"selected":"");?>>February</option>
            <option value="March" <?php echo ($upvo->month=="March"?"selected":"");?>>March</option>
            <option value="April" <?php echo ($upvo->month=="April"?"selected":"");?>>April</option>
            <option value="May" <?php echo ($upvo->month=="May"?"selected":"");?>>May</option>
            <option value="June" <?php echo ($upvo->month=="June"?"selected":"");?>>June</option>
            <option value="July" <?php echo ($upvo->month=="July"?"selected":"");?>>July</option>
            <option value="August" <?php echo ($upvo->month=="August"?"selected":"");?>>August</option>
            <option value="September" <?php echo ($upvo->month=="September"?"selected":"");?>>September</option>
            <option value="October" <?php echo ($upvo->month=="October"?"selected":"");?>>October</option>
            <option value="November" <?php echo ($upvo->month=="November"?"selected":"");?>>November</option>
            <option value="December" <?php echo ($upvo->month=="December"?"selected":"");?>>December</option>
          </select>
  </td>
          <td ><input type="button" name="search" value="Search" class="bttn" onclick="return redirect('index.php?p=user_payment')" />
          </td>
        </tr>
    
    </table></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<th width="7%" class="theader3">S.No. </th>
			<th width="19%" class="theader3">User Name</th>
          <th width="17%" class="theader3">Scheme</th>
          <th width="17%" class="theader3">Month Year</th>
	 	  <th width="11%" class="theader3">Amount</th>
	 	  <th width="14%" class="theader3">Entered Date</th>
	 	  <th width="15%" class="theader3">Operations</th>
		</tr>
		<?php
		 
				
		if($upvo->user_id!="" && $upvo->user_id>0)
			$search_value = " user_id='".$upvo->user_id."'";
		
		if($upvo->year!="" && $upvo->year>0)
		{
			if($search_value!="")
				$search_value.=" AND ";
			$search_value .= " year='".$upvo->year."'";
		}
		if($upvo->month!="" && $upvo->month>0)
		{
			if($search_value!="")
				$search_value.=" AND ";
			$search_value .= " month='".$upvo->month."'";
		}
			/////******for paging******/////////
			// echo ($dopagination);
			//$dopagination=1;
		$updao = new UserPaymentDAO();
		$list = $updao->fetchSelectedAll($search_value);
		
		if($search_value=="" && $search_value==null)
		{
			require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
			if($dopagination)
			{
			
				echo '<br/>'.$search_value;
				$list = $updao->fetchLimited($page, $perpage);
			}
		}
		else
		{
			require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
			if($dopagination)
			{
				echo '<br/>'.$search_value;
				$list = $updao->fetchSelectedLimited($page,$perpage, $search_value);
			}
		}
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $up)
				{
				?>
				<tr <?php if($_GET['id']==$up->prize_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $up->user_name ;?></td>
                    <td class="tcell2"><?php echo $up->scheme_name;?></td>
                    <td class="tcell2"><?php echo $up->month.' '.$up->year ;?></td>
                    <td class="tcell2"><?php echo $up->amount ;?></td>
                    <td class="tcell2"><?php echo $up->created_date ;?></td>
                    <td class="tcell2">
					<a href="index.php?p=aeuser_payment&upId=<?php echo $up->row_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=user_payment&upId=<?php echo $up->row_id;?>" onclick="return confirm('Make sure before you delete this User Payment?');"><img src="./images/delete.gif" border="0" /></a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td colspan="7" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="8">						<font color="#cc0000">
					<?php
						if($search_value=="" && $search_value==null)
						{
							$url = $_SERVER['REQUEST_URI'];
						}
						else
						{
							$url = $_SERVER['REQUEST_URI']."&search_value=".$search_value;  
						}
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">&nbsp;
					</font>			</tr>
			<?php
			}
		?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>

<a href="index.php?p=aeuser_payment&function=add" class="theader3"><strong>ADD User Payment </strong></a></div>
