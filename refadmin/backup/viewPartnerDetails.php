<?php

session_start();
require_once "../global.php";
require_once "loginchecker.php";
require_once "inc/pageFinder.php";
require_once "../".DIR_INCLUDES."/db_connection.php";
require_once "../".DIR_INCLUDES."/functions.php";

$p_id = $_GET['pId'];
$pvo = new PartnersVO();
$pdao = new PartnersDAO();
if ($_GET['pId'])
{
	$pvo = $pdao->fetchDetails($p_id);
}
?>
<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid #ccc;" align="center" >
  <tbody>
    <tr>
      <td colspan="2" align="left" class="text"><div align="center"><strong>Partners Details</strong></div></td>
    </tr>
    <tr>
      <td width="178" align="left" bgcolor="#F4F4F4" class="text"><div align="right"><strong>Title :&nbsp;</strong></div></td>
      <td width="789"  align="left" bgcolor="#F4F4F4" class="main"><?php echo $pvo->title; ?>      </td>
    </tr>
    <tr>
      <td align="left" class="text"> <div align="right"><strong>First Name:</strong></div></td>
      <td class="main"  align="left"><?php echo $pvo->first_name; ?></td>
    </tr>
    <tr>
      <td align="left" bgcolor="#F4F4F4" class="text"><div align="right"><strong>Last Name:</strong></div></td>
      <td  align="left" bgcolor="#F4F4F4" class="main"><?php echo $pvo->sur_name; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Company:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->company; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Street:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->street; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Postal Code:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->postal_code; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>City/Town:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->city_town; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Country:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->country ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Telephone:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->tel_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Mobile:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->mobile_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>URL:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->url; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Email:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->email; ?></td>
    </tr>
   
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>DOB:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->dob; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Bank:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->bank; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Account No.:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->account_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Account Holders:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->account_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Registered Date:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $pvo->registered_date; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Status:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $pvo->status; ?>  </td>
    </tr>
  </tbody>
</table>



             

