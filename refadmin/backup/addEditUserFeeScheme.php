<?php
$uf_id = $_REQUEST['ufId'];
$function = $_GET['function'];

$ufvo = new UserFeeSchemeVO();

$ufdao = new UserFeeSchemeDAO();

if($function == 'add')
	{
	echo"<h2>Add User Fee Scheme</h2>";
	}
else 
	{
	echo "<h2>Edit User Fee Scheme</h2>";
	if ($_GET['ufId'])
		{
		$ufvo = $ufdao->fetchDetails($uf_id);
		}
	}
// the different message for updating and adding the news
$updated_msg="<script language='javascript'>alert('User Fee Scheme has been Updated successfully.'); location='index.php?p=userfee_scheme';</script>\n";
$inserted_msg="<script language='javascript'>alert('User Fee Scheme has been Added successfully.'); location='index.php?p=userfee_scheme';\n</script>";
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$ufvo->scheme_id = $_POST['scheme_id'];
	$ufvo->scheme_name = $_POST['scheme_name'];
	$ufvo->monthly_rate = $_POST['monthly_rate'];
	$ufvo->validate_from = $_POST['validate_from'];
	$ufvo->validate_to = $_POST['validate_to']; 
	$ufvo->created_date = $_POST['created_date'];
	$ufvo->updated_date = date("Y-m-d");
	$ufvo->updated_by= $_SESSION['full_name'];
	
	$ufvo->formatInsertVariables();
	
	//checking server side validation for different fields
			
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_POST['scheme_id']!="") 
			{
			if($ufdao->update($ufvo))
				echo $updated_msg;
			}
		else
			{
			if($ufdao->insert($ufvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditUserFeeScheme" id="addEditUserFeeScheme" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>User Fee Scheme   Form:</strong></td>
                              </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                    <tbody>
                                              <tr>
                                                <td width="178" align="left" class="text">&nbsp;</td>
                                                <td width="789"  align="left" class="main">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Scheme Name :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="scheme_name" type="text" class="field" id="scheme_name" value="<?php echo $ufvo->scheme_name ?>" size="30" valiclass="required" req="2" valimessage="Scheme Name:This field is required!
" />
											
                                                  &nbsp;</td>
                                              </tr>

                                              <tr>
                                                <td align="left" class="text"> *Monthly Rate:</td>
                                                <td class="main"  align="left"><input name="monthly_rate" type="text" class="field" id="monthly_rate" value="<?php echo $ufvo->monthly_rate ?>" size="15" valiclass="required" req="2" valimessage="Title:This field is required!
" /></td>
                                      </tr>
                                              <tr>
                                                <td align="left" class="text">*Valid From :</td>
                                                <?php
                                                   $validate_from=$ufvo->validate_from;
													if($validate_from==""|| $validate_from=="0000-00-00")
													{
														$validate_from="<script>DateInput('validate_from',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$validate_from="<script>DateInput('validate_from',true,'YYYY-MM-DD','".$validate_from."');</script>";
													}
																		
													?>
                                                <td class="main"  align="left"><span class="content"><?php echo $validate_from ?></span></td>
                                      </tr>
                                              	
												 <tr>
												   <td align="left" valign="middle" class="content">*Valid To:</td>
                                                   <?php
                                                   $validate_to=$ufvo->validate_to;
													if($validate_to==""|| $validate_to=="0000-00-00")
													{
														$validate_to="<script>DateInput('validate_to',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$validate_to="<script>DateInput('validate_to',true,'YYYY-MM-DD','".$validate_to."');</script>";
													}
																		
													?>
                        					<td align="left" valign="middle" class="content"><?php echo $validate_to ?></td>
										      </tr>
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="scheme_id" id="scheme_id" value="<?php echo $ufvo->scheme_id;?>">
	<input type="hidden" name="created_date" id="created_date" value="<?php echo $ufvo->created_date;?>" />
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						 
                        </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>