<?php

error_reporting(ALL);
$row_id = $_REQUEST['upId'];
$function = $_GET['function'];
$upvo = new UserPaymentVO();
$updao = new UserPaymentDAO();
?>
<script language="javascript">
function getScheme(user_id)
{
	if(user_id!="")
	{
		var url = "index.php?p=aeuser_payment";
		var newurl=url+'&uid='+user_id+"&upId="+<?php echo $_REQUEST['upId'];?>;
		window.open(newurl,"_parent");
	}
}

</script>
<?php
	echo "<h2>Add/Edit User Payment</h2>";
		

	if ($_REQUEST['upId'])
		{
		$upvo = $updao->fetchDetails($_REQUEST['upId']);
		
		}
// the different message for updating and adding the user
$updated_msg="<script language='javascript'>\nalert('User Payment has been Updated successfully.');\n location='index.php?p=user_payment';</script>\n";
$inserted_msg="<script language='javascript'>\nalert('User Payment  has been Added successfully.');\n location='index.php?p=user_payment';\n</script>";


if($_REQUEST['uid'])
	{
		$udao = new UsersDAO();
		$uvo=$udao->fetchDetails($_REQUEST['uid']);
		$upvo->user_id=$_REQUEST['uid'];
		$upvo->scheme_id=$uvo->scheme_id;
		
		$udao = new UserFeeSchemeDAO();
		$uvo=$udao->fetchDetails($upvo->scheme_id);
		$upvo->amount = $uvo->monthly_rate;
	}

	
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$upvo->row_id = $_POST['row_id'];
	$upvo->user_id = $_POST['user_id'];
	$upvo->scheme_id = $_POST['scheme_id'];
	$upvo->year = $_POST['year'];
	$upvo->month = $_POST['month'];
	$upvo->amount = $_POST['amount'];
	$upvo->created_date = $_POST['created_date'];
	
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_REQUEST['upId']) 
			{
			if($updao->update($upvo))
				{
				
				echo $updated_msg;
				}
			}
		else
			{
			if($updao->insert($upvo))
				echo $inserted_msg;
			}
		}
	}
?>

<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?><style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditUserPayment" id="addEditUserPayment" enctype="multipart/form-data" action="" method="post">
					     
					    <input name="action" value="" type="hidden">
						<span class="style3">
						
						</span>	
                          <table width="80%" align="center">
                       
                        <?php //echo "<tr> 
                                //<td colspan='2'> <span class='normal10' style='background-color:#FAA3AB; padding:5px;'>$error</span>&nbsp;</td>
                              //</tr>"; ?>
                              
                              <tr>
                                <td align="left" class="main"><table width="105%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr>
                              <tr>
                                <td align="left" class="main"><strong>User Payment  Information:</strong></td>
                              </tr>
							   <tr>
							     <td class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;"><table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                          <tbody>
                                            <tr>
                                              <td width="178" align="left" class="text">&nbsp;</td>
                                              <td width="789"  align="left" class="main">&nbsp;</td>
                                            </tr>
                                           <tr>
                                              <td align="left" class="text">*User Name:</td>
                                              <td align="left" class="main">
                                              <?php 
                                              $userDAO = new UsersDAO();
                                              $userVO = new UsersVO();
                                              $list = $userDAO->FetchAll();
											  ?>
                                              <select name="user_id" id="user_id" class="field" valiclass="select" valimessage="Please select User Name." onchange="getScheme(this.value)">
                                                <option value="0" <?php echo($upvo->user_id!=""?"selected":""); ?>>Select </option>
                                                <?php	
                                             
											   foreach($list as $up)
											   { ?>
                                                <option value="<?php echo $up->user_id ?>"<?php if($up->user_id==$upvo->user_id) echo "selected"; ?>><?php echo $up->first_name.' '.$up->sur_name ?></option>
                                                   
                                                <?php
                                               }
											   
											   ?>
                                              </select>
                                              <input type="hidden" name="scheme_id" id="scheme_id" value="<?php echo $upvo->scheme_id;?>" /></td>
                                            </tr>
                                            <tr>
                                              <td align="left" class="text">*Year:&nbsp;</td>
                                              <td class="main"  align="left">
                                              <select name="year"  id="year" class="field" valiclass="select" valimessage="Please select Year.">
                                                  <option selected>Year</option>
                                                  <?php
                                                  for($i=1900;$i<=2100;$i++)
                                                     echo '<option value="'.$i.'" '.($upvo->year==$i?"selected":"").'>'.$i.'</option>';
                                                     ?>
                                    		 </select>                                      
                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="left" class="text">*Month:</td>
                                              <td class="main"  align="left">
                                              <select name="month"  id="month" class="field" valiclass="select" valimessage="Please select Month.">
                                                <option value="0" selected="selected">Month</option>
                                                <option value="January" <?php echo ($upvo->month=="January"?"selected":""); ?>>January</option>
                                                <option value="February" <?php echo ($upvo->month=="February"?"selected":""); ?> >February</option>
                                                <option value="March"  <?php echo ($upvo->month=="March"?"selected":""); ?>>March</option>
                                                <option value="April"  <?php echo ($upvo->month=="April"?"selected":""); ?>>April</option>
                                                <option value="May" <?php echo ($upvo->month=="May"?"selected":""); ?> >May</option>
                                                <option value="June" <?php echo ($upvo->month=="June"?"selected":""); ?> >June</option>
                                                <option value="July" <?php echo ($upvo->month=="July"?"selected":""); ?> >July</option>
                                                <option value="August" <?php echo ($upvo->month=="August"?"selected":""); ?>>August</option>
                                                <option value="September" <?php echo ($upvo->month=="September"?"selected":""); ?> >September</option>
                                                <option value="October"  <?php echo ($upvo->month=="October"?"selected":""); ?>>October</option>
                                                <option value="November"  <?php echo ($upvo->month=="November"?"selected":""); ?>>November</option>
                                                <option value="December" <?php echo ($upvo->month=="December"?"selected":""); ?>>December</option>
                                              </select>
                                                </td>
                                            </tr>
                                            <tr>
                                              <td align="left" class="text">*Amount:</td>
                                              <td class="main"  align="left"><input name="amount" type="text" id="amount" value="<?php echo $upvo->amount; ?>"  class="field" valiclass="number" req="1" valimessage="Amount:This field is required!" />
                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="left" valign="middle" class="content">*Payment Date:</td>
                                              <?php
                                                   $created_date=$upvo->created_date;
													if($created_date==""|| $created_date=="0000-00-00")
													{
														$created_date="<script>DateInput('created_date',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$created_date="<script>DateInput('created_date',true,'YYYY-MM-DD','".$created_date."');</script>";
													}
																		
													?>
                                              <td align="left" valign="middle" class="content"><?php echo $created_date; ?></td>
                                            </tr>
                                            <tr>
                                              <td align="left" class="text">&nbsp;</td>
                                              <td class="main">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                                                    
                              
                              <tr> 
                                <td  align="left"><input type="hidden" name="save" id="save" value="true" />
                                  <input type="hidden" name="row_id" id="row_id" value="<?php echo $upvo->row_id;?>">
	<input type="button" name="savebtn" id="savebtn" value="Save" class="theader3" onClick="this.form.save.value='true'; call_validate_ajax(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
				        <?php
	 					if($function == 'add') {?>
				        <input type='hidden' name='function' value='add'><?php	}else{?>
						<input type='hidden' name='function' value='edit'><?php }?>
						   <input type="hidden" name="<? //=$_GET[action]?>" value="yes"/>
						   <input name="id" type="hidden" value="<? //if(!isset($_GET['act'])){ echo $rows['id']; } else { echo ''; }?>" <?// }?>>
					   </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>