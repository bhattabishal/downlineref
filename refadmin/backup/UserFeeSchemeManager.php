<?php

include_once "../".DIR_INCLUDES."/functions.php";
$delId=$_GET['nId'];
$ufvo = new UserFeeSchemeVO();
$ufdao = new UserFeeSchemeDAO();

$count = 1;
	if(isset($_GET['ufId']) && intval($_GET['ufId'])!=0)
		{
		$rCS = new UserFeeSchemeDAO();
		$flag = $rCS->remove($_GET['ufId']);
		if($flag)
			$msg = "Selected User Fee Scheme has been removed successfully.";
		else
			$msg = "Some error prevented User Fee Scheme from being removed";
		}

	
	
	if(isset($_GET['sId']) && intval($_GET['sId'])!=0)
		{
		$sCS = new UserFeeSchemeDAO();
		$flag = $sCS->publishNunpublish($_GET['sId'],$_GET['status']);
		if($flag)
			$msg = "Status has been changed successfully.";
		else
			$msg = "Some error prevented User Fee Scheme from being updated";
		}
	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="28%" class="ptitle"><strong>Manage User Fee Scheme:</strong></td>
    <td width="72%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="5%" class="theader3"><strong>S.No.</strong></td>
			<td width="33%" class="theader3"><strong>Scheme Name</strong></td>
          <td width="19%" class="theader3"><strong>Monthly Rate</strong></td>
	 	  <td width="17%" class="theader3"><strong>Valid From</strong></td>
		 	<td width="16%" class="theader3"><strong>Valid To</strong></td>
	 	  <td width="10%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$UFdao = new UserFeeSchemeDAO();
		$list = $UFdao->fetchAll(); 
				/////******for paging******/////////
		require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
		if($dopagination)
			$list = $UFdao->fetchLimited($page, $perpage, "all");
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $uf)
				{
				?>
				<tr <?php if($_GET['id']==$uf->scheme_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $uf->scheme_name ;?></td>
                    <td class="tcell2"><?php echo $uf->monthly_rate ;?></td>
                    <td class="tcell2"><?php echo $uf->validate_from ;?></td>
                    <td class="tcell2"><?php echo $uf->validate_to ;?></td>
					<td class="tcell2">
					<a href="index.php?p=aeuserfee_scheme&amp;ufId=<?php echo $uf->scheme_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=userfee_scheme&amp;ufId=<?php echo $uf->scheme_id;?>" onclick="return confirm('Make sure before you delete this User Fee Scheme?');"><img src="./images/delete.gif" border="0" /></a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td colspan="6" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
				
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="6">						<font color="#cc0000">
					<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">
					<td width="0%" class="tcell2">&nbsp;</td>
						</font>			</tr>
			<?php
			}
		?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>
<div align="right"><a href="index.php?p=aeuserfee_scheme&function=add" class="theader3"><strong>ADD User Fee Scheme </strong></a></div>
