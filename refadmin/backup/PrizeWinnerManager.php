<?php
	include_once "../".DIR_INCLUDES."/functions.php";

	if(isset($_GET['rAid']) && intval($_GET['rAid'])!=0)
		{
		$rdao = new PrizeWinnersDAO();
		$flag = $rdao->remove($_GET['rAid']);
		if($flag)
			$msg = "Selected winner has been removed successfully.";
		else
			$msg = "Some error prevented this winner from being removed.";
		}
	
			
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="24%" class="ptitle"><strong>Manage  Prize Winners:</strong></td>
    <td class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2" class="ptitle">&nbsp;</td>
  </tr>
 <br /><br />
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>S.No.</strong></td>
			<td width="19%" class="theader3"><strong>Winners Name </strong></td>
			<td width="19%" class="theader3"><strong>Prize Name </strong></td>
			<td width="19%" class="theader3"><strong>&nbsp;Winning Date </strong></td>
		 	<td width="11%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$pwDAO = new PrizeWinnersDAO();
		$list = $pwDAO->fetchExtendedAll();
		
			/////******for paging******/////////
		
			require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
			if($dopagination)
				$list = $pwDAO->fetchExtendedLimited($page, $perpage);
	
		/////****end of paging*******//////////
		

		//$sn =0;
		if(!empty($list))
			{
				foreach($list as $pw)
				{
				?>
				<tr <?php if($_GET['id']==$pw->winner_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $pw->user_name;?></td>
					<td class="tcell2"><?php echo $pw->prize_name;?></td>
					<td class="tcell2"><?php echo $pw->prize_date;?></td>
					<td class="tcell2">
					<a href="index.php?p=aeprize_winner&pwId=<?php echo $pw->winner_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=prize_winner&rAid=<?php echo $pw->winner_id;?>" onClick="return confirm('Make sure before you delete this Winner?');">
				  	<img src="./images/delete.gif" border="0">				  	</a>				  	</td>
				</tr>
				<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td colspan="5" class="tcell_left"><div align="center"><font color="#cc0000">No Winners were found.</font></div></td>
			</tr>
			<?
			}
		?>
	</table>	</td>
  </tr>
    <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="2">
						<font color="#cc0000">
						<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
						</font>					</td>
			</tr>
			<?php
			}
		?>
</table>
<br/>
<div align="right"><a href="index.php?p=aeprize_winner" class="theader3"><strong>ADD Winner</strong></a></div>
