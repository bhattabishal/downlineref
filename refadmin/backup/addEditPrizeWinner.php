<?php

error_reporting(ALL);
$winner_id = $_REQUEST['pwId'];
$function = $_GET['function'];
$pwvo = new PrizeWinnersVO();
$pwdao = new PrizeWinnersDAO();
	echo "<h2>Add/Edit Prize Winners</h2>";
	if ($_REQUEST['pwId'])
		{
		$pwvo = $pwdao->fetchDetails($winner_id);
		}
// the different message for updating and adding the user
$updated_msg="<script language='javascript'>\nalert('Prize Winner has been Updated successfully.');\n location='index.php?p=prize_winner';</script>\n";
$inserted_msg="<script language='javascript'>\nalert('Prize Winner has been Added successfully.');\n location='index.php?p=prize_winner';\n</script>";
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$pwvo->winner_id = $_POST['winner_id'];
	$pwvo->prize_id = $_POST['prize_id'];
	$pwvo->user_id = $_POST['user_id'];
	$pwvo->prize_date = $_POST['prize_date'];
	
	
	
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_REQUEST['pwId']) 
			{
			if($pwdao->update($pwvo))
				{
				
				echo $updated_msg;
				}
			}
		else
			{
			if($pwdao->insert($pwvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?><style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditusers" id="addEditusers" enctype="multipart/form-data" action="" method="post">
					     
					    <input name="action" value="process" type="hidden">
						<span class="style3">
						
						</span>	
                          <table width="80%" align="center">
                       
                        <?php //echo "<tr> 
                                //<td colspan='2'> <span class='normal10' style='background-color:#FAA3AB; padding:5px;'>$error</span>&nbsp;</td>
                              //</tr>"; ?>
                              
                              <tr>
                                <td align="left" class="main"><table width="105%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr>
                              <tr>
                                <td align="left" class="main"><strong>Prize Winner Information:</strong></td>
                              </tr>
							   <tr>
							     <td class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;"><table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                          <tbody>
                                            <tr>
                                              <td width="178" align="left" class="text">&nbsp;</td>
                                              <td width="789"  align="left" class="main">&nbsp;</td>
                                            </tr>
                                           <tr>
                                              <td align="left" class="text">*Prize Name:</td>
                                              <td align="left" class="main">
                                              <select name="prize_id" id="prize_id" class="field" valiclass="select" valimessage="Please select Prize Name.">
                                                <option value="">Select </option>
                                                <?php	
                                               $mpdao = new MonthlyPrizeDAO();
											   $list=$mpdao->fetchAll();
											   foreach($list as $mp)
											   { ?>
                                                <option value="<?php echo $mp->prize_id ?>"<?php if($mp->prize_id==$pwvo->prize_id) echo "selected"; ?>><?php echo $mp->prize_name ?></option>
                                                   
                                                <?php
                                               }
											   
											   ?>
                                              </select></td>
                                            </tr>
                                            <tr>
                                              <td align="left" class="text">*Winner Name:&nbsp;</td>
                                              <td class="main"  align="left">
                                              <select name="user_id" id="user_id" class="field" valiclass="select" valimessage="Please select Winner Name.">
                                                  <option value="" selected="selected">Select</option>
                                                   <?php	
                                               $udao = new UsersDAO();
											   $list=$udao->fetchAll("onlypublished");
											   foreach($list as $u)
											   { ?>
                                                <option value="<?php echo $u->user_id ?>"<?php if($u->user_id==$pwvo->user_id) echo "selected"; ?>><?php echo $u->title.". ".$u->first_name." ".$u->sur_name; ?></option>
                                                										   
                                                <?php
                                               }
											   
											   ?>
                                                  
                                                </select>                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="left" valign="middle" class="content">*Prize Date:</td>
                                              <?php
                                                   $prize_date=$pwvo->prize_date;
													if($prize_date==""|| $prize_date=="0000-00-00")
													{
														$prize_date="<script>DateInput('prize_date',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$prize_date="<script>DateInput('prize_date',true,'YYYY-MM-DD','".$prize_date."');</script>";
													}
																		
													?>
                                              <td align="left" valign="middle" class="content"><?php echo $prize_date; ?></td>
                                            </tr>
                                            <tr>
                                              <td align="left" class="text">&nbsp;</td>
                                              <td class="main">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                                                    
                              
                              <tr> 
                                <td  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="winner_id" id="winner_id" value="<?php echo $pwvo->winner_id;?>">
	<input type="button" name="savebtn" id="savebtn" value="Save" class="theader3" onClick="this.form.save.value='true'; call_validate_ajax(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
				        <?php
	 					if($function == 'add') {?>
				        <input type='hidden' name='function' value='add'><?php	}else{?>
						<input type='hidden' name='function' value='edit'><?php }?>
						   <input type="hidden" name="<? //=$_GET[action]?>" value="yes"/>
						   <input name="id" type="hidden" value="<? //if(!isset($_GET['act'])){ echo $rows['id']; } else { echo ''; }?>" <?// }?>>
					   </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>