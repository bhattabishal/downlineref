<script type="text/javascript" src="../files/enlargeit.js"></script>
<?php
	
	
	/////////********fetching category name********//////////
	if(isset($_GET['cid']) && intval($_GET['cid'])!=0)
		{
		$catdao = new GalleryDAO();
		$catvo = $catdao->fetchDetails($_GET['cid']);
		if($catvo->id == 0)
			echo "<script>window.location = 'index.php?p=gallery';</script>";
		else
			$categoryname = $catvo->name;
		}
	else
		{
		echo "<script>window.location = 'index.php?p=gallery';</script>";
		}
	/////////********end of fetching categor name*********//////////////////
	
	/////////**********removing file part*********/////////////
	if(isset($_GET['rFid']) && intval($_GET['rFid'])!=0)
		{
		$rfdao = new ImageDAO();
		$flag = $rfdao->remove($_GET['rFid']);
		if($flag)
			$msg = "Selected image has been removed successfully.";
		else
			$msg = "Some error prevented selected image from being removed.";
		}
	//////////*******8end of removing file part*********////////////////////////
	
	/////////********changing publish or unpublish status*********////////////////
	elseif(isset($_GET['id']) && intval($_GET['id'])!=0 && isset($_GET['s']) && $_GET['s']!="")
		{
		$dao = new ImageDAO();
		$id = intval($_GET['id']);
		if($_GET['s']=='no')
			$publish = "no";
		else
			$publish = "yes";
		
		$flag = $dao->publishNunpublish($id, $publish);
		if($flag)
			$msg = "Publish/Unpublish status of selected image has been changed.";
		else
			$msg = "Publish/Unpublish status of image could not be changed.";
		}	
	///////////***********end of changing publish or unpublish part**********//////////////
	
		
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="24%" class="ptitle"><strong>Manage images:</strong></td>
    <td class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
   <tr>
    <td colspan="2" class="medium"><strong><?php echo $categoryname;?></strong></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="4%" class="theader3"><strong>S.No.</strong></td>
			<td width="18%" class="theader3"><strong>Image Title: </strong></td>
		 	<td width="16%" class="theader3"><strong>Short Description:</strong></td>
		 	<td width="19%" class="theader3" align="center"><strong>Image: </strong></td>
		 	<td width="18%" class="theader3"><strong>Publish/Unpublish:</strong></td>
		 	<td width="25%" class="theader3"><strong>Operations:</strong></td>
		</tr>
		<?php
		$dwnDAO = new ImageDAO();
		$list = $dwnDAO->fetchImagesOfCategory(intval($_GET['cid']), $par='list');
		
		$sn =0;
		if(!empty($list))
			{
				foreach($list as $images)
				{
				?>
				<tr <?php if($_GET['id']==$images->id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $images->name;?></td>
					<td class="tcell2"><?php echo $images->caption;?></td>
					<td class="tcell2" align="center"><img src="../tgincludes/vignette.php?f=../files/galleryimage/<?php echo $images->image;?>&amp;h=100&amp;w=100&amp;c=1" title="<?=$images->image;?>" id="pic<?php echo $sn;?>" alt="<?php echo $images->name;?>" onclick="enlarge(this);" longdesc="../files/galleryimage/<?php echo $images->image;?>" style="border-width:1px;border-style:solid;"/></td>
					<td class="tcell2">
					<?php
					if($images->publish == 'yes')
						echo "<a href='index.php?p=img&id=".$images->id."&s=no&cid=".$images->gallery_id."'>Unpublish</a>";
					elseif($images->publish == 'no')
						echo "<a href='index.php?p=img&id=".$images->id."&s=yes&cid=".$images->gallery_id."'>Publish</a>";
					?>					</td>
					<td class="tcell2">
					<a href="index.php?p=aeimage&amp;eDid=<?php echo $images->id;?>&amp;cid=<?php echo $images->gallery_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=img&rFid=<?php echo $images->id;?>&cid=<?php echo $images->gallery_id;?>" onClick="return confirm('Make sure before you delete this image?');">
				  	<img src="./images/delete.gif" border="0">					</a>					</td>
				</tr>
				<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td class="tcell_left">&nbsp;</td>
				<td colspan="5" align="center" class="tcell2"><font color="#cc0000">No Images  was found.</font></td>
			</tr>
			<?
			}
		?>
	</table>	</td>
  </tr>
  <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="2">
						<font color="#cc0000">
						<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
						</font>					</td>
			</tr>
			<?php
			}
		?>
</table>
<br/>
<div align="right">
	<a href="index.php?p=gi&id=<?php echo $_GET['cid'];?>&pg=<?php echo $page;?>" class="theader3"><strong>Back</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="index.php?p=aeimage&cid=<?php echo $_GET['cid'];?>" class="theader3"><strong>Add image</strong></a>
</div>
