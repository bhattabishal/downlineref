<?php
$mp_id = $_REQUEST['mpId'];
$function = $_GET['function'];

$mpvo = new MonthlyPrizeVO();

$mpdao = new MonthlyPrizeDAO();

if($function == 'add')
	{
	echo"<h2>Add Monthly Prize</h2>";
	}
else 
	{
	echo "<h2>Edit Monthly Prize</h2>";
	if ($_GET['mpId'])
		{
		$mpvo = $mpdao->fetchDetails($mp_id);
		$date=explode(" ",$mpvo->prize_month);
		$month=$date[0];
		$year=$date[1];
		}
	}
// the different message for updating and adding the news
$updated_msg="<script language='javascript'>alert('Monthly Prize has been Updated successfully.'); location='index.php?p=monthly_prize';</script>\n";
$inserted_msg="<script language='javascript'>alert('Monthly Prize has been Added successfully.'); location='index.php?p=monthly_prize';\n</script>";

if($_SERVER['REQUEST_METHOD']=="POST")
{

	$mpvo->prize_id = $_POST['prize_id'];
	$mpvo->prize_month = $_POST['month']." ".$_POST['year'];
	$mpvo->organiser_data = $_POST['organiser_data'];
	$mpvo->closure_date = $_POST['closure_date'];
	$mpvo->description = $_POST['description'];
	$mpvo->answer = $_POST['answer'];
	$mpvo->prize_value = $_POST['prize_value'];
	$mpvo->prize_image = $_FILES['prize_image']['name']; 
	$mpvo->updated_date = date("Y-m-d");
	$mpvo->updated_by= $_SESSION['full_name'];
	
	$mpvo->formatInsertVariables();
	
	//checking server side validation for different fields
			
	if(!$errmsg) //if the form is posted and there is no error at all
	{
		if($_POST['prize_id']!="") 
		{
			if($mpvo->prize_image)
			{
				if($_POST['old_prize_image'])
					$mpvo->removePicture($_POST['old_prize_image']);
				
				get_file_name_and_ext($mpvo->prize_image,$image,$file_ext); 
				if($file_ext!='jpg' && $file_ext!='png' && $file_ext!='gif' && $file_ext!='jpeg'  && $file_ext!='jpg')
				{
					$errmsg ='Invalid file format';
				}	
				else
				{
				
					srand(make_seed());
					$randval = rand();
					//concat the random value to the file name to avoid duplicate filen name
					$mpvo->prize_image = "prize_".$randval.".".$file_ext;
					//get the mime type of the picture
					$mimetype = $_FILES['prize_image']['type'];
					//get the thumbnail height and width according to the size of the image using user defined function
					getThumbnailHeightAndWidth($mimetype,$_FILES['prize_image']['tmp_name'],235,165,$thumb_w,$thumb_h);
					//make the thumbnail for the home page
					$mpvo->thumbnailAndUpload($mimetype,$_FILES['prize_image']['tmp_name'],$mpvo->prize_image,$thumb_w,$thumb_h);
				}
					/////////************end of image upload portion*****///////////
			}
			if($mpdao->update($mpvo))
				echo $updated_msg;
		}
		else
		{
			//////////*********image upload portion********/////////////////
			if($mpvo->prize_image)
			{
				
				get_file_name_and_ext($mpvo->prize_image,$image,$file_ext); 
				if($file_ext!='jpg' && $file_ext!='png' && $file_ext!='gif' && $file_ext!='jpeg'  && $file_ext!='jpg')
				{
					$errmsg ='Invalid file format';
				}	
				else
				{
					srand(make_seed());
					$randval = rand();
					//concat the random value to the file name to avoid duplicate filen name
					$mpvo->prize_image = "prize_".$randval.".".$file_ext;
					//get the mime type of the picture
					$mimetype = $_FILES['prize_image']['type'];
					//get the thumbnail height and width according to the size of the image using user defined function
					getThumbnailHeightAndWidth($mimetype,$_FILES['prize_image']['tmp_name'],235,165,$thumb_w,$thumb_h);
					
					//make the thumbnail for the home page
					$a=$mpvo->thumbnailAndUpload($mimetype,$_FILES['prize_image']['tmp_name'],$mpvo->prize_image,$thumb_w,$thumb_h);
					/////////************end of image upload portion*****///////////

				}	
			}
			if($mpdao->insert($mpvo))
				echo $inserted_msg;
			
		}
	}
}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditPrize" id="addEditPrize" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>Commission Scheme   Form:</strong></td>
                              </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                    <tbody>
                                              <tr>
                                                <td width="178" align="left" class="text">&nbsp;</td>
                                                <td width="789"  align="left" class="main">&nbsp;</td>
                                              </tr>

                                             
									  <tr>
                                                <td align="left" class="text"> *Prize Month:</td>
                                                <td class="main"  align="left">
												<select name="month" class="month" id="month">
												<option value="January" <?php if($month=='January') {?> selected="selected" <?php } ?>>January</option>
												<option value="February" <?php if($month=='February') {?> selected="selected" <?php } ?>>February</option>
												<option value="March" <?php if($month=='March') {?> selected="selected" <?php } ?>>March</option>
												<option value="April" <?php if($month=='April') {?> selected="selected" <?php } ?>>April</option>
												<option value="May" <?php if($month=='May') {?> selected="selected" <?php } ?>>May</option>
												<option value="June" <?php if($month=='June') {?> selected="selected" <?php } ?>>June</option>
												<option value="July" <?php if($month=='July') {?> selected="selected" <?php } ?>>July</option>
												<option value="August" <?php if($month=='August') {?> selected="selected" <?php } ?>>August</option>
												<option value="September" <?php if($month=='September') {?> selected="selected" <?php } ?>>September</option>
												<option value="October" <?php if($month=='October') {?> selected="selected" <?php } ?>>October</option>
												<option value="November" <?php if($month=='November') {?> selected="selected" <?php } ?>>November</option>
												<option value="December" <?php if($month=='December') {?> selected="selected" <?php } ?>>December</option>
												</select> &nbsp;&nbsp; 
												<select name="year" class="year" id="year">
													  <?php
													  for($i=1900;$i<=2100;$i++)
														if($date[1]==$i)
														{
															echo '<option value="'.$i.'" "selected">'.$i.'</option>';
														}
														else
														{
															echo '<option value="'.$i.'">'.$i.'</option>';
														}
													  ?>
													
													  </select>
  </td>
                                      </tr>
                                       <tr>
                                                <td align="left" class="text"> *Organiser Data:</td>
                                                <td class="main"  align="left"><input name="organiser_data" type="text" class="field" id="organiser_data" value="<?php echo $mpvo->organiser_data ?>" size="40" valiclass="required" req="2" valimessage="Prize Month:This field is required!
" /></td>
                                      </tr>
                                       <tr>
                                                <td align="left" class="text"> *Closure Date:</td>
                                                 <?php
                                                   $closure_date=$nvo->closure_date;
													if($closure_date==""|| $closure_date=="0000-00-00")
													{
														$closure_date="<script>DateInput('closure_date',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$closure_date="<script>DateInput('closure_date',true,'YYYY-MM-DD','".$closure_date."');</script>";
													}
																		
													?>
                                                <td class="main"  align="left"><?php echo $closure_date ?></td>
                                       </tr>
                                       <tr>
                                                <td align="left" class="text"> *Description:</td>
                                                <td class="main"  align="left"><textarea name="description" cols="35" rows="5" class="field" id="description" valiclass="required" req="2" valimessage="Prize Month:This field is required!
"><?php echo $mpvo->description ?></textarea></td>
                                      </tr>
                                       <tr>
                                                <td align="left" class="text"> *Answer:</td>
                                                <td class="main"  align="left"><input name="answer" type="text" class="field" id="answer" value="<?php echo $mpvo->answer ?>" size="40" valiclass="required" req="2" valimessage="Prize Month:This field is required!
" /></td>
                                      </tr>
                                              <tr>
                                                <td align="left" class="text">*Prize Value:</td>
                                               
                                                <td class="main"  align="left"> <input name="prize_value" type="text" class="field" id="prize_value" value="<?php echo $mpvo->prize_value ?>" size="15" valiclass="required" req="2" valimessage="Prize Value:This field is required!" /></td>
                                      </tr>
                                              	
												 <tr>
												   <td align="left" valign="middle" class="content">Prize Image:</td>
                                                   
                        					<td align="left" valign="middle" class="content"><input type="file" name="prize_image" id="prize_image" /></td>
										      </tr>
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="prize_id" id="prize_id" value="<?php echo $mpvo->prize_id;?>">
	<input type="hidden" name="old_prize_image" id="old_prize_image" value="<?php echo $mpvo->prize_image;?>" />
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						 
                        </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>