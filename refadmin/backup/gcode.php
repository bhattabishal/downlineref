<?php
require_once "../tgincludes/db_connection.php";

if(isset($_POST['save']) && $_POST['save']=='true')
	{
	$vo = new GalleryVO();
	$dao = new GalleryDAO();
	
	$vo->id = $_POST['id'];
	$vo->name = $_POST['name'];
	$vo->publish = $_POST['publish'];
	
	$vo->formatInsertVariables();
	if($vo->id == 0)
		{
		if($dao->insert($vo))
			{
			$id = $dao->insert_id();
			$msg = "New gallery has been successfully inserted.";
			}
		else
			$msg = "Error occured while inserting new gallery.";
		
		$qstring = "index.php?p=gi&id=".$id."&msg=".$msg;
		}
	else
		{
		if($dao->update($vo))
			$msg = "Selected gallery has been successfully updated.";
		else
			$msg = "Error occured while updating selected gallery.";
		
		$qstring = "index.php?p=gallery&id=".$vo->id."&msg=".$msg;
		}
	
	header("Location:".$qstring);
	}
?>