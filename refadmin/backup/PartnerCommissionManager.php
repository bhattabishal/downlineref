<?php
include_once "../".DIR_INCLUDES."/functions.php";
$delId=$_GET['pId'];
$pvo = new PartnerCommissionVO();
$pdao = new PartnerCommissionDAO();

$count = 1;
	if(isset($_GET['pId']) && intval($_GET['pId'])!=0)
		{
		$rP = new PartnerCommissionDAO();
		$flag = $rP->remove($_GET['pId']);
		if($flag)
			$msg = "Selected Commission has been removed successfully.";
		else
			$msg = "Some error prevented Commission from being removed.";
		}
	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="23%" class="ptitle"><strong>Manage Partners Commission:</strong></td>
    <td width="77%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="5%" class="theader3"><strong>S.No.</strong></td>
			<td width="26%" class="theader3"><strong>Partners Full Name</strong></td>
          <td width="13%" class="theader3"><strong>Total User Registered</strong></td>
	 	  <td width="14%" class="theader3"><strong>Commission Amount</strong></td>
	 	  <td width="13%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$Pdao = new PartnerCommissionDAO();
		$list = $Pdao->fetchGroupAll(); 
				/////******for paging******/////////
		require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
		if($dopagination)
			$list = $Pdao->fetchGroupLimited($page, $perpage, "all");
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $p)
				{
				?>
				<tr <?php if($_GET['id']==$p->partner_commission_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $p->partner_name; ?></td>
                    <td class="tcell2"><?php echo $p->total_reg_user; ?></td>
                    <td class="tcell2"><?php echo $p->amt; ?></td>
                    
					<td class="tcell2"><a href="" onclick="javascript:window.open('viewPartnerCommissionDetails.php?pId=<?php echo $p->partner_id;?>','title','toolbar=no,menubar=no,scrollbars=yes,width=450,height=550');">View Details</a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td colspan="5" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="6">						<font color="#cc0000">
					<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">&nbsp;
					</font>			</tr>
			<?php
			}
		?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>
<!-- <div align="right"><a href="index.php?p=aepartners&function=add" class="theader3"><strong>ADD Partners </strong></a></div> -->
