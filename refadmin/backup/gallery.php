<?php
	
	if(isset($_GET['rCid']) && intval($_GET['rCid'])!=0)
		{
		$gdao = new GalleryDAO();
		$flag = $gdao->remove($_GET['rCid']);
		if($flag)
			$msg = "Selected gallery has been removed successfully.";
		else
			$msg = "Some error prevented this gallery from being removed.";
		}
	elseif(isset($_GET['id']) && intval($_GET['id'])!=0 && isset($_GET['s']) && $_GET['s']!="")
		{
		$gdao = new GalleryDAO();
		$id = intval($_GET['id']);
		if($_GET['s']=='no')
			$publish = "no";
		else
			$publish = "yes";
		
		$flag = $gdao->publishNunpublish($id, $publish);
		if($flag)
			$msg = "Publish/Unpublish status of selected gallery has been changed.";
		else
			$msg = "Publish/Unpublish status of gallery could not be changed.";
		}	
		
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="24%" class="ptitle"><strong>Manage Image Gallery:</strong></td>
    <td class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="4%" class="theader3"><strong>S.No.</strong></td>
			<td width="54%" class="theader3"><strong>GalleryName: </strong></td>
		 	<td width="23%" class="theader3"><strong>Publish/Unpublish </strong></td>
		 	<td width="19%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$gDAO = new GalleryDAO();
		$list = $gDAO->fetchAll();
		
		$sn =0;
		if(!empty($list))
			{
				foreach($list as $gallery)
				{
				?>
				<tr <?php if($_GET['id']==$gallery->id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $gallery->name;?></td>
					<td class="tcell2">
					<?php 
					if($gallery->publish == "yes")
						echo "<a href='index.php?p=gallery&s=no&id=".$gallery->id."'>Unpublish</a>";
					else
						echo "<a href='index.php?p=gallery&s=yes&id=".$gallery->id."'>Publish</a>";
					?>					</td>
					<td class="tcell2">
					<a href="index.php?p=aegallery&eCid=<?php echo $gallery->id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=gallery&rCid=<?php echo $gallery->id;?>" onClick="return confirm('Make sure before you delete this gallery?');">
				  	<img src="./images/delete.gif" border="0">				  	</a> |<a href="index.php?p=aeimage"> Add	Image</a> </td>
				</tr>
				<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td class="tcell_left">&nbsp;</td>
				<td class="tcell2" colspan="2" align="center"><font color="#cc0000">No Gallery
				     has been inserted yet.</font></td>
				<td class="tcell2">&nbsp;</td>
			</tr>
			<?
			}
		?>
	</table>
	
	
	</td>
  </tr>
  <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="2">
						<font color="#cc0000">
						<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
						</font>
					</td>
			</tr>
			<?php
			}
		?>
</table><br/>
<div align="right"><a href="index.php?p=aegallery" class="theader3"><strong>Add
      Gallery </strong></a></div>
