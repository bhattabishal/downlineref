<script language="JavaScript">
function ToggleAll(e)
{
	if (e.checked) {
	    CheckAll();
	}
	else {
	    ClearAll();
	}
}

function CheckAll(){
	var fo = document.frmSubscriber;
	var len = fo.elements.length;
		for (var i = 0; i < len; i++) {
	    var e = fo.elements[i];
	    if (e.name == "chkNewsletter[]") {
	       if(!e.checked){
	           e.click();
		   }
	    }
	}
	fo.toggleAll.checked = true;
}
function ClearAll(){
	var fo = document.frmSubscriber;
	var len = fo.elements.length;
	for (var i = 0; i < len; i++) {
	    var e = fo.elements[i];
	    if (e.name == "chkNewsletter[]") {
		   if(e.checked){
	           e.click();
		   }
	    }
	}
	fo.toggleAll.checked = false;
}

	function submitForm(obj, action) {
		var chk=CheckClicked();
		if (action=="delete") {
			if (chk) {
				submitForm1(obj, action); 
			} else {
				alert("Please select the subscribers");
				return false;
			}
		} else {
			if (chk && obj.newsletter_id.selectedIndex!=0) {
				submitForm1(obj, action);
			} else {
				if (chk==0) {
					alert("Please select the subscribers");
				} 
				if (obj.newsletter_id.selectedIndex==0) 
					alert("Please select the newsletter to be sent");
				return false;
			}
		}
	}
	
	function submitForm1(obj, action) {
		obj.mode.value=action;
		obj.submit();
		return true;
	}
	
	function CheckClicked() {
		var fo = document.frmSubscriber;
		var len = fo.elements.length;
		var checked=0;
		for (var i = 0; i < len; i++) {
			var e = fo.elements[i];
			if (e.name == "chkNewsletter[]") {
			   if(e.checked){
			   		checked=1;		
				   break;
			   }
			}
		}
		return checked;
	}
</script>

<script language="javascript">
function validateForm()
{
	if(document.getElementById('txtname').value=="")
	{
		alert("Please enter content name.");
		document.getElementById('txtname').focus();
		return false;
	}
	if(document.getElementById('email').value=="")
	{
		alert("Please enter the email address.");
		document.getElementById('email').focus();
		return false;
	}
		
}
</script>


<?PHP
extract($_POST);

$mode="";
if(isset($_REQUEST['mode']))
$mode=$_REQUEST['mode'];
switch($mode)
{
	case '':
		$result = $subsObject->selectAllSubscribers();
		/*****************************************************/
		//CODE FOR PAGINATION
		/*****************************************************/
		$count_rows=$db->row_count;
		$limit_total=20;
		$limit_from=0;
		if(!isset($_GET['p'])){$p=0;}else{$p=$_GET['p'];}
		if(isset($_GET['p']) and $_GET['p']!="")
			$page=$p-1;
		else
			$page=0;
		$limit_from=($page*$limit_total);
	 
		$result2=$subsObject->selectPagedSubscribe($limit_total,$limit_from);
		$sn=$limit_from;
		$num_rows=$db->row_count;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td align="left"><h1>&nbsp;</h1></td>
    <td align="right"><a href="index2.php?sr=manageNewsletterSubs&mode=add" class="containerTitle">Add Member (Newsletter/Articles)   </a></td>
  </tr>
</table>
<?PHP if($num_rows){?>
<form name="frmSubscriber" action="index.php?p=sendnews&mode=send_newsletter" method="post" enctype="multipart/form-data" >
<table width="100%" border="0" cellpadding="5" bgcolor="#FFFFFF">
  <tr class="">
    <td colspan="2" ><h1>Newsletter to be sent:</h1></td>
    <td colspan="4" align="left">
	<select name="newsletter_id" id="newsletter_id" >
	<option value="0">Select the subject of the newsletter</option>
			<?php
			$sDAO = new NewsletterDAO();
			$list = $sDAO->fetchAll();
			if(!empty($list))
				foreach($list as $news)
					{
					if($news->id == $_POST['speaking_id'])
						echo "<option value='".$news->id."' selected>".$news->subject."</option>";
					else
						echo "<option value='".$news->id."'>".$news->subject."</option>";
					}
			?>
		</select>
	<?php /*?><select name="newsletter_id" id="newsletter_id">
      <option value="0">Select the subject of the newsletter</option>
      <?PHP   
		  $result = $newsletterObject->selectAllnewsletter();
		  while($row=$db->get_row($result,'MYSQL_BOTH')){ 
			   if(strlen($row['newsletter_subject'])>50)
					 $newsletter_subject=substr($row['newsletter_subject'],0,50)."...";
			  else
 	 				$newsletter_subject=$row['newsletter_subject'];?>
      <option value="<?PHP echo $row['id']?>"> <?PHP echo $newsletter_subject ?></option>
      <?PHP }   ?>
    </select><?php */?></td>
  </tr>
 
  <tr class="gridHead">
    <td width="21%" align="left"><input name="toggleAll" type="checkbox" id="toggleAll" value="1" onClick="ToggleAll(this);" /></td>
    <td width="22%" align="left">Name</td>
    <td width="28%" align="left">Email</td>
    <td width="28%" align="left">State</td>
    <!--<td width="14%" align="left">Country</td>-->
    <td width="15%" align="left">Sign UP Date</td>
  </tr> 
  <?PHP 
  $count=0;
  while($row=$db->get_row($result2,'MYSQL_BOTH')){
   if($count%2==0){
		$class ="even";
	   }
	   else{
		$class = "odd";
	   }
  ?>
  <tr class="<?PHP echo $class ?>">
    <td ><input name="chkNewsletter[]" type="checkbox" id="chkNewsletter[]" value="<?=$row['id'];?>"></td>
    <td ><?PHP echo $row['name']?></td>
    <td ><?PHP echo $row['email']?></td>
    <td ><?PHP echo $row['city']?></td>
    <?php /*?><td ><?PHP echo $row['country']?></td><?php */?>
    <td ><?PHP echo $row['subscribed_date']?></td>
  </tr>
  <?PHP $count++;}?>
   <tr class="">
    <td colspan="6" >
	<?PHP
/*	if(isset($_GET['p']))
		{
			$pos=strpos($_SERVER['REQUEST_URI'],"&p=");
			$loc=substr($_SERVER['REQUEST_URI'],0,$pos);
		}
	else
			$loc=$_SERVER['REQUEST_URI'];
			$totalPages=ceil($count_rows/$limit_total);
			for($i=1;$i<=$totalPages;$i++)
				{
					if($p==$i)
						{
							echo "&nbsp;".$i."&nbsp;";
						}
					else
						{
							echo "&nbsp;"."<a href='".$loc."&p=$i'>".$i."</a>&nbsp;";
						}
				}*/
	?></td>
  </tr>
   <tr class="">
     <td colspan="6" >      </td>
   </tr>
</table>
 <input name="send_newsletter" type="button" value="Send Newsletter" onClick="submitForm(this.form, 'send_newsletter');"  class="field" />
	  <input name="delete" type="button" value="Delete" onClick="submitForm(this.form, 'delete');"  class="field" />
<?PHP }else {
	print"<table width='100%' border='0' cellpadding='5' bgcolor='#FFFFFF'>
	  <tr class=''>
		<td width='21%'>&nbsp;</td>
		<td width='17%' colspan='2' align='right'></td>
	  </tr>
	  <tr class='gridHead'>
		<td colspan='3' align='center'>No members have been posted their request till now.</td>
	  </tr>
	</table>";
	}
?>
<?PHP 
 break;
 /*?>case 'add':
	if(isset($_POST['add']))
	   {
	  
			$name=$_POST['txtname'];
			$email=$_POST['email'];
			$state=$_POST['state'];
			$subscribed_date  	=date("Y-m-d");
			$subsObject->insertNewsletterSub($name,$email,$state,$subscribed_date);
			echo"<script>alert('Member has been Added successfully.');location.href='index2.php?sr=manageNewsletterSubs'</script>";
	  }
	   else
	   {
?>
<legend>Add Content</legend>
<form name="form1" action="" method="post" enctype="multipart/form-data" onSubmit="return validateForm();">
  <table width="100%" border="0" cellpadding="5" bgcolor="#FFFFFF">
  <tr>
    <td width="21%"> <strong>Name</strong>:<font color="#FF0000" size="1">*</font></td>
    <td width="79%"><input name="txtname" type="text" id="txtname" size="40" class="field"></td>
  </tr>
  <tr>
    <td><strong>Email:<font color="#FF0000" size="1">*</font></strong></td>
    <td><input name="email" type="text" id="email" size="40" class="field" /></td>
  </tr>
  <tr>
    <td><strong>State:</strong> </td>
    <td><textarea name="state" cols="80" rows="3" id="state" class="field"></textarea></td>
  </tr>
  

  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="add" id="add" value="Submit"  class="field">
	 <input type="reset" name="reset" id="reset" value="Reset"  class="field"><font size="1"> (<font color="#FF0000">*</font>) marked fields are required </font></td>
  </tr>
</table>
</form>
<?php */?>
<?php //}?>
<?PHP 
// break;
 
case 'send_newsletter':
	$file_dir="./../UploadNewsletter/";
	if(isset($_POST['sbmit'])=='true')
	   {
	   		$newsletter_id=$_POST['newsletter_id'];
			$chkNewsletter=$_POST['chkNewsletter'];
			if (is_array($chkNewsletter)) {
				for ($i=0; $i<sizeof($chkNewsletter); $i++ ) {
					 $sql="SELECT email,
							subscribed_date
							FROM tbl_newslettersubscribers 
							WHERE id=".$chkNewsletter[$i]."";
						//	exit;
					$result=$db->select($sql);
					if ($row=$db->get_row($result,'MYSQL_BOTH')) {
						$arr_email[$i]=$row['email'];
						} //end of if ($row=$db->get_row($result,'MYSQL_BOTH')) {
				} //end of for ($i=0; $i<sizeof($chkNewsletter); $i++ ) {...
						 $sql="SELECT newsletter_subject,
							 newsletter_desc,
							 newsletter_file,
							 published_date  
							 FROM tbl_newsletter 
							 WHERE id=$newsletter_id";
					$result=$db->select($sql);
					if($row=$db->get_row($result,'MYSQL_BOTH')) {
						$subject=stripslashes($row['newsletter_subject']);
						$message=$row['newsletter_desc'] . " Published date:" . $row['published_date'];
						$file_name=$row['newsletter_file'];
						$file_path=$file_dir . $file_name;
					}
		/* ----------------------- begin: send newsletter -------------------*/
					require_once("mailer/mail_class.php");
						$mail = new MyMailer();
						for ($i=0; $i<sizeof($arr_email); $i++) {
							$mail->AddBCC($arr_email[$i]);
						}
						$mail->From = "padma_24gc@yahoo.com";
						$mail->FromName = "www.drjacknin.com";
						$mail->Subject = $subject;
						$mail->IsHtml(true);
						
						for($j=0; $j<count($arr_email); $j++) {
						//echo $fname[$j];
								//$message="Dear ".$fname[$j]."<br/>".$message;
								 $mail->Body=$message;
								 $mail->AddAddress($arr_email[$j]);
								 
									// $mail->Body = ;
									/*$mail->IsSMTP();
									$mail->Host = "mail.www.drjacknin.com";
									$mail->SMTPAuth = true;
									$mail->Username = "padma_24gc@yahoo.com";
									$mail->Password = "";*/
									
									if ($file_name) {
										$mail->AddAttachment($file_path); 
									}
									
									if(!$mail->Send())
									{
									   echo $msg="Message was not sent";
									   echo "Mailer Error: " . $mail->ErrorInfo;
									}
									else
									{
										$msg="<center><h3>Newsletter has been successfully sent!!<br />";
										echo"<script>location.href='index2.php?sr=manageNewsletterSubs&$msg'</script>";
									}
									$mail->ClearAddresses();
									$mail->ClearAttachments();
								 
						}
						
			
	  }
?>
<?PHP

}?> 
 <input name="mode" type="hidden" id="mode">
<input name="sbmit" type="hidden" id="sbmit" value="true">
</form>