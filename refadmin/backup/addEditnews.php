<?php
$n_id = $_REQUEST['nId'];
$function = $_GET['function'];

$nvo = new NewsVO();
/*if(isset($_GET['nId']) && intval($_GET['nId'])!=0)
{
	$newsDAO = new NewsDAO();
	$id = intval($_GET['nId']);
	$tvo = $newsDAO->fetchDetails($id);
}
*/
$ndao = new NewsDAO();

if($function == 'add')
	{
	echo"<h2>Add News</h2>";
	}
else 
	{
	echo "<h2>Edit News</h2>";
	if ($_GET['nId'])
		{
		$nvo = $ndao->fetchDetails($n_id);
		}
	}
// the different message for updating and adding the news
$updated_msg="<script language='javascript'>alert('News has been Updated successfully.'); location='index.php?p=news';</script>\n";
$inserted_msg="<script language='javascript'>alert('News has been Added successfully.'); location='index.php?p=news';\n</script>";
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$nvo->news_id = $_POST['news_id'];
	$nvo->title = $_POST['title'];
	$nvo->brief = $_POST['brief'];
	$nvo->detail = $_POST['detail'];
	$nvo->news_date = $_POST['news_date']; 
	$nvo->publish = $_POST['publish'];
	$nvo->updated_date = $cur_time;
	$nvo->updated_by= $_SESSION['full_name'];
	
	$nvo->formatInsertVariables();
	
	//checking server side validation for different fields
			
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_POST['news_id']!="") 
			{
			if($ndao->update($nvo))
				echo $updated_msg;
			}
		else
			{
			if($ndao->insert($nvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditNews" id="addEditNews" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>News   Form:</strong></td>
                              </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                    <tbody>
                                              <tr>
                                                <td width="178" align="left" class="text">&nbsp;</td>
                                                <td width="789"  align="left" class="main">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Title :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="title" type="text" class="field" id="title" value="<?php echo $nvo->title ?>" size="70" valiclass="required" req="2" valimessage="Title:This field is required!
" />
											
                                                  &nbsp;</td>
                                              </tr>
                                          
                                              <tr>
                                                <td align="left" class="text"> Brief Description :</td>
                                                <td class="main"  align="left">
												<?php
													include "fckeditor2/fckeditor.php";
													$oFCKeditor = new FCKeditor('brief') ;
													$oFCKeditor->BasePath = 'fckeditor2/' ;
													$oFCKeditor->Value	=  $nvo->brief;
													$oFCKeditor->Create() ;
												?>												</td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">Detail Description :</td>
                                                <td class="main"  align="left"><?php
													include "fckeditor2/fckeditor.php";
													$oFCKeditor = new FCKeditor('detail') ;
													$oFCKeditor->BasePath = 'fckeditor2/' ;
													$oFCKeditor->Value	=  $nvo->detail;
													$oFCKeditor->Create() ;
												?></td>
                                              </tr>
                                              	
												 <tr>
												   <td align="left" valign="middle" class="content">*News Date :</td>
                                                   <?php
                                                   $news_date=$nvo->news_date;
													if($news_date==""|| $news_date=="0000-00-00")
													{
														$news_date="<script>DateInput('news_date',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$news_date="<script>DateInput('news_date',true,'YYYY-MM-DD','".$news_date."');</script>";
													}
																		
													?>
                        					<td align="left" valign="middle" class="content"><?php echo $news_date ?></td>
										      </tr>
                                              <tr>
                                                <td align="left" class="text">Status :</td>
                                                <td class="main"  align="left">
												<select name="publish" id="publish">
												<option value="1" <?php if($nvo->publish == "yes") echo "selected"; ?>  >Yes</option>
												<option value="0" <?php if($nvo->publish == "no") echo "selected"; ?>  >No</option>
											 	 </select></td>
                                              </tr>
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="news_id" id="news_id" value="<?php echo $nvo->news_id;?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						 
                        </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>