<?php
	$gVO = new GalleryVO();
	if(isset($_GET['eCid']) && intval($_GET['eCid'])!=0)
		{
		$gDAO = new GalleryDAO();
		$id = intval($_GET['eCid']);
		$gVO = $gDAO->fetchDetails($id);
		}
?>

<div class="ptitle">Add/Edit Gallery: </div>
<p>Fields with <span class="red">*</span> are required fields.</p>
<form name="addEditCountries" id="addEditCountries" action="./gcode.php" method="post">
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #ccc;">
	<tr>
		<td>
		
			<table width="100%" border="0" cellspacing="2">
			  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%">Gallery Name <span class="red">*</span>: </td>
				<td width="64%">
				  <input type="text" name="name" id="name" value="<?php echo $gVO->name;?>" valiclass="required" req="1"  valimessage="Enter name of the gallery." class="field" size="40">						</td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td>Publish: </td>
			    <td>
				<select name="publish" class="field" id="publish">
					<option value="yes" <?php if($gVO->publish=='yes') echo "selected";?>>Publish</option>
					<option value="no"  <?php if($gVO->publish=='no') echo "selected";?>>Unpublish</option>
				</select>
				</td>
		      </tr>
			   <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>

		</td>
	</tr>
</table><br/>
<div align="left">
	<a href="index.php?p=gallery" class="theader3">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="hidden" name="save" id="save">
	<input type="hidden" name="id" id="id" value="<?php echo $gVO->id;?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);">
</div>		
</form>