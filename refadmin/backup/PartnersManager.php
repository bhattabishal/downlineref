<?php

include_once "../".DIR_INCLUDES."/functions.php";
$delId=$_GET['pId'];
$pvo = new PartnersVO();
$pdao = new PartnersDAO();

$count = 1;
	if(isset($_GET['pId']) && intval($_GET['pId'])!=0)
		{
		$rP = new PartnersDAO();
		$flag = $rP->remove($_GET['pId']);
		if($flag)
			$msg = "Selected Partners has been removed successfully.";
		else
			$msg = "Some error prevented Partners from being removed";
		}

	
	
	if(isset($_GET['sId']) && intval($_GET['sId'])!=0)
		{
		$sP = new PartnersDAO();
		$flag = $sP->publishNunpublish($_GET['sId'],$_GET['status']);
		if($flag)
			$msg = "Status has been changed successfully.";
		else
			$msg = "Some error prevented Partners from being updated";
		}
	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="23%" class="ptitle"><strong>Manage Partners:</strong></td>
    <td width="77%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="5%" class="theader3"><strong>S.No.</strong></td>
			<td width="26%" class="theader3"><strong>Partners Full Name</strong></td>
          <td width="17%" class="theader3"><strong>Street City/Town</strong></td>
	 	  <td width="13%" class="theader3"><strong>Country</strong></td>
	 	  <td width="14%" class="theader3"><strong>Email</strong></td>
	 	  <td width="12%" class="theader3"><strong>Status</strong></td>
	 	  <td width="13%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$Pdao = new PartnersDAO();
		$list = $Pdao->fetchAll(); 
				/////******for paging******/////////
		require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
		if($dopagination)
			$list = $Pdao->fetchLimited($page, $perpage, "all");
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $p)
				{
				?>
				<tr <?php if($_GET['id']==$p->partner_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $p->title.". ". $p->first_name." ".$p->sur_name ;?></td>
                    <td class="tcell2"><?php echo $p->street."<br \>".$p->city_town; ?></td>
                    <td class="tcell2"><?php echo $p->country ;?></td>
                    <td class="tcell2"><?php echo $p->email ;?></td>
                   <?php if($p->status=="valid"){$stat="invalid";}else{$stat="valid";} ?>
                    <?php if($p->scheme_id!="" && $p->scheme_id!=0)
					{
					?>
					<td class="tcell2"><a href="index.php?p=partners&amp;sId=<?php echo $p->partner_id;?>&amp;status=<?php echo $stat; ?>"><?php echo $p->status ;?></a></td>
                    <?php 
					}
                    else
                    {
					?>
                    <td class="tcell2"><?php echo $p->status ;?></a></td>
					<?php
					}
					?>
                    
					<td class="tcell2">
					<a href="index.php?p=aepartners&amp;pId=<?php echo $p->partner_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=partners&amp;pId=<?php echo $p->partner_id;?>" onclick="return confirm('Make sure before you delete this Partners?');"><img src="./images/delete.gif" border="0" /></a> | <a href="" onclick="javascript:window.open('viewPartnerDetails.php?pId=<?php echo $p->partner_id;?>','title','toolbar=no,menubar=no,scrollbars=yes,width=450,height=550');">View Details</a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td colspan="7" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="7">						<font color="#cc0000">
					<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">
					<td width="0%" class="tcell2">&nbsp;</td>
						</font>			</tr>
			<?php
			}
		?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>
<div align="right"><a href="index.php?p=aepartners&function=add" class="theader3"><strong>ADD Partners </strong></a></div>
