<?php

include_once "../".DIR_INCLUDES."/functions.php";
$delId=$_GET['mpId'];
$mpvo = new MonthlyPrizeVO();
$mpdao = new MonthlyPrizeDAO();

$count = 1;
	if(isset($_GET['mpId']) && intval($_GET['mpId'])!=0)
		{
		$rMP = new MonthlyPrizeDAO();
		$flag = $rMP->remove($_GET['mpId']);
		if($flag)
			$msg = "Selected Monthly Prize has been removed successfully.";
		else
			$msg = "Some error prevented Monthly Prize from being removed.";
		}

	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<?php 

$search_value=$_REQUEST['search_value'];
?>
<script language="javascript">
function redirect(url)
{
	var newurl=url+'&search_value='+document.searchform.search_value.value;
	window.open(newurl,"_parent");
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="28%" class="ptitle"><strong>Manage Monthly Prize:</strong></td>
    <td width="72%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
   <tr>
    <td colspan="2" class="ptitle"><table>
      <form action="index.php?p=monthly_prize" method="post" name="searchform" id="searchform">
        <tr>
          <td><strong>Filter By Monthly Prize : </strong>&nbsp;&nbsp; </td>
          <td><input type="text" name="search_value" value="<?=$search_value?>" size="30" maxlength="60" />
          </td>
          <td><input type="button" name="search" value="Search" class="bttn" onclick="return redirect('index.php?p=monthly_prize')" />
          </td>
        </tr>
      </form>
    </table></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="6%" class="theader3"><strong>S.No.</strong></td>
			<td width="11%" class="theader3"><strong>Prize Month</strong></td>
            <td width="16%" class="theader3"><strong>Organiser Data</strong></td>
          <td width="10%" class="theader3"><strong>Closure Date</strong></td>
	 	  <td width="16%" class="theader3"><strong>Description</strong></td>
	 	  <td width="16%" class="theader3"><strong>Answer</strong></td>
	 	  <td width="12%" class="theader3"><strong>Prize Value</strong></td>
	 	  <td width="13%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$MPdao = new MonthlyPrizeDAO();
		$list = $MPdao->fetchAll(); 
				
		
		
		if($search_value=="" && $search_value==null)
		{
			$list = $MPdao->fetchAll();
		}
		else
		{
			$list = $MPdao->fetchSelectedAll($search_value);
		}
			/////******for paging******/////////
		if($search_value=="" && $search_value==null)
		{
			require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
			if($dopagination)
				$list = $MPdao->fetchLimited($page, $perpage, "all");
		}
		else
		{
			require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
			if($dopagination)
				$list = $MPdao->fetchSelectedLimited($page,$perpage, $search_value);
		}
		/////****end of paging*******//////////
		
		
		
		
		
		
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $mp)
				{
				?>
				<tr <?php if($_GET['id']==$mp->prize_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $mp->prize_month ;?></td>
                    <td class="tcell2"><?php echo $mp->organiser_data;?></td>
                    <td class="tcell2"><?php echo $mp->closure_date ;?></td>
                    <td class="tcell2"><?php echo $mp->description ;?></td>
                    <td class="tcell2"><?php echo $mp->answer ;?></td>
                    <td class="tcell2"><?php echo $mp->prize_value ;?></td>
                    <td class="tcell2">
					<a href="index.php?p=aemonthly_prize&amp;mpId=<?php echo $mp->prize_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=monthly_prize&amp;mpId=<?php echo $mp->prize_id;?>" onclick="return confirm('Make sure before you delete this Monthly Prize?');"><img src="./images/delete.gif" border="0" /></a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td colspan="8" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="9">						<font color="#cc0000">
					<?php
						if($search_value=="" && $search_value==null)
						{
							$url = $_SERVER['REQUEST_URI'];
						}
						else
						{
							$url = $_SERVER['REQUEST_URI']."&search_value=".$search_value;  
						}
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">&nbsp;
					</font>			</tr>
			<?php
			}
		?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>

<div align="right"><a href="index.php?p=importmonthly_prize" class="theader3"><strong>Import Monthly Prize </strong></a>&nbsp;&nbsp;<a href="index.php?p=aemonthly_prize&function=add" class="theader3"><strong>ADD Monthly Prize </strong></a></div>
