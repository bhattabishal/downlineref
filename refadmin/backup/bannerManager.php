<?php
include_once "../mvpincludes/functions.php";
	
$delId=$_GET['nId'];
$nvo = new BannerVO();
$ndao = new BannerDAO();

$count = 1;
if($delId != 0)
	{	
	$count_banner_pics = $ndao->countBannerfiles($delId);	
	//echo $count_gal_pics;
	if( $count_banner_pics <= 0 )
		{
		$ndao->remove($delId) ;	
		$msg = 'Banner has been deleted successfully';
		}
	else
		{
		$msg = 'Banner cannot be deleted because it is not empty';
		}		
	$deleted_msg="<script> alert('$msg'); document.location='index.php?p=banner';</script>";
	echo $deleted_msg;
	}
		
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="28%" class="ptitle"><strong>Manage Banner:</strong></td>
    <td width="72%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="4%" class="theader3"><strong>S.No.</strong></td>
			<td width="32%" class="theader3"><strong>Title</strong></td>
		 	<td width="32%" class="theader3"><strong>Status</strong></td>
		 	<td width="32%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$bannerdao = new BannerDAO();
		$list = $bannerdao->fetchAll(); 

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $banner)
				{
				?>
				<tr <?php if($_GET['id']==$banner->id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $banner->title ;?></td>
					<td class="tcell2"><?php if($banner->publish=="1"){echo"yes";}else{echo"No";}?></td>
					<td class="tcell2">
					<a href="index.php?p=addeditbanner&amp;nId=<?php echo $banner->id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=banner&amp;nId=<?php echo $banner->id;?>" onclick="return confirm('Make sure before you delete this banner?');"><img src="./images/delete.gif" border="0" /></a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td class="tcell_left">&nbsp;</td>
				<td colspan="2" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
				<td class="tcell2">&nbsp;</td>
			</tr>
			<?php
			}
			?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>
<div align="right"><a href="index.php?p=addeditbanner&function=add" class="theader3"><strong>ADD banner </strong></a></div>
