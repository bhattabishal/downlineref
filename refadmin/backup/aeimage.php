<?php
$imagevo = new ImageVO();
	if(isset($_GET['eDid']) && intval($_GET['eDid']))
		{
		$dao = new ImageDAO();
		$id = intval($_GET['eDid']);
		$imagevo = $dao->fetchDetails($id);
		}

?>

<div class="ptitle">Add/Edit Image: </div>
<p>Fields with <span class="red">*</span> are required fields.</p>
<form name="distributorsForm" id="distributorsForm" action="icode.php" method="post" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="1" style="border:1px solid #ccc;">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="medium">Title<span  style="color:#FF0000;">*</span>:</td>
    <td>
      <input type="text" name="name" id="name" class="field" value="<?php echo $imagevo->name;?>" size="40" valiclass="required" req="1" valimessage="Enter title of image">    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="medium">Gallery<span  style="color:#FF0000;">*</span>:</td>
    <td><select name="gallery_id" id="gallery_id" class="field" valiclass="select" valimessage="Select Download Section for Image.">
      <option value="">Select</option>
      <?php
		$gallery = new GalleryDAO();
		$list = $gallery->fetchAll();
		if(!empty($list))
			foreach($list as $galleryname)
				{
				if($galleryname->id == $imagevo->gallery_id)
					echo "<option value='".$galleryname->id."' selected>".$galleryname->name."</option>";
				else
					echo "<option value='".$galleryname->id."'>".$galleryname->name."</option>";
				}
		?>
    </select></td>
  </tr>
  <tr>
      <td>&nbsp;</td>
      <td class="medium">Short Description:</td>
      <td><label>
	  <textarea name="caption" cols="20" rows="5"><?php echo $imagevo->caption;?></textarea>
        <!--<input name="caption" type="text" id="caption" value="<?php echo $imagevo->caption;?>" size="40" />-->
      </label></td>
    </tr>
  
  <?php
  if($imagevo->image != "")
  	{
	?>
	<tr>
		<td width="7%">&nbsp;</td>
		<td width="13%" class="medium">&nbsp;</td>
		<td width="80%"><img src="../tgincludes/vignette.php?f=../files/galleryimage/<?php echo $imagevo->image;?>&amp;h=100&amp;w=100&amp;c=1" title="<?php echo $imagevo->name;?>"/></td>
  	</tr>
	<?php
	}
  ?>
    
    <tr>
    <td width="7%">&nbsp;</td>
    <td width="13%" class="medium">Image<span  style="color:#FF0000;">*</span>:</td>
    <td width="80%"><input type="file" name="image" id="image" class="field" size="40" <?php if($imagevo->id == 0) echo 'valiclass="required" req="1" valimessage="Please provide image."';?>>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="medium">Publish:</td>
    <td>
	<select name="publish" id="publish" class="field">
		<option value="yes" <?php if($imagevo->publish=='yes') echo "selected";?>>Publish</option>
		<option value="no" <?php if($imagevo->publish=="no") echo "selected";?>>Unpublish</option>
	</select>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="medium">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br/>
<div align="left">
	<a href="index.php?p=img&pg=<?php echo $_GET['pg'];?>" class="theader3">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="hidden" name="save" id="save">
	<input type="hidden" name="id" id="id" value="<?php echo $imagevo->id;?>" />
	<input type="hidden" name="pg" id="pg" value="<?php echo $_GET['pg'];?>">
	<input type="hidden" name="oldImage" id="oldImage" value="<?php echo $imagevo->image;?>">
	<input name="cid" type="hidden" id="cid" value="<?= $gallery_id;?>" /><?= $gallery_id;?>
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);">
</div>		<?php echo $image->id;?>
</form>