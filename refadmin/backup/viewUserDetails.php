<?php

session_start();
require_once "../global.php";
require_once "loginchecker.php";
require_once "inc/pageFinder.php";
require_once "../".DIR_INCLUDES."/db_connection.php";
require_once "../".DIR_INCLUDES."/functions.php";

$u_id = $_GET['uId'];
$uvo = new UsersVO();
$udao = new UsersDAO();
if ($_GET['uId'])
{
	$uvo = $udao->fetchDetails($u_id);
}
?>
<table border="0" cellpadding="3" cellspacing="0" width="100%" style="border:1px solid #ccc;" align="center" >
  <tbody>
    <tr>
      <td colspan="2" align="left" class="text"><div align="center"><strong>User Details</strong></div></td>
    </tr>
    <tr>
      <td width="241" align="left" bgcolor="#F4F4F4" class="text"><div align="right"><strong>Title :</strong></div></td>
      <td width="960"  align="left" bgcolor="#F4F4F4" class="main"><?php echo $uvo->title; ?>      </td>
    </tr>
    <tr>
      <td align="left" class="text"> <div align="right"><strong>First Name:</strong></div></td>
      <td class="main"  align="left"><?php echo $uvo->first_name; ?></td>
    </tr>
    <tr>
      <td align="left" bgcolor="#F4F4F4" class="text"><div align="right"><strong>Last Name:</strong></div></td>
      <td  align="left" bgcolor="#F4F4F4" class="main"><?php echo $uvo->sur_name; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Street:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $uvo->street; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Postal Code:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->postal_code; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>City/Town:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $uvo->city_town; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Country:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->country ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Telephone:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $uvo->tel_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Mobile:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->mobile_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Email:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->email; ?></td>
    </tr>
   
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>DOB:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $uvo->dob; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Bank:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->bank; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Account No.:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $uvo->account_no; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Account Owner:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->owner_of_acc; ?></td>
    </tr>
     <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Balance:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->balance; ?></td>
    </tr>
     <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Bank Status:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->bank_status; ?></td>
    </tr>
     <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Addational Offers:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->addational_offers; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><div align="right"><strong>Registered Date:</strong></div></td>
      <td align="left" valign="middle" bgcolor="#F4F4F4" class="content"><?php echo $uvo->registered_date; ?></td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="content"><div align="right"><strong>Status:</strong></div></td>
      <td align="left" valign="middle" class="content"><?php echo $uvo->status; ?>  </td>
    </tr>
  </tbody>
</table>



             

