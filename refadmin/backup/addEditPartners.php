<?php
$p_id = $_REQUEST['pId'];
$function = $_GET['function'];

$pvo = new PartnersVO();

$pdao = new PartnersDAO();

if($function == 'add')
	{
	echo"<h2>Add Partners</h2>";
	}
else 
	{
	echo "<h2>Edit Partners</h2>";
	if ($_GET['pId'])
		{
		$pvo = $pdao->fetchDetails($p_id);
		}
	}
// the different message for updating and adding the news
$updated_msg="<script language='javascript'>alert('Partners has been Updated successfully.'); location='index.php?p=partners';</script>\n";
$inserted_msg="<script language='javascript'>alert('Partners has been Added successfully.'); location='index.php?p=partners';\n</script>";
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$pvo->partner_id = $_POST['partner_id'];
	$pvo->scheme_id = $_POST['scheme_id'];
	$pvo->title = $_POST['title'];
	$pvo->first_name = $_POST['first_name'];
	$pvo->sur_name = $_POST['sur_name'];
	$pvo->company = $_POST['company'];
	$pvo->street = $_POST['street'];
	$pvo->postal_code = $_POST['postal_code'];
	$pvo->city_town = $_POST['city_town'];
	$pvo->country = $_POST['country'];
	$pvo->tel_no = $_POST['tel_no'];
	$pvo->mobile_no = $_POST['mobile_no'];
	$pvo->url = $_POST['url'];
	$pvo->email = $_POST['email'];
	$pvo->password = $_POST['password'];
	$pvo->dob = $_POST['dob'];
	$pvo->bank = $_POST['bank'];
	$pvo->account_no = $_POST['account_no'];
	$pvo->account_holders = $_POST['account_holders'];
	$pvo->status = $_POST['status'];
	$pvo->registered_date = $_POST['registered_date'];
	$pvo->updated_date = $_POST['updated_date'];
	$pvo->updated_by = $_POST['updated_by'];
	
	$pvo->formatInsertVariables();
	
	//checking server side validation for different fields
			
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_POST['partner_id']!="") 
			{
			if($pdao->update($pvo))
				echo $updated_msg;
			}
		else
			{
			if($pdao->insert($pvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditPartners" id="addEditPartners" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>Partners Form:</strong></td>
                              </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                    <tbody>
                                              <tr>
                                                <td width="178" align="left" class="text">&nbsp;</td>
                                                <td width="789"  align="left" class="main">&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">*Commission Scheme :</td>
                                                <td align="left" class="main"><select name="scheme_id" id="scheme_id" class="field" valiclass="select" valimessage="Please select Scheme For this Partner.">
                                                <option value="">Select </option>
                                                  <?php	
                                               //$pvo=new PartnersVO;
											   $csdao = new CommissionSchemeDAO();
											   $list=$csdao->fetchAll();
											   foreach($list as $cs)
											   { ?>
											   	<option value="<?php echo $cs->commission_scheme_id ?>"<?php if($cs->commission_scheme_id==$pvo->scheme_id) echo "selected"; ?>><?php echo $cs->scheme_name ?></option>;
											   <?php
                                               }
											   
											   ?>
                                                </select></td>
                                              </tr>
                                              <tr> 
                                    <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Title :&nbsp;</td>
                                    <td class="main"  align="left">
                                    <select name="title" id="title" class="field" valiclass="select" valimessage="Please select title.">
                                    <option value="0" selected="selected">Ausw�hlen</option>
                                    <option value="Herr" <?php if($pvo->title=="Herr") echo "selected";?>>Herr</option>
                                    <option value="Frau" <?php if($pvo->title=="Frau") echo "selected";?>>Frau </option>
                                    </select>                                                
                                    </td>
                                              </tr>

                                              <tr>
                                                <td align="left" class="text"> *First Name:</td>
                                                <td class="main"  align="left"><input name="first_name" type="text" class="field" id="first_name" value="<?php echo $pvo->first_name ?>" size="30" valiclass="required" req="2" valimessage="First Name:This field is required!
" /></td>
                                      </tr>
                                              <tr>
                                                <td align="left" class="text">*Last Name:</td>
                                                
                                                <td class="main"  align="left"><input name="sur_name" type="text" class="field" id="sur_name" value="<?php echo $pvo->sur_name ?>" size="30" valiclass="required" req="2" valimessage="Last Name:This field is required!
" /></td>
                                      </tr>
                                              	
												 <tr>
												   <td align="left" valign="middle" class="content">Company:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="company" type="text" class="field" id="company" value="<?php echo $pvo->company ?>" size="30" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Street:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="street" type="text" class="field" id="street" value="<?php echo $pvo->street ?>" size="30" valiclass="required" req="2" valimessage="Street:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Postal Code:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="postal_code" type="text" class="field" id="postal_code" value="<?php echo $pvo->postal_code ?>" size="30" valiclass="required" req="2" valimessage="Postal Code:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*City/Town:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="city_town" type="text" class="field" id="city_town" value="<?php echo $pvo->city_town ?>" size="30" valiclass="required" req="2" valimessage="City/Town:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Country:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="country" type="text" class="field" id="country" value="<?php echo $pvo->country ?>" size="30" valiclass="required" req="2" valimessage="Country:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">Telephone:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="tel_no" type="text" class="field" id="tel_no" value="<?php echo $pvo->tel_no ?>" size="30" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">Mobile:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="mobile_no" type="text" class="field" id="mobile_no" value="<?php echo $pvo->mobile_no ?>" size="30" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*URL:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="url" type="text" class="field" id="url" value="<?php echo $pvo->url ?>" size="30" valiclass="required" req="2" valimessage="URL:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Email:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="email" type="text" class="field" id="email" value="<?php echo $pvo->email ?>" size="30" valiclass="email"  valimessage="Enter valid email address." /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Password:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="password" type="password" class="field" id="password" value="<?php echo $pvo->password ?>" size="30" valiclass="required" req="2" valimessage="Password:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*DOB:</td>
                                                <?php
                                                   $dob=$pvo->dob;
													if($dob==""|| $dob=="0000-00-00")
													{
														$dob="<script>DateInput('dob',true,'YYYY-MM-DD');</script>";
													}
													else
													{
														$dob="<script>DateInput('dob',true,'YYYY-MM-DD','".$dob."');</script>";
													}
																		
													?>  
                        					<td align="left" valign="middle" class="content"><?php echo $dob; ?></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Bank:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="bank" type="text" class="field" id="bank" value="<?php echo $pvo->bank ?>" size="30" valiclass="required" req="2" valimessage="Bank:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Account No.:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="account_no" type="text" class="field" id="account_no" value="<?php echo $pvo->account_no ?>" size="30" valiclass="required" req="2" valimessage="Account No:This field is required!
" /></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Account Holders:</td>
                                                  
                        					<td align="left" valign="middle" class="content"><input name="account_holders" type="text" class="field" id="account_holders" value="<?php echo $pvo->account_holders ?>" size="30" valiclass="required" req="2" valimessage="Account Holders:This field is required!
" /></td>
								      </tr>
                                      <tr>
                                       <td align="left" valign="middle" class="content">*Registered Date:</td>
                                      <?php
                                       $registered_date=$pvo->registered_date;
                                        if($registered_date==""|| $registered_date=="0000-00-00")
                                        {
                                            $registered_date="<script>DateInput('registered_date',true,'YYYY-MM-DD');</script>";
                                        }
                                        else
                                        {
                                            $registered_date="<script>DateInput('registered_date',true,'YYYY-MM-DD','".$registered_date."');</script>";
                                        }
                                                            
                                        ?>  
                        					<td align="left" valign="middle" class="content"><?php echo $registered_date; ?></td>
								      </tr>
                                      <tr>
												   <td align="left" valign="middle" class="content">*Status:</td>
                                                  
                        					<td align="left" valign="middle" class="content">
                                            <select name="status" id="status" class="field" valiclass="select" valimessage="Please select status.">
                        					  <option value="valid" <?php if($pvo->status=="valid") echo "selected";?>>Valid</option>
                        					  <option value="invalid" <?php if($pvo->status=="invalid") echo "selected";?>>Invalid</option>
                                            </select></td>
								      </tr>
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="partner_id" id="partner_id" value="<?php echo $pvo->partner_id;?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						 
                        </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>