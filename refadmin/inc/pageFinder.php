<?php  
	switch($_GET['p'])
	{
		case "welcome":				$includepage = "welcome.php";
									$pagecode 	= "welcome";
									break;
		
		case "content": 		    $includepage = "contentManager.php";
									$pagecode = "content";
									break;
		
		case "aecontent":			$includepage = "addEditContent.php";
									$pagecode = "content";
									break;
		
		case "news":				$includepage = "NewsManager.php";
									$pagecode = "news";
									break;
		
		case "aenews":				$includepage = "addEditnews.php";
									$pagecode = "news";
									break;
									
		case "video":				$includepage = "videoManager.php";
									$pagecode = "video";
									break;
		
		case "aevideo":				$includepage = "addEditVideo.php";
									$pagecode = "video";
									break;
									
		case "blog":				$includepage = "blogManager.php";
									$pagecode = "blog";
									break;
		
		case "aeblog":				$includepage = "addEditBlog.php";
									$pagecode = "blog";
									break;
								
		case "comment":				$includepage = "commentManager.php";
									$pagecode = "comment";
									break;
		
		case "aecomment":			$includepage = "addEditComment.php";
									$pagecode = "comment";
									break;
		
		case "title":				$includepage = "titleManager.php";
									$pagecode = "title";
									break;
											
		case "aetitle":				$includepage = "addEditTitle.php";
									$pagecode = "title";
									break;
									
		case "product":			$includepage = "productManager.php";
									$pagecode = "product";
									break;
		
		case "aeproduct":		$includepage = "addEditProduct.php";
									$pagecode = "product";
									break;
									
		case "slidercontent":			$includepage = "sliderContentManager.php";
									$pagecode = "slidercontent";
									break;
		
		case "aeslidercontent":		$includepage = "addEditSliderContent.php";
									$pagecode = "slidercontent";
									break;
									
		case "wiseword":			$includepage = "WiseWordManager.php";
									$pagecode = "wiseword";
									break;
		
		case "aewiseword":			$includepage = "addEditWiseWord.php";
									$pagecode = "wiseword";
									break;
		
		case "advertisment":		$includepage = "advertismentManager.php";
									$pagecode = "advertisment";
									break;
		
		case "aeadvertisment":		$includepage = "addEditAdvertisment.php";
									$pagecode = "advertisment";
									break;
									
		case "credit":		$includepage = "creditManager.php";
									$pagecode = "credit";
									break;
		
		case "aecredit":		$includepage = "addEditCredit.php";
									$pagecode = "credit";
									break;
									
		case "downline":		$includepage = "downlineManager.php";
									$pagecode = "downline";
									break;
		
		case "aedownline":		$includepage = "addEditDownline.php";
									$pagecode = "downline";
		
									break;
		
		//for user credits							
		case "usercredit":		$includepage = "usercreditManager.php";
									$pagecode = "usercredit";
									break;
									
		case "listcredits":		$includepage = "usercreditListManager.php";
									$pagecode = "listcredits";
									break;
									
		case "aelistcredits":		$includepage = "addEditUserCredit.php";
									$pagecode = "listcredits";
									break;
		//user credits ends
		
		//user ratings
		
			case "userrating":		$includepage = "userratingManager.php";
									$pagecode = "userrating";
									break;
									
		case "listratings":		$includepage = "userratingListManager.php";
									$pagecode = "listratings";
									break;
									
		case "aelistratings":		$includepage = "addEditUserRating.php";
									$pagecode = "listratings";
									break;
		//user rating ends
									
		case "support":				$includepage = "supportManager.php";
									$pagecode = "support";
									break;
		
		case "aesupport":			$includepage = "addEditSupport.php";
									$pagecode = "support";
									break;
		
		case "banner":			$includepage = "bannerManager.php";
									$pagecode = "banner";
									break;
		
		case "aebanner":			$includepage = "addEditBanner.php";
									$pagecode = "banner";
									break;
		
		
									
		case "users":				$includepage = "usersManager.php";
									$pagecode = "users";
									break;
		
		case "aeusers":				$includepage = "addEditUsers.php";
									$pagecode = "aeusers";
									break;
									
		case "adminuser": 			$includepage = "adminUsers.php";
									$pagecode = "adminuser";
									break;
		
		case "aeadminuser":			$includepage = "addEditAdminUsers.php";
									$pagecode = "aeadminuser";
									break;
									
		case "siteuser": 			$includepage = "siteUsers.php";
									$pagecode = "siteuser";
									break;
		
		case "aesiteuser":			$includepage = "addEditSiteUsers.php";
									$pagecode = "aesiteuser";
									break;
									
		case "siteconfigure": 			$includepage = "siteConfigure.php";
									$pagecode = "siteconfigure";
									break;
		
		case "aesiteconfigure":			$includepage = "addEditSiteConfigure.php";
									$pagecode = "aesiteconfigure";
									break;
													
																					
		case "changepassword":  $includepage = "changePassword.php";
								$pagecode = "changepassword";
								break;
								
								
		case "menu": 		   		$includepage = "menuManager.php";
									$pagecode = "menu";
									break;
		
		case "aemenu":				$includepage = "addEditMenu.php";
									$pagecode = "aemenu";
									break;
		
		case "submenu": 		    $includepage = "subMenuManager.php";
									$pagecode = "submenu";
									break;
		
		case "aesubmenu":			$includepage = "addEditSubMenu.php";
									$pagecode = "aesubmenu";
									break;						
		//gallery section
		case "gi": 			$includepage = "gi.php";
								$pagecode = "gi";
								break;	
		case "aeimage":		$includepage = "aeimage.php";
								$pagecode = "aeimage";
								break;			
								break;
		//gallery section
		case "gallery": 	$includepage = "galleryManager.php";
								$pagecode = "gallery";
								break;
		
		case "aegallery":	$includepage = "addEditGallery.php";
								$pagecode = "aegallery";
								break;
								
		case "album" : 	$includepage = "albumManager.php";
								$pagecode = "image";
								break;
								
		case "aealbum":		$includepage = "addEditAlbum.php";
								$pagecode = "aeimage";
								break;			
		// gallery ends		
								
		case "image" : 	$includepage = "image.php";
								$pagecode = "image";
								break;
								
		case "img" : $includepage = "downfiles.php";
									$pagecode = "img";
									break;
		// gallery ends													
								
																				
																
		default	: 				$includepage = "welcome.php";	
								$pagecode = "welcome";
		}
?>