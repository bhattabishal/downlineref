<?php
$n_id = $_REQUEST['nId'];
$function = $_GET['function'];

$nvo = new advertismentVO();
/*if(isset($_GET['nId']) && intval($_GET['nId'])!=0)
{
	$newsDAO = new NewsDAO();
	$id = intval($_GET['nId']);
	$tvo = $newsDAO->fetchDetails($id);
}
*/
$ndao = new advertismentDAO();

if($function == 'add')
	{
	echo"<h2>Add Advertisment</h2>";
	}
else 
	{
	echo "<h2>Edit Advertisment</h2>";
	if ($_GET['nId'])
		{
		$nvo = $ndao->fetchDetails($n_id);
		}
	}
// the different message for updating and adding the news
$updated_msg="<script language='javascript'>alert('Advertisment has been Updated successfully.'); location='index.php?p=advertisment';</script>\n";
$inserted_msg="<script language='javascript'>alert('Advertisment has been Added successfully.'); location='index.php?p=advertisment';\n</script>";
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$nvo->id = $_POST['id'];
	$nvo->title = $_POST['title'];
	$nvo->short_desc = $_POST['short_desc'];
	$nvo->credits = $_POST['credits'];
	$nvo->website_url = $_POST['website_url'];
	$nvo->package_size=$_POST['package_size'];
	$nvo->sub_total=$_POST['sub_total'];
	$nvo->span_fees=$_POST['span_fees'];
	$nvo->total=$_POST['total'];
	$nvo->status = $_POST['status'];
	$nvo->updated_date = date('Y-m-d');
	$nvo->created_date = $_POST['created_date'];
	$nvo->created_by= $_POST['created_by'];
	$nvo->updated_by= $_SESSION['useradmin'];
	$nvo->update_count=$_POST['update_count']+1;
	
	$nvo->formatInsertVariables();
	
	//checking server side validation for different fields
			
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_POST['id']!="") 
			{
			if($ndao->update($nvo))
				echo $updated_msg;
			}
		else
			{
			$nvo->created_date = date('Y-m-d');
			$nvo->created_by= $_SESSION['useradmin'];
			if($ndao->insert($nvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditNews" id="addEditNews" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>Advertisment Form:</strong></td>
                            </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                    <tbody>
                                              <tr>
                                                <td width="144" align="left" class="text">&nbsp;</td>
                                                <td width="617"  align="left" class="main">&nbsp;</td>
                                      </tr>
                                              <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Advertisment Title :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="title" type="text" class="field" id="title" value="<?php echo $nvo->title; ?>" size="70" valiclass="required" req="2" valimessage="Title :This field is required!
" />
											
                                                  &nbsp;</td>
                                              </tr>
                                               <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Advertisment Description :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="short_desc" type="text" class="field" id="title" value="<?php echo $nvo->short_desc; ?>" size="70" valiclass="required" req="2" valimessage="Advertisment Description Is Required!
" />
											
                                                  &nbsp;</td>
                                              </tr>
                                               <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Advertisment Credit :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="credits" type="text" class="field" id="title" value="<?php echo $nvo->credits; ?>" size="70" valiclass="required" req="1" valimessage="Advertisment Credit Is Required!
" />
											
                                                  &nbsp;</td>
                                              </tr>
                                           <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Website URL :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="website_url" type="text" class="field" id="website_url" value="<?php echo $nvo->website_url ?>" size="70" valiclass="required" req="2" valimessage="Website URL:This field is required!
" />
											
                                                  &nbsp;e.g.: http://www.example.com</td>
                                              </tr>
                                               <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Package Size :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="package_size" type="text" class="field" id="package_size" value="<?php echo $nvo->package_size; ?>" size="70" valiclass="required" req="2" valimessage="Package Size Is Required!
" />
											
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1"></span></span>Sub Total :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="sub_total" type="text" class="field" id="sub_total" value="<?php echo $nvo->sub_total; ?>" size="70"  />

											
                                                </td>
                                              </tr>
                                               <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1"></span></span>Up Line Fees :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="span_fees" type="text" class="field" id="span_fees" value="<?php echo $nvo->span_fees; ?>" size="70" />

											
                                                </td>
                                              </tr>
                                               <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1"></span></span>Total :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="total" type="text" class="field" id="total" value="<?php echo $nvo->total; ?>" size="70" />

											
                                                </td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">Status :</td>
                                                <td class="main"  align="left">
												<select name="status" id="publish">
												<option value="1" <?php if($nvo->publish == "yes") echo "selected"; ?>  >Yes</option>
												<option value="0" <?php if($nvo->publish == "no") echo "selected"; ?>  >No</option>
											 	 </select></td>
                                              </tr>
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="id" id="id" value="<?php echo $nvo->id;?>">
    <input type="hidden" name="created_by" id="entered_by" value="<?php echo $nvo->created_by;?>">
    <input type="hidden" name="created_date" id="entered_date" value="<?php echo $nvo->created_date;?>">
    <input type="hidden" name="update_count" id="update_count" value="<?php echo $nvo->update_count;?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						 
                        </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>