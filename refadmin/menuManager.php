<?php 
include_once "../".DIR_INCLUDES."/functions.php";
$delId=$_GET['nId'];
$nvo = new menuVO();
$ndao = new menuDAO();

$count = 1;
	if(isset($_GET['nId']) && intval($_GET['nId'])!=0)
	{
		$rMenu = new menuDAO();
		if(!$rMenu->hasSubmenus($_GET['nId']))
		{
			$flag = $rMenu->remove($_GET['nId']);
			if($flag)
				$msg = "Selected menu has been removed successfully.";
			else
				$msg = "Some error prevented menu from being removed";
		}
		else
		{
			$msg = "There are Sub Menu under this Menu and can not be deleted.";
		}
	}
	
	/*elseif(isset($_GET['id']) && intval($_GET['id'])!=0 && isset($_GET['s']) && $_GET['s']!="")
		{
		$blogDAO = new blogDAO();
		$id = intval($_GET['id']);
		
	}*/	
	
	
	if(isset($_GET['sId']) && intval($_GET['sId'])!=0)
		{
		$sMenu = new menuDAO();
		$flag = $sMenu->publishNunpublish($_GET['sId'],$_GET['status']);
		if($flag)
			$msg = "Status has been changed successfully.";
		else
			$msg = "Some error prevented menu from being updated";
		}
	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>

<div><strong>Manage Menu</strong></div>

<div>
<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
		  <td width="5%" class="theader3"><strong>S.No.</strong></td>
		  <td width="17%" class="theader3"><strong>Name Eng.</strong></td>
         <!-- <td width="18%" class="theader3"><strong>Name Nep.</strong></td>-->
	 	  <td width="12%" class="theader3"><strong>Index</strong></td>
          <td width="12%" class="theader3"><strong>Menu type</strong></td>
	 	  <td width="11%" class="theader3"><strong>Has Content</strong></td>
		  <td width="9%" class="theader3"><strong>Sub Menu</strong></td>
		  <td width="6%" class="theader3"><strong>Status</strong></td>
 	  	  <td width="9%" class="theader3"><strong>Operations</strong></td>
	</tr>
		<?php
		$dao = new menuDAO();
		$list = $dao->fetchAll(); 
				/////******for paging******/////////
		require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
		if($dopagination)
			$list = $dao->fetchLimited($page, $perpage, "all");
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $menu)
				{
					$subdao=new sub_menuDAO();
					$sublist = $subdao->fetchAll($menu->menu_id,"all");
										
				?>
                
				<tr <?php if($_GET['id']==$menu->menu_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $menu->name_en ;?></td>
                   <!-- <td class="tcell2"><?php //echo $menu->name_np ;?></td>-->
                    <td class="tcell2"><?php echo $menu->menu_index ;?></td>
                    <td class="tcell2"><?php echo $menu->menu_type ;?></td>
                    <td class="tcell2"><?php echo $menu->has_content ;?></td>
                    <td class="tcell2"><a href="index.php?p=submenu&mid=<?php echo $menu->menu_id;?>"><?php echo "(".sizeof($sublist).")";?></a></td>
                    
                    <?php if($menu->publish=="yes"){$sta="Yes";$stat="no";}else{$sta="No";$stat="yes";}?>
					<td class="tcell2"><a href="index.php?p=menu&sId=<?php echo $menu->menu_id;?>&status=<?php echo $stat;?>&pg=<?php echo $_GET['pg'];?>"><?php echo $sta; ?></a></td>
					<td class="tcell2">
					<a href="index.php?p=aemenu&nId=<?php echo $menu->menu_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=menu&nId=<?php echo $menu->menu_id;?>" onclick="return confirm('Make sure before you delete this menu?');"><img src="./images/delete.gif" border="0" /></a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td class="tcell_left">&nbsp;</td>
				<td colspan="9" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
				
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="9">						<font color="#cc0000">
					<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">
					<td width="1%" class="tcell2">&nbsp;</td>
						</font>			</tr>
			<?php
			}
		?>
	</table>
    
<div align="right"><a href="index.php?p=aemenu&function=add" class="theader3"><strong>ADD Menu </strong></a></div>

<form name="menu" id="menu" method="post" action="">

<table width="95%">

</table>
</form>
</div>