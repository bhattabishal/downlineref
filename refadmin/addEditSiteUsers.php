<?php
	$adminvo = new siteusersVO();
	$admindao = new siteusersDAO();
	if(isset($_GET['eAid']) && intval($_GET['eAid'])!=0)
		{
		$admindao = new siteusersDAO();
		$id = intval($_GET['eAid']);
		$adminvo = $admindao->fetchDetails($id);
		
		}
$cDAO = new countryDAO();
$country=$cDAO->fetchAll();
$allusers=$admindao->fetchAll();
?>

<div class="ptitle">Add/Edit Website Users: </div>
<form name="addEditAdminusers" id="addEditAdminusers" action="./usercodes.php" method="post">
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #ccc;">
	<tr>
		<td>
		
			<table width="100%" border="0" cellspacing="2">
			  
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
               <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%" class="medium">Referred By  : </td>
				<td width="64%">
                	<select name="user_parent_id">
                    	<option value="0" selected="selected">No One</option>
                        <?php
							foreach($allusers as $value)
							{
								?>
                                <option value="<?php echo $value->user_id; ?>"><?php echo $value->username; ?></option>
                                <?php
							}
						?>
                    </select>
				</td>
			  </tr>
               <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%" class="medium">Membership Type  : </td>
				<td width="64%">
                	<select name="mem_type">
                    	<option value="0" <?php if($adminvo->mem_type==0){ echo 'selected="selected"';} ?>>Standard Member</option>
                        <option value="1" <?php if($adminvo->mem_type==1){echo 'selected="selected"';} ?>>Premium Member</option>
                    </select>
				</td>
			  </tr>
               <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%" class="medium">Random Distributed  : </td>
				<td width="64%">
                	<select name="random_distributed">
                    	<option value="No" <?php if($adminvo->random_distributed=='No'){ echo 'selected="selected"';} ?>>No</option>
                        <option value="Yes" <?php if($adminvo->random_distributed=='Yes'){echo 'selected="selected"';} ?>>Yes</option>
                    </select>
				</td>
			  </tr>
                <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%" class="medium">First Name  : </td>
				<td width="64%">
                	<input type="text" name="first_name" id="first_name" value="<?php echo $adminvo->first_name; ?>" valiclass="required" req="2" valimessage="First Name Is Required" />
				</td>
			  </tr>
               <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%" class="medium">Last Name  : </td>
				<td width="64%">
                	<input type="text" name="last_name" id="last_name" value="<?php echo $adminvo->last_name; ?>" valiclass="required" valimessage="Last Name Is Required" req="2" />
				</td>
			  </tr>
			  <tr>
				<td width="11%">&nbsp;</td>
				<td width="25%" class="medium">User Name  : </td>
				<td width="64%">
				  <input type="text" name="username" id="username" value="<?php echo $adminvo->username;?>" valiclass="required" req="5"  valimessage="Enter login name of at least 5 characters." class="field" size="40">						</td>
			  </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td class="medium">Password: </td>
			    <td><input type="password" name="password" id="password" <?php if($adminvo->user_id==0) echo ' valiclass="required" req="5"  valimessage="Enter password of at least 5 characters."';?> class="field" size="40"></td>
		      </tr>
               <tr>
			    <td>&nbsp;</td>
			    <td class="medium">Referral URL: </td>
			    <td><input type="text" name="referral_url" id="referral_url" value="<?php echo $adminvo->referral_url; ?>" class="field" size="40" valiclass="required" req="2" valimessage="Rererral URL is Required">e.g. http://www.downlinerefs.com/?username</td>
		      </tr>
               <tr>
			     <td>&nbsp;</td>
			     <td class="medium">Country: </td>
			     <td>
                 	<select name="country" valiclass="select" valimessage="Please select Country">
                    	<option value="select">select</option>
                        <?php
							foreach($country as $value)
							{
						?>
                        	<option value="<?php echo $value->id; ?>" <?php if($value->id==$adminvo->country){echo 'selected="selected"';} ?>><?php echo $value->name; ?></option>
                        <?php
							}
						?>
                    </select>
                 </td>
		      </tr>
			   <tr>
			     <td>&nbsp;</td>
			     <td class="medium">Email Address: </td>
			     <td><input type="text" name="email" id="email" value="<?php echo $adminvo->email;?>" valiclass="email"  valimessage="Enter valid email address." class="field" size="40"></td>
		      </tr>
               <tr>
			     <td>&nbsp;</td>
			     <td class="medium">Gender: </td>
			     <td>
                 	<input type="radio" name="gender" value="male"  <?php if($adminvo->gender=='male'){echo 'checked="checked"';} ?>/>Male
                    <input type="radio" name="gender" value="female" style="margin-left:5px" <?php if($adminvo->gender=='female'){echo 'checked="checked"';} ?> />Female
                 </td>
		      </tr>
			   <tr>
			     <td>&nbsp;</td>
			     <td class="medium">Status:</td>
			     <td>
				 <select name="status" class="field" id="status">
				   <option value="1" <?php if($adminvo->status==1){echo 'selected="selected"';} ?>>Active</option>
				   <option value="0" <?php if($adminvo->status==0){echo 'selected="selected"';} ?>>Passive</option>
                 </select>
				 </td>
		      </tr>
			   <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			</table>

		</td>
	</tr>
</table><br/>
<div align="left">
	<a href="index.php?p=siteuser" class="theader3">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="hidden" name="save" id="save">
	<input type="hidden" name="oldPassword" id="oldPassword" value="<?php echo $adminvo->password;?>">
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $adminvo->user_id;?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);">
</div>		
</form>