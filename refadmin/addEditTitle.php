<?php
$t_id = $_REQUEST['nId'];
$function = $_GET['function'];

$nvo = new titleVO();
if(isset($_GET['nId']) && intval($_GET['nId'])!=0)
		{
		$titleDAO = new titleDAO();
		$id = intval($_GET['nId']);
		$nvo = $titleDAO->fetchDetails($id);
		}

$ndao = new titleDAO();
$nvo = $ndao->fetchDetailsTop();
		
/*if($function == 'Add')
	{
	echo"<h2>Add Title</h2>";
	}
else 
	{
	echo "<h2>Title Details</h2>";
	if ($_REQUEST['nId'])
		{
		$nvo = $ndao->fetchDetailsCond($id);
		}
	}*/

	
	
// the different message for updating and adding the title
$updated_msg="<script language='javascript'>\nalert('title has been Updated successfully.');\n location='index.php?p=aetitle&bId=".$_GET['bId']."';</script>\n";
$inserted_msg="<script language='javascript'>\nalert('title has been Added successfully.');\n location='index.php?p=aetitle&bId=".$_GET['bId']."';\n</script>";
if($_POST)
	{
	
	$nvo->id = $_POST['id'];
	$nvo->title_en = $_POST['title_en'];
	$nvo->title_np = $_POST['title_np'];
	$nvo->publish = $_POST['publish'];
	$nvo->updated_date = date('Y-m-d');
	$nvo->updated_by= $_SESSION['useradmin'];
			
		
	$nvo->formatInsertVariables();
	
		
	//validate the different fields
	if(!$nvo->title_en)
		{
		$errmsg="Please Enter the title.<br />";
		}
		
	
		
	/*if($nvo->image) //if the image is uploaded from the file choose
		{
		//now code here goes to check the image type and make the thumbnail
	 	get_file_name_and_ext($nvo->image,$image,$file_ext); 
		if($file_ext!='jpg' && $file_ext!='png' && $file_ext!='gif' && $file_ext!='jpeg'  && $file_ext!='jpg')
			{
		 	$errmsg ='Invalid file format';
		 	}			
		else
			{
			
			if($_POST['pict'])
				$nvo->removePicture($_POST['pict']);
		
			srand(make_seed());
			$randval = rand();
			//concat the random value to the file name to avoid duplicate filen name
			$nvo->image = "vid_".$randval.".".$file_ext;
			//get the mime type of the picture
			$mimetype = $_FILES['image']['type'];
			//get the thumbnail height and width according to the size of the image using user defined function
			getThumbnailHeightAndWidth($mimetype,$_FILES['image']['tmp_name'],120,120,$thumb_w,$thumb_h);
			
			//make the thumbnail for the home page
			$nvo->thumbnailAndUpload($mimetype,$_FILES['image']['tmp_name'],$nvo->image,$thumb_w,$thumb_h);
			
			
			}
			
		}
	else
 		$nvo->image = $_POST['pict'];
		
	*/
	
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($nvo->id)//($_GET['nId']) 
			{
			if($ndao->update($nvo))
				//echo"";
				echo $updated_msg;
			}
		else
			{
			$nvo->entered_date = date('Y-m-d');
			$nvo->entered_by= $_SESSION['useradmin'];
			if($ndao->insert($nvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>
<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditusers" id="addEditusers" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="105%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>Title    Form:</strong></td>
                              </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="0" cellspacing="0" width="784" >
                                        <tbody>
                                              <tr>
                                                <td width="153" align="left" class="text">&nbsp;</td>
                                                <td width="631"  align="left" class="main">&nbsp;</td>
                                          </tr>
                                              <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main"  align="left">&nbsp;</td>
                                              </tr>
                                              
                                              <tr> 
                                                <td height="87" align="left" class="text">English Title :</td>
                                                <td class="main"  align="left"><textarea name="title_en" cols="50" rows="4" id="title_en"  ><?php echo $nvo->title_en; ?></textarea></td>
                                          </tr>
                                              
                                              <tr>
                                                <td height="82" align="left" class="text"> Nepali Title :</td>
                                                <td class="main"  align="left"><textarea name="title_np" cols="50" rows="4" id="title_np"  ><?php echo $nvo->title_np; ?></textarea></td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">Publish :</td>
                                                <td class="main"  align="left"><select name="publish" id="publish">
                                                  <option value="yes" <?php echo ($nvo->publish=="yes"?"selected":""); ?>>Yes</option>
                                                  <option value="no" <?php echo ($nvo->publish=="no"?"selected":""); ?>>No</option>
                                                </select></td>
                                              </tr>
                                              
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="id" id="id" value="<?php echo $nvo->id ?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						  <input type="hidden" name="register_posted" value="yes" id="register_posted" >
						  <?php
	 					if($function == 'add') {?><input type='hidden' name='function' value='add'><?php	}else{?>
						<input type='hidden' name='function' value='edit'><?php }?>
					    <input type="hidden" name="<? //=$_GET[action]?>" value="yes"/>
					   </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>