<?php 
include_once "../".DIR_INCLUDES."/functions.php";
$mid=$_REQUEST['mid'];
$mdao = new menuDAO();
$mvo=$mdao->fetchDetails($mid);

$delId=$_GET['nId'];
$nvo = new sub_menuVO();
$ndao = new sub_menuDAO();

$count = 1;
	if(isset($_GET['nId']) && intval($_GET['nId'])!=0)
	{
		$rSMenu = new sub_menuDAO();
		$flag = $rSMenu->remove($_GET['nId']);
		if($flag)
			$msg = "Selected sub menu has been removed successfully.";
		else
			$msg = "Some error prevented sub menu from being removed";
		
	}
		
	
	if(isset($_GET['sId']) && intval($_GET['sId'])!=0)
		{
		$sMenu = new sub_menuDAO();
		$flag = $sMenu->publishNunpublish($_GET['sId'],$_GET['status']);
		if($flag)
			$msg = "Status has been changed successfully.";
		else
			$msg = "Some error prevented News from being updated";
		}
	
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>


<div><strong>Manage Sub Menu</strong></div>

<div>

<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
        
        <tr>
       		<td colspan="6">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="6">Menu: <?php echo $mvo->name_en.":".$mvo->name_np;?></td>
        </tr>
		<tr style="background-color:#ccc;">
		  <td width="5%" class="theader3"><strong>S.No.</strong></td>
		  <td width="17%" class="theader3"><strong>Name Eng.</strong></td>
          <td width="18%" class="theader3"><strong>Name Nep.</strong></td>
	 	  <td width="12%" class="theader3"><strong>Index</strong></td>
          <td width="6%" class="theader3"><strong>Status</strong></td>
 	  	  <td width="9%" class="theader3"><strong>Operations</strong></td>
	</tr>
		<?php
		$dao = new sub_menuDAO();
		$list = $dao->fetchAll($mid,"all"); 
				/////******for paging******/////////
		require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
		if($dopagination)
			$list = $dao->fetchLimited($page, $perpage, "all");
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $smenu)
				{
												
				?>
                
				<tr <?php if($_GET['id']==$smenu->sub_menu_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $smenu->name_en ;?></td>
                    <td class="tcell2"><?php echo $smenu->name_np ;?></td>
                    <td class="tcell2"><?php echo $smenu->sub_menu_index ;?></td>
                                        
                    <?php if($smenu->publish=="yes"){$sta="Yes";$stat="no";}else{$sta="No";$stat="yes";}?>
					<td class="tcell2"><a href="index.php?p=submenu&mid=<?php echo $mid;?>&sId=<?php echo $smenu->sub_menu_id;?>&status=<?php echo $stat;?>&pg=<?php echo $_GET['pg'];?>"><?php echo $sta; ?></a></td>
					<td class="tcell2">
					<a href="index.php?p=aesubmenu&mid=<?php echo $mid;?>&nId=<?php echo $smenu->sub_menu_id;?>"><img src="./images/edit.gif" border="0"></a> | 
				  	<a href="index.php?p=submenu&mid=<?php echo $mid;?>&nId=<?php echo $smenu->sub_menu_id;?>" onclick="return confirm('Make sure before you delete this menu?');"><img src="./images/delete.gif" border="0" /></a></td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td class="tcell_left">&nbsp;</td>
				<td colspan="5" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
				
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="5">						<font color="#cc0000">
					<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">
					<td width="1%" class="tcell2">&nbsp;</td>
						</font>			</tr>
			<?php
			}
		?>
	</table>
    
<div align="right"><a href="index.php?p=aesubmenu&function=add&mid=<?php echo $mid;?>" class="theader3"><strong>ADD Sub Menu </strong></a></div>


</div>