<?php
require_once "../global.php";
include_once "../".DIR_INCLUDES."/db_connection.php";

//print_r($_POST);


if(isset($_POST['saveContent']) && $_POST['saveContent']=='true')
	{
	$vo = new contentVO();
	$dao = new contentDAO();
	
	$vo->content_id = $_POST['content_id'];
	$vo->heading = $_POST['heading'];
	$vo->heading_np = "";
	$vo->page_title = $_POST['page_title'];
	$vo->page_metatag = $_POST['page_metatag'];
	$vo->page_keywords = $_POST['page_keywords'];
	$vo->page_description = $_POST['page_description'];
	$vo->detail_desc_en= $_POST['detail_desc_en'];
	$vo->brief_desc_en= $_POST['brief_desc_en'];
	$vo->detail_desc_np= "";
	$vo->brief_desc_np= "";
	
	$vo->menu_id = $_POST['menu_id'];
	$vo->sub_menu_id= $_POST['sub_menu_id'];
	$vo->publish= $_POST['publish'];
	$vo->updated_by= $_SESSION['useradmin'];
	$vo->updated_date=  date("Y-m-d");
	$vo->update_count= $_POST['update_count']+1;

	$vo->formatInsertVariables();
	if($vo->content_id == 0)
		{
		//die("insert");

		$vo->entered_by= $_SESSION['useradmin'];
		$vo->entered_date= date("Y-m-d");
		if($dao->insert($vo))
			$msg = "New content has been successfully inserted.";
		else
			$msg = "Some error prevented new content from being saved.";
		}
	else
		{
		
		//die("update");

		if($dao->update($vo))
			$msg = "Selected content has been successfully updated.";
		else
			$msg = "Some error prevented content from being updated.";
		
		}
	//header("Location: index.php?p=content&msg=".$msg);
	echo '<script language="javascript">location="index.php?p=content&msg='.$msg.'";</script>';	
	}
?>
