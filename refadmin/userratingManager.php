<?php

include_once "../".DIR_INCLUDES."/functions.php";
include_once "../".DIR_COMMON."/functions.php";

			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="28%" class="ptitle"><strong>Manage User Rating Details:</strong></td>
    <td width="72%" class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="4%" class="theader3"><strong>S.No.</strong></td>
			<td width="20%" class="theader3"><strong>User Name</strong></td>
            <td width="20%" class="theader3"><strong>Referral Rating</strong></td>
            <td class="theader3" width="20%"><strong>Promotor Rating</strong></td>
            
             
		 	<td width="10%" class="theader3"><strong>User Status</strong></td>
	 	  <td width="10%" class="theader3"><strong>Operations</strong></td>
		</tr>
		<?php
		$LinksDAO = new siteusersDAO();
		$list = $LinksDAO->fetchAll($publish='all'); 
				/////******for paging******/////////
		require_once "./inc/paginationConfig.php";//initializes totalpages, current page, serial number etc.
		if($dopagination)
			$list = $LinksDAO->fetchLimited($page, $perpage, "all");
		/////****end of paging*******//////////
		

		$sn =0;
		if(!empty($list))
			{
				foreach($list as $links)
				{
				
					//get credits info
					$referralrating=getReferralRating($links->user_id);
					$promotorrating=getPromotorRating($links->user_id);
					
					
					
				?>
		<tr <?php if($_GET['sId']==$links->user_id) echo 'bgcolor="#ffcccc"'; elseif($sn%2==0) echo 'bgcolor="#efefef"';?>>
					<td class="tcell_left"><?php echo ++$sn;?></td>
					<td class="tcell2"><?php echo $links->username ;?></td>
                    <td class="tcell2"><?php echo $referralrating;?></td>
                      <td class="tcell2"><?php echo $promotorrating ;?></td>
                    
                    
					<td class="tcell2"><?php if($links->status==1){ echo "Active";}else { echo "Inactive";} ?></td>
					<td class="tcell2" align="center">
					<a href="index.php?p=listratings&amp;id=<?php echo $links->user_id;?>" title="List Credits"><img src="./images/ts_list_off.png" border="0" alt="List Credits"></a>
				  </td>
		</tr>
		<?php
				}
			}
		else
			{
			?>
			<tr bgcolor="#efefef">
				<td class="tcell_left">&nbsp;</td>
				<td colspan="6" align="center" class="tcell2"><font color="#cc0000">No records were found.</font></td>
			</tr>
			<?php
			}
			?>
			 <?php
		if($dopagination)
			{
			?>
			<tr>
					<td align="center" colspan="6">						<font color="#cc0000">
					<?php
						$url = $_SERVER['REQUEST_URI'];
		  				echo paginate($url, $perpage, $total, $page);//these variables are initialized in paginationConfig.php
						?>
					</font><font color="#cc0000">&nbsp;
					</font>			</tr>
			<?php
			}
		?>
	</table>
	
	
	</td>
  </tr>
 
</table><br/>
<!--<div align="right"><a href="index.php?p=aedownline&amp;function=add" class="theader3"><strong>ADD Program Downline</strong></a></div>-->
