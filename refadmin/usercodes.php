<?php
require_once "../global.php";
include_once "../".DIR_INCLUDES."/db_connection.php";
include_once "../common/common_functions.php";

if(isset($_POST['save']) && $_POST['save']=='true')
	{
	$vo = new siteusersVO();
	$dao = new siteusersDAO();
	
	$vo->user_id = $_POST['user_id'];
	$vo->username = $_POST['username'];
	$vo->user_parent_id=$_POST['user_parent_id'];
	$vo->mem_type=$_POST['mem_type'];
	$vo->random_distributed=$_POST['random_distributed'];
	$vo->first_name=$_POST['first_name'];
	$vo->last_name=$_POST['last_name'];
	$vo->computer_ip=getRealIpAddr();
	$vo->country=$_POST['country'];
	$vo->referral_url=$_POST['referral_url'];
	
	if($_POST['password']!="")
		$vo->password = md5($_POST['password']);
	else
		$vo->password = $_POST['oldPassword'];
	
	$vo->email = $_POST['email'];
	$vo->status = $_POST['status'];
	$vo->joined_date = date("Y-m-d H:i:s");
	$vo->gender=$_POST['gender'];
	
	$vo->formatInsertVariables();
	
	if($vo->user_id == 0)
		{
		if($dao->insert($vo))
			{
			$id = $dao->insert_id();
			$msg = "New user has been created.";
			}
		else
			$msg = "Error occured while creating  admin user.";
		}
	else
		{
		if($dao->update($vo))
			$msg = "Selected  user has been updated.";
		else
			$msg = "Error occured while updating selected  user.";
		
		$id = $vo->user_id;
		}
	
	//header("Location:index.php?p=adminuser&id=".$id."&msg=".$msg);
	?>
	<script language='javascript'>alert('User has been Added successfully.'); location='index.php?p=siteuser&id=<?php echo $id ?>&msg=<?php echo $msg ?>'; </script>
    <?php
	}
?>