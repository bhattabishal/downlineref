<?php
$n_id = $_REQUEST['nId'];
$function = $_GET['function'];

$nvo = new menuVO();
/*if(isset($_GET['nId']) && intval($_GET['nId'])!=0)
{
	$newsDAO = new NewsDAO();
	$id = intval($_GET['nId']);
	$tvo = $newsDAO->fetchDetails($id);
}
*/
$ndao = new menuDAO();

if($function == 'add')
	{
	echo"<h2>Add Menu</h2>";
	}
else 
	{
	echo "<h2>Edit Menu</h2>";
	if ($_GET['nId'])
		{
		$nvo = $ndao->fetchDetails($n_id);
		}
	}
// the different message for updating and adding the news
$updated_msg="<script language='javascript'>alert('Menu has been Updated successfully.'); location='index.php?p=menu';</script>\n";
$inserted_msg="<script language='javascript'>alert('Menu has been Added successfully.'); location='index.php?p=menu';\n</script>";
if($_SERVER['REQUEST_METHOD']=="POST")
	{
	
	$nvo->menu_id = $_POST['menu_id'];
	$nvo->name_en = $_POST['name_en'];
	//$nvo->name_np = $_POST['name_np'];
	$nvo->menu_type = $_POST['menu_type'];
	$nvo->has_content = $_POST['has_content'];
	$nvo->menu_index = $_POST['menu_index'];
	$nvo->publish = $_POST['publish'];
	$nvo->updated_date = date('Y-m-d');
	$nvo->entered_date = $_POST['entered_date'];
	$nvo->entered_by= $_POST['entered_by'];
	$nvo->updated_by= $_SESSION['useradmin'];
	$nvo->update_count=$_POST['update_count']+1;
	
	$nvo->formatInsertVariables();
	
	//checking server side validation for different fields
			
	if(!$errmsg) //if the form is posted and there is no error at all
		{
		if($_POST['menu_id']!="") 
			{
			if($ndao->update($nvo))
				echo $updated_msg;
			}
		else
			{
			$nvo->entered_date = date('Y-m-d');
			$nvo->entered_by= $_SESSION['useradmin'];
			if($ndao->insert($nvo))
				echo $inserted_msg;
			}
		}
	}
?>
<?php
//to display the error message
echo "<div align='center' class='style10'>$errmsg</div>";
?>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td valign="top" width="100%"> 
             
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                  <tbody>
                    <tr> 
                      <!-- body_text //-->
                      <td valign="top" width="100%">
					   <form name="addEditNews" id="addEditNews" enctype="multipart/form-data" action="" method="post">
                          <table width="80%" align="center">
                              <tr>
                                <td colspan="2" align="left" class="main"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                                          <tr>
                                            <TD width="43%" class="pageHeading" >&nbsp;</TD>
                                            <TD width="57%"  align="center" class="pageHeading"><span class="style1">*</span> <span class="style1" >Required information</span></TD>
                                          </table></td>
                              </tr><BR />
                              <tr>
                                <td colspan="2" align="left" class="main"></td>
                              </tr>
                              <tr>
                                <td colspan="2" align="left" class="main"><strong>Menu   Form:</strong></td>
                            </tr>
							   <tr>
							     <td colspan="2" class="main"><table class="infoBox" border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tbody>
                                      <tr class="infoBoxContents"> 
                                        <td style="border: 1px solid #CCCCCC;">
										<table border="0" cellpadding="3" cellspacing="3" width="100%" >
                                    <tbody>
                                              <tr>
                                                <td width="125" align="left" class="text">&nbsp;</td>
                                                <td width="494"  align="left" class="main">&nbsp;</td>
                                      </tr>
                                              <tr> 
                                                <td align="left" class="text"><span class="inputRequirement"><span class="style1">*</span></span>Name Eng :&nbsp;</td>
                                                <td class="main"  align="left">
												  <input name="name_en" type="text" class="field" id="name_en" value="<?php echo $nvo->name_en ?>" size="50" valiclass="required" req="2" valimessage="Heading English:This field is required!
" />
											
                                                  &nbsp;</td>
                                              </tr>
                                          <!-- <tr> 
                                                <td align="left" class="text">Name Nep :</td>
                                                <td class="main"  align="left">
												  <input name="name_np" type="text" class="field" id="name_np" value="<?php //echo $nvo->name_np ?>" size="50"/>
                                                </td>
                                              </tr>-->
                                              	
                                              <tr>
                                                <td align="left" class="text"> Menu Type</td>
                                                <td class="main"  align="left"><select name="menu_type" id="menu_type">
                                                  <option value="" selected="selected">Select</option>
                                                  <option value="Home" <?php if($nvo->menu_type == "Home") echo "selected"; ?>  >Home</option>
                                                  <option value="Program"  <?php if($nvo->menu_type == "Program") echo "selected"; ?>  >Program</option>
                                                   <option value="Adv"  <?php if($nvo->menu_type == "Adv") echo "selected"; ?>  >Advertisment</option>
                                                    <option value="Help"  <?php if($nvo->menu_type == "Help") echo "selected"; ?>  >Help</option>
                                                 
                                                 
                                                </select></td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">Menu Index</td>
                                                <td class="main"  align="left"><input name="menu_index" type="text" class="field" id="menu_index" value="<?php echo $nvo->menu_index ?>" size="5"  /></td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">Has Content :</td>
                                                <td class="main"  align="left"><select name="has_content" id="has_content">
                                                  <option value="yes" <?php if($nvo->has_content == "yes") echo "selected"; ?>  >Yes</option>
                                                  <option value="no" <?php if($nvo->has_content == "no") echo "selected"; ?>  >No</option>
                                                </select></td>
                                              </tr>
                                              <tr>
                                                <td align="left" class="text">Status :</td>
                                                <td class="main"  align="left"><select name="publish" id="publish">
                                                  <option value="yes" <?php if($nvo->publish == "yes") echo "selected"; ?>  >Yes</option>
                                                  <option value="no" <?php if($nvo->publish == "no") echo "selected"; ?>  >No</option>
                                                </select></td>
                                              </tr>
											   <tr>
                                                <td align="left" class="text">&nbsp;</td>
                                                <td class="main">&nbsp;</td>
                                              </tr>
                                            </tbody>
                                        </table></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
					        </tr>
                              <tr> 
                                <td colspan="2"  align="left"><input type="hidden" name="save" id="save" value="true">
	<input type="hidden" name="menu_id" id="menu_id" value="<?php echo $nvo->menu_id;?>">
    <input type="hidden" name="entered_by" id="entered_by" value="<?php echo $nvo->entered_by;?>">
    <input type="hidden" name="entered_date" id="entered_date" value="<?php echo $nvo->entered_date;?>">
    <input type="hidden" name="update_count" id="update_count" value="<?php echo $nvo->update_count;?>">
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Save" onClick="this.form.save.value='true'; call_validate(this.form,0,this.form.length);"></td>
                              </tr>
                          </table>
						 
                        </form></td>
                      <!-- body_text_eof //-->
                    </tr>
                  </tbody>
                </table>
                <!-- body_eof //-->
                <!-- footer //--></td>
            </tr>
          </tbody>
        </table>