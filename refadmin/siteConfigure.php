<?php
include_once "../".DIR_INCLUDES."/functions.php";
$cvo=new code_valueVO();
$cdao=new code_valueDAO();

	if(isset($_POST))
	{
	foreach($_POST as $key=>$value)
	{
		
		
		
			$cvo->code_value=$value;
			$cvo->code_label=$key;
			$cvo->code_type=$key;
			
	$cdao->update($cvo);
			$msg="Updated Successfully";
		
		
	}
		
	}
		
	if(isset($_GET['msg']) && $_GET['msg']!= "")
		$msg = $_GET['msg'];
			
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="24%" class="ptitle"><strong>Manage Website Configurations</strong></td>
    <td class="medium"><font color="#cc0000"><?php echo $msg;?></font></td>
  </tr>
  <tr>
    <td colspan="2">
	<form name="addEditAdminusers" id="addEditAdminusers" action="" method="post">
	
	<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border:1px solid #ccc;">
		<tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Pay Pal Account Email</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="paypal_email" style="width:300px" valiclass="email" valimessage="Your Paypal Email Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("paypal_email");echo $list->code_value; ?>" /></td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Alertpay Account Email</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="alertpay_email" style="width:300px"  valiclass="email" valimessage="Your Alertpay Email Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("alertpay_email");echo $list->code_value; ?>"/></td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Alertpay IPN Security Code:</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="alertpay_ipn_code" style="width:300px"  valiclass="required" req="2" valimessage="Your Alertpay IPN Security Code Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("alertpay_ipn_code");echo $list->code_value; ?>"/></td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Premium Membership cost for 6 months</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left">
            Price: <input type="text" name="premium_sixmonth" valiclass="required" req="1" valimessage="Membership cost for six months required" value="<?php $list=$cdao->fetchDetailsByLabel("premium_sixmonth");echo $list->code_value; ?>" /> $
            <label style="margin-left:20px">Discount Price: </label><input type="text" name="premium_sixmonth_discount" value="<?php $list=$cdao->fetchDetailsByLabel("premium_sixmonth_discount");echo $list->code_value; ?>" /> $
            </td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Premium Membership cost for 1 year</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left">
           Price: <input type="text" name="premium_oneyear" valiclass="required" req="1" valimessage="Membership cost for one year required" value="<?php $list=$cdao->fetchDetailsByLabel("premium_oneyear");echo $list->code_value; ?>"/> $
            <label style="margin-left:20px">Discount Price: </label><input type="text" name="premium_oneyear_discount"  value="<?php $list=$cdao->fetchDetailsByLabel("premium_oneyear_discount");echo $list->code_value; ?>"/>$
            </td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Premium Members Discount On Referral Request</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="premium_referral_request_discount" value="<?php $list=$cdao->fetchDetailsByLabel("premium_referral_request_discount");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required" /> %</td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>No. Of Promotable Program Using Referral Request  For Standard Users </strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="standard_user_promotable_programs" value="<?php $list=$cdao->fetchDetailsByLabel("standard_user_promotable_programs");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required" /> programs</td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Premium Members Bonus On Credit Purchase</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="premium_credit_purchase_bonus" value="<?php $list=$cdao->fetchDetailsByLabel("premium_credit_purchase_bonus");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required" /> %</td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Referrals Earning Level Deep</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="referral_earning_level"  value="<?php $list=$cdao->fetchDetailsByLabel("referral_earning_level");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required"/> Level Deep</td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Referrals Earning Level Percentage</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="referral_earning_percentage"  value="<?php $list=$cdao->fetchDetailsByLabel("referral_earning_percentage");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required"/> %</td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Minimum No. Of Credits Per Click(In Advertisment)</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="min_credits_per_adv_click"  value="<?php $list=$cdao->fetchDetailsByLabel("min_credits_per_adv_click");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required"/> credits</td>
		</tr>
         <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Minimum Advertisment Package Size(minimum Clicks)</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="min_adv_package_size"  value="<?php $list=$cdao->fetchDetailsByLabel("min_adv_package_size");echo $list->code_value; ?>" valiclass="required" req="1" valimessage="This Field Is Required"/> clicks</td>
		</tr>
        <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Minimum Credits Required For Referral Request</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="referral_request_minimum_credits" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("referral_request_minimum_credits");echo $list->code_value; ?>" /> $</td>
		</tr>
         <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Advertise First Spot Detail</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left">
             Duration <input type="text" name="advertise_first_spot_duration" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("advertise_first_spot_duration");echo $list->code_value; ?>" /> Days For Only
            <input type="text" name="advertise_first_spot_price" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("advertise_first_spot_price");echo $list->code_value; ?>" /> $
            </td>
		</tr>
         <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Advertise Second Spot</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left">
             Duration <input type="text" name="advertise_second_spot_duration" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("advertise_second_spot_duration");echo $list->code_value; ?>" /> Days For Only
            <input type="text" name="advertise_second_spot_price" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("advertise_second_spot_price");echo $list->code_value; ?>" /> $
            </td>
		</tr>
         <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Advertise Third Spot</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left">
             Duration <input type="text" name="advertise_third_spot_duration" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("advertise_third_spot_duration");echo $list->code_value; ?>" /> Days For Only
            <input type="text" name="advertise_third_spot_price" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("advertise_third_spot_price");echo $list->code_value; ?>" /> $
            </td>
		</tr>
        
         <tr style="background-color:#ccc;">
			<td width="7%" class="theader3"><strong>Site URL</strong></td>			
		</tr>		
		<tr>
			<td class="tcell_left"><input type="text" name="website_url" valiclass="required" req="1" valimessage="This Field Is Required" value="<?php $list=$cdao->fetchDetailsByLabel("website_url");echo $list->code_value; ?>" style="width:300px"/></td>
		</tr>
        
				
	</table>
	<div align="left">
	
	
	
	<input type="button" class="theader3" name="savebtn" id="savebtn" value="Update" onClick="call_validate(this.form,0,this.form.length);">
</div>	
	</form>
	</td>
  </tr>
 
</table><br/>
</div>