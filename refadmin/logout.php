<?php
session_start();
require_once "../global.php";
require_once "../".DIR_INCLUDES."/db_connection.php";
$user_id = intval($_SESSION['user_id']);
$logintime =  $_SESSION['logintime'];



unset($_SESSION['admin_id']);
unset($_SESSION['admin']);
unset($_SESSION['auth']);
unset($_SESSION['logintime']);
unset($_SESSION['lastlogin']);

header("Location: loginpage.php");
?>