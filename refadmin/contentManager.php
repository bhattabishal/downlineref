<?php
	if(isset($_REQUEST['mainmenu']) && intval($_REQUEST['mainmenu'])!=0)
		{
		$mainmenuDAO = new menuDAO();
		$id = intval($_REQUEST['mainmenu']);
		$hassubmenu = $mainmenuDAO->hasSubmenus($id);
		//$showform=true;
		if(!$hassubmenu || $hassubmenu)
			$showform = true;
		elseif(isset($_REQUEST['submenu']) && intval($_REQUEST['submenu'])!=0)
			{
				$showform = true;
				$hassubmenu = true;
			}
		}
	else
		{
			$showform = false;
			$hassubmenu = false;
			
		}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="24%" class="ptitle"><strong>
    <h1>Manage Content:</h1>
    </strong></td>
    <td width="76%" class="medium"><font color="#cc0000"><?php echo $_GET['msg'];?></font></td>
  </tr>
  
  <tr>
    <td colspan="2">
	<form name="ContentForm" id="ContentForm" action="" method="post">
		<table width="100%" cellpadding="0" cellspacing="0" border="0"   style="border:1px solid #ccc;">
			<tr>
			  <td class="medium" align="left">&nbsp;</td>
			  <td class="medium" align="left">&nbsp;</td>
		  </tr>
			<tr>
				<td width="22%" class="medium" align="left">Main Menu:&nbsp;
                  <select name="mainmenu" id="mainmenu" class="field" onChange="this.form.submit();">
						<option value="" selected="selected">Select</option>
						<?php
						$mainmenuDAO = new menuDAO();
						$subDAO = new sub_menuDAO();
						$list = $mainmenuDAO->fetchAll("all");
						
						if(!empty($list))
							foreach($list as $mainmenu)
								{
								$sublist = $subDAO->fetchAll($mainmenu->menu_id, "content");
								$subcount = count($sublist);
								echo $subcount;
									if($mainmenu->has_content=='yes')
										{
											if($mainmenu->menu_id == $_REQUEST['mainmenu'])
												echo "<option value='".$mainmenu->menu_id."' selected>".$mainmenu->name_en."-".$mainmenu->name_np."</option>";
											else
												echo "<option value='".$mainmenu->menu_id."'>".$mainmenu->name_en."-".$mainmenu->name_np."</option>";
										}
								}
						?>
					</select>			  </td>
				
<?php
				if($hassubmenu)//hasmenu flag gets true if main menu has submenus.
					{
					?>
					<td width="78%" class="medium" align="left">Submenu:&nbsp;
						<select name="submenu" id="submenu" class="field" onChange="this.form.submit();">
							<option value="">Select</option>
							<?php
							$submenuDAO = new sub_menuDAO();
							$list = $submenuDAO->fetchAll($_REQUEST['mainmenu'],"content");
							if(!empty($list))
								foreach($list as $submenu)
									{
									if($submenu->sub_menu_id == $_REQUEST['submenu'])
										echo "<option value='".$submenu->sub_menu_id."' selected>".$submenu->name_en."-".$submenu->name_np."</option>";
									else
										echo "<option value='".$submenu->sub_menu_id."'>".$submenu->name_en."-".$submenu->name_np."</option>";
									}
							?>
						</select>				  </td>
				  <?php
				  }
				  ?>
			</tr>
			<tr>
			  <td class="medium" align="left">&nbsp;</td>
			  <td class="medium" align="left">&nbsp;</td>
		  </tr>
		</table>
	</form>
	</td>
  </tr>
</table>
<br/>
<div align="right">
	<?php
	if($showform)
		{
		include_once "addEditContent.php";
		}
	?>
</div>