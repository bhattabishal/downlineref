<?php
session_start();
require_once "./global.php";
require_once "./".DIR_INCLUDES."/db_connection.php";
require_once "./".DIR_INCLUDES."/functions.php";
require_once "./".DIR_COMMON."/common_functions.php";
require_once "./".DIR_COMMON."/functions.php";
require_once "logincheck.php";

$user_id=$_SESSION['SES_ID'];




//code to insert records in to the database
$sVO=new siteusersVO();
$sDAO=new siteusersDAO();

$uDAO=new user_accountDAO();
$uVO=new user_accountVO();



$user_details=$sDAO->fetchDetails($user_id);
$action="form";







?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Account Balance</title>
<link rel="stylesheet" type="text/css" href="css/syle.css">
<link rel="stylesheet" type="text/css" href="css/buttons.css">
<link rel="stylesheet" type="text/css" href="css/accounttable.css"/>
<!--for header banner-->
<link rel="stylesheet" type="text/css" href="css/banner.css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/headerbanner.js"></script>
<!--header banner ends-->
<script type="text/javascript" src="js/jquery-latest.js"></script>
<script type="text/javascript" src="js/jquery.innerfade.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	$('#image_rotate').innerfade({ 
		speed: 'slow', 
		timeout: 4000, 
		type: 'sequence', 
		containerheight: '249px',
		containerwidth:'926px'
	});
});
</script>
<!--header banner ends-->
</head>
<body>
<!-- Frame (Start) -->
<div id="frame">
  <!-- Header (Start) -->
  	<?php
			include_once("includes/header.php");
	?>
  <!-- Header (End) -->
  <!-- Menu (Start) -->
  	<?php
			include_once("includes/topmenu.php");
	?>
  <!-- Menu (End) -->
  <!-- Gallery (Start) -->
  	<?php
			include_once("includes/topbox.php");
	?>
  <!-- Gallery (End) -->
  <!-- Container (Start) -->
  <div id="container">
    <!-- Lft (Start) -->
    <div class="lft">
     	<div class="block">
        <!-- Ttle (Start) -->
        <div class="ttle">
          	<h1> 
				Account Balance
        	</h1>
        </div>
        <div style="clear:both"></div>
        <!--inner content start-->
        <div class="innermaincontent" style="margin:15px">
        <div style="clear:both"></div>
        <!--table starts-->
         
            <div class="message-block">
            	
                
            </div>
        	<div class="container-table">
                    <div class="wrap-table"> 
                   
                    	
                    	<table cellspacing="0" cellpadding="0" border="0" width="610">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="r_name no_line">Quick Balance Overview(credits)</td>
                                </tr>
                                <tr>
                                    <td>Earned By Program SignUp</td>
                                    <td>
										<?php $amount=$uDAO->fetchProgramSignUpCreditsByUserId($user_id);if($amount==''){echo "+0.00";}else{echo "+".$amount; } ?>
                                        
                                    </td>
                                </tr>
                                 <tr>
                                   <td>Earned By Clicking Ads</td>
                                   <td>
                                   	<?php $amount=$uDAO->fetchAdvCreditsByUserId($user_id);if($amount==''){echo "+0.00";}else{echo "+".$amount; } ?>
                                   
                                    </td>
                                </tr>
                                 <tr>
                                   <td>Earned By Referring New Members</td>
                                   <td><?php  echo "+".GetTotalReferralEarningsNewMember($user_id); ?> </td>
                                </tr>
                                 <tr>
                                   <td>Purchased Credits</td>
                                   <td><?php $amount=$uDAO->fetchPurchagedCreditsByUserId($user_id);if($amount==''){echo "+0.00";}else{echo "+".$amount; } ?></td>
                                </tr>
                                 <tr>
                                   <td>Spend On Referral Requests</td>
                                   <td>
                                  <?php $amount=$uDAO->fetchReferralCreditsByUserId($user_id);if($amount==''){echo "-0.00";}else{echo "-".$amount; }?>
                                    
                                   </td>
                                </tr>
                                <tr>
                                   <td>Spend On Advertisment Packages</td>
                                   <td>
                                   		<?php $amount=$uDAO->fetchAdvPackageCreditsByUserId($user_id);if($amount==''){echo "-0.00";}else{echo "-".$amount; } ?>
                                        
                                  	</td>
                                </tr>
                                 
                                 <tr>
                                   <td>Spend On Jackpot Game Tickets</td>
                                   <td>
                                   		<?php $amount=$uDAO->fetchJackpotGamePackageCreditsByUserId($user_id);if($amount==''){echo "-0.00";}else{echo "-".$amount; } ?>
                                        
                                   </td>
                                </tr>
                               <tr>
                               		<td>&nbsp;</td>
                               		<td><div class="buttons">
                                    		<button type="button" class="regular" name="Edit Account" value="Edit Account" onclick="location.href='trxnhistory.php'"><img src="images/viewIcon.png" alt=""/>View Transaction History</button>
                                        </div></td>
                               </tr>
                                
                                  
                
            </div>
                               
                            </tbody>
                        </table>
                         
                        </form>
                        </div>
                </div>
           <!--table end-->
          <div style="clear:both"></div>
    	<!--inner content close-->
      	</div>
         
      </div>
    </div>
    <!-- Lft (End) -->
    <!-- Rgt (Start) -->
    		<?php
			include_once("includes/right.php");
			?>
    <!-- Rgt (End) -->
    
    <!-- Footer (Start) -->
   		<?php
			include_once("includes/footer.php");
			?>
    <!-- Footer (End) -->
    <div style="clear:both;"> </div>
  </div>
 
  <!--block end-->
  <!-- Container (End) -->
  <div style="clear:both;"> </div>
</div>
<!-- Frame (End) -->
</body>
</html>
