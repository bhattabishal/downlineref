<?php

class AjaxPoll {
	
	private $fileContents = '';
	
	/**
	 * Contructor function
	 *
	 * @param string $fileName
	 * 
	 * @return AjaxPoll
	 */
	public function AjaxPoll($fileName) {
		
		if (trim($fileName) == '' || !is_file($fileName) || !is_readable($fileName)) {
			trigger_error("Invalid filename or no access to read file: ".$fileName, E_USER_ERROR);
		}
		else {
			$this->fileContents = file_get_contents($fileName);
		}
	}
	
	/**
	 * This function replaces the tag by actual value
	 *
	 * @param string $tagname
	 * @param mixed $value
	 */
	public function tag($tagname, $value) {
		if (strtolower(trim($tagname)) == 'options') {
			if (!is_array($value)) {
				trigger_error("Please provide options in an array.", E_USER_ERROR);				
			}
			else {
				
				$str = '';
				for($i = 0; $i < 4; $i++) {
					$str .= '<INPUT type="radio" id="opt'.$i.'" name="poll" value="'.$value[$i].'" checked="checked" onclick="selectOption(this)" />'.$value[$i].'<SPAN class="answer"> &nbsp;</SPAN><BR />';
				}
				$this->fileContents = str_replace("{options}", $str, $this->fileContents);
			}			
		}
		else {
			$this->fileContents = str_replace("{".strtolower($tagname)."}", $value, $this->fileContents);
		}
	}
	
	
	/**
	 * This function returns the file contents 
	 *
	 * @return string
	 */
	public function write() {
		return $this->fileContents;
	}
}
?>