<?PHP
	define('TIME_LIMIT', 86400); //Time (in sec) after a user can vote again from a IP
	session_start();

	if (!isset($_SESSION['ip'])) {
		$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
		$_SESSION['start_time'] = time();
	}
	else {
		if ( ($_SESSION['start_time'] - time()) < TIME_LIMIT) {
			die("ip=".$_SESSION['ip']);
		}
	}
	/**
	 * Function to increase the team counter
	 *
	 * @param string $team
	 * 
	 * @return void
	 */
	function increaseCounter($team) {
		$score = file_get_contents('includes/pollresult/'.$team.'.txt') or die("Error: Cannot read $team file.");
		
		$score = $score +1;
		
		$fp = fopen($team.'.txt', 'w') or die("Error: Cannot open $team file.");
		fwrite($fp, $score) or die("Error: Cannot write to $team file.");
		fclose($fp);		
	}
	
	if (isset($_POST['option'])) {
		$vote = $_POST['option'];
		
		$brazil = file_get_contents('brazil.txt');
		$england = file_get_contents('england.txt');
		$france = file_get_contents('france.txt');
		$germany = file_get_contents('germany.txt');
		
		$total = ($brazil+$germany+$france+$england);
		
		$return = "<BR /><BR />";
		
		$return .= "&nbsp;&nbsp;Brazil: ". (int) (($brazil/$total)*100)."% <BR />";
		$return .= "&nbsp;&nbsp;England: ". (int) (($england/$total)*100)."% <BR />";
		$return .= "&nbsp;&nbsp;France: ". (int) (($france/$total)*100)."% <BR />";
		$return .= "&nbsp;&nbsp;Germany: ". (int) (($germany/$total)*100)."% <BR />";		
		
		$return .= "<BR /><BR />";
		echo $return;
	}
	else {
		echo "Internal error";
	}	
?>